#version 150
uniform sampler2D texture1;
uniform vec3 ViewDir;


in vec2 v_uv1;
in vec4 v_color;
out vec4 PixelColor;

void main()
{
    PixelColor = texture(texture1, v_uv1) * v_color;
}
