#version 150
in vec2 v_uv;
out vec4 PixelColor;

uniform sampler2D ColorTex;
uniform float VStep;

void main()
{
	vec4 sum = vec4(0,0,0,0);   

	sum += texture(ColorTex,vec2(v_uv.x, v_uv.y - 4*VStep));
	sum += texture(ColorTex,vec2(v_uv.x, v_uv.y - 3*VStep));
	sum += texture(ColorTex,vec2(v_uv.x, v_uv.y - 2*VStep));
	sum += texture(ColorTex,vec2(v_uv.x, v_uv.y - 1*VStep));
	sum += texture(ColorTex,vec2(v_uv.x, v_uv.y));
	sum += texture(ColorTex,vec2(v_uv.x, v_uv.y + 1*VStep));
	sum += texture(ColorTex,vec2(v_uv.x, v_uv.y + 2*VStep));
	sum += texture(ColorTex,vec2(v_uv.x, v_uv.y + 3*VStep));
	sum += texture(ColorTex,vec2(v_uv.x, v_uv.y + 4*VStep));

	sum /= 9.0;

   	PixelColor = sum;
}

