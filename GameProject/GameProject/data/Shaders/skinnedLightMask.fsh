#version 150

uniform sampler2D texture1;
uniform sampler2D texture2;
uniform float fade;
uniform mat3 WorldIT;
uniform vec3 ViewDir;
uniform vec4 ColorMask;

in vec2 v_uv1;
in vec3 v_normal;
in vec4 v_color;
out vec4 PixelColor;

void main()
{
	vec3 n = normalize( v_normal );
    vec4 color = texture(texture1, v_uv1) * v_color * ColorMask;
    PixelColor = color;
}
