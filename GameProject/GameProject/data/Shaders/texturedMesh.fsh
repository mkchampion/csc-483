#version 150
uniform sampler2D texture1;
uniform vec3 ViewDir;

// Light
uniform vec4 LightAmbient;
uniform vec4 LightSpecular;
uniform vec4 LightDiffuse;
uniform vec3 LightDir;

in vec2 v_uv1;
in vec2 v_uv2;
out vec4 PixelColor;

void main()
{
    PixelColor = texture(texture1, v_uv1);
}
