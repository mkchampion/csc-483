#version 150

uniform sampler2D texture1;
uniform sampler2D texture2;
uniform float fade;
uniform mat3 WorldIT;
uniform vec3 ViewDir;
uniform vec3 LightDir;

uniform sampler2D shadowMap;

in vec3 normalTest;
in vec2 v_uv1;
in vec4 v_lightPos;
out vec4 PixelColor;
in vec4 v_color;

void main()
{
	//vec3 n = normalize( v_normal );
	vec4 texel = texture(texture1, v_uv1);
	if (texel.a < 0.83)
		discard;

    PixelColor = texel * v_color;
    //PixelColor = texture(texture1, v_uv1) * v_color;
}
