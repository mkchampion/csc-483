#version 150
in vec2 v_uv;
out vec4 PixelColor;

uniform sampler2D ColorTex;

void main()
{
	vec4 sum = texture(ColorTex, v_uv);
   	PixelColor = sum;
}

