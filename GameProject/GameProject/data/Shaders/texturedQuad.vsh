#version 150
uniform mat4 WorldViewProj;
uniform mat3 WorldIT;

// Light
uniform vec4 Ambient;
uniform vec3 LightDir;
uniform vec4 LightColor;
out vec4 v_color;

in vec4 a_position;
in vec3 a_normal;
in vec2 a_uv1;
out vec2 v_uv1;

void main()
{
    gl_Position = WorldViewProj * a_position;
	v_uv1 = a_uv1;

	vec3 n = a_normal;

	// Max of light and ambient
	v_color = LightColor * max (0, dot(n,LightDir)) + Ambient;

}
