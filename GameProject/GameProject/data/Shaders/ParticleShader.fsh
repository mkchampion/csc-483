#version 150

in vec2 v_uv1;

in vec4 v_color;
out vec4 PixelColor;

uniform sampler2D tex;

void main()
{
    PixelColor = v_color;
    vec4 textureValues = texture(tex, v_uv1);

    PixelColor.a = PixelColor .a * textureValues.a;

    if (PixelColor.a <= 0.1)
    	discard;
    

}
