#version 150
in vec2 v_uv;
out vec4 PixelColor;

uniform sampler2D ColorTex;
uniform float UStep;

void main()
{
	vec4 sum = vec4(0,0,0,0);

	sum += texture(ColorTex, vec2(v_uv.x - 4*UStep, v_uv.y));
	sum += texture(ColorTex, vec2(v_uv.x - 3*UStep, v_uv.y));
	sum += texture(ColorTex, vec2(v_uv.x - 2*UStep, v_uv.y));
	sum += texture(ColorTex, vec2(v_uv.x - 1*UStep, v_uv.y));
	sum += texture(ColorTex, vec2(v_uv.x, v_uv.y));
	sum += texture(ColorTex, vec2(v_uv.x + 1*UStep, v_uv.y));
	sum += texture(ColorTex, vec2(v_uv.x + 2*UStep, v_uv.y));
	sum += texture(ColorTex, vec2(v_uv.x + 3*UStep, v_uv.y));
	sum += texture(ColorTex, vec2(v_uv.x + 4*UStep, v_uv.y));

	sum /= 9.0;

   	PixelColor = sum;
}

