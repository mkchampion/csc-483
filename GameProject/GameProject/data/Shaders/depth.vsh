#version 150
uniform mat4 WorldViewProj;
in vec4 a_position;
in vec3 a_normal;
in vec4 a_color;

void main()
{
    gl_Position = WorldViewProj * a_position;
}


