#version 150
in vec2 v_uv;
out vec4 PixelColor;

uniform sampler2D ColorTex;
uniform float GradTo;
uniform float WindowWidth;
uniform float WindowHeight;

void main()
{
   	vec4 c = texture(ColorTex, v_uv);

	float xVal;
	float yVal;
	xVal = 1.0;
	yVal = 1.0;

	float gradX = WindowWidth*GradTo;
	float gradY = WindowHeight*GradTo;

	// X
	// From left
	if (gl_FragCoord.x < gradX)
	{

		float xPos = gl_FragCoord.x / WindowWidth;

		float valueOutOfGrad = xPos/GradTo;

		xVal = valueOutOfGrad;
	}

	// From right
	if (gl_FragCoord.x > WindowWidth - gradX)
	{

		float xPos = 1 - gl_FragCoord.x / WindowWidth;

		float valueOutOfGrad = xPos/GradTo;

		xVal = valueOutOfGrad;
	}

	// Y
	if (gl_FragCoord.y < gradY)
	{

		float yPos = gl_FragCoord.y / WindowHeight;

		float valueOutOfGrad = yPos/GradTo;

		yVal = valueOutOfGrad;
	}

	// From right
	if (gl_FragCoord.y > WindowHeight - gradY)
	{

		float yPos = 1 - gl_FragCoord.y / WindowHeight;

		float valueOutOfGrad = yPos/GradTo;

		yVal = valueOutOfGrad;
	}

	PixelColor = mix(vec4(0.5,0.05,0.05,1.0), c, min(xVal,yVal));
}

