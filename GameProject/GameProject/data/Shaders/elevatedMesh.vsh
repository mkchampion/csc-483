#version 150
uniform mat4 WorldViewProj;
uniform mat3 WorldIT;
uniform mat4 LightMatrix;

// Light
uniform vec4 Ambient;
uniform vec3 LightDir;
uniform vec4 LightColor;
out vec4 v_color;
out vec4 v_lightPos;

in vec4 a_position;
in vec3 a_normal;
in vec2 a_uv1;
out vec2 v_uv1;
out float height;

void main()
{
    gl_Position = WorldViewProj * a_position;
    v_lightPos = LightMatrix * a_position;
	v_uv1 = a_uv1;

	vec3 n = a_normal;

	// Max of light and ambient
	v_color = LightColor * max (0, dot(n,LightDir)) + Ambient;
	height = a_position.y;

}
