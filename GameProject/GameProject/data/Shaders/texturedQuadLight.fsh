#version 150
uniform sampler2D texture1;
uniform vec3 ViewDir;

in vec4 v_lightPos;
in vec2 v_uv1;
in vec4 v_color;
out vec4 PixelColor;

uniform vec4 Ambient;
uniform sampler2D shadowMap;

void main()
{
   	vec3 vProj = v_lightPos.xyz / v_lightPos.w;
   	vec4 shadowSample = texture(shadowMap, vProj.xy);

   	if (shadowSample.r <= vProj.z)
   		PixelColor = texture(texture1, v_uv1) * vec4(0.4, 0.4, 0.4, 1.0);
   	else
   		PixelColor = texture(texture1, v_uv1) * v_color;
}
