#version 150

uniform sampler2D texture1;
uniform sampler2D texture2;
uniform float fade;
uniform mat3 WorldIT;
uniform vec3 ViewDir;

in vec2 v_uv1;
in vec3 v_normal;
out vec4 PixelColor;

void main()
{
	vec3 n = normalize( v_normal );
    PixelColor = texture(texture1, v_uv1);
}
