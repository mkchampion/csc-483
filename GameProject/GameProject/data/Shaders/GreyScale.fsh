#version 150
in vec2 v_uv;
out vec4 PixelColor;

uniform sampler2D ColorTex;
uniform float FadeTime;
uniform float DeathTimer;

void main()
{
	vec4 screen = texture(ColorTex, v_uv);

	float time = clamp(DeathTimer / FadeTime, 0.0, 1.0);

	float L = dot(screen.rgb, vec3(0.3, 0.59, 0.11));
	vec3 greyColor = mix(screen.rgb, vec3(L,L,L), time);

   	PixelColor.rgb = greyColor.rgb;
   	PixelColor.a = screen.a;
}

