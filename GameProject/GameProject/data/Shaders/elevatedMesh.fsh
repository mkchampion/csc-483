#version 150
uniform sampler2D texture1;
uniform sampler2D texture2;
uniform sampler2D texture3;
uniform sampler2D texture4;
uniform sampler2D shadowMap;
uniform vec3 ViewDir;
uniform float Distortion;
uniform float Break1;
uniform float Break2;

in vec2 v_uv1;
in vec4 v_color;
out vec4 PixelColor;
in float height;
in vec4 v_lightPos;

void main()
{

	// Get Height
	float percent = height / Distortion;

	// Texture values
	vec4 t1Color = texture(texture1, v_uv1);
	vec4 t2Color = texture(texture2, v_uv1);
	vec4 t3Color = texture(texture3, v_uv1);
	vec4 t4Color = texture(texture4, v_uv1);

	// Range / Values
	float a = 0.0;
	float b = Break1;
	float c = Break2;
	float d = 1.0;

	// Texture A->B
	float x_ab = percent / b;
	vec4 y_ab = mix(t1Color, t2Color, x_ab);
	vec4 t12Low = min(t1Color, t2Color);
	vec4 t12High = max(t1Color, t2Color);
	y_ab = clamp(y_ab, t12Low, t12High);	

	// Texture C->D
	float x_cd = (percent - c)/(d-c);
	vec4 y_cd = mix(t3Color, t4Color, x_cd);
	vec4 t34Low = min(t3Color, t4Color);
	vec4 t34High = max(t3Color, t4Color);
	y_cd = clamp(y_cd, t34Low, t34High);

	// Between
	float x_abcd = (percent-b)/(c-b);
	vec4 y = mix(y_ab, y_cd, x_abcd);
	y = clamp(y, y_ab, y_cd);

	vec3 vProj = v_lightPos.xyz / v_lightPos.w;
   	vec4 shadowSample = texture(shadowMap, vProj.xy);

   	if (shadowSample.r <= vProj.z)
   		PixelColor = y * v_color * vec4(0.4, 0.4, 0.4, 1.0);
   	else
   		PixelColor = y * v_color;
    
}
