#version 150

uniform mat4 projViewWorld;
in vec4 a_position;

in vec2 a_uv1;
in vec4 a_color;
out vec2 v_uv1;
out vec4 v_color;



void main()
{
    gl_Position = projViewWorld * a_position;
	v_color = a_color;
	v_uv1 = a_uv1;
}
