#version 150
in vec2 v_uv;
out vec4 PixelColor;

uniform float PositionX;
uniform float PositionY;
uniform float VelocityX;
uniform float VelocityZ;
uniform float MaxVelocity;
uniform float WindowWidth;
uniform float WindowHeight;

uniform sampler2D ColorTex;
uniform sampler2D VerticalBlur;
uniform sampler2D HorizontalBlur;

void main()
{

	vec4 previousScreen = texture(ColorTex, v_uv); 
	vec4 vertBlur = texture(VerticalBlur, v_uv);
	vec4 horizontalBlur = texture(VerticalBlur, v_uv);
	vec4 sum = previousScreen;

	float AdjustmentMax = 0.5;

	// Get fraction of velocity / 1
	float percentX = VelocityX/MaxVelocity;
	float percentZ = VelocityZ/MaxVelocity;

	// Take the percentange from the Adjustment
	float maxHorizontalBlur = percentX * AdjustmentMax;
	float maxVerticalBlur = percentZ * AdjustmentMax;

	// Get position to right / left
	float distanceRight = 1.0 - PositionX;
	float distanceLeft = PositionX;

	float distanceTop = 1.0 - PositionY;
	float distanceBottom = PositionY;

	// Position of pixel
	float pixelX = gl_FragCoord.x / WindowWidth;
	float pixelY = gl_FragCoord.y / WindowHeight;

	// Vertical Blur
	if (pixelX > PositionX)
	{
		float currentPos = min(pixelX - PositionX, maxVerticalBlur);
		sum = mix(previousScreen, vertBlur, currentPos);
	}
	else
	{
		float currentPos = min(PositionX - pixelX, maxVerticalBlur);
		sum = mix(previousScreen, vertBlur, currentPos);
	}

	// HorizontalBlur
	if (pixelY > PositionY)
	{
		float currentPos = min (pixelY - PositionY, maxHorizontalBlur);
		sum = mix(sum, horizontalBlur, currentPos); 
	}
	else
	{		
		float currentPos = min (PositionY - pixelY, maxHorizontalBlur);
		sum = mix(sum, horizontalBlur, currentPos); 
	}


	
   	PixelColor = sum;
}

