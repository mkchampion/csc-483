#version 150

uniform mat4 WorldViewProj;
uniform mat3 WorldIT;
uniform mat4 LightMatrix;

in vec4 a_position;
in vec2 a_uv1;
in vec3 a_normal;
out vec2 v_uv1;
out vec3 v_normal;

// Lights
uniform vec3 LightDir;
uniform vec4 LightColor;
uniform vec4 Ambient;

out vec4 v_color;
out vec4 v_lightPos;

void main()
{
    gl_Position = WorldViewProj * a_position;
    v_lightPos = LightMatrix * a_position;
	v_uv1 = a_uv1;

    // Normal as inverseWorld * normal
	vec3 n = WorldIT * a_normal;
	n = normalize(n);

	// Max of light and ambient
	v_color = LightColor * max(0, dot(n, LightDir)) + Ambient;
}
