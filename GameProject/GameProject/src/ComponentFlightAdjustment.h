//------------------------------------------------------------------------
// ComponentFlightAdjustment.h
//	
// Component used to adjust game objects if they go over flight boundries
//------------------------------------------------------------------------

#ifndef COMPONENTFLIGHTADJUSTMENT_H
#define COMPONENTFLIGHTADJUSTMENT_H

#include "GameObjectManager.h"
#include <glm\glm.hpp>
struct FlyBoundsWorld;

class ComponentFlightAdjustment : public Common::ComponentBase
{

public:

	// Structors
	ComponentFlightAdjustment();
	~ComponentFlightAdjustment();

	void Init(float normalHeightP, float elevatedHeightP, float elevationSpeedP);

	// Factory
	static Common::ComponentBase* CreateComponent(TiXmlNode* p_pNode);

	// Update/Family
	virtual void Update(float p_fDelta);
	virtual const std::string FamilyID() { return std::string("GOC_FlightAdjustment"); }
	virtual const std::string ComponentID(){ return std::string("GOC_FlightAdjustment"); }

private:

	// Min/max bob
	float normHeight;
	float elevatedHeight;
	float elevationSpeed;

	// Bounds to check for the fly zones
	std::vector<FlyBoundsWorld*>* m_lFlyBoundsWorld;

};


#endif