#include "ObjectDisappearedEvent.h"
#include "DynamicCollisionEvent.h"
#include "StaticCollisionEvent.h"
#include "PlayerDamagedEvent.h"
#include "ConeWeaponEvent.h"

// Declaring event types
const EventType ObjectDisappearedEvent::s_EventType = (0xfd4c7dbc);
const EventType DynamicCollisionEvent::s_EventType = (0x3575cc0d);
const EventType StaticCollisionEvent::s_EventType = (0xd45d11d1);
const EventType PlayerDamagedEvent::s_EventType = (0xf3378f2a);
const EventType ConeWeaponEvent::s_EventType = (0xa50c1179);





