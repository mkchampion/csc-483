//------------------------------------------------------------------------
// EventManager
//	
// This class is the event manager.  Allows components to register / remove listeners
// and processes the event queue.
// Based off of EventManager from Game Coding Complete
//------------------------------------------------------------------------

#ifndef EVENTMANAGER_H
#define EVENTMANAGER_H

#include <map>
#include <list>
#include <functional>
#include <memory>
#include "IEventData.h"
#include "EventDelegate.h"

struct RemoveListenerData
{
	EventType type;
	const EventDelegate* delegate;
};

class EventManager
{
		
	// Typedef for function delegate
	//typedef std::tr1::function<void(const EventDataPointer& p_pEventData)> EventDelegateT;

	// Typedefs (Taken from Game Coding Complete)
	typedef std::list <const EventDelegate*> EventListenerList;
	typedef std::map<EventType, EventListenerList> EventListenerMap;
	typedef std::list<EventDelegate::EventDataPointer> EventQueue;
	typedef std::list<RemoveListenerData> RemoveListenerQueue;


public:

	// Instance methods
	static void CreateInstance();
	static void DestroyInstance();
	static EventManager* Instance();

	// Add listener
	bool AddListener(const EventType& p_eEventType, const EventDelegate* p_pEventDelegateT);

	// Remove Listener
	bool RemoveListener(const EventType& p_eEventType, const EventDelegate* p_pEventDelegateT);
	void QueueRemoveListener(const EventType& p_eEventType, const EventDelegate* p_pEventDelegateT);

	// Trigger
	bool TriggerEvent(const EventDelegate::EventDataPointer& p_pEventData);

	// Queue
	bool QueueEvent(const EventDelegate::EventDataPointer& p_pEventData);

		
	void ClearEvents();

	// Update
	void Update(float p_fDelta);

private:

	// Constructor / Destructor
	EventManager();
	virtual ~EventManager();

	// Instance
	static EventManager* s_pEventManagerInstance;

	// List of listener data that should be romoved upon this update
	RemoveListenerQueue m_lListenersToRemove;

	// Members
	EventListenerMap m_lEventMap;
	EventQueue m_qEventQueue;

};

#endif
