//------------------------------------------------------------------------
// ComponentDrops
//	
// Component to specify what enemies will drop (Could be a weapon / health pickup)
//------------------------------------------------------------------------

#ifndef COMPONENTDROPS_H
#define COMPONENTDROPS_H

#include <string>
#include "EventDelegate.h"
#include "GameObjectManager.h"

struct DropRatesItem
{
	float dropChance = 0.0f;
	std::string gameObjectPath = "";
};

class ComponentDrops : public Common::ComponentBase
{

public:

	// Structors
	ComponentDrops();
	~ComponentDrops();

	void Init(std::vector<DropRatesItem> p_lDropTable);

	// Factory
	static Common::ComponentBase* CreateComponent(TiXmlNode* p_pNode);

	// Update/Family
	virtual void Update(float p_fDelta) {};
	virtual const std::string FamilyID() { return std::string("GOC_Drops"); }
	virtual const std::string ComponentID(){ return std::string("GOC_Drops"); }
	virtual void ComponentDeath();

private:

	// Need to track drop chances somehow
	std::vector<DropRatesItem> m_lDropTable;


};

#endif