//------------------------------------------------------------------------
// GameObject
//
// Created:	2012/12/14
// Author:	Carel Boers
//	
// This class defines a "GameObject". An GameObject for our purpose is 
// essentially just an empty container to define any object in our game 
// world. The definition of the object is through the aggregation of various
// components.
//------------------------------------------------------------------------

#include "GameObject.h"
#include <sstream>

using namespace Common;

// Static game object counter
unsigned int GameObject::s_uiGameObjectCount = 0;

//------------------------------------------------------------------------------
// Method:    GameObject
// Parameter: GameObjectManager * p_pGameObjectManager
// Returns:   
// 
// Constructor.
//------------------------------------------------------------------------------
GameObject::GameObject(GameObjectManager* p_pGameObjectManager)
	:
	m_pGameObjectManager(p_pGameObjectManager)
{
	// Increment count of all GOs ever created on this machine
	++s_uiGameObjectCount;

	// Create a unique (per this application instance) guid
	std::ostringstream ss;
	ss << "GameObject_" << s_uiGameObjectCount;
	m_strGUID = ss.str();
}

//------------------------------------------------------------------------------
// Method:    ~GameObject
// Returns:   
// 
// Destructor.
//------------------------------------------------------------------------------
GameObject::~GameObject()
{
	this->DeleteAllComponents();
}

//------------------------------------------------------------------------------
// Method:    AddComponent
// Parameter: BaseComponent * p_pComponent
// Returns:   bool
// 
// Add a component to the GameObject. Returns true if it was added, false 
// otherwise.
//------------------------------------------------------------------------------
bool GameObject::AddComponent(ComponentBase* p_pComponent)
{
	// Make sure we don't already have a component of this type
	if (m_mComponentMap.find(p_pComponent->FamilyID()) == m_mComponentMap.end())
	{
		m_mComponentMap.insert(std::pair<std::string, ComponentBase*>(p_pComponent->FamilyID(), p_pComponent));
		p_pComponent->SetGameObject(this);

		// Check if it has a bounding circle
		if (p_pComponent->ComponentID() == "GOC_BoundingSphere")
		{
			// If the component is a collidable, add it to the collision list
			GameObjectManager* pGOManager = this->GetManager();
			pGOManager->AddCollidable(this);
		}

		return true;
	}

	// Already have a component with this family Id.
	return false;
}

//------------------------------------------------------------------------------
// Method:    GetComponent
// Parameter: const std::string & p_strFamilyId
// Returns:   BaseComponent*
// 
// Get a component with the given family Id. Returns NULL if it isn't found.
//------------------------------------------------------------------------------
ComponentBase* GameObject::GetComponent(const std::string &p_strFamilyId)
{
	ComponentMap::iterator it = m_mComponentMap.find(p_strFamilyId);
	if (it != m_mComponentMap.end())
	{
		return (ComponentBase*)it->second;
	}

	// Don't have it.
	return NULL;
}

//------------------------------------------------------------------------------
// Method:    RemoveComponent
// Parameter: const std::string & p_strFamilyId
// Returns:   BaseComponent*
// 
// Remove a component with the given family Id and returns it. It is not 
// deleted to allow moving this component to another GameObject if desired. 
//------------------------------------------------------------------------------
ComponentBase* GameObject::RemoveComponent(const std::string &p_strFamilyId)
{
	ComponentBase* pComponent = NULL;

	ComponentMap::iterator it = m_mComponentMap.find(p_strFamilyId);
	if (it != m_mComponentMap.end())
	{
		pComponent = (ComponentBase*)it->second;
		m_mComponentMap.erase(it);
	}

	return pComponent;
}

//------------------------------------------------------------------------------
// Method:    DeleteAllComponents
// Returns:   void
// 
// Remove all components from the GameObject and deletes them.
//------------------------------------------------------------------------------
void GameObject::DeleteAllComponents()
{
	ComponentMap::iterator it = m_mComponentMap.begin(), end = m_mComponentMap.end();
	for (; it != end; ++it)
	{
		ComponentBase* pComponent = (ComponentBase*)it->second;
		delete pComponent;
	}
	m_mComponentMap.clear();
}

//------------------------------------------------------------------------------
// Method:    Death
// Returns:   void
// 
// Remove all components from the GameObject and deletes them.
//------------------------------------------------------------------------------
void GameObject::Death()
{
	ComponentMap::iterator it = m_mComponentMap.begin(), end = m_mComponentMap.end();
	for (; it != end; ++it)
	{
		ComponentBase* pComponent = (ComponentBase*)it->second;
		pComponent->ComponentDeath();
	}
}

//------------------------------------------------------------------------------
// Method:    Update
// Parameter: float p_fDelta
// Returns:   void
// 
// Updates the internal components of the GameObject.
//------------------------------------------------------------------------------
void GameObject::Update(float p_fDelta)
{
	ComponentMap::iterator it = m_mComponentMap.begin(), end = m_mComponentMap.end();
	for (; it != end; ++it)
	{
		ComponentBase* pComponent = (ComponentBase*)it->second;
		pComponent->Update(p_fDelta);
	}


	// List of any modifiers that need to be removed
	std::vector<std::string> lRemovedModifiers;

	// Update any modifiers
	std::map<std::string, Modifier*>::iterator modIT = m_mModifiers.begin(), modEnd = m_mModifiers.end();
	for (; modIT != modEnd; ++modIT)
	{
		Modifier* pModifier = (Modifier*)modIT->second;
		pModifier->Update(p_fDelta);

		if (pModifier->IsExpired())
		{
			lRemovedModifiers.push_back(modIT->first);
		}
	}

	// Remove any expired modifiers
	for (int i = 0; i < lRemovedModifiers.size(); i++)
	{
		m_mModifiers.erase(lRemovedModifiers[i]);
	}

}

//------------------------------------------------------------------------------
// Method:    AddTag
// Returns:   void
// 
// Adds a tag to an object
//------------------------------------------------------------------------------
void GameObject::AddTag(const std::string& p_strTag)
{
	m_vObjectTags.push_back(p_strTag);
}

//------------------------------------------------------------------------------
// Method:    RemoveTag
// Returns:   void
// 
// Removes a tag from the object
//------------------------------------------------------------------------------
void GameObject::RemoveTag(const std::string& p_strTag)
{
	// Find element to be removed
	std::vector<std::string>::iterator itRemove;
	bool found = false;

	for (int i = 0; i < m_vObjectTags.size(); i++)
	{
		if (std::strcmp(p_strTag.c_str(), m_vObjectTags[i].c_str()) == 0)
		{
			found = true;
			itRemove = m_vObjectTags.begin() + i;
		}
	}

	if (found)
	{
		m_vObjectTags.erase(itRemove);
	}
}

//------------------------------------------------------------------------------
// Method:    HasTag
// Returns:   bool
// 
// Check if an object has a tag
//------------------------------------------------------------------------------
bool GameObject::HasTag(const std::string& p_strTag) const
{

	for (int i = 0; i < m_vObjectTags.size(); i++)
	{
		if (std::strcmp(p_strTag.c_str(), m_vObjectTags[i].c_str()) == 0)
			return true;
	}

	return false;
}

void GameObject::SetDirection(const float m_fDir)
{
	m_fDirection = m_fDir;
	m_Transform.SetRotationXYZ(0.0f, m_fDir, 0.0f);
}

//------------------------------------------------------------------------------
// Method:    AddModifier
// Parameter: std::string p_strName, float p_fTime
// Returns:   bool
// 
// Add a component to the GameObject. Returns true if it was added, false 
// otherwise.
//------------------------------------------------------------------------------
void GameObject::AddModifier(std::string p_strName, const float p_fTime)
{
	// Make sure we don't already have this modifier (If we do, extend it)
	if (m_mModifiers.find(p_strName) == m_mModifiers.end())
	{
		m_mModifiers.insert(std::pair<std::string, Modifier*>(p_strName, new Modifier(p_strName, p_fTime)));		
	}
	else
	{
		m_mModifiers[p_strName]->ExtendDuration(p_fTime);
	}
}

//------------------------------------------------------------------------------
// Method:    HasModifier
// Param:	  p_strName
// Returns:   bool
// 
// Check if an object has a tag
//------------------------------------------------------------------------------
bool GameObject::HasModifier(const std::string& p_strName) const
{
	if (m_mModifiers.find(p_strName) == m_mModifiers.end())
	{
		return false;
	}

	return true;
}