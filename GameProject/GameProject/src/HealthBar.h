// ================================================================
// HealthBar.h
// Class that tracks and renders a health bar based on a certain GO?
// =================================================================


#ifndef HEALTHBAR_H
#define HEALTHBAR_H

#include "W_BufferManager.h"
#include "W_VertexBuffer.h"
#include "W_VertexDeclaration.h"
#include "W_MaterialManager.h"

#include "GameObjectManager.h"

// Struct for the quad
struct HealthVertex
{
	GLfloat x, y;			// Position
	GLfloat r, g, b, a;		// Color
};

class HealthBar
{

public:

	// Constructor/Destructor
	HealthBar(Common::GameObjectManager* p_pManager, const std::string& p_strTargetID, float borderWidthP);
	~HealthBar();

	// Update / Render
	void Update(float p_fDelta);
	void Render(const glm::mat4& p_mView, const glm::mat4& p_mProj);

	// Set position (On screen)
	void SetPosition(float x, float y);

	// Method to create/declare the vertex
	bool DeclareVertexBuffer();

private:

	// Buffers
	wolf::VertexBuffer* m_pVertexBuffer;
	wolf::IndexBuffer* m_pIndexBuffer;
	wolf::VertexDeclaration* m_pVertexDeclaration;

	// Going to try and use materials 
	wolf::Material* m_pMaterial;

	// Vectors for vertices / indices
	std::vector<HealthVertex> healthVertices;
	std::vector<GLushort> healthIndices;

	// Health bar height / width
	float height = 30.0f;
	float width = 200.0f;

	// Border width
	bool hasBorder = true;
	float borderWidth = 0.0f;

	// Track percent health
	float percentHealth = 1.0f;

	// 2 important vertices (The ones that need to shrink / scale)
	int healthRightT;
	int healthRightB;

	// World Transform
	glm::mat4 m_mWorldTransform;

	// proj/view
	glm::mat4 proj;
	glm::mat4 view;

	// GameObject to track?
	Common::GameObjectManager* m_pManager;
	std::string m_pObjectID;

	// Bool for whether to render or not
	bool shouldRender = false;


};

#endif

