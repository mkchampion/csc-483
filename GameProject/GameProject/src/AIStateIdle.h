//------------------------------------------------------------------------
// AIStateIdle
//
// Created:	2013/02/10
// Author:	Carel Boers
//	
// Idle behaviour state.
//------------------------------------------------------------------------

#ifndef STATEIDLE_H
#define STATEIDLE_H

#include "StateBase.h"


class AIStateIdle : public Common::StateBase
{
public:
	//------------------------------------------------------------------------------
	// Public methods.
	//------------------------------------------------------------------------------
	AIStateIdle();
	virtual ~AIStateIdle();

	// Overridden from StateBase
	virtual void Enter(ActionData* p_stateData);
	virtual void Update(float p_fDelta);
	virtual void Exit();
};

#endif // STATEIDLE_H

