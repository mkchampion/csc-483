//------------------------------------------------------------------------
// GameObject
//
// Created:	2012/12/14
// Author:	Carel Boers
//	
// This class defines a "GameObject". An GameObject for our purpose is 
// essentially just an empty container to define any object in our game 
// world. The definition of the object is through the aggregation of various
// components.
//------------------------------------------------------------------------

#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include "ComponentBase.h"
#include "GameObjectManager.h"
#include "Transform.h"
#include "States.h"
#include "Modifier.h"
#include <map>
#include <vector>

namespace Common
{
	class GameObject
	{
		// Typedef for convenience
		typedef std::map<std::string, ComponentBase*> ComponentMap;

		// Only GameObjectManager can create instances (private constructor/destructor)
		friend class GameObjectManager;

	public:
		//------------------------------------------------------------------------------
		// Public methods.
		//------------------------------------------------------------------------------
		const std::string& GetGUID()	{ return m_strGUID; }
		Transform& GetTransform()		{ return m_Transform; }
		GameObjectManager* GetManager() { return m_pGameObjectManager; }

		bool AddComponent(ComponentBase* p_pComponent);
		ComponentBase* GetComponent(const std::string &p_strFamilyId);
		ComponentBase* RemoveComponent(const std::string &p_strFamilyId);
		void DeleteAllComponents();

		virtual void Update(float p_fDelta);

		// State
		void SetState(int p_iState) { m_iState = p_iState; }
		int GetState() const { return m_iState; }

		// Tags
		void AddTag(const std::string& p_strTag);
		void RemoveTag(const std::string& p_strTag);
		bool HasTag(const std::string& p_strTag) const;

		// Modifiers
		void AddModifier(std::string p_strName, const float p_fTime);
		void RemoveModifier(const std::string& p_strName);
		bool HasModifier(const std::string& p_strName) const;

		void SetDirection(const float p_fDir);
		float GetDirection() const { return m_fDirection; }

		// Death, triggers death effects
		void Death();
		
	private:
		//------------------------------------------------------------------------------
		// Private methods.
		//------------------------------------------------------------------------------

		// Only GameObjectManager can create instances (private constructor/destructor)
		GameObject(GameObjectManager* p_pGameObjectManager);
		~GameObject();

		// Only GameObjectManager can set new GUID as it maintains the mapping of GUID -> GO
		void SetGUID(const std::string& p_strGUID) { m_strGUID = p_strGUID; }

	private:
		//------------------------------------------------------------------------------
		// Private members.
		//------------------------------------------------------------------------------

		// A pointer back to the GameObjectManager that created us
		GameObjectManager* m_pGameObjectManager;

		// Running tally for all game objects created
		static unsigned int s_uiGameObjectCount;

		// GUID
		std::string m_strGUID;

		// All entities have a transform
		Transform m_Transform;

		// List of components
		ComponentMap m_mComponentMap;

		// A state that it belongs to
		int m_iState = 0;

		// A list of tags?  Something that can identify it as part of a group but not as a unique object like the GUID
		std::vector<std::string> m_vObjectTags;
		
		std::map<std::string, Modifier*> m_mModifiers;

		// Direction (in degrees)
		float m_fDirection = 0.0f;

	};
}

#endif // GAMEOBJECT_H