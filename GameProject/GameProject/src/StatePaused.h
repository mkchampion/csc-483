//------------------------------------------------------------------------
// StatePaused
//	
// An altered class made from StateMainMenu
//------------------------------------------------------------------------

#ifndef STATE_PAUSED_H
#define STATE_PAUSED_H

#include "GameObject.h"
#include "StateBase.h"
#include "GameController.h"

class StatePaused : public Common::StateBase
{
public:
	//------------------------------------------------------------------------------
	// Public methods.
	//------------------------------------------------------------------------------
	StatePaused();
	virtual ~StatePaused();

	// Overridden from StateBase
	virtual void Enter(ActionData* p_data);
	virtual void Update(float p_fDelta);
	virtual void Exit();

private:
	//------------------------------------------------------------------------------
	// Private members.
	//------------------------------------------------------------------------------
	GameController* m_pGameController;

	bool startReleased = false;

};

#endif

