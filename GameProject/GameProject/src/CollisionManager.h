//------------------------------------------------------------------------
// CollisionManager
//	
// A collision manager to handle all the collisions that may take place
// Only bounding boxes at the moment
//------------------------------------------------------------------------

#ifndef COMMON_COLLISIONMANAGER_H
#define COMMON_COLLISIONMANAGER_H

#include "ComponentBoundingBox.h"
#include "LevelInfo.h"
#include <vector>
#include <tinyxml.h>
#include "BoundingBoxDebugDraw.h"
#include "CollisionPoints.h"
#include "GameObjectManager.h"
#include "ExplosionInfo.h"

struct CollisionMapData
{
	float width;
	float depth;
	float offsetX;
	float offsetZ;
};


class CollisionManager
{

public:
	//------------------------------------------------------------------------------
	// Public methods.
	//------------------------------------------------------------------------------
	static void CreateInstance();
	static void DestroyInstance();
	static CollisionManager* Instance();

	// Load the collisionMap
	void LoadCollisionMap(const std::string& p_sMapPath);

	// Dynamic collision components
	void AddCollisionComponent(ComponentBoundingBox* p_pCollisionComponent);
	void DeleteCollisionComponent(ComponentBoundingBox* p_pCollisionComponent);

	// Static terrain components
	void SetStaticCollisionData(std::vector<TMXObjectInfo*>* p_pStaticTerrain);

	void Update(float p_fDelta, const glm::mat4& proj, const glm::mat4& view, float aspectRatioP);

	std::vector<CollisionPoints>* GetCollisionPoints() { return &m_lStaticCollision; }

	// Public function to check collision points against eachother
	bool CheckCollisionPoints(CollisionPoints* p_pBox1, CollisionPoints* p_pBox2);

	// Another public collision check (Specifically called from the weapon, since the bounding box isn't associated with a persisting object)
	void CheckConeWeaponCollision(CollisionPoints* p_pBox1, float p_fDamageVal);

	void SetPlayerID(std::string p_strID) { playerID = p_strID; }
	void SetGOManager(Common::GameObjectManager* p_pManager) { m_pManager = p_pManager; }

	// Add to the explosion list
	void AddExplosion(ExplosionInfo* newExplosion) { m_lActiveExplosions.push_back(newExplosion); }

private:
	//------------------------------------------------------------------------------
	// Private methods.
	//------------------------------------------------------------------------------

	// Constructor/Destructor are private because we're a Singleton
	CollisionManager();
	virtual ~CollisionManager();

	// Functions to help with checking collisions
	bool CheckCollisionAxis(ComponentBoundingBox* p_pBox1, ComponentBoundingBox* p_pBox2, glm::vec3 axisP);
	bool CheckCollisionAxis(ComponentBoundingBox* p_pBox1, CollisionPoints* p_pBox2, glm::vec3 axisP);
	bool CheckCollisionAxis(CollisionPoints* p_pBox1, CollisionPoints* p_pBox2, glm::vec3 axisP);
	glm::vec3 CalculateProjectionCam(glm::vec4 pointP, glm::vec3 axisP);
	glm::vec3 CalculateProjectionWorld(glm::vec4 pointP, glm::vec3 axisP);


	// Checking explosions
	void CheckExplosions(float p_fDelta, const glm::mat4& proj, const glm::mat4& view, float aspectRatioP);

	// Circle / rect collision
	bool CircleCollision(ComponentBoundingBox* p_pBox1, glm::vec4 point, float radius);
	float simpleClamp(float valueP, float min, float max);

private:
	//------------------------------------------------------------------------------
	// Private members.
	//------------------------------------------------------------------------------

	// Static singleton instance
	static CollisionManager* s_pCollisionManagerInstance;

	std::map<std::string, CollisionMapData> m_pCollisionMapData;

	// Vector of collision components
	std::vector<ComponentBoundingBox*> m_lCollisionComponents;	
	std::vector<CollisionPoints> m_lStaticCollision;

	// List of explosion info?
	std::vector<ExplosionInfo*> m_lActiveExplosions;

	// Collision manager has player (For player specific collisions)
	std::string playerID;
	Common::GameObjectManager* m_pManager;

	// Resets the points
	bool newPlayer = true;

#if DEBUG_BOXES
	std::vector<CollisionPoints> m_lDebugBoxes;
#endif

};

#endif // COMMON_SCENEMANAGER_H