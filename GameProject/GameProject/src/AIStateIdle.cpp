//------------------------------------------------------------------------
// AIStateIdle
//
// Created:	2013/02/10
// Author:	Carel Boers
//	
// Idle behaviour state.
//------------------------------------------------------------------------

#include "AIStateIdle.h"
#include "ComponentBase.h"
#include "ComponentAIController.h"
#include "GameObject.h"

//------------------------------------------------------------------------------
// Method:    AIStateIdle
// Returns:   
// 
// Constructor.
//------------------------------------------------------------------------------
AIStateIdle::AIStateIdle()
{
}

//------------------------------------------------------------------------------
// Method:    ~AIStateIdle
// Returns:   
// 
// Destructor.
//------------------------------------------------------------------------------
AIStateIdle::~AIStateIdle()
{
}

//------------------------------------------------------------------------------
// Method:    Enter
// Returns:   void
// 
// Called when this state becomes active.
//------------------------------------------------------------------------------
void AIStateIdle::Enter(ActionData* p_stateData)
{
}

//------------------------------------------------------------------------------
// Method:    Update
// Parameter: float p_fDelta
// Returns:   void
// 
// Called each from when this state is active.
//------------------------------------------------------------------------------
void AIStateIdle::Update(float p_fDelta)
{
}

//------------------------------------------------------------------------------
// Method:    Exit
// Returns:   void
// 
// Called when this state becomes inactive.
//------------------------------------------------------------------------------
void AIStateIdle::Exit()
{	
	// Nothing to do in order to clean up idling
}