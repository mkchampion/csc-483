//-----------------------------------------------------------------------------
// DirectionalLight
//
//  Weapon that acts as a cone (Effectivly, a flame thrower weapon)
//-----------------------------------------------------------------------------

#include "DirectionalLight.h"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

bool lightCam = false;

// Constructor
DirectionalLight::DirectionalLight()
{
	// Bias matrix
	float a[] =
	{
		0.5f, 0.0f, 0.0f, 0.0f,
		0.0f, 0.5f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.5f, 0.0f,
		0.5f, 0.5f, 0.5f, 1.0f
	};
	bias = glm::make_mat4(a);
}

// Update
void DirectionalLight::Update()
{

	// Get the position of the camera
	glm::vec3 cameraTargetPos = m_pCameraTarget->GetPos();

	// Set the current position to be offset from that
	this->lightPos = glm::vec3(cameraTargetPos.x + offsetX, cameraTargetPos.y + offsetY, cameraTargetPos.z + offsetZ);
	this->targetPos = cameraTargetPos + glm::vec3(targetOffsetX, targetOffsetY, targetOffsetZ);

	isViewDirty = true;

}

//------------------------------------------------------------------------------
// Method:    GetProjectionMatrix
// Returns:   const glm::mat4&
// 
// Returns the projection matrix, recalculating it as needed.
//------------------------------------------------------------------------------
const glm::mat4& DirectionalLight::GetProjectionMatrix()
{
	if (isProjDirty)
	{
		// proj = glm::perspective(45.0f, 1280.0f/720.0f, 0.1f, 1200.0f);
		proj = glm::ortho(-width / 2.0f, width / 2.0f, -height / 2.0f, height / 2.0f, 0.1f, 1000.0f);
		isProjDirty = false;
	}
	return proj;
}

// View
const glm::mat4& DirectionalLight::GetViewMatrix()
{
	if (isViewDirty)
	{
		view = glm::lookAt(lightPos, targetPos, glm::vec3(0.0f, 1.0f, 0.0f));
		//view = glm::lookAt(glm::vec3(330.0f, 300.0f, -150.0f), glm::vec3(330.0f, 0.0f, -150.0f), glm::vec3(0.0f, 0.0f, -1.0f));
		isViewDirty = false;
	}
	return view;
}

// View
const glm::mat4& DirectionalLight::GetLightMatrix()
{
	if (isLightMatrixDirty)
	{
		lightMatrix = bias * proj * view;
	}
	return lightMatrix;
}