//-----------------------------------------------------------------------------
// File:			IWeapon.h
// Original Author:	Matthew Champion
//-----------------------------------------------------------------------------

#ifndef WEAPON_H
#define WEAPON_H

#include <string>
#include "GameObject.h"

class IWeapon
{

public:
	virtual ~IWeapon() {};
	virtual void Update(float p_fDelta, Common::GameObject* ownerP, const glm::vec2& dirP) = 0;
	virtual void StopWeapon() = 0;
	virtual void SetTarget(Common::GameObject*) = 0;
};

#endif