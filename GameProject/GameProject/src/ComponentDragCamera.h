//------------------------------------------------------------------------
// ComponentDragCamera
//
// A camera behavior that is meant to follow the player and drag behind them / to the sides
//------------------------------------------------------------------------

#ifndef DRAGCAMERA_H
#define DRAGCAMERA_H

#include "ComponentCamera.h"
#include <tinyxml.h>
#include "GameObjectManager.h"

class ComponentDragCamera : public ComponentCamera
{

public:

	// Constructor / Destructor
	ComponentDragCamera();
	virtual ~ComponentDragCamera() {}

	// Factory
	static Common::ComponentBase* CreateComponent(TiXmlNode* p_pNode);

	void Init(const float verticalP, const float horizontalP, const float yOffsetP, const float zOffsetP);

	void SetTarget(Common::GameObject* p_pTarget);
	void SetTarget(const std::string& p_strTargetID, Common::GameObjectManager* p_pGOManager);

	// ID / Update
	virtual const std::string ComponentID() { return std::string("GOC_DragCamera"); }
	virtual void Update(float p_fDelta);

private:

	// Target
	Common::GameObject* m_pTarget;
	std::string m_strTargetID;

	// Values for when it should drag
	float verticalDrag;
	float horizontalDrag;

	// Camera offsets
	float offsetY;
	float offsetZ;

};

#endif 
