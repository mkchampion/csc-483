//------------------------------------------------------------------------
// PathFinder
//	
// Class actually used to find a path after being given a grid of nodes
//------------------------------------------------------------------------

#include "PathFinder.h"
#include <assert.h>

#define NULL 0

// Static singleton instance
PathFinder* PathFinder::s_pPathFinderInstance = 0;

// Create
void PathFinder::CreateInstance()
{
	assert(s_pPathFinderInstance == NULL);
	s_pPathFinderInstance = new PathFinder();
}

// Destroy
void PathFinder::DestroyInstance()
{
	assert(s_pPathFinderInstance != NULL);
	delete s_pPathFinderInstance;
	s_pPathFinderInstance = NULL;
}

// Instance
PathFinder* PathFinder::Instance()
{
	assert(s_pPathFinderInstance);
	return s_pPathFinderInstance;
}

// Constructor
PathFinder::PathFinder()
{
}

// Destructor
PathFinder::~PathFinder()
{
}

PathFinder::NodeList PathFinder::FindPath(const glm::vec3& startPos, const glm::vec3& targetPos, EnemyPathType enemyPathTypeP, PathNode* exceptionP)
{
	// End nodes
	PathNode* startNode = grid->GetClosestUsableNode(startPos, enemyPathTypeP, exceptionP);
	PathNode* endNode = grid->GetClosestUsableNode(targetPos, enemyPathTypeP, exceptionP);

	// Resetting flags on all the nodes?
	for (int i = 0; i < grid->GetSizeX(); i++)
	{
		for (int j = 0; j < grid->GetSizeY(); j++)
		{
			grid->nodes[i][j]->bInOpenList = false;
			grid->nodes[i][j]->bInClosedList = false;
			grid->nodes[i][j]->G = 0.0f;
			grid->nodes[i][j]->H = 0.0f;
			grid->nodes[i][j]->parent = NULL;
		}
	}

	// Open/Closed sets
	std::list<PathNode*> openSet;
	std::list<PathNode*> closedSet;

	// Set info on start node
	startNode->parent = NULL;
	startNode->bInOpenList = true;
	startNode->bInClosedList = false;
	startNode->G = 0;
	startNode->H = glm::length(startNode->position - endNode->position);

	// Add it to the open set
	openSet.push_back(startNode);


	bool goalFound = false;
	PathNode* currentNode = NULL;

	while (!goalFound)
	{
		// Break if the open set is empty, no path?
		if (openSet.empty())
		{
			break;
		}

		// Current node is start of open set
		currentNode = openSet.front();

		// Iterate over open set
		std::list<PathNode*>::iterator openIT = openSet.begin(), openEnd = openSet.end();
		std::list<PathNode*>::iterator bestIT = openIT;

		for (; openIT != openEnd; ++openIT)
		{
			// Get the node
			PathNode* testNode = (PathNode*)(*openIT);

			if (testNode->G + testNode->H < currentNode->G + currentNode->H || ((testNode->G + testNode->H) == (currentNode->G + currentNode->H) && (testNode->H < currentNode->H)))
			{
				currentNode = testNode;
				bestIT = openIT;
			}
		}

		// Remove from open set
		openSet.erase(bestIT);
		closedSet.push_back(currentNode);

		// Set flags
		currentNode->bInOpenList = false;
		currentNode->bInClosedList = true;

		// We've found the result
		if (currentNode == endNode)
		{
			goalFound = true;
			break;
		}


		// Add neighbors to list
		for (int i = 0; i < currentNode->neighbourNodes.size(); i++)
		{

			// Node we're testing
			PathNode* neighbour = currentNode->neighbourNodes.at(i);

			if (!grid->CanUseType(enemyPathTypeP, neighbour->m_pathNodeTypes) || neighbour->bInClosedList || neighbour->occupiedFlag)
			{
				continue;
			}

			// Explored, but could be better
			else if (neighbour->bInOpenList)
			{

				// Get the current G cost
				int currentG = neighbour->G;
				int newG = currentNode->G + glm::length(currentNode->position - neighbour->position);

				// Adding an occupied cost to the G to prevent characters from wanting to overlap(?)
				newG += currentNode->occupiedCost;

				if (newG < currentG)
				{
					neighbour->parent = currentNode;
					neighbour->G = newG;
				}

			}

			// New node
			else
			{
				neighbour->parent = currentNode;
				neighbour->bInOpenList = true;
				neighbour->bInClosedList = false;

				neighbour->G = currentNode->G + glm::length(currentNode->position - neighbour->position);	// Parent cost + "as the crow flies" cost to get from parent to us
				neighbour->H = glm::length(neighbour->position - endNode->position);			// "As the crow flies" cost to get to the end square

				openSet.push_back(neighbour);
			}

		}

	}


	// Setup the path
	NodeList path;
	path.clear();

	if (goalFound && currentNode != NULL)
	{
		PathNode* node = currentNode;
		while (node->parent)
		{
			path.push_front(node->position);
			node = node->parent;
		}
	}
	
	return path;

}


