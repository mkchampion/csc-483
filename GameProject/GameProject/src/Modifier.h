//------------------------------------------------------------------------
// Modifier
//	
// Represents some sort of status modifier for an object
//------------------------------------------------------------------------

#ifndef MODIFIER_H
#define MODIFIER_H

#include <string>


struct Modifier
{
	
	Modifier(const std::string& p_strName, const float p_fTime) : m_strModifierName(p_strName), m_fTotalTime(p_fTime) {}
	~Modifier();

	void Update(float p_fDelta)
	{
		m_fCurrentTime += p_fDelta;

		if (m_fCurrentTime > m_fTotalTime)
		{
			m_bExpired = true;
		}
	};

	std::string GetName() const { return m_strModifierName; }
	bool IsExpired() const { return m_bExpired; }

	// Method to add time to a modifier should it already exist
	void ExtendDuration(float p_fTime) { m_fTotalTime += p_fTime; }

private:

	float m_fTotalTime;
	float m_fCurrentTime = 0.0f;
	std::string m_strModifierName;

	// Expiration bool to check against
	bool m_bExpired = false;


};

#endif