//------------------------------------------------------------------------
// AIStateChanios
//
// State for an enemy chasing a player
//------------------------------------------------------------------------

#ifndef STATECHASING_H
#define STATECHASING_H

#include "StateBase.h"
#include "GameObject.h"
#include <list>
#include <glm\glm.hpp>

class AIStateChasing : public Common::StateBase
{
public:
	//------------------------------------------------------------------------------
	// Public methods.
	//------------------------------------------------------------------------------
	AIStateChasing();
	virtual ~AIStateChasing();

	// Overridden from StateBase
	virtual void Enter(ActionData* p_stateData);
	virtual void Update(float p_fDelta);
	virtual void Exit();

private:

	// Target to chase
	Common::GameObject* target;
	std::string targetID;

	// Current path/last position
	std::list<glm::vec3> currentPath;
	glm::vec3 lastPos;

	float turnSpeed;
	float moveSpeed;
	float turnLeniance;

	bool tilt;

};

#endif
