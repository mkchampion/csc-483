//------------------------------------------------------------------------
// EffectTracker
//	
// A class that determines what effects should be used depending on gameplay state
// and the parameters that they are fed, etc.  Hardcoding effects mostly
//------------------------------------------------------------------------

#include "EffectTracker.h"
#include "PlayerDamagedEvent.h"
#include "EventManager.h"
#include "ComponentCharacterController.h"
#include "HeliGame.h"

// Constructor
EffectTracker::EffectTracker()
{
	m_pEffects = new std::vector<EffectAndTarget>();

	// Basic setup for damage indicator
	damageEffect.mat = wolf::MaterialManager::CreateMaterial("DamageMat");
	damageEffect.mat->SetProgram("GameProject/GameProject/data/Shaders/2d.vsh", "GameProject/GameProject/data/Shaders/DamageEffect.fsh");
	damageEffect.mat->SetUniform("GradTo", 0.0f);
	damageEffect.mat->SetUniform("WindowWidth", (float)HeliGame::GetInstance()->GetWindowWidth());
	damageEffect.mat->SetUniform("WindowHeight", (float)HeliGame::GetInstance()->GetWindowHeight());
	damageEffect.renderTarget = new wolf::RenderTarget(HeliGame::GetInstance()->GetWindowWidth(), HeliGame::GetInstance()->GetWindowHeight(), 1.0f, false);
	damageEffect.renderTarget->SetupColorBuffer(true);
	damageEffect.renderTarget->SetupDepthBuffer(false);

	// Blur effects temp
	verticalBlur.mat = wolf::MaterialManager::CreateMaterial("VerticalBlur");
	verticalBlur.mat->SetProgram("GameProject/GameProject/data/Shaders/2d.vsh", "GameProject/GameProject/data/Shaders/verticalBlur.fsh");
	verticalBlur.renderTarget = new wolf::RenderTarget(HeliGame::GetInstance()->GetWindowWidth(), HeliGame::GetInstance()->GetWindowHeight(), 1.0f, false);
	verticalBlur.renderTarget->SetupColorBuffer(true);
	verticalBlur.renderTarget->SetupDepthBuffer(false);
	verticalBlur.mat->SetUniform("VStep", 1.0f / 450.0f);

	horizontalBlur.mat = wolf::MaterialManager::CreateMaterial("HorizontalBlur");
	horizontalBlur.mat->SetProgram("GameProject/GameProject/data/Shaders/2d.vsh", "GameProject/GameProject/data/Shaders/horizontalBlur.fsh");
	horizontalBlur.renderTarget = new wolf::RenderTarget(HeliGame::GetInstance()->GetWindowWidth(), HeliGame::GetInstance()->GetWindowHeight(), 1.0f, false);
	horizontalBlur.renderTarget->SetupColorBuffer(true);
	horizontalBlur.renderTarget->SetupDepthBuffer(false);
	horizontalBlur.mat->SetUniform("UStep", 1.0f / 800.0f);

	// Blurred grad testing
	blurredGrad.mat = wolf::MaterialManager::CreateMaterial("BlurredGrad");
	blurredGrad.mat->SetProgram("GameProject/GameProject/data/Shaders/2d.vsh", "GameProject/GameProject/data/Shaders/blurredGrad.fsh");
	blurredGrad.renderTarget = new wolf::RenderTarget(HeliGame::GetInstance()->GetWindowWidth(), HeliGame::GetInstance()->GetWindowHeight(), 1.0f, false);
	blurredGrad.renderTarget->SetupColorBuffer(true);
	blurredGrad.renderTarget->SetupDepthBuffer(false);
	blurredGrad.mat->SetUniform("WindowWidth", (float)HeliGame::GetInstance()->GetWindowWidth());
	blurredGrad.mat->SetUniform("WindowHeight", (float)HeliGame::GetInstance()->GetWindowHeight());

	deathEffect.mat = wolf::MaterialManager::CreateMaterial("TestMatTest");
	deathEffect.mat->SetProgram("GameProject/GameProject/data/Shaders/2d.vsh", "GameProject/GameProject/data/Shaders/GreyScale.fsh");
	deathEffect.renderTarget = new wolf::RenderTarget(HeliGame::GetInstance()->GetWindowWidth(), HeliGame::GetInstance()->GetWindowHeight(), 1.0f, false);
	deathEffect.renderTarget->SetupColorBuffer(true);
	deathEffect.renderTarget->SetupDepthBuffer(false);
}

// Destructor
EffectTracker::~EffectTracker()
{

}

// Init delegate
void EffectTracker::InitDelegates()
{

	std::string delegateString = "EffectTrackerListener";
	EventDelegate* delegate = new EventDelegate(delegateString, PlayerDamagedEvent::s_EventType, std::tr1::bind(&EffectTracker::HandlePlayerDamage, this, std::tr1::placeholders::_1));

	// Add to the projectile count so there is different listeners

	EventManager::Instance()->AddListener(PlayerDamagedEvent::s_EventType, delegate);
}

// Handle Player Damage event
void EffectTracker::HandlePlayerDamage(EventDelegate::EventDataPointer p_pPlayerDamage)
{
	std::shared_ptr<PlayerDamagedEvent> pCastCollision = std::static_pointer_cast<PlayerDamagedEvent>(p_pPlayerDamage);

	// Mess with the 
	testGrad += 0.07f;

	if (testGrad > maxGrad)
	{
		testGrad = maxGrad;
	}
}

void EffectTracker::UpdateDamage(float p_fDelta)
{
	testGrad -= p_fDelta*0.08;
	damageEffect.mat->SetUniform("GradTo", testGrad);

	if (testGrad < 0.0)
	{
		testGrad = 0.0f;
	}
}

// Update
void EffectTracker::Update(float p_fDelta, const Common::SceneCamera* p_pCam)
{
	// CLear current effects
	m_pEffects->clear();

	// Update the damage
	UpdateDamage(p_fDelta);

	// Calculate the players position in screen coordinates
	Common::GameObject* target = m_pManager->GetGameObject(playerID);
	if (target != NULL)
	{		
		glm::vec3 pos = target->GetTransform().GetTranslation();
		lastCameraPos = p_pCam->GetProjectionMatrix() * p_pCam->GetViewMatrix() * target->GetTransform().GetTransformation() * glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
		
		// Dividing  by W / Aspect ratio
		lastCameraPos.x = (lastCameraPos.x / lastCameraPos.w);
		lastCameraPos.y = (lastCameraPos.y / lastCameraPos.w);

		// To 0 - 1
		lastCameraPos.x = (lastCameraPos.x + 1.0f) / 2.0f;
		lastCameraPos.y = (lastCameraPos.y + 1.0f) / 2.0f;

		// Grab the speeds		
		ComponentCharacterController* pController = static_cast<ComponentCharacterController*>(target->GetComponent("GOC_CharacterController"));
		float velocityX = pController->GetVelocityX();
		float velocityZ = pController->GetVelocityZ();
		float maxVelocity = pController->GetMaxVelocity();

		// Set uniforms in blur
		blurredGrad.mat->SetUniform("PositionX", lastCameraPos.x);
		blurredGrad.mat->SetUniform("PositionY", lastCameraPos.y);
		blurredGrad.mat->SetUniform("VelocityX", abs(velocityX));
		blurredGrad.mat->SetUniform("VelocityZ", abs(velocityZ));
		blurredGrad.mat->SetUniform("MaxVelocity", maxVelocity);

		if (deathTimer > 0.0f)
		{
			deathTimer -= (1.75 * p_fDelta);

			if (deathTimer < 0.0f)
			{
				deathTimer = 0.0f;
			}
		}
		
		deathEffect.mat->SetUniform("DeathTimer", deathTimer);
		deathEffect.mat->SetUniform("FadeTime", fadeTime);
		
		velocityDeath = 0.0f;
		
	}
	else
	{
		// Test some stuff for death effect
		deathTimer += (1.0 * p_fDelta);
		deathEffect.mat->SetUniform("DeathTimer", deathTimer);
		deathEffect.mat->SetUniform("FadeTime", fadeTime);

		velocityDeath += (1.5f * p_fDelta);

		// Set uniforms in blur
		blurredGrad.mat->SetUniform("PositionX", lastCameraPos.x);
		blurredGrad.mat->SetUniform("PositionY", lastCameraPos.y);
		blurredGrad.mat->SetUniform("VelocityX", abs(velocityDeath));
		blurredGrad.mat->SetUniform("VelocityZ", abs(velocityDeath));
		blurredGrad.mat->SetUniform("MaxVelocity", 5.0f);
	}

	

	// Render screen to blur
	verticalBlur.renderTarget->Render(verticalBlur.mat, RenderTargetManager::Instance()->GetMainScene());
	horizontalBlur.renderTarget->Render(horizontalBlur.mat, RenderTargetManager::Instance()->GetMainScene());
	
	blurredGrad.mat->SetTexture("VerticalBlur", verticalBlur.renderTarget->GetColorBufferT());
	blurredGrad.mat->SetTexture("HorizontalBlur", horizontalBlur.renderTarget->GetColorBufferT());	

	m_pEffects->push_back(blurredGrad);
	m_pEffects->push_back(damageEffect);
	m_pEffects->push_back(deathEffect);

	RenderTargetManager::Instance()->SetEffects(m_pEffects);

}