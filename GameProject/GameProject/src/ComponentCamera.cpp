//------------------------------------------------------------------------
// CameraComponent
//	
// A CameraComponent that can be attached to a GameObject,
// Will have children classes that implement specific behavior
//------------------------------------------------------------------------

#include "ComponentCamera.h"

//------------------------------------------------------------------------------
// Method:    ComponentCamera
// Returns:   
// 
// Constructor
//------------------------------------------------------------------------------
ComponentCamera::ComponentCamera()
{
	// Made with default parameters for now
	m_pSceneCamera = new Common::SceneCamera();
}

//------------------------------------------------------------------------------
// Method:    ~ComponentCamera
// Returns:   
// 
// Destructor
//------------------------------------------------------------------------------
ComponentCamera::~ComponentCamera()
{
	// Delete the SceneCamera
	delete m_pSceneCamera;
}


