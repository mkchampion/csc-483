//------------------------------------------------------------------------
// StaticCollisionEvent
//
// An event that is triggered when a dynamic object collides with static geometry
//------------------------------------------------------------------------

#ifndef STATICCOLLISION_H
#define STATICCOLLISION_H

#include "BaseEventData.h"
#include "GameObject.h"

class StaticCollisionEvent : public BaseEventData
{

public:

	// Static event type
	static const EventType s_EventType;

	// Constructor / Destructor
	explicit StaticCollisionEvent(Common::GameObject* p_pObject) : m_pObject(p_pObject) {}
	~StaticCollisionEvent() {};

	// Accessor for event type
	const EventType& GetEventType() const { return s_EventType; }

	// Acccessor for object
	Common::GameObject* GetObject() const { return m_pObject; }

private:

	Common::GameObject* m_pObject;

};

#endif

