//-----------------------------------------------------------------------------
// File:			Emitter.cpp
// Original Author:	Matthew Champion
//-----------------------------------------------------------------------------

#include "Emitter.h"

// Included affectors
#include "Affectors\AddVelAffector.h"
#include "Affectors\FadeAffector.h"
#include "Affectors\ScaleAffector.h"
#include "Affectors\ColorAffector.h"
#include "Affectors\GravityAffector.h"
#include "Affectors\AccelerationAffector.h"

#include "GameProject\GameProject\src\ComponentCharacterController.h"

using namespace std;

static wolf::Program* p_Program = 0;

//-----------------------------------------------------------------------------
// Constructor, given a file to read and an offset from the effect
//-----------------------------------------------------------------------------
Emitter::Emitter(string filePathP, glm::vec3 offSetP)
{
	// Starts off playing
	m_Stopped = false;

	// Set initial values for the emitter, age is zero, and no current accumulated spawn
	m_Age = 0.0f;
	m_ToSpawn_accumulator = 0.0f;
	
	// Accept offset
	m_OffSet = offSetP;

	// 0 Rotation to start
	rotationY = 0.0f;

	// Set the initial linked lists to point to 0
	pFreeList = 0;
	pActiveListHead = 0;
	pActiveListTail = 0;

	
	// Default Values for a basic emitter in case nothing is read in
	// =============================================
	m_Name = "No_Emitter_Name";

	// Emitter Info
	m_PoolCount = 10;
	m_LifeTime = -1.0;

	m_SpawnMode = CONTINOUS;

	m_BirthRate = 2;

	m_TimeToBurst = 0;
	m_BurstTime = 1.0;
	m_ParticlePerBurst = 1;
	m_RepeatBurst = true;
	m_bShouldContinueBurst = true;

	m_BurstTimeMin = 0.0;
	m_BurstTimeMax = 1.0;
	m_BurstMode = CONSTANT;

	m_PointBurst = false;
	m_PointBurstMode = POINTSPAWN;

	m_PointSpawn = glm::vec3(0,0,0);
	m_BoxMin = glm::vec3(-1,0,-1);
	m_BoxMax = glm::vec3(1,0,1);

	m_SphereRadius = 1.0;

	m_SpawnArea = POINTSPAWN;

	m_ScaleStart = 1.0f;

	// Defaulted texture is empty
	m_TextureName = "";
	m_TexturePath = "";

	m_bRandomDirectionalVelocity = false;
	m_floatVelocityMin = 0.0f;
	m_floatVelocityMax = 0.0f;

	// ===================================================



	// Read in the values
	ReadFile(filePathP);	

	// Setup the buffer
	SetupBuffer();

}

//-----------------------------------------------------------------------------
// Destructor to delete all the particles and rendering objects
//-----------------------------------------------------------------------------
Emitter::~Emitter()
{
	
	// Go through the active list
	if (pActiveListHead != 0)
	{
		Particle* current = pActiveListHead->next;

		while (current != 0)
		{
			Particle* next = current->next;
			delete current;
			current = next;
		}

	}	

	if (pFreeList != 0)
	{
			
		Particle* current = pFreeList->next;

		while (current != 0)
		{
			Particle* next = current->next;
			delete current;
			current = next;
		}
	}
	

	delete[] particles;
	delete[] particleBuffer;

	// Free up wolf objects
	//wolf::MaterialManager::DestroyMaterial(p_Material);
	//wolf::BufferManager::DestroyBuffer(p_vertexBuffer);
	//delete(p_vDeclaration);
}


//-----------------------------------------------------------------------------
// Sets information based on a file read in from the effect
//-----------------------------------------------------------------------------
void Emitter::ReadFile(string pathP)
{

	// Creating XMLDoc
	TiXmlDocument testDoc;

	// Converting to char* and passing file to XMLDoc
	char* filePath = (char*)pathP.c_str();
	testDoc.LoadFile(filePath);

	// Retrieve the emitter element and pass it to the helper method
	TiXmlElement* emitter = testDoc.FirstChildElement();
	ReadEmitterProperties(emitter);

	// Get the information for the spawn properties
	TiXmlElement* emitterSpawnInfo = emitter->FirstChildElement("spawn_information");
	ReadSpawnInformation(emitterSpawnInfo);

	// Spawn properties for each particle
	TiXmlElement* spawnPropertyInfo = emitter->FirstChildElement("spawn_property");
	ReadSpawnProperties(spawnPropertyInfo);

	// Affectors
	TiXmlElement* affectors = emitter->FirstChildElement("affector");
	ReadAffectors(affectors);
	
}


//-----------------------------------------------------------------------------
// Sets up the rendering objects
//-----------------------------------------------------------------------------
void Emitter::SetupBuffer()
{
	// Make an array of particles
	particles = new Particle*[m_PoolCount];
	particleBuffer = new pVertex[m_PoolCount*6];


	// Add each particle to the free list
	for (int i = 0; i < m_PoolCount; i++)
	{
		
		// Instantiate the particle struct, one time values
		particles[i] = new Particle();
		particles[i]->parentEmitter = this;
		
		// Add this to the pool
		AddToPool(particles[i]);

	}	
	
	// Create the vertex buffer to use, based on the read in pool count
	p_vertexBuffer = wolf::BufferManager::CreateVertexBuffer(particleBuffer, sizeof(pVertex) * m_PoolCount*6);

	// Vertex declaration
	p_vDeclaration = new wolf::VertexDeclaration();
	p_vDeclaration->Begin();
	p_vDeclaration->AppendAttribute(wolf::AT_Position, 3, wolf::CT_Float);
	p_vDeclaration->AppendAttribute(wolf::AT_Color, 4, wolf::CT_Float);
	p_vDeclaration->AppendAttribute(wolf::AT_TexCoord1, 2, wolf::CT_Float);
	p_vDeclaration->SetVertexBuffer(p_vertexBuffer);
	p_vDeclaration->End();

	// Create a material based on the texture input, always uses the same particle shader for now    
	p_Material = wolf::MaterialManager::CreateMaterial(m_Name);
	p_Material->SetProgram("GameProject/GameProject/data/Shaders/ParticleShader.vsh", "GameProject/GameProject/data/Shaders/ParticleShader.fsh");

	// Check for blending
	p_Material->SetBlend(m_BlendEnabled);

	// If enabled, set the modes
	if (m_BlendEnabled)
	{
		p_Material->SetBlendEquation(m_BlendEquation);
		p_Material->SetBlendMode(m_BlendSource, m_BlendDest);
	}

	p_Material->SetDepthTest(true);
	p_Material->SetDepthWrite(false);

	p_Material->SetTexture(m_TextureName, wolf::TextureManager::CreateTexture(m_TexturePath));

}


//-----------------------------------------------------------------------------
// Helper method to the ReadFile method, this method reads the properties of the emitter
//-----------------------------------------------------------------------------
void Emitter::ReadEmitterProperties(TiXmlElement* p_pElement)
{
	// Emitter element to read from
	TiXmlElement* emitter = p_pElement;

	// Name
	m_Name = emitter->Attribute("name");

	// Get the initial attributes of the emitter (Name, Lifetime, PoolCount, birthrate, spawnType, etc)
	emitter->QueryIntAttribute("pool_count", &m_PoolCount);
	emitter->QueryFloatAttribute("life_time", &m_LifeTime);

		// Checking the current mode
	string emitterMode = emitter->Attribute("type");

	if (emitterMode.compare("continous") == 0)
		m_SpawnMode = CONTINOUS;
	else if (emitterMode.compare("burst") == 0)
		m_SpawnMode = BURST;



	// Continous values	
	if (m_SpawnMode == CONTINOUS)
	{
		emitter->QueryFloatAttribute("birth_rate", &m_BirthRate);
	}

	// Burst specific
	else if (m_SpawnMode == BURST)
	{

		emitter->QueryIntAttribute("particle_per_burst", &m_ParticlePerBurst);
		emitter->QueryFloatAttribute("burst_delay", &m_BurstTime);
		emitter->QueryFloatAttribute("initial_burst", &m_TimeToBurst);

		// Get the burst mode (Default to constant)
		m_BurstMode = CONSTANT;

		string burstMode = emitter->Attribute("burst_mode");
		
		// Random burst mode
		if (burstMode.compare("random") == 0)
		{

			m_BurstMode = RANDOM;

			// Now check for min/max
			emitter->QueryFloatAttribute("min_delay", &m_BurstTimeMin);
			emitter->QueryFloatAttribute("max_delay", &m_BurstTimeMax);
		}

		else if (burstMode.compare("constant") == 0)
		{
			m_BurstMode = CONSTANT;
			emitter->QueryFloatAttribute("delay", &m_BurstTime);
		}
		

		// Repeat bursts
		string repeatBurst = emitter->Attribute("repeat_burst");
			
		if (repeatBurst.compare("true") == 0)
			m_RepeatBurst = true;
		else
			m_RepeatBurst = false;

		// Point burst
		emitter->QueryBoolAttribute("point_burst", &m_PointBurst);

	}

	

	// Checking for texture name/path
	if (emitter->Attribute("texture_name"))
		m_TextureName = emitter->Attribute("texture_name");

	if (emitter->Attribute("texture_path"))
		m_TexturePath = emitter->Attribute("texture_path");



	// Checking the blend information below the initial information
	if (emitter->FirstChildElement("blending_information"))
	{
				
		TiXmlElement* blendInfo = emitter->FirstChildElement("blending_information");

		blendInfo->QueryBoolAttribute("enabled", &m_BlendEnabled);

		if (m_BlendEnabled)
		{
			// Get the equation and two modes
			if (blendInfo->Attribute("equation"))
			{
				string blendEquation = blendInfo->Attribute("equation");
				m_BlendEquation = BlendEquationFromString(blendEquation);
			}
			else
			{
				m_BlendEquation = wolf::BlendEquation::BE_Add;
			}
			

			// Both source and destination modes
			if (blendInfo->Attribute("source"))
			{
				string sourceMode = blendInfo->Attribute("source");
				m_BlendSource = BlendModeFromString(sourceMode);
			}
			else
			{
				m_BlendSource = wolf::BlendMode::BM_One;
			}

			if (blendInfo->Attribute("dest"))
			{
				string destMode = blendInfo->Attribute("dest");
				m_BlendDest = BlendModeFromString(destMode);
			}
			else
			{
				m_BlendDest = wolf::BlendMode::BM_One;
			}
			
		}
	}
	else
	{
		m_BlendEnabled = false;
	}


}

//-----------------------------------------------------------------------------
// Helper to ReadFile method, reads the spawn information (Area)
//-----------------------------------------------------------------------------
void Emitter::ReadSpawnInformation(TiXmlElement* p_pElement)
{

	// Spawning Area for the emitter
	TiXmlElement* spawnArea = p_pElement;

	// Get the type
	string areaType = spawnArea->Attribute("type");

	// Check types
	// ================================

	// Point
	if (areaType.compare("point") == 0)
	{
		m_PointSpawn = SplitVec3(spawnArea->Attribute("point"));
		m_SpawnArea = POINTSPAWN;
	}


	// Sphere
	else if (areaType.compare("sphere") == 0)
	{
		m_PointSpawn = SplitVec3(spawnArea->Attribute("point"));
		spawnArea->QueryFloatAttribute("radius", &m_SphereRadius);
		m_SpawnArea = SPHERE;
	}


	// Box
	else if (areaType.compare("box") == 0)
	{
		m_BoxMin = SplitVec3(spawnArea->Attribute("box_min"));
		m_BoxMax = SplitVec3(spawnArea->Attribute("box_max"));
		m_SpawnArea = BOX;
	}


	// Set the pointBurstSpawn to be equal to the spawning mode
	if (m_PointBurst)
	{
		m_PointBurstMode = m_SpawnArea;
		m_SpawnArea = POINTSPAWN;
	}


	// =======================================
}


//-----------------------------------------------------------------------------
// Helper method to ReadFile, reads in the particle spawn properties
//-----------------------------------------------------------------------------
void Emitter::ReadSpawnProperties(TiXmlElement* p_pElement)
{	
		

	// Set spawnPropery to first given spawn property
	TiXmlElement* spawnProperty = p_pElement;	


	// While there are spawn properties to be read
	while (spawnProperty)
	{	

		// Get the name of the property
		string propertyName = spawnProperty->Attribute("name");

		// Check the type
		string propertyType = spawnProperty->Attribute("type");


		// Velocity
		// ##################
		if (propertyName.compare("velocity") == 0)
		{
			
			// Check type 
			if (propertyType.compare("constant") == 0)
			{					
				m_VelocityConst = SplitVec3(spawnProperty->Attribute("value"));
				m_VelocitySpawnType = CONSTANT;
			}

			else if (propertyType.compare("random") == 0)
			{
				m_VelocityMin =  SplitVec3(spawnProperty->Attribute("min"));
				m_VelocityMax =  SplitVec3(spawnProperty->Attribute("max"));
				m_VelocitySpawnType = RANDOM;
			}

			else if (propertyType.compare("randomDirection") == 0)
			{
				m_VelocityMin = SplitVec3(spawnProperty->Attribute("minDir"));
				m_VelocityMax = SplitVec3(spawnProperty->Attribute("maxDir"));
				spawnProperty->QueryFloatAttribute("min", &m_floatVelocityMin);
				spawnProperty->QueryFloatAttribute("max", &m_floatVelocityMax);
				m_bRandomDirectionalVelocity = true;
				m_VelocitySpawnType = RANDOM;
			}
		}
		



		// Lifetime
		// ##################
		else if (propertyName.compare("lifeTime") == 0)
		{
			
			// Check type 
			if (propertyType.compare("constant") == 0)
			{
				spawnProperty->QueryFloatAttribute("value", &m_LifeTimeConst);
				m_LifeTimeSpawnType = CONSTANT;
			}

			else if (propertyType.compare("random") == 0)
			{
				spawnProperty->QueryFloatAttribute("min", &m_LifeTimeMin);
				spawnProperty->QueryFloatAttribute("max", &m_LifeTimeMax);
				m_LifeTimeSpawnType = RANDOM;
			}

		}





		// Color
		// ######################
		else if (propertyName.compare("color") == 0)
		{
			// Check type 
			if (propertyType.compare("constant") == 0)
			{
				// Get color
				glm::vec3 ColorAlpha = SplitVec3(spawnProperty->Attribute("value"));

				// Setting constant color
				m_ColorConst = glm::vec3(ColorAlpha.x, ColorAlpha.y, ColorAlpha.z);

				// Setting color spawn
				m_ColorSpawnType = CONSTANT;
				
			}

			else if (propertyType.compare("random") == 0)
			{
				// Get both combined
				glm::vec3 ColorAlphaMin = SplitVec3(spawnProperty->Attribute("min"));
				glm::vec3 ColorAlphaMax = SplitVec3(spawnProperty->Attribute("max"));

				// Set
				m_ColorMin = glm::vec3(ColorAlphaMin.x, ColorAlphaMin.y, ColorAlphaMin.z);
				m_ColorMax = glm::vec3(ColorAlphaMax.x, ColorAlphaMax.y, ColorAlphaMax.z);

				m_ColorSpawnType = RANDOM;
			}
		}



		// Fade
		// #########################
		else if (propertyName.compare("fade") == 0)
		{
			// Check type 
			if (propertyType.compare("constant") == 0)
			{
				// Get value
				float fadeValue = 0.0f;
				spawnProperty->QueryFloatAttribute("value", &fadeValue);
	
				// Set fade
				m_FadeConst = fadeValue;

				m_FadeSpawnType = CONSTANT;
				
			}

			else if (propertyType.compare("random") == 0)
			{
				// Get both combined
				float fadeMin = 0.0f;
				float fadeMax = 0.0f;

				spawnProperty->QueryFloatAttribute("min", &fadeMin);
				spawnProperty->QueryFloatAttribute("max", &fadeMax);

				// Set
				m_FadeMin = fadeMin;
				m_FadeMax = fadeMax;

				m_FadeSpawnType = RANDOM;
			}
		}




		// Size
		// ################################
		else if (propertyName.compare("size") == 0)
		{
			// Check type 
			if (propertyType.compare("constant") == 0)
			{
				// Get value
				float sizeValue = 0.0f;
				spawnProperty->QueryFloatAttribute("value", &sizeValue);
	
				// Set fade
				m_SizeConst = sizeValue;

				m_SizeSpawnType = CONSTANT;
				
			}

			else if (propertyType.compare("random") == 0)
			{
				// Get both combined
				float sizeMin = 0.0f;
				float sizeMax = 0.0f;
				spawnProperty->QueryFloatAttribute("min", &sizeMin);
				spawnProperty->QueryFloatAttribute("max", &sizeMax);

				// Set
				m_SizeMin = sizeMin;
				m_SizeMax = sizeMax;

				m_SizeSpawnType = RANDOM;
			}
		}
				

		// Get the next spawn property if there is one
		spawnProperty = spawnProperty->NextSiblingElement("spawn_property");

	}

}


//-----------------------------------------------------------------------------
// Helper method to ReadFile, Reads in the affectors and adds them to the list
//-----------------------------------------------------------------------------
void Emitter::ReadAffectors(TiXmlElement* p_pElement)
{
	

	// Setting affector to first given affector
	TiXmlElement* affector = p_pElement;


	// Loop through each affector
	while (affector)
	{
		
		// Get the type of the affector
		string affectorType = affector->Attribute("type");

		
		// Velocity
		// ####################
		if (affectorType.compare("add_velocity") == 0)
			m_Affectors.push_back(new AddVelAffector());



		// Fade
		// ####################
		else if (affectorType.compare("fade") == 0)
		{
			// Values to pass to the affector (With default values
			AffectorMode mode = OVER_LIFE;
			float fadeSpeed = 0.0f;
			float startFade = 0.0f;
			float endFade = 1.0f;
			
			// Get the type/mode
			TiXmlElement* affectorProperty = affector->FirstChildElement("property");

			// Loop through each property
			while (affectorProperty)
			{			
				// Check the name
				string propertyName = affectorProperty->Attribute("name");

				// Set values depending on propertyName

				// Mode
				if (propertyName.compare("mode") == 0)
				{
					
					// Check for the mode
					string modeString = affectorProperty->Attribute("value");
					
					if (modeString.compare("over_life") == 0)
						mode = OVER_LIFE;
					else if (modeString.compare("constant_speed") == 0)
						mode = CONSTANT_SPEED;

				}

				// Speed
				else if (propertyName.compare("speed") == 0)
				{
					affectorProperty->QueryFloatAttribute("value", &fadeSpeed);
				}

				// End
				else if (propertyName.compare("end") == 0)
				{
					affectorProperty->QueryFloatAttribute("value", &endFade);
				}				
							
				// Move to next property
				affectorProperty = affectorProperty->NextSiblingElement("property");

			}

			// Make the fade affector
			m_Affectors.push_back(new FadeAffector(fadeSpeed,endFade,mode));
			
		}


		// Color
		// ####################
		else if (affectorType.compare("color") == 0)
		{
			// Values to pass to the affector (With default values)
			AffectorMode mode = OVER_LIFE;
			glm::vec3 startColor = glm::vec3(0.0,0.0,0.0);
			glm::vec3 endColor = glm::vec3(1.0,1.0,1.0);
			float changeSpeed = 0.0f;
			
			// Get the type/mode
			TiXmlElement* affectorProperty = affector->FirstChildElement("property");

			// Loop through each property
			while (affectorProperty)
			{			
				// Check the name
				string propertyName = affectorProperty->Attribute("name");

				// Set values depending on propertyName

				// Mode
				if (propertyName.compare("mode") == 0)
				{

					// Check for the mode
					string modeString = affectorProperty->Attribute("value");

					if (modeString.compare("over_life") == 0)
						mode = OVER_LIFE;
					else if (modeString.compare("constant_speed") == 0)
						mode = CONSTANT_SPEED;

				}

				// Speed
				else if (propertyName.compare("speed") == 0)
				{
					affectorProperty->QueryFloatAttribute("value", &changeSpeed);
				}

				// End
				if (propertyName.compare("end") == 0)
				{
					endColor = SplitVec3(affectorProperty->Attribute("value"));
				}				
							
				// Move to next property
				affectorProperty = affectorProperty->NextSiblingElement("property");

			}

			// Make the fade affector
			m_Affectors.push_back(new ColorAffector(endColor, changeSpeed, mode));
			
		}

		// Acceleration
		// ####################
		else if (affectorType.compare("acceleration") == 0)
		{
			// Values to pass to the affector (With default values)
			AffectorMode mode = CONSTANT_SPEED;
			glm::vec3 endColor = glm::vec3(0.0, 0.0, 0.0);
			float changeSpeed = 0.0f;

			// Get the type/mode
			TiXmlElement* affectorProperty = affector->FirstChildElement("property");

			// Loop through each property
			while (affectorProperty)
			{
				// Check the name
				string propertyName = affectorProperty->Attribute("name");

				// Set values depending on propertyName

				// Mode
				if (propertyName.compare("mode") == 0)
				{

					// Check for the mode
					string modeString = affectorProperty->Attribute("value");

					if (modeString.compare("over_life") == 0)
						mode = OVER_LIFE;
					else if (modeString.compare("constant_speed") == 0)
						mode = CONSTANT_SPEED;

				}

				// Speed
				else if (propertyName.compare("speed") == 0)
				{
					affectorProperty->QueryFloatAttribute("value", &changeSpeed);
				}

				// End
				if (propertyName.compare("end") == 0)
				{
					endColor = SplitVec3(affectorProperty->Attribute("value"));
				}

				// Move to next property
				affectorProperty = affectorProperty->NextSiblingElement("property");

			}

			// Make the fade affector
			m_Affectors.push_back(new AccelerationAffector(endColor, changeSpeed, mode));

		}


		// Scale
		// ##########################

		else if (affectorType.compare("scale") == 0)
		{
			// Values to pass to the affector (With default values
			AffectorMode mode = OVER_LIFE;
			float scaleSpeed = 0.0f;
			float startScale = 0.0f;
			float endScale = 1.0f;
			
			// Get the type/mode
			TiXmlElement* affectorProperty = affector->FirstChildElement("property");

			// Loop through each property
			while (affectorProperty)
			{			
				// Check the name
				string propertyName = affectorProperty->Attribute("name");

				// Set values depending on propertyName

				// Mode
				if (propertyName.compare("mode") == 0)
				{
					
					// Check for the mode
					string modeString = affectorProperty->Attribute("value");
					
					if (modeString.compare("over_life") == 0)
						mode = OVER_LIFE;
					else if (modeString.compare("constant_speed") == 0)
						mode = CONSTANT_SPEED;

				}

				// Speed
				else if (propertyName.compare("speed") == 0)
				{
					affectorProperty->QueryFloatAttribute("value", &scaleSpeed);
				}

				// Start
				else if (propertyName.compare("start") == 0)
				{
					affectorProperty->QueryFloatAttribute("value", &startScale);

					// Also set the scale property for the whole emitter
					m_ScaleStart = startScale;
				}

				// End
				else if (propertyName.compare("end") == 0)
				{
					affectorProperty->QueryFloatAttribute("value", &endScale);
				}				
							
				// Move to next property
				affectorProperty = affectorProperty->NextSiblingElement("property");

			}

			// Make the fade affector
			m_Affectors.push_back(new ScaleAffector(scaleSpeed,startScale,endScale,mode));
			
		}
		

		// Gravity
		// ##########################
		if (affectorType.compare("gravity") == 0)
		{

			// Get the gravity value
			float gravityValue = 0.0f;
			affector->FirstChildElement("property")->QueryFloatAttribute("value", &gravityValue);

			// Make the affector and add it to the list
			GravityAffector* gravAffector = new GravityAffector(gravityValue);
			m_Affectors.push_back(gravAffector);

		}

		// Move on to the next affector
		affector = affector->NextSiblingElement("affector");

	}

	// Tell the first affector to make sure to update with the parent
	if (m_Affectors.at(0))
	{
		m_Affectors.at(0)->UpdateWithParent();
	}

}

//-----------------------------------------------------------------------------
// Update method, adds to the age, and will update the active particles and spawn new ones
//-----------------------------------------------------------------------------
void Emitter::Update(float deltaTimeP)
{
	// Update the age
	m_Age += deltaTimeP;

	// Update the existing particles
	UpdateActive(deltaTimeP);

	// Update the lifetime of the emitter and update the spawns
	if ((m_Age < m_LifeTime || m_LifeTime < 0.0) && !m_Stopped && !m_flaggedForDelete)
		UpdateSpawning(deltaTimeP);


}


//-----------------------------------------------------------------------------
// Updates all active particles
//-----------------------------------------------------------------------------
void Emitter::UpdateActive(float deltaTimeP)
{
	// Get the active head
	Particle* activeListP = pActiveListHead;

	// While there is particles in the active list
	while (activeListP)
	{
		// Particle to operate on next
		Particle* next = activeListP->next;

		// Each affector
		for (int j = 0; j < m_Affectors.size(); j++)
		{
			m_Affectors[j]->Apply(deltaTimeP, activeListP);
		}

		activeListP = next;
	}

}


//-----------------------------------------------------------------------------
// Spawns particles based on emitter properties
//-----------------------------------------------------------------------------
void Emitter::UpdateSpawning(float deltaTimeP)
{

	// Continous Spawning Mode
	if (m_SpawnMode == CONTINOUS)
	{
		

		m_ToSpawn_accumulator += m_BirthRate * deltaTimeP;
		int numSpawns = int(m_ToSpawn_accumulator);
		m_ToSpawn_accumulator -= numSpawns;

		// Spawn them
		for (int i = 0; i < numSpawns; i++)
		{
			SpawnParticle();
		}

	}


	// Burst Spawning Mode
	else if (m_SpawnMode == BURST)
	{

		// Calculate time left to burst
		m_TimeToBurst -= deltaTimeP;
		

		// Burst if necessary
		if (m_TimeToBurst <= 0.0f && m_bShouldContinueBurst)
		{
				// A bit of a hack for a pointBurst so that it can be used with other spawn modes,
				// If its labeled as point burst, the position changes depending on spawnArea rather than each particle
				if (m_PointBurst)
				{

					// Test against new spawning mode
					if (m_PointBurstMode == BOX)
						m_PointSpawn = RandomInRange(m_BoxMin,m_BoxMax);
					else if (m_PointBurstMode == SPHERE)
						m_PointSpawn += RandomInRange(glm::vec3(0,0,0), glm::vec3(m_SphereRadius, m_SphereRadius, m_SphereRadius));
						
				}

			// Calculate the number of spawns
			int numSpawns = m_ParticlePerBurst;

			for (int i = 0; i < numSpawns; i++)
			{
				SpawnParticle();
			}


			// Repeat if necessary
			if (m_RepeatBurst)
			{
				CalcBurstTime();
			}
			else
			{
				m_bShouldContinueBurst = false;
			}
		}
	}
}


//-----------------------------------------------------------------------------
// Calculates the burst time for a burst emitter
//-----------------------------------------------------------------------------
void Emitter::CalcBurstTime()
{

	// Check times
	if (m_BurstMode == CONSTANT)
		m_TimeToBurst = m_BurstTime;
	else if (m_BurstMode == RANDOM)
	{
		m_TimeToBurst = RandomInRange(m_BurstTimeMin,m_BurstTimeMax);
	}
}

//-----------------------------------------------------------------------------
// Sets the defaults of a newly spawned particle
//-----------------------------------------------------------------------------
void Emitter::SetParticleDefaults(Particle* p)
{

	// Resetting all the default values based on modes
	
	// First set the age back to 0
	p->age = 0.0f;
	p->scale = m_ScaleStart;	


	// Determine position
	if (m_SpawnArea == POINTSPAWN)
	{
		p->position = m_PointSpawn;
	}
	else if (m_SpawnArea = BOX)
	{
		p->position = RandomInRange(m_BoxMin,m_BoxMax);
	}
	else if (m_SpawnArea = SPHERE)
	{
		p->position.x = RandomInRange(m_PointSpawn.x - m_SphereRadius, m_PointSpawn.x + m_SphereRadius);
		p->position.y = RandomInRange(m_PointSpawn.y - m_SphereRadius, m_PointSpawn.y + m_SphereRadius);
		p->position.z = RandomInRange(m_PointSpawn.z - m_SphereRadius, m_PointSpawn.z + m_SphereRadius);
	}

	// Add the offset effects to the position
	p->position = p->position + m_OffSet + m_Position;

	// Determine a new lifetime
	if (m_LifeTimeSpawnType == CONSTANT)
	{

		// Make sure it doesn't try dividing by 0 for a 0 lifetime input
		if (m_LifeTimeConst == 0)
			p->lifeTimeRecip = 0.0f;
		else
			p->lifeTimeRecip = 1.0/(m_LifeTimeConst);
	}

	else if (m_LifeTimeSpawnType == RANDOM)
	{
		p->lifeTimeRecip = 1.0/(RandomInRange(m_LifeTimeMin,m_LifeTimeMax));				
	}
	

	// Give the particle a new velocity
	if (m_VelocitySpawnType == CONSTANT)
	{
		p->velocity = m_VelocityConst;
	}

	else if (m_VelocitySpawnType == RANDOM)
	{
		if (!m_bRandomDirectionalVelocity)
		{
			p->velocity = RandomInRange(m_VelocityMin, m_VelocityMax);
		}
		else
		{
			glm::vec3 randomDir = RandomInRange(m_VelocityMin, m_VelocityMax);
			randomDir = glm::normalize(randomDir);
			p->velocity = randomDir * RandomInRange(m_floatVelocityMin, m_floatVelocityMax);
		}
	}

	

	// Rotate the velocity
	glm::vec4 rotatedVelocityV4 = glm::rotate(rotationY, 0.0f, 1.0f, 0.0f) * glm::vec4(p->velocity, 1.0f);
	p->velocity = glm::vec3(rotatedVelocityV4);

	// Check for target
	if (hasTarget)
	{
		ComponentCharacterController* characterController = static_cast<ComponentCharacterController*>(m_pTarget->GetComponent("GOC_CharacterController"));
		p->velocity += glm::vec3(characterController->GetVelocityX(), 0.0f, characterController->GetVelocityZ());
	}

	// Give the particle a new color
	if (m_ColorSpawnType == CONSTANT)
	{
		p->color = m_ColorConst;
	}
	else if (m_ColorSpawnType == RANDOM)
	{
		p->color = RandomInRange(m_ColorMin, m_ColorMax);
	}

	// Set start color to the same
	p->startColor = p->color;

	// Give the particle a new fade (alpha?)
	if (m_FadeSpawnType == CONSTANT)
	{
		p->alpha = m_FadeConst;
	}
	else if (m_FadeSpawnType == RANDOM)
	{
		p->alpha = RandomInRange(m_FadeMin, m_FadeMax);
	}

	// Set the start alpha to the same
	p->startAlpha = p->alpha;


	// Give the particle a new size
	if (m_SizeSpawnType == CONSTANT)
	{
		p->size = m_SizeConst;
	}
	else if (m_SizeSpawnType == RANDOM)
	{
		p->size = RandomInRange(m_SizeMin, m_SizeMax);
	}


}


//-----------------------------------------------------------------------------
// Spawns a particle
//-----------------------------------------------------------------------------
void Emitter::SpawnParticle()
{
	
	// Check if the freeList is null, if it is, recycle a particle
	if (pFreeList == 0)
	{

		Particle* p = pActiveListTail;		
		RemoveFromActive(p);
		AddToActive(p);

		SetParticleDefaults(p);

	}

	// Otherwise just take from the free list
	else
	{
		Particle* p = GetFreeParticle();
		AddToActive(p);

		SetParticleDefaults(p);
	}
	
}

//-----------------------------------------------------------------------------
// Kills a particle, 
//-----------------------------------------------------------------------------
void Emitter::KillParticle(Particle* p)
{

	// Remove it and add it back to the pool
	if (p)
	{
		RemoveFromActive(p);
		AddToPool(p);
	}	
	
}



//-----------------------------------------------------------------------------
// Add a particle to the pool of free particles
//-----------------------------------------------------------------------------
void Emitter::AddToPool(Particle* p)
{

	if (pFreeList != 0)
	{
		pFreeList->prev = p;
		p->next = pFreeList;
		p->prev = 0;	
	}
	else
	{
		p->next = pFreeList;
		p->prev = 0;
	}

	// Make it the head of the list
	pFreeList = p;

}

//-----------------------------------------------------------------------------
// Gets a free particle from the list, if full recycles
//-----------------------------------------------------------------------------
Particle* Emitter::GetFreeParticle()
{
	// The particle to return from the head of the list
	Particle* freeParticle = pFreeList;
	
	// Move the current head to the next one
	pFreeList = pFreeList->next;

	// If there is still a particle in the head, make its previous null
	if (pFreeList)
		pFreeList->prev = 0;
	

	// Change the particle to return so it isn't linked anymore
	freeParticle->next = 0;
	freeParticle->prev = 0;

	return freeParticle;

}

//-----------------------------------------------------------------------------
// Remove a particle from the active list
//-----------------------------------------------------------------------------
void Emitter::RemoveFromActive(Particle* p)
{

	// Check if it has a previous (if not, its the head)
	if (p->prev == 0)
	{
		// Update the head
		pActiveListHead = p->next;

		if (pActiveListHead)
			pActiveListHead->prev = 0;
		
	}

	// Check for tail
	else if (p->next == 0)
	{
		pActiveListTail = p->prev;
		pActiveListTail->next = 0;
	}
	else
	{		
		p->next->prev = p->prev;
		p->prev->next = p->next;
	}

	
	// Removing its next/prev
	p->next = 0;
	p->prev = 0;	
	

}

//-----------------------------------------------------------------------------
// Adds a particle to the active list
//-----------------------------------------------------------------------------
void Emitter::AddToActive(Particle* p)
{
	// Check if its null
	if (!pActiveListHead)
	{
		pActiveListHead = p;
		pActiveListTail = p;

		pActiveListHead->next = 0;
		pActiveListHead->prev = 0;

		pActiveListTail->next = 0;
		pActiveListTail->prev = 0;

	}
	else
	{		
		// Make sure the head changes, tail remains as the initial one
		p->next = pActiveListHead;
		p->prev = 0;
		pActiveListHead->prev = p;

		pActiveListHead = p;

	}

}


//-----------------------------------------------------------------------------
// Tells the emitter to stop spawning
//-----------------------------------------------------------------------------
void Emitter::SetStop(bool p_bSpawning)
{
	m_Stopped = p_bSpawning;
}


//-----------------------------------------------------------------------------
// Renders the particles
//-----------------------------------------------------------------------------
void Emitter::UpdatePos(glm::vec3 p_vPosition)
{
	m_Position = p_vPosition;
}

//-----------------------------------------------------------------------------
// Renders the particles
//-----------------------------------------------------------------------------
void Emitter::Render(glm::mat4 p_pProj, glm::mat4 p_pView)
{
	// Projection/World/View to use for the vertex buffer
	glm::mat4 mProj = p_pProj;
	glm::mat4 mView = p_pView;

	// Grab the rotation part of the view matrix and transpose it for billboarding
	glm::mat3 inverseView = glm::mat3(mView);
	inverseView = glm::transpose(inverseView);

	// Make the projViewWorld for the shader
	glm::mat4 mWorld = glm::translate(0.0f, 0.0f, 0.0f); // m_mTranslation*m_mRotation;
	glm::mat4 mProjViewWorld = mProj*mView*mWorld;




	// Calculating all the new vertices for the particles, and adding them to the array that the buffer uses

	// Set the vertex/particle count to 0 for this render pass
	int vertexCount = 0;
	int particleCount = 0;

	// Retrieve the head of the active list
	Particle* activeParticle = pActiveListHead;

	// Go through each particle and compute vertices
	while (activeParticle != 0)
	{

		// Get the coordinates of the current particle
		float x = activeParticle->position.x;
		float y = activeParticle->position.y;
		float z = activeParticle->position.z;

		// Color values
		float r = activeParticle->color.x;
		float g = activeParticle->color.y;
		float b = activeParticle->color.z;
		float a = activeParticle->alpha;

		// Size adjustment for each particle based on the starting size, and the scale over time
		float s = activeParticle->scale * activeParticle->size;

		// Create a standard 1x1 quad centered at 0
		glm::vec3 p1M = glm::vec3((-0.5)*s, (-0.5)*s, 0.0f);
		glm::vec3 p2M = glm::vec3((-0.5)*s, (+0.5)*s, 0.0f);
		glm::vec3 p3M = glm::vec3((+0.5)*s, (+0.5)*s, 0.0f);

		glm::vec3 p4M = glm::vec3((-0.5)*s, (-0.5)*s, 0.0f);
		glm::vec3 p5M = glm::vec3((+0.5)*s, (-0.5)*s, 0.0f);
		glm::vec3 p6M = glm::vec3((+0.5)*s, (+0.5)*s, 0.0f);


		// Rotate them according to the camera
		p1M = inverseView * p1M;
		p2M = inverseView * p2M;
		p3M = inverseView * p3M;

		p4M = inverseView * p4M;
		p5M = inverseView * p5M;
		p6M = inverseView * p6M;


		// Translate this new quad to the current particle position
		pVertex p1 = { x + p1M.x, y + p1M.y, z + p1M.z, r, g, b ,a, 0.0f, 0.0f};
		pVertex p2 = { x + p2M.x, y + p2M.y, z + p2M.z, r, g, b ,a, 0.0f, 1.0f};
		pVertex p3 = { x + p3M.x, y + p3M.y, z + p3M.z, r, g, b ,a, 1.0f, 1.0f};

		pVertex p4 = { x + p4M.x, y + p4M.y, z + p4M.z, r, g, b ,a, 0.0f, 0.0f};
		pVertex p5 = { x + p5M.x, y + p5M.y, z + p5M.z, r, g, b ,a, 1.0f, 0.0f};
		pVertex p6 = { x + p6M.x, y + p6M.y, z + p6M.z, r, g, b ,a, 1.0f, 1.0f};

		
		// Add them to the particle buffer
		particleBuffer[particleCount*6 + 0] = p1;
		particleBuffer[particleCount*6 + 1] = p2;
		particleBuffer[particleCount*6 + 2] = p3;

		particleBuffer[particleCount*6 + 3] = p4;
		particleBuffer[particleCount*6 + 4] = p5;
		particleBuffer[particleCount*6 + 5] = p6;

		// Get the next particle and increment the count
		activeParticle = activeParticle->next;

		particleCount++;
		vertexCount += 6;

	}
	
		
	// Bind current buffer and write the particles to it
	p_vertexBuffer->Bind();
	p_vertexBuffer->Write(particleBuffer, sizeof(pVertex) * vertexCount); 
	
	// Bind declaration and apply material
	p_vDeclaration->Bind();
	p_Material->SetUniform("projViewWorld", mProjViewWorld);
	p_Material->Apply();

	// Render all the particles
	glDrawArrays(GL_TRIANGLES, 0, vertexCount);
	
}

bool Emitter::IsStillActive()
{
	if (pActiveListHead != 0)
	{
		return true;
	}
	else if (m_Age < m_LifeTime && m_LifeTime > 0.0)
	{
		return true;
	}
	else if (m_LifeTime < 0.0f && !m_flaggedForDelete)
	{
		return true;
	}
	else
	{
		return false;
	}

}
