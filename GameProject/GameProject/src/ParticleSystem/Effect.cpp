//-----------------------------------------------------------------------------
// File:			Effect.cpp
// Original Author:	Matthew Champion
//-----------------------------------------------------------------------------

#include "Effect.h"

//-----------------------------------------------------------------------------
// Constructer
//-----------------------------------------------------------------------------
Effect::Effect(string filePathP)
{		
	LoadEffect(filePathP);
	
	// Initial effect is at 0,0,0 and is playing
	m_Paused = false;
	m_Stopped = false;

	m_Position = glm::vec3(0,0,0);
	effectRotation = 0.0f;

	hasTarget = false;
}

//-----------------------------------------------------------------------------
// Destructor
//-----------------------------------------------------------------------------
Effect::~Effect()
{
	// Destroy each emitter in the vector
	for (int i = 0; i < m_Emitters.size(); i++)
	{
		m_Emitters[i]->~Emitter();
	}	
}

//-----------------------------------------------------------------------------
// Loads the effect from a given file
//-----------------------------------------------------------------------------
void Effect::LoadEffect(string filePathP)
{

	// Free each emitter
	for (int i = 0; i < m_Emitters.size(); i++)
	{
		m_Emitters[i]->~Emitter();
	}

	// Reset the vector to size 0
	m_Emitters.resize(0);


	// Set up the document
	// Reading in the given file
	TiXmlDocument effectDoc;

	// Converting to char* and passing to XMLDocument
	char* filePath = (char*)filePathP.c_str();
	effectDoc.LoadFile(filePath);

	// Overall effect element
	TiXmlElement* effect = effectDoc.FirstChildElement();	

	// Get the m_Emitter
	TiXmlElement* emitter = effect->FirstChildElement("emitter");

	// Go through each emitter
	while (emitter)
	{

		// Get the file
		string filePath = emitter->Attribute("file");

		// Get the vector
		glm::vec3 offSet = SplitVec3(emitter->Attribute("offset"));

		// Create the emitter
		Emitter* newEmitter = new Emitter(filePath, offSet);

		m_Emitters.push_back(newEmitter);


		emitter = emitter->NextSiblingElement("emitter");
	}
}

//-----------------------------------------------------------------------------
// Add an Emitter to the Effect
//-----------------------------------------------------------------------------
void Effect::AddEmitter(string filePathP, glm::vec3 offSetP)
{
	Emitter* newEmitter = new Emitter(filePathP, offSetP);
	m_Emitters.push_back(newEmitter);
}

//-----------------------------------------------------------------------------
// Update the effect
//-----------------------------------------------------------------------------
void Effect::Update(float deltaTimeP)
{
	// Update each of the emitters
	if (!m_Paused)
	{
		for (int i = 0; i < m_Emitters.size(); i++)
		{
			m_Emitters.at(i)->Update(deltaTimeP);
		}
	}
}

//-----------------------------------------------------------------------------
// Render the effect
//-----------------------------------------------------------------------------
void Effect::Render(glm::mat4 p_pProj, glm::mat4 p_pView)
{
	// Render each of the m_Emitter
	for (int i = 0; i < m_Emitters.size(); i++)
	{
		m_Emitters.at(i)->Render(p_pProj, p_pView);
	}
}

//-----------------------------------------------------------------------------
// Pauses the effect, just freezes every particle
//-----------------------------------------------------------------------------
void Effect::Pause()
{
	m_Paused = true;
}

//-----------------------------------------------------------------------------
// Stops the effect, only stops spawning
//-----------------------------------------------------------------------------
void Effect::Stop()
{
	m_Stopped = true;

	// Set all the emitters to stop spawning
	for (int i = 0; i < m_Emitters.size(); i++)
	{
		m_Emitters[i]->SetStop(true);
	}
}

//-----------------------------------------------------------------------------
// Resumes the effect if paused/stopped
//-----------------------------------------------------------------------------
void Effect::Play()
{
	m_Paused = false;
	m_Stopped = false;

	// Set all the emitters to resume spawning
	for (int i = 0; i < m_Emitters.size(); i++)
	{
		m_Emitters[i]->SetStop(false);
	}
}

//-----------------------------------------------------------------------------
// Sets position, updates all emitters positions
//-----------------------------------------------------------------------------
void Effect::SetPosition(glm::vec3 p_pPosition)
{
	// Updates all emitter positions
	for (int i = 0; i < m_Emitters.size(); i++)
	{
		m_Emitters[i]->UpdatePos(p_pPosition);
	}
}

//-----------------------------------------------------------------------------
// Sets Rotation, updates all emitters positions
//-----------------------------------------------------------------------------
void Effect::SetRotation(float rotationP)
{
	effectRotation = rotationP;

	// Updates all emitter positions
	for (int i = 0; i < m_Emitters.size(); i++)
	{
		m_Emitters[i]->SetRotation(rotationP);
	}
}

// Flag for delete
void Effect::FlagForDelete()
{
	// Updates all emitter positions
	for (int i = 0; i < m_Emitters.size(); i++)
	{
		m_Emitters[i]->FlagForDelete();
	}
}

// Is the effect ready to be fully deleted
bool Effect::IsReadyToDelete()
{
	for (int i = 0; i < m_Emitters.size(); i++)
	{
		if (m_Emitters[i]->IsStillActive())
			return false;
	}

	return true;
}

// Setting Target
void Effect::SetTarget(Common::GameObject* p_pTarget)
{
	m_pTarget = p_pTarget;
	hasTarget = true;

	// Free each emitter
	for (int i = 0; i < m_Emitters.size(); i++)
	{
		m_Emitters[i]->SetTarget(p_pTarget);
	}
}