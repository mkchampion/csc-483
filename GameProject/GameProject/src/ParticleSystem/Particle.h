//-----------------------------------------------------------------------------
// File:			Particle.h
// Original Author:	Matthew Champion
//
// A struct that represents a particle. Contains various properties
// and a reference to its parent emitter
//-----------------------------------------------------------------------------


#ifndef PARTICLE
#define PARTICLE

#include "W_Common.h"

class Emitter;

// Struct for the particle
struct Particle
{
	
	// Emitter that the particle belongs to
	Emitter* parentEmitter;

	// Position and velocity
	glm::vec3 position;
	glm::vec3 velocity;

	// float for the lifetime of the particle and the current age of the particle
	float lifeTimeRecip; 
	float age;

	// Scale and size value, Size acts as an initial scale, scale will act over the course of the pixels lifetim
	float size;
	float scale;

	// Color and alpha (Used for fade)
	glm::vec3 color;
	glm::vec3 startColor;
	float alpha;
	float startAlpha;

	// Pointers for the linked list
	Particle* next;
	Particle* prev;

};


#endif 