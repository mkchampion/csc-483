//-----------------------------------------------------------------------------
// File:			Emitter.h
// Original Author:	Matthew Champion
//
// A class to represent an emitter for the particle system.
//
// - Read in from a file.
// - Manages a pool of Particle structs with linked lists.
// - Spawns particles and updates them through a list of Affectors
// 
//-----------------------------------------------------------------------------

#ifndef EMITTER
#define EMITTER

#include <vector>
#include "W_Common.h"
#include "W_MaterialManager.h"
#include "W_BufferManager.h"
#include "W_VertexDeclaration.h"
#include "W_ProgramManager.h"
#include "W_TextureManager.h"
#include "W_Types.h"
#include "ParticleUtilities.h"
#include <tinyxml.h>
#include "Particle.h"
#include "GameProject\GameProject\src\GameObject.h"

// Forward declare Affector
class Affector;

using namespace std;

// Enum for emitter spawn modes
enum SpawnMode
{
	CONTINOUS = 0,
	BURST,	
};

// Enum for spawn property types
enum SpawnPropertyType
{
	CONSTANT = 0,
	RANDOM,
};

// Enum for spawning area
enum SpawnArea
{
	POINTSPAWN = 0,
	BOX,
	SPHERE,
};

// Struct for the particleVertex
struct pVertex
{
	GLfloat x,y,z;
	GLfloat r,g,b,a;
	GLfloat u,v;
};


// Class for the emitter
class Emitter
{

public:

	// Accept the path for the emitter file, and the offset
	Emitter(string filePathP, glm::vec3 offsetP);
	~Emitter();

	// Update/Render methods
	void Update(float deltaTime);
	void Render(glm::mat4 p_pProj, glm::mat4 p_pView);

	// Method to kill a particle (Needs to be public so particles can kill themselves through the affector)
	void KillParticle(Particle*);

	// Method to toggle spawning, lets the effect turn off spawning with stop
	void SetStop(bool p_bStopped);

	// Flag for deletion
	void FlagForDelete() { m_flaggedForDelete = true; }
	bool IsStillActive();

	// Method to set the position (Based on updates from Effect)
	void UpdatePos(glm::vec3 p_vPosition);

	// Set the rotation
	void SetRotation(const float rotationP) { rotationY = rotationP; }

	// Target for the velocity changes?
	void SetTarget(Common::GameObject* p_pTarget) { m_pTarget = p_pTarget; hasTarget = true; }


private:
	
	// List of affectors to apply to each particle
	vector<Affector*> m_Affectors;

	// Reading the file to get data
	void ReadFile(string pathP);

	// Helper methods to read each segment (Easier to read)
	void ReadEmitterProperties(TiXmlElement* p_pElement);
	void ReadSpawnInformation(TiXmlElement* p_pElement);
	void ReadSpawnProperties(TiXmlElement* p_pElement);
	void ReadAffectors(TiXmlElement* p_pElement);

	// Method to setup the buffer
	void SetupBuffer();
	
	// bool to determine if stopped by Effect
	bool m_Stopped;

	// Bool for whether it should delete?
	bool m_flaggedForDelete = false;


	// Methods and variables for management of the particle list
	// ===========================================

	// Spawning a particle
	void SpawnParticle();	

	// Private methods for segmented spawning of different areas
	void UpdateSpawning(float deltaTime);
	void UpdateActive(float deltaTime);


	// Removing from an active list, and Retrieving a free one
	void RemoveFromActive(Particle*);
	Particle* GetFreeParticle();

	// Adding to each pool
	void AddToActive(Particle*);
	void AddToPool(Particle*);

	// References to active list head/tail and the free list
	Particle* pActiveListHead;
	Particle* pActiveListTail;

	Particle* pFreeList;

	// Vector of particles that belong to the emitter
	Particle** particles;

	// =============================================

		

	// ============================================
	// Emitter Properties
	// =============================================

	// Offset taken from the effect
	glm::vec3 m_OffSet;
	glm::vec3 m_Position;

	// Rotation on Y axis
	float rotationY;

	// Given name
	string m_Name;

	int m_PoolCount;
	float m_LifeTime;
	float m_Age;

	// SpawnMode that determines burst/Continous
	SpawnMode m_SpawnMode;

	// Continous Specific
	float m_BirthRate;
	float m_ToSpawn_accumulator;

	// Burst Specific
	float m_TimeToBurst;
	float m_BurstTime;
	int m_ParticlePerBurst;
	bool m_RepeatBurst;
	bool m_bShouldContinueBurst;

	float m_BurstTimeMin;
	float m_BurstTimeMax;
	SpawnPropertyType m_BurstMode;

	bool m_PointBurst;
	SpawnArea m_PointBurstMode;

	// Calcuclating the burst time
	void CalcBurstTime();
	

	// Spawning location / type
	glm::vec3 m_PointSpawn;
	glm::vec3 m_BoxMin;
	glm::vec3 m_BoxMax;

	float m_SphereRadius;

	SpawnArea m_SpawnArea;

	// Texture name and path
	string m_TextureName;
	string m_TexturePath;

	// BlendingModes and Type
	bool m_BlendEnabled;
	wolf::BlendEquation m_BlendEquation;
	wolf::BlendMode m_BlendSource;
	wolf::BlendMode m_BlendDest;

	// =================================================




	// ==============================================================
	// Spawning Properties and their types (Include a min/max/constant)
	// ===============================================================
		
	// The method that sets these properties for each particle when spawning
	void SetParticleDefaults(Particle* p);


	// Lifetime
	float m_LifeTimeConst;
	float m_LifeTimeMin;
	float m_LifeTimeMax;

	SpawnPropertyType m_LifeTimeSpawnType;


	// Velocity
	glm::vec3 m_VelocityConst;
	glm::vec3 m_VelocityMin;
	glm::vec3 m_VelocityMax;

	// Velocities in float if using specific type
	float m_floatVelocityMin;
	float m_floatVelocityMax;
	bool m_bRandomDirectionalVelocity;

	SpawnPropertyType m_VelocitySpawnType;


	// Position (Position gets an extra spawning enum)
	glm::vec3 m_PositionConst;
	glm::vec3 m_PositionMin;
	glm::vec3 m_PositionMax;

	SpawnPropertyType m_PositionSpawnType;


	// Size (I'm assuming this is a uniform scale for now)
	float m_SizeConst;
	float m_SizeMin;
	float m_SizeMax;

	SpawnPropertyType m_SizeSpawnType;


	// Scale (Used along with size to determine final scale)
	float m_ScaleStart;


	// Color
	glm::vec3 m_ColorConst;
	glm::vec3 m_ColorMin;
	glm::vec3 m_ColorMax;

	SpawnPropertyType m_ColorSpawnType;


	// Fade (Accepted in the color parameter in the xml file
	float m_FadeConst;
	float m_FadeMin;
	float m_FadeMax;

	SpawnPropertyType m_FadeSpawnType;

	// ===========================================================

	
	// Rendering information
	// ===================================
		
	// Array of vertices to render
	pVertex* particleBuffer;

	// Material to use and VBO
	wolf::Material* p_Material;
	wolf::VertexBuffer* p_vertexBuffer;
	wolf::VertexDeclaration* p_vDeclaration;
	wolf::Program* p_Program2;

	// ====================================


	// Target information for velocity reasons?
	// ====================================
	Common::GameObject* m_pTarget;
	bool hasTarget = false;
	// ====================================

	


};


#endif