//-----------------------------------------------------------------------------
// File:			ParticleUtilities.h
// Original Author:	Matthew Champion
//
// Some methods that I thought may be used in a couple of places across the entire system.
//-----------------------------------------------------------------------------

#ifndef PARTICLE_UTILITIES
#define PARTICLE_UTILITIES

#include <random>
#include "W_Common.h"
#include <string>
#include <sstream>

using namespace std;

// Methods for randomizing in a range of float or vector
float RandomInRange(float, float);
glm::vec3 RandomInRange(glm::vec3, glm::vec3);

// Method to split a comma delimited string into a vec3
glm::vec3 SplitVec3(string vecNumbersP);

// Retrieves blend equation  from string
wolf::BlendEquation BlendEquationFromString(string blendEquationP);

// Retrieves a given blend mode from a string
wolf::BlendMode BlendModeFromString(string blendModeP);

#endif