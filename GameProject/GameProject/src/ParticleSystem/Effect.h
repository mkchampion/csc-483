//-----------------------------------------------------------------------------
// File:			Effect.h
// Original Author:	Matthew Champion
//
// A class to represent an Effect for the particle system
//
// - Read in from a file.
// - Creates emitters based on file and updates/renders them
//-----------------------------------------------------------------------------

#ifndef EFFECT
#define EFFECT


#include "W_Common.h"
#include "W_Types.h"
#include <vector>
#include <sstream>
#include "ParticleUtilities.h"
#include <tinyxml.h>
#include "Emitter.h"
#include "GameProject\GameProject\src\GameObject.h"

class Effect
{

public:
	
	// Constructor
	Effect(string filePathP);
	~Effect();

	// Update/Render
	void Update(float deltaTime);
	void Render(glm::mat4 p_pProj, glm::mat4 p_pViewP);

	// Ability to add an emitter to the effect
	void AddEmitter(string filePathP, glm::vec3 offSetP);

	// Method to load effect
	void LoadEffect(string filePathP);

	// Setting the position
	void SetPosition(glm::vec3);
	void SetRotation(float rotation);
	float GetEffectRotation() const { return effectRotation; }

	// Controlling the effect
	void Play();
	void Stop();
	void Pause();

	// Flag the effect for deletion
	void FlagForDelete();
	bool IsReadyToDelete();

	// Give a target
	void SetTarget(Common::GameObject* p_pTarget);

private:

	// The transform to place the effect
	glm::vec3 m_Position;

	float effectRotation;

	// A vector of emitters
	std::vector<Emitter*> m_Emitters;

	// Bool to check if its paused or stopped
	bool m_Paused;
	bool m_Stopped;

	// GameObject to get velocities from
	Common::GameObject* m_pTarget;
	bool hasTarget = false;

};


#endif