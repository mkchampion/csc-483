//-----------------------------------------------------------------------------
// File:			FadeAffector.cpp
// Original Author:	Matthew Champion
//-----------------------------------------------------------------------------

#include "FadeAffector.h"

//-----------------------------------------------------------------------------
// Constructor accepts a speed, end value, and mode
//-----------------------------------------------------------------------------
FadeAffector::FadeAffector(float fadeSpeedP, float fadeEndP, AffectorMode fadeModeP) : Affector()
{
	// Accept parameters
	m_fadeSpeed = fadeSpeedP;
	m_fadeEnd = fadeEndP;

	m_fadeMode = fadeModeP;

}

FadeAffector::~FadeAffector()
{
}

//-----------------------------------------------------------------------------
// Apply and fade over time
//-----------------------------------------------------------------------------
void FadeAffector::Apply(float deltaTimeP, Particle* p)
{

	// Updates with parent
	if (updateWithParent)
		Affector::Apply(deltaTimeP, p);

	// -----------------------------------------------------------------------------------
	// Note on fading
	// Chose to implement it in such a way that the particle must know its starting value.
	// This lets me fade out/in linearly over time using the range between end/start.
	// I also chose to do it in such a way that the fade over time is based on a fully opaque/transparent particle.
	// This means if you specified an end value of 0.0, and a starting range of 1.0 - 0.8, the values starting at 0.8 would reach 0.0 sooner.
	// I believe this would look more appropriate for most effects.
	//
	//
	// The other option would be to have the alpha determined by
	// p->alpha = p->startAlpha - (p->age) * (m_fadeEnd - p->startAlpha);
	// p->alpha = p->startAlpha + (p->age) * (m_fadeEnd - p->startAlpha);
	// --------------------------------------------------------------------------------------

	// Check the mode type
	if (m_fadeMode == OVER_LIFE)
	{

		// Always fade towards the given end value
		if (p->startAlpha > m_fadeEnd)
		{
			// Update,
			p->alpha = (1.0 - p->age) - (1.0 - p->startAlpha);

			// Make sure it doesn't go over
			if (p->alpha < m_fadeEnd)
				p->alpha = m_fadeEnd;
		}

		else 
		{
			// Update
			p->alpha = p->age + p->startAlpha;

			// Make sure it doesn't go over
			if (p->alpha > m_fadeEnd)
				p->alpha = m_fadeEnd;
		}

	}

	// Fade with a constant speed
	else if (m_fadeMode == CONSTANT_SPEED)
	{
		// Fading out
		if (p->startAlpha > m_fadeEnd)
		{
			p->alpha -= m_fadeSpeed * deltaTimeP;

			if (p->alpha < m_fadeEnd)
				p->alpha = m_fadeEnd;
		}

		// Fading in
		else if (p->startAlpha < m_fadeEnd)
		{
			p->alpha += m_fadeSpeed * deltaTimeP;

			if (p->alpha > m_fadeEnd)
				p->alpha = m_fadeEnd;
		}

	}

}