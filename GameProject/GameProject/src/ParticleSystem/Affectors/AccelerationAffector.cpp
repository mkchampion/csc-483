//-----------------------------------------------------------------------------
// File:			AccelerationAffect.cpp
// Original Author:	Matthew Champion
//-----------------------------------------------------------------------------

#include "AccelerationAffector.h"

//-----------------------------------------------------------------------------
// Constructor accepts an ending color to change to
//-----------------------------------------------------------------------------
AccelerationAffector::AccelerationAffector(glm::vec3 endVelocityP, float changeSpeedP, AffectorMode affectorModeP) : Affector()
{
	m_endVelocity = endVelocityP;
	m_affectorMode = affectorModeP;
	m_changeSpeed = changeSpeedP;
}

//-----------------------------------------------------------------------------
// Transforms a particle to a given color over time
//-----------------------------------------------------------------------------
void AccelerationAffector::Apply(float deltaTimeP, Particle* p)
{

	// Works similar to the fadeAffector, takes the starting color of the particle and goes towards the ending particle

	// Update with parent
	if (updateWithParent)
		Affector::Apply(deltaTimeP, p);


	if (m_affectorMode == CONSTANT_SPEED)
	{
		// First handle the x velocity
		if (p->velocity.x < m_endVelocity.x)
		{
			// Update
			p->velocity.x += m_changeSpeed * deltaTimeP;

			// Make sure it doesn't go over
			if (p->velocity.x > m_endVelocity.x)
				p->velocity.x = m_endVelocity.x;
		}
		else
		{
			// Update
			p->velocity.x -= m_changeSpeed * deltaTimeP;

			// Make sure it doesn't go over
			if (p->velocity.x < m_endVelocity.x)
				p->velocity.x = m_endVelocity.x;
		}

		// Y velocity
		if (p->velocity.y < m_endVelocity.y)
		{
			// Update
			p->velocity.y += m_changeSpeed * deltaTimeP;

			// Make sure it doesn't go over
			if (p->velocity.y > m_endVelocity.y)
				p->velocity.y = m_endVelocity.y;
		}
		else
		{
			// Update
			p->velocity.y -= m_changeSpeed * deltaTimeP;

			// Make sure it doesn't go over
			if (p->velocity.y < m_endVelocity.y)
				p->velocity.y = m_endVelocity.y;
		}


		// Z Velocity
		if (p->velocity.z < m_endVelocity.z)
		{
			// Update
			p->velocity.z += m_changeSpeed * deltaTimeP;

			// Make sure it doesn't go over
			if (p->velocity.z > m_endVelocity.z)
				p->velocity.z = m_endVelocity.z;
		}
		else
		{
			// Update
			p->velocity.z -= m_changeSpeed * deltaTimeP;

			// Make sure it doesn't go over
			if (p->velocity.z < m_endVelocity.z)
				p->velocity.z = m_endVelocity.z;
		}
	}

}