//-----------------------------------------------------------------------------
// File:			AddVelAffector.cpp
// Original Author:	Matthew Champion
//-----------------------------------------------------------------------------

#include "AddVelAffector.h"



//-----------------------------------------------------------------------------
// Update the position of a particle based on its velocity
// Will also kill a particle if its lifetime is up, due to base apply
//-----------------------------------------------------------------------------
void AddVelAffector::Apply(float deltaTimeP, Particle* p)
{

	// Update the lifetime with the parent
	if (updateWithParent)
		Affector::Apply(deltaTimeP, p);

	// Tell the particle to update its position with its velocity
	p->position += p->velocity;

}