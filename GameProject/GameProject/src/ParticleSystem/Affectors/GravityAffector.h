//-----------------------------------------------------------------------------
// File:			GravityAffector.h
// Original Author:	Matthew Champion
//
// An affector to add the particles velocity and change its position over time
//-----------------------------------------------------------------------------

#ifndef GRAVITY_AFFECTOR
#define GRAVITY_AFFECTOR

#include "Affector.h"

class GravityAffector: public Affector
{

public:

	GravityAffector(float p_fGravity);
	void Apply(float deltaTime, Particle* p);

private:
	
	float m_Gravity;

};



#endif