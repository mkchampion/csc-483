//-----------------------------------------------------------------------------
// File:			ColorAffector.cpp
// Original Author:	Matthew Champion
//-----------------------------------------------------------------------------

#include "ColorAffector.h"

//-----------------------------------------------------------------------------
// Constructor accepts an ending color to change to
//-----------------------------------------------------------------------------
ColorAffector::ColorAffector(glm::vec3 endColorP, float changeSpeedP, AffectorMode affectorModeP) : Affector()
{
	m_endColor = endColorP;
	m_affectorMode = affectorModeP;
	m_changeSpeed = changeSpeedP;
}

//-----------------------------------------------------------------------------
// Transforms a particle to a given color over time
//-----------------------------------------------------------------------------
void ColorAffector::Apply(float deltaTimeP, Particle* p)
{
	
	// Works similar to the fadeAffector, takes the starting color of the particle and goes towards the ending particle
	
	// Update with parent
	if (updateWithParent)
		Affector::Apply(deltaTimeP, p);


	if (m_affectorMode == OVER_LIFE)
	{
		// First handle the red value
		if (p->startColor.r < m_endColor.r)
		{
			// Update
			p->color.r = p->age + p->startColor.r;

			// Make sure it doesn't go over
			if (p->color.r > m_endColor.r)
				p->color.r = m_endColor.r;
		}
		else
		{
			p->color.r = (1.0 - p->age) - (1.0 - p->startColor.r);

			// Make sure it doesn't go over
			if (p->color.r < m_endColor.r)
				p->color.r = m_endColor.r;
		}

		// Green value now
		if (p->startColor.g < m_endColor.g)
		{
			p->color.g = p->age + p->startColor.g;

			if (p->color.g > m_endColor.g)
				p->color.g = m_endColor.g;
		}
		else if (p->startColor.g > m_endColor.g)
		{
			p->color.g = (1.0 - p->age) - (1.0 - p->startColor.g);

			if (p->color.g < m_endColor.g)
				p->color.g = m_endColor.g;
		}

		// Blue value now
		if (p->startColor.b < m_endColor.b)
		{
			p->color.b = p->age + p->startColor.b;

			if (p->color.b > m_endColor.b)
				p->color.b = m_endColor.b;

		}
		else if (p->startColor.b > m_endColor.b)
		{
			p->color.b = (1.0 - p->age) - (1.0 - p->startColor.b);

			if (p->color.b < m_endColor.b)
				p->color.b = m_endColor.b;
		}
	}

	else if (m_affectorMode == CONSTANT_SPEED)
	{
		// First handle the red value
		if (p->startColor.r < m_endColor.r)
		{
			// Update
			p->color.r += m_changeSpeed * deltaTimeP;

			// Make sure it doesn't go over
			if (p->color.r > m_endColor.r)
				p->color.r = m_endColor.r;
		}
		else
		{
			p->color.r -= m_changeSpeed * deltaTimeP;;

			// Make sure it doesn't go over
			if (p->color.r < m_endColor.r)
				p->color.r = m_endColor.r;
		}

		// Green value now
		if (p->startColor.g < m_endColor.g)
		{
			p->color.g += m_changeSpeed * deltaTimeP;;

			if (p->color.g > m_endColor.g)
				p->color.g = m_endColor.g;
		}
		else if (p->startColor.g > m_endColor.g)
		{
			p->color.g -= m_changeSpeed * deltaTimeP;;

			if (p->color.g < m_endColor.g)
				p->color.g = m_endColor.g;
		}

		// Blue value now
		if (p->startColor.b < m_endColor.b)
		{
			p->color.b += m_changeSpeed * deltaTimeP;;

			if (p->color.b > m_endColor.b)
				p->color.b = m_endColor.b;

		}
		else if (p->startColor.b > m_endColor.b)
		{
			p->color.b -= m_changeSpeed * deltaTimeP;;

			if (p->color.b < m_endColor.b)
				p->color.b = m_endColor.b;
		}
	}

}