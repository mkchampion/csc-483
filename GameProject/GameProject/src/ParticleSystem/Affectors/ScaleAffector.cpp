//-----------------------------------------------------------------------------
// File:			ScaleAffector.cpp
// Original Author:	Matthew Champion
//-----------------------------------------------------------------------------

#include "ScaleAffector.h"

//-----------------------------------------------------------------------------
// Constructor accepts scale speed, starting scale, end scale, and scale mode
//-----------------------------------------------------------------------------
ScaleAffector::ScaleAffector(float scaleSpeedP, float scaleStartP, float scaleEndP, AffectorMode scaleModeP) : Affector()
{

	// Accept parameters
	m_scaleSpeed = scaleSpeedP;
	m_scaleStart = scaleStartP;
	m_scaleEnd = scaleEndP;

	m_scaleMode = scaleModeP;

}

//-----------------------------------------------------------------------------
// Alters the scale over time
//-----------------------------------------------------------------------------
void ScaleAffector::Apply(float deltaTimeP, Particle* p)
{
	// Update with parent if this one is responsible
	if (updateWithParent)
		Affector::Apply(deltaTimeP, p);

	// Check mode type
	if (m_scaleMode == OVER_LIFE)
	{		
		p->scale = m_scaleStart + p->age * (m_scaleEnd - m_scaleStart);
	}
	else if (m_scaleMode == CONSTANT_SPEED)
	{
		// Add based on speed
		p->scale = m_scaleStart + m_scaleSpeed * deltaTimeP;

		// Make sure it doesn't go past end values
		if ((m_scaleStart > m_scaleEnd && p->scale < m_scaleEnd) || (m_scaleStart < m_scaleEnd && p->scale > m_scaleEnd))
			p->scale = m_scaleEnd;

	}





}