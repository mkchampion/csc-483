#ifndef ACCELERATION_AFFECTOR
#define  ACCELERATION_AFFECTOR

#include "Affector.h"

class AccelerationAffector : public Affector
{

public:

	AccelerationAffector(glm::vec3 endVelocityP, float changeSpeedP, AffectorMode affectorModeP);
	void Apply(float deltaTime, Particle* p);

private:

	glm::vec3 m_endVelocity;
	float m_changeSpeed;

	AffectorMode m_affectorMode;

};

#endif