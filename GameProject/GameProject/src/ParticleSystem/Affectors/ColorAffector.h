#ifndef COLOR_AFFECTOR
#define  COLOR_AFFECTOR

#include "Affector.h"

class ColorAffector: public Affector
{

public:

	ColorAffector(glm::vec3 endColorP, float changeSpeedP, AffectorMode affectorModeP);
	void Apply(float deltaTime, Particle* p);

private:

	glm::vec3 m_endColor;
	glm::vec3 m_startColor;
	float m_changeSpeed;

	AffectorMode m_affectorMode;


};

#endif