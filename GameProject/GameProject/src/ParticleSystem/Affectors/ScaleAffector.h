//-----------------------------------------------------------------------------
// File:			ScaleAffector.h
// Original Author:	Matthew Champion
//
// An affector to scale the particle between two scale values
//-----------------------------------------------------------------------------

#ifndef SCALE_AFFECTOR
#define SCALE_AFFECTOR

#include "Affector.h"

class ScaleAffector: public Affector
{


public:

	ScaleAffector(float scaleSpeedP, float scaleStartP, float scaleEndP, AffectorMode scaleModeP);
	void Apply(float deltaTime, Particle* p);
	
private:

	// Scale values to go across
	float m_scaleSpeed;
	float m_scaleStart;
	float m_scaleEnd;

	// Mode for scaling
	AffectorMode m_scaleMode;


};


#endif