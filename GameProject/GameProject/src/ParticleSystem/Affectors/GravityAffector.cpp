//-----------------------------------------------------------------------------
// File:			GravityAffector.cpp
// Original Author:	Matthew Champion
//-----------------------------------------------------------------------------

#include "GravityAffector.h"


//-----------------------------------------------------------------------------
// Constructor accepts a gravity value
//-----------------------------------------------------------------------------
GravityAffector::GravityAffector(float p_fGravity) : Affector()
{
	m_Gravity = p_fGravity;
}

//-----------------------------------------------------------------------------
// Changes the velocity of the affector based on input value
//-----------------------------------------------------------------------------
void GravityAffector::Apply(float deltaTimeP, Particle* p)
{
	// Updates if this affector is responsible
	if (updateWithParent)
		Affector::Apply(deltaTimeP, p);

	// Tell the particle to update its position with its velocity
	p->velocity.y -= m_Gravity;

}