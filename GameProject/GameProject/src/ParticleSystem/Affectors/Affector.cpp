//-----------------------------------------------------------------------------
// File:			Affector.cpp
// Original Author:	Matthew Champion
//-----------------------------------------------------------------------------

#include "Affector.h"


#include "GameProject\GameProject\src\ParticleSystem\Emitter.h"

//-----------------------------------------------------------------------------
// Constructor, nothing to set up
//-----------------------------------------------------------------------------
Affector::Affector()
{
	updateWithParent = false;
}

//-----------------------------------------------------------------------------
// Destructor
//-----------------------------------------------------------------------------
Affector::~Affector()
{
}

//-----------------------------------------------------------------------------
// Update a given particle based on the time
// This apply updates the age of the particle, and kills it if its past its lifetime
//-----------------------------------------------------------------------------
void Affector::Apply(float deltaTime, Particle* p)
{

	// Add to the age of the particle, and if its greater then the specified lifetime, tell its emitter to kill it
	p->age += deltaTime * p->lifeTimeRecip;

	if (p->age >= 1.0)
	{
		p->parentEmitter->KillParticle(p);
	}

}

//-----------------------------------------------------------------------------
// Ensures one affector updates with its parent
//-----------------------------------------------------------------------------
void Affector::UpdateWithParent()
{
	updateWithParent = true;
}