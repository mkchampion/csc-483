//-----------------------------------------------------------------------------
// File:			Affector.h
// Original Author:	Matthew Champion
//
// A class to represent the Affectors in a particle system
//
// - Has two modes that can be used,
//		- Over the life time of the particle
//		- Constant speed
// - Updates the particle in some way described by the child class
//-----------------------------------------------------------------------------

#ifndef AFFECTOR
#define AFFECTOR

#include"../Particle.h"


// Enum to determine if the affector should apply over the life time, or a constant speed
enum AffectorMode
{
	OVER_LIFE = 0,
	CONSTANT_SPEED,
};


class Affector
{

public:
	
	// Constructor
	Affector();
	~Affector();

	// Virtual apply method to be overridden
	virtual void Apply(float deltaTime, Particle* p) = 0;

	// Method to tell this affector to make sure to update with the parent (Only 1 should)
	void UpdateWithParent();

protected:

	// A boolean telling this affector to update via parent
	bool updateWithParent;

};



#endif