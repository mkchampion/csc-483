//-----------------------------------------------------------------------------
// File:			FadeAffector.h
// Original Author:	Matthew Champion
//
// An affector to fade in/out the particle over its lifetime
//-----------------------------------------------------------------------------

#ifndef FADE_AFFECTOR
#define FADE_AFFECTOR

#include "Affector.h"

class FadeAffector: public Affector
{

public:

	FadeAffector(float fadeSpeedP, float fadeEndP, AffectorMode fadeModeP);
	~FadeAffector();

	void Apply(float deltaTime, Particle* p);


private:

	// Fade values to go across
	float m_fadeSpeed;
	float m_fadeEnd;

	// Mode for the fading
	AffectorMode m_fadeMode;


};

#endif