//-----------------------------------------------------------------------------
// File:			AddVelAffector.h
// Original Author:	Matthew Champion
//
// An affector to add the particles velocity and change its position over time
//-----------------------------------------------------------------------------

#ifndef ADDVELAFFECTOR
#define ADDVELAFFECTOR

#include "Affector.h"

class AddVelAffector: public Affector
{

public:

	AddVelAffector():Affector(){};
	void Apply(float deltaTime, Particle* p);

};



#endif