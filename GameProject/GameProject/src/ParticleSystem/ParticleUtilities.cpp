//-----------------------------------------------------------------------------
// File:			ParticleUtilities.cpp
// Original Author:	Matthew Champion
//-----------------------------------------------------------------------------

#include "ParticleUtilities.h"

//-----------------------------------------------------------------------------
// Return a float within a range
//-----------------------------------------------------------------------------
float RandomInRange(float numMin, float numMax)
{
	return numMin + (float)rand()/(float)RAND_MAX * (numMax-numMin);	
}

//-----------------------------------------------------------------------------
// Return a vec3 within a range
//-----------------------------------------------------------------------------
glm::vec3 RandomInRange(glm::vec3 vecMin, glm::vec3 vecMax)
{

	// Get the new x value 
	float newX = RandomInRange((float)vecMin.x, (float)vecMax.x);
	float newY = RandomInRange((float)vecMin.y, (float)vecMax.y);
	float newZ = RandomInRange((float)vecMin.z, (float)vecMax.z);

	return glm::vec3(newX,newY,newZ);

}

//-----------------------------------------------------------------------------
// Splits a vector given by comma delimited string
//-----------------------------------------------------------------------------
glm::vec3 SplitVec3(string vecNumbersP)
{
		// Vector for the three values
	vector<string> numbers;

	// Set up a stream for the vector
	stringstream ss(vecNumbersP);

	// Add it to the vector, which will be converted
	while (ss)
	{
		string value;
		getline(ss,value,',');
		numbers.push_back(value);
	}

	// new numbers
	float x = atof(numbers[0].c_str());
	float y = atof(numbers[1].c_str());
	float z = atof(numbers[2].c_str());

	// Constructing and returning the vec
	glm::vec3 newVec = glm::vec3(x,y,z);
	return newVec;
}

//-----------------------------------------------------------------------------
// Retrieves a given blending equation from a string
//-----------------------------------------------------------------------------
wolf::BlendEquation BlendEquationFromString(string blendEquationP)
{
	// Checks each mode and returns a BlendMode, defaults to BM_One

	if (blendEquationP.compare("BE_Add") == 0)
		return wolf::BlendEquation::BE_Add;
	else if (blendEquationP.compare("BE_Subtract") == 0)
		return wolf::BlendEquation::BE_Subtract;
	else if (blendEquationP.compare("BE_ReverseSubtract") == 0)
		return wolf::BlendEquation::BE_ReverseSubtract;
	else 
		return wolf::BlendEquation::BE_Add;
	

}

//-----------------------------------------------------------------------------
// Retrieves a given blending mode from a string
//-----------------------------------------------------------------------------
wolf::BlendMode BlendModeFromString(string blendModeP)
{
	// Checks each mode and returns a BlendMode, defaults to BM_One

	if (blendModeP.compare("BM_SrcAlpha") == 0)
		return wolf::BlendMode::BM_SrcAlpha;
	else if (blendModeP.compare("BM_One") == 0)
		return wolf::BlendMode::BM_One;
	else if (blendModeP.compare("BM_SrcColor") == 0)
		return wolf::BlendMode::BM_SrcColor;
	else if (blendModeP.compare("BM_OneMinusSrcColor") == 0)
		return wolf::BlendMode::BM_OneMinusSrcColor;
	else if (blendModeP.compare("BM_OneMinusSrcAlpha") == 0)
		return wolf::BlendMode::BM_OneMinusSrcAlpha;
	else if (blendModeP.compare("BM_DstAlpha") == 0)
		return wolf::BlendMode::BM_DstAlpha;
	else if (blendModeP.compare("BM_OneMinusDstAlpha") == 0)
		return wolf::BlendMode::BM_OneMinusDstAlpha;
	else if (blendModeP.compare("BM_DstColor") == 0)
		return wolf::BlendMode::BM_DstColor;
	else if (blendModeP.compare("BM_OneMinusDstColor") == 0)
		return wolf::BlendMode::BM_OneMinusDstColor;
	else if (blendModeP.compare("BM_Zero") == 0)
		return wolf::BlendMode::BM_Zero;
	else if (blendModeP.compare("BM_SrcAlpha") == 0)
		return wolf::BlendMode::BM_SrcAlpha;
	else
		return wolf::BlendMode::BM_One;
	

}