//------------------------------------------------------------------------
// EventDelegate
//	
// This class acts a container for a function delegate with some additional info for comparison
//------------------------------------------------------------------------

#ifndef EVENT_DELEGATE_H
#define EVENT_DELEGATE_H

#include <functional>
#include <memory>
#include "IEventData.h"


class EventDelegate
{

public:

	typedef std::shared_ptr<IEventData> EventDataPointer;
	typedef std::tr1::function<void(const EventDataPointer& p_pEventData)> FunctionDelegate;

	// Constructor/Destructor
	EventDelegate(const std::string& p_strEventHandler, const EventType& p_eEventType, const FunctionDelegate& p_pFunctionDelegate);
	~EventDelegate() {};

	// Accesors if necessary
	std::string GetHandler() const { return m_strEventHandler; }
	EventType GetType() const { return m_eEventType; }
	FunctionDelegate GetDelegate() const { return m_pFunctionDelegate; }

	// Equal check (== was being funny, so I'm just going to use this)
	bool IsEqual(const EventDelegate* compareDelegate) const;

private:

	// Members
	std::string m_strEventHandler;
	EventType m_eEventType;
	FunctionDelegate m_pFunctionDelegate;


};

#endif
