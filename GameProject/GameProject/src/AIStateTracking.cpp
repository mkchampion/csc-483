//------------------------------------------------------------------------
// AIStateTracking
//
// State for the AI to be rotating / facing the player or something of that sort
//------------------------------------------------------------------------

#include "AIStateTracking.h"
#include "ComponentAIController.h"
#include "ComponentRenderableMesh.h"
#include <glm/gtx/vector_angle.hpp>
#include "ComponentProjectile.h"
#include "ComponentWeapon.h"

// Constructor
AIStateTracking::AIStateTracking()
{
	fireRate = 1.5f;
	fireTimer = 0.0f;
}

// Destructor
AIStateTracking::~AIStateTracking()
{
}

// Enter
void AIStateTracking::Enter(ActionData* p_stateData)
{
	ComponentAIController* controller = static_cast<ComponentAIController*>(m_pStateMachine->GetStateMachineOwner());
	Common::GameObject* gameObject = controller->GetGameObject();

	m_pTarget = gameObject->GetManager()->GetGameObject(std::string(p_stateData->trackingData.targetID));

	hasMeshRotation = p_stateData->trackingData.hasMeshRotation;
	if (hasMeshRotation)
	{
		meshNum = p_stateData->trackingData.meshNum;
	}
}

// Update
void AIStateTracking::Update(float p_fDelta)
{

	// Getting AI Component / GO
	ComponentAIController* controller = static_cast<ComponentAIController*>(m_pStateMachine->GetStateMachineOwner());
	Common::GameObject* gameObject = controller->GetGameObject();
	
	Common::Transform& transform = gameObject->GetTransform();
	Common::Transform& targetTransform = m_pTarget->GetTransform();
	
	// Adjust the target position to be on the same y to prevent jumps during tracking
	glm::vec3 targetPos = targetTransform.GetTranslation();
	targetPos.y = transform.GetTranslation().y;

	glm::vec3 vDiff = targetPos - transform.GetTranslation();
	vDiff = glm::normalize(vDiff);

	if (hasMeshRotation)
	{
		// Rotate facing direction	
		float fAngle = glm::orientedAngle(glm::vec3(0.0f, 0.0f, 1.0f), vDiff, glm::vec3(0.0f, 1.0f, 0.0f));

		// Adjusting the mesh to face the target
		ComponentRenderableMesh* mesh = static_cast<ComponentRenderableMesh*>(gameObject->GetComponent("GOC_Renderable"));
		mesh->SetMeshRotation(meshNum, false, 0.0f, fAngle, glm::vec3(0.0f, 1.0f, 0.0f));
	}

	// Grab the weapon component if it exists
	ComponentWeapon* weapon = static_cast<ComponentWeapon*>(gameObject->GetComponent("GOC_Weapon"));
	IWeapon* activeWeapon = weapon->GetActiveWeapon();

	activeWeapon->Update(p_fDelta, gameObject, glm::vec2(vDiff.x, -vDiff.z));

}

// Exit
void AIStateTracking::Exit()
{
}


