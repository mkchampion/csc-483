//------------------------------------------------------------------------
// States
//
// Created:	2013/02/10
// Author:	Carel Boers
//	
// State enumerations for our example game.
//------------------------------------------------------------------------

#ifndef STATES_H
#define STATES_H

enum States
{
	eStateGameplay_Paused = 1,
	eStateGameplay_Gameplay
};

#endif // STATES_H