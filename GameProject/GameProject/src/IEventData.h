//------------------------------------------------------------------------
// IEventData.h
//
// Interface for events
//
// Based off of the IEventData from Game Coding Complete but with some components removed
//------------------------------------------------------------------------

#ifndef EVENTDATA_INTERFACE_H
#define EVENTDATA_INTERFACE_H

#include <string>


// EventType typedef for the GUID
typedef unsigned long EventType;
	
class IEventData
{

public:
	// Interface methods

	// Retrieve the event type
	virtual const EventType& GetEventType() const = 0;

	// Retrieve time stamp for when the event occurs
	virtual float GetTimeStamp() const = 0;

};

#endif

