//-----------------------------------------------------------------------------
// ConeWeapon.h
//
//  Weapon that acts as a cone (Effectivly, a flame thrower weapon)
//-----------------------------------------------------------------------------

#ifndef CONEWEAPON_H
#define CONEWEAPON_H

#include "IWeapon.h"
#include <string>
#include "ParticleSystem\Effect.h"
#include "CollisionPoints.h"

class ConeWeapon : IWeapon
{

public:

	// Read weapon from file
	ConeWeapon(const std::string& weaponPathP);
	~ConeWeapon();

	void Update(float p_fDelta, Common::GameObject* ownerP, const glm::vec2& dirP);
	void StopWeapon();

	void SetTarget(Common::GameObject* p_pTarget);

private:
	
	// Effect to use
	Effect* m_pParticleEffect;
	bool effectStarted = false;
	glm::vec3 lastDir = glm::vec3(0.0f, 0.0f, -1.0f);

	CollisionPoints m_pBoundries;

	// Position effect is located at
	glm::vec3 m_vEffectPos;

	std::string soundName = "";

	// dps
	float damage;

	// Collision Points
	CollisionPoints m_collisionPoints;

};

#endif