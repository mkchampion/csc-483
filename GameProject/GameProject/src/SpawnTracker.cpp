//------------------------------------------------------------------------
// SpawnTracker
//
// Created:	2015/10/26
// Author:	Matthew Champion
//	
// A simple object to contain information about which objects should spawn
//------------------------------------------------------------------------

#include "SpawnTracker.h"
#include "HeliGame.h"
#include "ComponentCamera.h"
#include "ComponentRenderableMesh.h"
#include "SoundManager.h"

// Function to compare TMXObjectInfo for spawn positions
bool SortPosition(TMXObjectInfo* i, TMXObjectInfo* j)
{
	if (i->y < j->y)
		return true;

	return false;
}

// Position for vec
bool SortPositionVec(glm::vec3 i, glm::vec3 j)
{
	if (i.z < j.z)
		return true;

	return false;
}

// Constructor
SpawnTracker::SpawnTracker(std::string targetP, std::vector<TMXObjectInfo*>* spawnListP, std::map<std::string, std::string>* GOMapP, std::vector<glm::vec3>* checkPointsP, std::vector<TMXObjectInfo*>* weaponsP)
{
	// Set members
	m_strTargetID = targetP;
	spawnList = spawnListP;
	weaponPickups = weaponsP;
	GOMap = GOMapP;
	checkPoints = checkPointsP;

	// Sort the given spawn list
	std::sort(spawnList->begin(), spawnList->end(), SortPosition);
	std::sort(weaponPickups->begin(), weaponPickups->end(), SortPosition);
	std::sort(checkPoints->begin(), checkPoints->end(), SortPositionVec);

	// Default distance of 10.0f
	spawnDistance = 20.0f;

	timerStarted = false;
	spawnTimer = 0.0f;
	resetTime = 6.0f;

	currentSpawn = glm::vec3(336.0f, 40.0f, -150.0f);

	spawnPos = 0;
	weaponPos = 0;

}

// Destructor
SpawnTracker::~SpawnTracker()
{
	// Delete the spawned objects
	for (int i = 0; i < spawnList->size(); i++)
	{
		delete spawnList->at(i);
	}
	spawnList->clear();
	delete spawnList;

	// Delete the spawned objects
	for (int i = 0; i < weaponPickups->size(); i++)
	{
		delete weaponPickups->at(i);
	}
	weaponPickups->clear();
	delete weaponPickups;

	
}

// Update
void SpawnTracker::Update(float p_fDelta)
{

	Common::GameObjectManager* GOManager = HeliGame::GetInstance()->GameObjectManager();

	// Try to get GO
	Common::GameObject* target = GOManager->GetGameObject(m_strTargetID);

	if (target)
	{

		// Spawning Enemies
		if (spawnList->size() - spawnPos > 0)
		{
			// Get the Z position of the target
			float targetZ = target->GetTransform().GetTranslation().z;

			// Get the last one in the list
			TMXObjectInfo* objectInfo = spawnList->at((spawnList->size() - 1) - spawnPos);

			if (targetZ < (float)objectInfo->y + spawnDistance)
			{

				// Make a transform given the info 
				glm::vec3 positionToSpawn = glm::vec3(objectInfo->x, objectInfo->spawnHeight, objectInfo->y);

				// Make the GO
				Common::GameObject* spawnedObject = GOManager->CreateGameObject(GOMap->at(objectInfo->type));
				spawnedObject->GetTransform().SetTranslation(positionToSpawn);
				spawnedObject->AddTag("Enemy");

				// Remove from list
				spawnPos++;
			}
		}

		// Spawning weapon pickups
		if (weaponPickups->size() - weaponPos > 0)
		{
			// Get the Z position of the target
			float targetZ = target->GetTransform().GetTranslation().z;

			// Get the last one in the list
			TMXObjectInfo* objectInfo = weaponPickups->at((weaponPickups->size() - 1) - weaponPos);

			if (targetZ < (float)objectInfo->y + spawnDistance)
			{

				// Make a transform given the info 
				glm::vec3 positionToSpawn = glm::vec3(objectInfo->x, 15.0f, objectInfo->y);

				// Make the GO
				Common::GameObject* spawnedObject = GOManager->CreateGameObject(GOMap->at(objectInfo->type));
				spawnedObject->AddTag("ItemDrop");
				spawnedObject->GetTransform().SetTranslation(positionToSpawn);

				weaponPos++;
			}
		}

		// Checkpoints
		if (checkPoints->size() > 0)
		{
			glm::vec3 closestSpawn = checkPoints->back();

			float targetZ = target->GetTransform().GetTranslation().z;
			if (targetZ < closestSpawn.z)
			{
				currentSpawn = checkPoints->back();
				checkPoints->pop_back();

				// Deletes the enemies (Has the issue where ones really close to the checkpoint don't respawn, which is actually good for enemies)
				for (int i = 0; i < spawnPos; i++)
				{
					TMXObjectInfo* spawnInfo = spawnList->back();
					spawnList->pop_back();

					delete spawnInfo;
				}

				// Doesn't delete the weapons (Isn't as much of a memory impact, since there is less, and helps with the respawn distance)
				spawnPos = 0;
				weaponReset = std::max(0,weaponPos - 1);

			}
		}


		// Check for the level being over (All enemies killed?)
		if (spawnList->size() - spawnPos <= 0)
		{

			if (!levelFinished)
			{

				std::vector<Common::GameObject*> enemies = GOManager->GetObjectsWithTag("Enemy");
				
				if (enemies.size() <= 0)
				{

					printf("Finished!\n");

					// Create effect
					Effect* fireworks = new Effect("GameProject/GameProject/data/ParticleEffects/Effects/FireWorksEffect.effect");
					fireworks->SetPosition(fireworkPos);

					Common::SceneManager::Instance()->AddEffect(fireworks);

					SoundManager::Instance()->PlaySoundFile("LevelFinish");

					levelFinished = true;
				}

			}
		}

		
	}

	else
	{
		if (spawnTimer > resetTime)
		{
			// Spawn player
			Common::GameObject* helicopter = GOManager->CreateGameObject("GameProject/GameProject/data/GameObjects/Helicopter.xml");
			helicopter->GetTransform().SetTranslationXYZ(currentSpawn.x, currentSpawn.y, currentSpawn.z);
			helicopter->SetDirection(180.0f);
			helicopter->AddTag("Player");
			GOManager->SetGameObjectGUID(helicopter, "Player");

			ComponentRenderableMesh* meshComponent = static_cast<ComponentRenderableMesh*>(helicopter->GetComponent("GOC_Renderable"));

			Common::GameObject* camera = GOManager->GetGameObject("Camera");
			ComponentDragCamera* camComponent = static_cast<ComponentDragCamera*>(camera->GetComponent("GOC_Camera"));
			camComponent->SetTarget(helicopter);

			// Delete active enemies/pickups?
			std::vector<Common::GameObject*> enemies = GOManager->GetObjectsWithTag("Enemy");
			std::vector<Common::GameObject*> itemDrops = GOManager->GetObjectsWithTag("ItemDrop");

			for (int i = 0; i < enemies.size(); i++)
			{
				GOManager->DestroyGameObject(enemies.at(i));
			}

			for (int i = 0; i < itemDrops.size(); i++)
			{
				GOManager->DestroyGameObject(itemDrops.at(i));
			}

			// Reset timer;
			spawnTimer = 0.0f;

			// Reset spawning
			spawnPos = 0;
			weaponPos = weaponReset;

		}
		else
		{
			spawnTimer += p_fDelta;
		}
	}

}