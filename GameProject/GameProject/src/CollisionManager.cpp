//------------------------------------------------------------------------
// CollisionManager
//	
// A collision manager to handle all the collisions that may take place
// Only bounding boxes at the moment
//------------------------------------------------------------------------

#include "CollisionManager.h"
#include "DynamicCollisionEvent.h"
#include "StaticCollisionEvent.h"
#include "ConeWeaponEvent.h"
#include "EventManager.h"
#include <glm/gtx/transform.hpp>
#include <algorithm>
#include "BoundingBoxDebugDraw.h"
#include "SceneManager.h"
#include "ComponentHealth.h"

CollisionManager* CollisionManager::s_pCollisionManagerInstance = NULL;
static int testCollisionCount = 0;
static int testCollisionCount2 = 0;

// Create Instance
void CollisionManager::CreateInstance()
{
	assert(s_pCollisionManagerInstance == NULL);
	s_pCollisionManagerInstance = new CollisionManager();
}

// Destroy Instance
void CollisionManager::DestroyInstance()
{
	assert(s_pCollisionManagerInstance != NULL);
	delete s_pCollisionManagerInstance;
	s_pCollisionManagerInstance = NULL;
}

// Access Instance
CollisionManager* CollisionManager::Instance()
{
	assert(s_pCollisionManagerInstance != NULL);
	return s_pCollisionManagerInstance;
}

// Constructor
CollisionManager::CollisionManager()
{
}

// Destructor
CollisionManager::~CollisionManager()
{
}

void CollisionManager::LoadCollisionMap(const std::string& p_sCollisionMap)
{
	
	// Opening up the given TMX file
	printf("%s\n", p_sCollisionMap.c_str());
	TiXmlDocument doc(p_sCollisionMap.c_str());

	// If it can't open it, print an error
	if (doc.LoadFile() == false)
	{
		printf("Failed to load Collision Map");
	}
	else
	{

		TiXmlNode* mapNode = doc.FirstChild("CollisionMap");

		// Go through each child element of the node
		TiXmlElement* collisionLink = mapNode->FirstChildElement("BoundingBox");
		while (collisionLink)
		{

			std::string objectType = "";

			float width = 0.0f;
			float depth = 0.0f;
			float offsetX = 0.0f;
			float offsetZ = 0.0f;

			// Type
			const char* type = collisionLink->Attribute("type");
			if (type == NULL)
			{
				printf("Error reading type from collision map\n");
			}
			objectType = type;

			// Width
			if (collisionLink->QueryFloatAttribute("width", &width) != TIXML_SUCCESS)
			{
				printf("Error reading width from collision map\n");
			}

			// Depth
			if (collisionLink->QueryFloatAttribute("depth", &depth) != TIXML_SUCCESS)
			{
				printf("Error reading depth from collision map\n");
			}

			// offsetX
			if (collisionLink->QueryFloatAttribute("offsetX", &offsetX) != TIXML_SUCCESS)
			{
				printf("Error reading offsetX from collision map\n");
			}

			// offsetZ
			if (collisionLink->QueryFloatAttribute("offsetZ", &offsetZ) != TIXML_SUCCESS)
			{
				printf("Error reading offsetZ from collision map\n");
			}

			CollisionMapData data;
			data.depth = depth;
			data.width = width;
			data.offsetX = offsetX;
			data.offsetZ = offsetZ;

			m_pCollisionMapData.insert(std::pair<std::string, CollisionMapData>(objectType, data));

			collisionLink = collisionLink->NextSiblingElement("BoundingBox");	

		}
	}


}

// Add Collision Component
void CollisionManager::AddCollisionComponent(ComponentBoundingBox* p_pCollisionComponent)
{
	m_lCollisionComponents.push_back(p_pCollisionComponent);
}

// Remove Collision Component
void CollisionManager::DeleteCollisionComponent(ComponentBoundingBox* p_pCollisionComponent)
{
	std::vector<ComponentBoundingBox*>::iterator iter = std::find(m_lCollisionComponents.begin(), m_lCollisionComponents.end(), p_pCollisionComponent);
	if (iter != m_lCollisionComponents.end())
	{
		m_lCollisionComponents.erase(iter);
	}
}

// Adding static collision points
void CollisionManager::SetStaticCollisionData(std::vector<TMXObjectInfo*>* p_pStaticTerrain)
{

	// Remove previous CollisionPoints
	m_lStaticCollision.clear();

	// Iterate over the static terrain
	for (int i = 0; i < p_pStaticTerrain->size(); i++)
	{

		TMXObjectInfo* object = p_pStaticTerrain->at(i);

		// Check if there is collision data for this object
		if (m_pCollisionMapData.count(object->type) > 0)
		{
			
			CollisionMapData collisionData = m_pCollisionMapData.at(object->type);

			// Construct a collision point
			CollisionPoints points;
			points.topLeft = glm::vec4(collisionData.offsetX, 0.0f, collisionData.offsetZ, 1.0f);
			points.topRight = glm::vec4(collisionData.offsetX + collisionData.width, 0.0f, collisionData.offsetZ, 1.0f);
			points.bottomLeft = glm::vec4(collisionData.offsetX, 0.0f, collisionData.offsetZ - collisionData.depth, 1.0f);
			points.bottomRight = glm::vec4(collisionData.offsetX + collisionData.width, 0.0f, collisionData.offsetZ - collisionData.depth, 1.0f);

			// Rotation/Scale/Translation Matrix to apply to the points
			glm::mat4 scale = glm::scale(glm::vec3(object->scale, object->scale, object->scale));
			glm::mat4 rotation = glm::rotate(float(object->rotation), 0.0f, 1.0f, 0.0f);
			glm::mat4 translation = glm::translate(glm::vec3(object->x, 0.0f, object->y));
			glm::mat4 transform = translation * rotation * scale;
			points.transform = transform;
			
			// Add to the list
			m_lStaticCollision.push_back(points);

#if DEBUG_BOXES
			CollisionPoints debugPoints;
			debugPoints.topLeft = transform * points.topLeft;
			debugPoints.topRight = transform * points.topRight;
			debugPoints.bottomLeft = transform * points.bottomLeft;
			debugPoints.bottomRight = transform * points.bottomRight;

			m_lDebugBoxes.push_back(debugPoints);
#endif
		}
	}
}

// Update
void CollisionManager::Update(float p_fDelta, const glm::mat4& proj, const glm::mat4& view, float aspectRatioP)
{

#if DEBUG_BOXES
	// Add the static collision elements to the line drawer (Should be debug flagged later)
	for (int i = 0; i < m_lDebugBoxes.size(); i++)
	{
		CollisionPoints points = m_lDebugBoxes.at(i);

		// Add to drawer
		BoundingBoxDebugDraw::Instance()->AddLine(glm::vec3(points.bottomLeft), glm::vec3(points.bottomRight));
		BoundingBoxDebugDraw::Instance()->AddLine(glm::vec3(points.bottomRight), glm::vec3(points.topRight));
		BoundingBoxDebugDraw::Instance()->AddLine(glm::vec3(points.topRight), glm::vec3(points.topLeft));
		BoundingBoxDebugDraw::Instance()->AddLine(glm::vec3(points.topLeft), glm::vec3(points.bottomLeft));
	}
#endif

	// Iterate over vector and check for collisions?
	if (m_lCollisionComponents.size() > 1)
	{
		// Check like AABB for now!
		for (int i = 0; i < m_lCollisionComponents.size() - 1; i++)
		{
			for (int j = i + 1; j < m_lCollisionComponents.size(); j++)
			{
				// Boxes to use
				ComponentBoundingBox* box1 = m_lCollisionComponents.at(i);
				ComponentBoundingBox* box2 = m_lCollisionComponents.at(j);

				// Get the axis for rotated checking
				glm::vec3 axis1 = glm::vec3(box1->adjustedTR - box1->adjustedTL);
				glm::vec3 axis2 = glm::vec3(box1->adjustedTR - box1->adjustedBR);
				glm::vec3 axis3 = glm::vec3(box2->adjustedTL - box2->adjustedBL);
				glm::vec3 axis4 = glm::vec3(box2->adjustedTL - box2->adjustedTR);

				// Check each axis (Break if it finds a non-collision)
				if (!CheckCollisionAxis(box1, box2, axis1))
					continue;
				if (!CheckCollisionAxis(box1, box2, axis2))
					continue;
				if (!CheckCollisionAxis(box1, box2, axis3))
					continue;
				if (!CheckCollisionAxis(box1, box2, axis4))
					continue;

			
				// Send out collision event (Maybe just handle it here?)
				std::shared_ptr<DynamicCollisionEvent> pCollisionEvent(new DynamicCollisionEvent(box1->GetGameObject(), box2->GetGameObject()));
				EventManager::Instance()->QueueEvent(pCollisionEvent);

			}
		}
	}

	// Adjust the collision components based on the current camera position
	std::vector<CollisionPoints> currentCamPoints;

	for (int i = 0; i < m_lStaticCollision.size(); i++)
	{
		CollisionPoints points = m_lStaticCollision.at(i);

		glm::mat4 projViewWorld = proj * view * points.transform;

		// Adjust the points based on the transform
		points.topLeft = projViewWorld * points.topLeft;
		points.topRight = projViewWorld * points.topRight;
		points.bottomLeft = projViewWorld * points.bottomLeft;
		points.bottomRight = projViewWorld * points.bottomRight;

		// Divide by W
		float aspectRatio = Common::SceneManager::Instance()->GetCamera()->GetAspectRatio();
		points.topLeft.x = (points.topLeft.x / points.topLeft.w) * aspectRatio;
		points.topRight.x = (points.topRight.x / points.topRight.w) * aspectRatio;
		points.bottomLeft.x = (points.bottomLeft.x / points.bottomLeft.w) * aspectRatio;
		points.bottomRight.x = (points.bottomRight.x / points.bottomRight.w) * aspectRatio;


		points.topLeft.y = points.topLeft.y / points.topLeft.w;
		points.topRight.y = points.topRight.y / points.topRight.w;
		points.bottomLeft.y = points.bottomLeft.y / points.bottomLeft.w;
		points.bottomRight.y = points.bottomRight.y / points.bottomRight.w;

		currentCamPoints.push_back(points);
	}

	// Check the dynamic components over the static terrain (Should really only be bullets?)
	for (int i = 0; i < m_lCollisionComponents.size(); i++)
	{

		ComponentBoundingBox* dynamicBox = m_lCollisionComponents.at(i);
		
		for (int j = 0; j < currentCamPoints.size(); j++)
		{

			CollisionPoints staticPoints = currentCamPoints.at(j);
			CollisionPoints worldPosition = m_lStaticCollision.at(j);

			// Check the collision
			// Get the axis for rotated checking
			glm::vec3 axis1 = glm::vec3(dynamicBox->adjustedTR - dynamicBox->adjustedTL);
			glm::vec3 axis2 = glm::vec3(dynamicBox->adjustedTR - dynamicBox->adjustedBR);
			glm::vec3 axis3 = glm::vec3(staticPoints.topLeft - staticPoints.bottomLeft);
			glm::vec3 axis4 = glm::vec3(staticPoints.topLeft - staticPoints.topRight);

			// Check each axis (Break if it finds a non-collision)
			if (!CheckCollisionAxis(dynamicBox, &staticPoints, axis1))
				continue;
			if (!CheckCollisionAxis(dynamicBox, &staticPoints, axis2))
				continue;
			if (!CheckCollisionAxis(dynamicBox, &staticPoints, axis3))
				continue;
			if (!CheckCollisionAxis(dynamicBox, &staticPoints, axis4))
				continue;
			
			std::shared_ptr<StaticCollisionEvent> pCollisionEvent(new StaticCollisionEvent(dynamicBox->GetGameObject()));
			EventManager::Instance()->QueueEvent(pCollisionEvent);

			printf("Static Collision:%d\n", testCollisionCount);
			testCollisionCount++;


		}		
	}

	CheckExplosions(p_fDelta, proj, view, aspectRatioP);
}

// Checking an axis between two dynamic bodies
bool CollisionManager::CheckCollisionAxis(ComponentBoundingBox* p_pBox1, ComponentBoundingBox* p_pBox2, glm::vec3 axisP)
{

	// Get all the projected points
	float box1TL = glm::dot(CalculateProjectionCam(p_pBox1->adjustedTL, axisP), axisP);
	float box1TR = glm::dot(CalculateProjectionCam(p_pBox1->adjustedTR, axisP), axisP);
	float box1BL = glm::dot(CalculateProjectionCam(p_pBox1->adjustedBL, axisP), axisP);
	float box1BR = glm::dot(CalculateProjectionCam(p_pBox1->adjustedBR, axisP), axisP);

	float box2TL = glm::dot(CalculateProjectionCam(p_pBox2->adjustedTL, axisP), axisP);
	float box2TR = glm::dot(CalculateProjectionCam(p_pBox2->adjustedTR, axisP), axisP);
	float box2BL = glm::dot(CalculateProjectionCam(p_pBox2->adjustedBL, axisP), axisP);
	float box2BR = glm::dot(CalculateProjectionCam(p_pBox2->adjustedBR, axisP), axisP);

	// Max/min
	float maxBox1 = std::max(box1TL, std::max(box1TR, std::max(box1BL, box1BR)));
	float minBox1 = std::min(box1TL, std::min(box1TR, std::min(box1BL, box1BR)));

	float maxBox2 = std::max(box2TL, std::max(box2TR, std::max(box2BL, box2BR)));
	float minBox2 = std::min(box2TL, std::min(box2TR, std::min(box2BL, box2BR)));

	if (minBox2 <= maxBox1 && maxBox2 >= minBox1)
		return true;

	return false;
}

// Checking an axis between dynamic and static body
bool CollisionManager::CheckCollisionAxis(ComponentBoundingBox* p_pBox1, CollisionPoints* p_pBox2, glm::vec3 axisP)
{

	// Get all the projected points
	float box1TL = glm::dot(CalculateProjectionCam(p_pBox1->adjustedTL, axisP), axisP);
	float box1TR = glm::dot(CalculateProjectionCam(p_pBox1->adjustedTR, axisP), axisP);
	float box1BL = glm::dot(CalculateProjectionCam(p_pBox1->adjustedBL, axisP), axisP);
	float box1BR = glm::dot(CalculateProjectionCam(p_pBox1->adjustedBR, axisP), axisP);

	float box2TL = glm::dot(CalculateProjectionCam(p_pBox2->topLeft, axisP), axisP);
	float box2TR = glm::dot(CalculateProjectionCam(p_pBox2->topRight, axisP), axisP);
	float box2BL = glm::dot(CalculateProjectionCam(p_pBox2->bottomLeft, axisP), axisP);
	float box2BR = glm::dot(CalculateProjectionCam(p_pBox2->bottomRight, axisP), axisP);

	// Max/min
	float maxBox1 = std::max(box1TL, std::max(box1TR, std::max(box1BL, box1BR)));
	float minBox1 = std::min(box1TL, std::min(box1TR, std::min(box1BL, box1BR)));

	float maxBox2 = std::max(box2TL, std::max(box2TR, std::max(box2BL, box2BR)));
	float minBox2 = std::min(box2TL, std::min(box2TR, std::min(box2BL, box2BR)));

	if (minBox2 <= maxBox1 && maxBox2 >= minBox1)
		return true;

	return false;
}

// Checking an axis between static and static body
bool CollisionManager::CheckCollisionAxis(CollisionPoints* p_pBox1, CollisionPoints* p_pBox2, glm::vec3 axisP)
{

	// Get all the projected points
	float box1TL = glm::dot(CalculateProjectionWorld(p_pBox1->topLeft, axisP), axisP);
	float box1TR = glm::dot(CalculateProjectionWorld(p_pBox1->topRight, axisP), axisP);
	float box1BL = glm::dot(CalculateProjectionWorld(p_pBox1->bottomLeft, axisP), axisP);
	float box1BR = glm::dot(CalculateProjectionWorld(p_pBox1->bottomRight, axisP), axisP);

	float box2TL = glm::dot(CalculateProjectionWorld(p_pBox2->topLeft, axisP), axisP);
	float box2TR = glm::dot(CalculateProjectionWorld(p_pBox2->topRight, axisP), axisP);
	float box2BL = glm::dot(CalculateProjectionWorld(p_pBox2->bottomLeft, axisP), axisP);
	float box2BR = glm::dot(CalculateProjectionWorld(p_pBox2->bottomRight, axisP), axisP);

	// Max/min
	float maxBox1 = std::max(box1TL, std::max(box1TR, std::max(box1BL, box1BR)));
	float minBox1 = std::min(box1TL, std::min(box1TR, std::min(box1BL, box1BR)));

	float maxBox2 = std::max(box2TL, std::max(box2TR, std::max(box2BL, box2BR)));
	float minBox2 = std::min(box2TL, std::min(box2TR, std::min(box2BL, box2BR)));

	if (minBox2 <= maxBox1 && maxBox2 >= minBox1)
		return true;

	return false;
}

// Public method for checking CollisionPoints
bool CollisionManager::CheckCollisionPoints(CollisionPoints* p_pBox1, CollisionPoints* p_pBox2)
{
	// Get the axis for rotated checking
	glm::vec3 axis1 = glm::vec3(p_pBox1->topRight - p_pBox1->topLeft);
	glm::vec3 axis2 = glm::vec3(p_pBox1->topRight - p_pBox1->bottomRight);
	glm::vec3 axis3 = glm::vec3(p_pBox2->topLeft - p_pBox2->bottomLeft);
	glm::vec3 axis4 = glm::vec3(p_pBox2->topLeft - p_pBox2->topRight);

	// Check each axis (Break if it finds a non-collision)
	if (!CheckCollisionAxis(p_pBox1, p_pBox2, axis1))
		return false;
	if (!CheckCollisionAxis(p_pBox1, p_pBox2, axis2))
		return false;
	if (!CheckCollisionAxis(p_pBox1, p_pBox2, axis3))
		return false;
	if (!CheckCollisionAxis(p_pBox1, p_pBox2, axis4))
		return false;

	return true;
}

void CollisionManager::CheckConeWeaponCollision(CollisionPoints* p_pBox1, float p_fDamageVal)
{

	// Camera
	Common::SceneCamera* cam = Common::SceneManager::Instance()->GetCamera();

	// Go over collision components
	for (int i = 0; i < m_lCollisionComponents.size(); i++)
	{

		ComponentBoundingBox* dynamicBox = m_lCollisionComponents.at(i);

		// Get the axis for rotated checking
		glm::vec3 axis1 = glm::vec3(dynamicBox->adjustedTR - dynamicBox->adjustedTL);
		glm::vec3 axis2 = glm::vec3(dynamicBox->adjustedTR - dynamicBox->adjustedBR);
		glm::vec3 axis3 = glm::vec3(p_pBox1->topLeft - p_pBox1->bottomLeft);
		glm::vec3 axis4 = glm::vec3(p_pBox1->topLeft - p_pBox1->topRight);

		// Adjust the points passed in based on camera
		CollisionPoints adjustedPoints;
		adjustedPoints.bottomLeft = cam->GetProjectionMatrix() * cam->GetViewMatrix() * p_pBox1->bottomLeft;
		adjustedPoints.bottomRight = cam->GetProjectionMatrix() * cam->GetViewMatrix() * p_pBox1->bottomRight;
		adjustedPoints.topLeft = cam->GetProjectionMatrix() * cam->GetViewMatrix() * p_pBox1->topLeft;
		adjustedPoints.topRight = cam->GetProjectionMatrix() * cam->GetViewMatrix() * p_pBox1->topRight;

		// Adjust them
		float aspectRatio = cam->GetAspectRatio();
		adjustedPoints.bottomLeft.x = (adjustedPoints.bottomLeft.x / adjustedPoints.bottomLeft.w) * aspectRatio;
		adjustedPoints.bottomRight.x = (adjustedPoints.bottomRight.x / adjustedPoints.bottomRight.w) * aspectRatio;
		adjustedPoints.topLeft.x = (adjustedPoints.topLeft.x / adjustedPoints.topLeft.w) * aspectRatio;
		adjustedPoints.topRight.x = (adjustedPoints.topRight.x / adjustedPoints.topRight.w) * aspectRatio;

		adjustedPoints.bottomLeft.y= (adjustedPoints.bottomLeft.y / adjustedPoints.bottomLeft.w);
		adjustedPoints.bottomRight.y = (adjustedPoints.bottomRight.y / adjustedPoints.bottomRight.w);
		adjustedPoints.topLeft.y = (adjustedPoints.topLeft.y / adjustedPoints.topLeft.w);
		adjustedPoints.topRight.y = (adjustedPoints.topRight.y / adjustedPoints.topRight.w);

		// Check each axis (Break if it finds a non-collision)
		if (!CheckCollisionAxis(dynamicBox, &adjustedPoints, axis1))
			continue;
		if (!CheckCollisionAxis(dynamicBox, &adjustedPoints, axis2))
			continue;
		if (!CheckCollisionAxis(dynamicBox, &adjustedPoints, axis3))
			continue;
		if (!CheckCollisionAxis(dynamicBox, &adjustedPoints, axis4))
			continue;

		std::shared_ptr<ConeWeaponEvent> pCollisionEvent(new ConeWeaponEvent(dynamicBox->GetGameObject(), p_fDamageVal));
		EventManager::Instance()->QueueEvent(pCollisionEvent);
		
	}

}

// Calculate project in cam with the y coordinate
glm::vec3 CollisionManager::CalculateProjectionCam(glm::vec4 pointP, glm::vec3 axisP)
{
	// Calculate first part of equation
	float numerator = (pointP.x * axisP.x) + (pointP.y * axisP.y);
	float denominator = pow(axisP.x, 2) + pow(axisP.y, 2);
	float val1 = numerator / denominator;

	glm::vec3 result;
	result.x = val1 * axisP.x;
	result.y = val1 * axisP.y;

	return result;
}

// Calculate project in world with z
glm::vec3 CollisionManager::CalculateProjectionWorld(glm::vec4 pointP, glm::vec3 axisP)
{
	// Calculate first part of equation
	float numerator = (pointP.x * axisP.x) + (pointP.z * axisP.z);
	float denominator = pow(axisP.x, 2) + pow(axisP.z, 2);
	float val1 = numerator / denominator;

	glm::vec3 result;
	result.x = val1 * axisP.x;
	result.z = val1 * axisP.z;

	return result;
}

// Checking explosions
void CollisionManager::CheckExplosions(float p_fDelta, const glm::mat4& proj, const glm::mat4& view, float aspectRatioP)
{

	// Get the player
	Common::GameObject* player = m_pManager->GetGameObject(playerID);

	if (player)
	{
		// Get the bounding box and create collision points
		ComponentBoundingBox* box = static_cast<ComponentBoundingBox*>(player->GetComponent("GOC_BoundingBox"));		

		// List of explosions to delete
		std::vector<ExplosionInfo*> expiredExplosions;

		// Go through the active explosions
		for (int i = 0; i < m_lActiveExplosions.size(); i++)
		{

			// Current
			ExplosionInfo* current = m_lActiveExplosions.at(i);

			// Transform radius to screen
			glm::vec4 newPoint = proj * view * glm::vec4(current->point, 1.0f);
			newPoint.x = (newPoint.x / newPoint.w) * aspectRatioP;
			newPoint.y = (newPoint.y / newPoint.w);

#if DEBUG_BOXES

			// Get the points
			glm::vec3 point = current->point;

			glm::vec3 debugBL = glm::vec3(point.x - current->radius, point.y, point.z + current->radius);
			glm::vec3 debugBR = glm::vec3(point.x + current->radius, point.y, point.z + current->radius);
			glm::vec3 debugTL = glm::vec3(point.x - current->radius, point.y, point.z - current->radius);
			glm::vec3 debugTR = glm::vec3(point.x + current->radius, point.y, point.z - current->radius);

			BoundingBoxDebugDraw::Instance()->AddLine(glm::vec3(debugBL), glm::vec3(debugBR));
			BoundingBoxDebugDraw::Instance()->AddLine(glm::vec3(debugBR), glm::vec3(debugTR));
			BoundingBoxDebugDraw::Instance()->AddLine(glm::vec3(debugTR), glm::vec3(debugTL));
			BoundingBoxDebugDraw::Instance()->AddLine(glm::vec3(debugTL), glm::vec3(debugBL));
#endif

			// Check for collision
			bool isCollision = CircleCollision(box, newPoint, current->radius);

			if (isCollision)
			{

				// Get the health Component and trigger collision
				ComponentHealth* playerHealth = static_cast<ComponentHealth*>(player->GetComponent("GOC_Health"));
				playerHealth->HandleExplosionDamage(current->damagePerSecond * p_fDelta);

			}

			// Duration
			if (current->timeActive > current->duration)
			{
				expiredExplosions.push_back(current);
			}

			else
			{
				current->timeActive += p_fDelta;
			}

			// Clean up dead effects
			for (int i = 0; i < expiredExplosions.size(); i++)
			{
				std::vector<ExplosionInfo*>::iterator it = std::find(m_lActiveExplosions.begin(), m_lActiveExplosions.end(), expiredExplosions.at(i));
				if (it != m_lActiveExplosions.end())
				{
					m_lActiveExplosions.erase(it);
				}

				// Delete it as well
				delete expiredExplosions.at(i);
			}

			// Clear the explosions that have expired
			expiredExplosions.clear();

		}

	}
	else
	{
		newPlayer = true;
	}


}

// Circle Collision
bool CollisionManager::CircleCollision(ComponentBoundingBox* p_pBox1, glm::vec4 pointP, float radiusP)
{

	// Perform check
	float closestX = simpleClamp(pointP.x, p_pBox1->adjustedTL.x, p_pBox1->adjustedTR.x);
	float closestY = simpleClamp(pointP.y, p_pBox1->adjustedTL.y, p_pBox1->adjustedBL.y);

	float distanceX = pointP.x - closestX;
	float distanceY = pointP.y - closestY;

	// Check distance^2
	float distanceSquared = (distanceX * distanceX) + (distanceY * distanceY);
	return distanceSquared < (radiusP * radiusP);
	
}

// Clamp
float CollisionManager::simpleClamp(float value, float min, float max)
{
	if (value >= max)
		return max;
	else if (value <= min)
		return min;
	else
		return value;
}