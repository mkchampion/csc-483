//-----------------------------------------------------------------------------
// ConeWeapon.h
//-----------------------------------------------------------------------------

#include "ConeWeapon.h"
#include "SceneManager.h"
#include <glm/gtx/vector_angle.hpp>
#include "BoundingBoxDebugDraw.h"
#include "CollisionManager.h"
#include "SoundManager.h"

// Constructor
ConeWeapon::ConeWeapon(const std::string& weaponPathP)
{
	// Default
	effectStarted = false;
	damage = 0.0f;
	std::string effectPath = "";

	float width = 0.0f;
	float depth = 0.0f;
	glm::vec2 offset;

	// Read from file
	// Opening up the given TMX file
	TiXmlDocument doc(weaponPathP.c_str());

	// If it can't open it, print an error
	if (doc.LoadFile() == false)
	{
		printf("TMX Error", "Failed to locate / load TMX file");
		return;
	}
	else
	{
		// Initial weapon
		TiXmlNode* weaponNode = doc.FirstChild("Weapon");

		// Get other properties
		TiXmlNode* pChildNode = weaponNode->FirstChild();
		while (pChildNode != NULL)
		{
			const char* szNodeName = pChildNode->Value();

			if (strcmp(szNodeName, "Effect") == 0)
			{

				// Parse attributes
				TiXmlElement* pElement = pChildNode->ToElement();

				// Name
				const char* szName = pElement->Attribute("path");
				if (szName == NULL)
				{
					return;
				}

				
				effectPath = szName;
			}

			else if (strcmp(szNodeName, "Damage") == 0)
			{

				// Parse attributes
				TiXmlElement* pElement = pChildNode->ToElement();

				// Rotation Correction
				if (pElement->QueryFloatAttribute("value", &damage) != TIXML_SUCCESS)
				{
					return;
				}
			}

			if (strcmp(szNodeName, "Offset") == 0)
			{

				// Parse attributes
				TiXmlElement* pElement = pChildNode->ToElement();

				// x offset
				if (pElement->QueryFloatAttribute("x", &offset.x) != TIXML_SUCCESS)
				{
					return;
				}

				// z offset (in +z?)
				if (pElement->QueryFloatAttribute("z", &offset.y) != TIXML_SUCCESS)
				{
					return;
				}
			}

			else if (strcmp(szNodeName, "Width") == 0)
			{

				// Parse attributes
				TiXmlElement* pElement = pChildNode->ToElement();

				// Rotation Correction
				if (pElement->QueryFloatAttribute("value", &width) != TIXML_SUCCESS)
				{
					return;
				}
			}

			else if (strcmp(szNodeName, "Depth") == 0)
			{

				// Parse attributes
				TiXmlElement* pElement = pChildNode->ToElement();

				// Rotation Correction
				if (pElement->QueryFloatAttribute("value", &depth) != TIXML_SUCCESS)
				{
					return;
				}
			}

			else if (strcmp(szNodeName, "Sound") == 0)
			{

				// Parse attributes
				TiXmlElement* pElement = pChildNode->ToElement();

				// Name
				const char* szName = pElement->Attribute("value");
				if (szName == NULL)
				{
					return;
				}


				soundName = szName;
			}

			pChildNode = pChildNode->NextSibling();
		}
	}

	// Setup the effect
	m_pParticleEffect = new Effect(effectPath);
	m_pParticleEffect->Stop();
	Common::SceneManager::Instance()->AddEffect(m_pParticleEffect);

	// Setup box
	m_pBoundries.bottomLeft = glm::vec4(offset.x, 0.0f, offset.y, 1.0f);
	m_pBoundries.bottomRight = glm::vec4(offset.x + width, 0.0f, offset.y, 1.0f);
	m_pBoundries.topLeft = glm::vec4(offset.x, 0.0f, offset.y - depth, 1.0f);
	m_pBoundries.topRight = glm::vec4(offset.x + width, 0.0f, offset.y - depth, 1.0f);

}

// Destructonator
ConeWeapon::~ConeWeapon()
{
	SoundManager::Instance()->StopSoundFile(soundName);
	m_pParticleEffect->Stop();
}

// Update
void ConeWeapon::Update(float p_fDelta, Common::GameObject* ownerP, const glm::vec2& dirP)
{

	// Find the rotation/position
	float fAngle = glm::orientedAngle(glm::vec3(0.0f, 0.0f, -1.0f), glm::vec3(dirP.x, 0.0f, -dirP.y), glm::vec3(0.0f, 1.0f, 0.0f));
	glm::vec3 pos = ownerP->GetTransform().GetTranslation();

	// Adjust it to be 0-360 for ease of use
	if (fAngle < 0)
	{
		fAngle += 360;
	}

	m_pParticleEffect->SetRotation(fAngle);
	m_pParticleEffect->SetPosition(pos);

	if (!effectStarted)
	{
		SoundManager::Instance()->PlaySoundFile(soundName);
		m_pParticleEffect->Play();
		effectStarted = true;
	}

	// Transform the base
	CollisionPoints adjustedPoints;

	adjustedPoints.bottomLeft = ownerP->GetTransform().GetTransformation() * glm::rotate(fAngle, glm::vec3(0.0f, 1.0f, 0.0f)) * m_pBoundries.bottomLeft;
	adjustedPoints.bottomRight = ownerP->GetTransform().GetTransformation() * glm::rotate(fAngle, glm::vec3(0.0f, 1.0f, 0.0f)) * m_pBoundries.bottomRight;
	adjustedPoints.topLeft = ownerP->GetTransform().GetTransformation() * glm::rotate(fAngle, glm::vec3(0.0f, 1.0f, 0.0f)) * m_pBoundries.topLeft;
	adjustedPoints.topRight = ownerP->GetTransform().GetTransformation() * glm::rotate(fAngle, glm::vec3(0.0f, 1.0f, 0.0f)) * m_pBoundries.topRight;

	// Check collision
	CollisionManager::Instance()->CheckConeWeaponCollision(&adjustedPoints, damage);

#if DEBUG_BOXES
	// Add it to the debug drawer
	BoundingBoxDebugDraw::Instance()->AddLine(glm::vec3(adjustedPoints.bottomLeft), glm::vec3(adjustedPoints.bottomRight));
	BoundingBoxDebugDraw::Instance()->AddLine(glm::vec3(adjustedPoints.bottomRight), glm::vec3(adjustedPoints.topRight));
	BoundingBoxDebugDraw::Instance()->AddLine(glm::vec3(adjustedPoints.topRight), glm::vec3(adjustedPoints.topLeft));
	BoundingBoxDebugDraw::Instance()->AddLine(glm::vec3(adjustedPoints.topLeft), glm::vec3(adjustedPoints.bottomLeft));
#endif
}

void ConeWeapon::StopWeapon()
{
	// If the particle is active, stop it
	SoundManager::Instance()->StopSoundFile(soundName);
	m_pParticleEffect->Stop();
	effectStarted = false;

}

void ConeWeapon::SetTarget(Common::GameObject* p_pTarget)
{
	m_pParticleEffect->SetTarget(p_pTarget);
}