//------------------------------------------------------------------------
// GameObjectManager
//
// Created:	2012/12/26
// Author:	Carel Boers
//	
// Manages the collection of Game Objects used by a game.
//------------------------------------------------------------------------

#include "GameObjectManager.h"
#include "ComponentRenderable.h"
//#include "ComponentBoundingSphere.h"
#include "ObjectDisappearedEvent.h"
//#include "CollisionEvent.h"
#include "EventManager.h"

using namespace Common;
static int testCount = 0;

//------------------------------------------------------------------------------
// Method:    GameObjectManager
// Returns:   
// 
// Constructor.
//------------------------------------------------------------------------------
GameObjectManager::GameObjectManager()
{
}

//------------------------------------------------------------------------------
// Method:    ~GameObjectManager
// Returns:   
// 
// Destructor.
//------------------------------------------------------------------------------
GameObjectManager::~GameObjectManager()
{
}

//------------------------------------------------------------------------------
// Method:    CreateGameObject
// Returns:   GameObject*
// 
// Factory creation method for empty GameObjects.
//------------------------------------------------------------------------------
GameObject* GameObjectManager::CreateGameObject()
{
	GameObject* pGO = new GameObject(this);
	m_mGOMap.insert(std::pair<std::string, GameObject*>(pGO->GetGUID(), pGO));
	return pGO;
}

//------------------------------------------------------------------------------
// Method:    DestroyGameObject
// Parameter: GameObject * p_pGameObject
// Returns:   void
// 
// Destroys the given Game Object and removes it from internal map of all Game
// Objects.
//------------------------------------------------------------------------------
void GameObjectManager::DestroyGameObject(GameObject* p_pGameObject)
{
	// Remove it from the collision map first
	RemoveCollidable(p_pGameObject->GetGUID());

	GameObject* pGO = NULL;
	GameObjectMap::iterator it = m_mGOMap.find(p_pGameObject->GetGUID());

	if (it != m_mGOMap.end())
	{
		pGO = (GameObject*)it->second;
		delete pGO;
		m_mGOMap.erase(it);
	}
}

//------------------------------------------------------------------------------
// Method:    DestroyAllGameObjects
// Returns:   void
// 
// Clears the internal map of all GameObjects and deletes them.
//------------------------------------------------------------------------------
void GameObjectManager::DestroyAllGameObjects()
{
	GameObject* pGO = NULL;
	GameObjectMap::iterator it = m_mGOMap.begin(), end = m_mGOMap.end();
	for (; it != end; ++it)
	{
		pGO = (GameObject*)it->second;
		delete pGO;
	}
	m_mGOMap.clear();
}

//------------------------------------------------------------------------------
// Method:    DestroyAllStateObjects
// Returns:   void
// 
// Clears the objects of a given state
//------------------------------------------------------------------------------
void GameObjectManager::DestroyAllStateObjects(int p_iState)
{
	GameObject* pGO = NULL;
	GameObjectMap::iterator it = m_mGOMap.begin(), end = m_mGOMap.end();

	// Vector of iterators to delete
	std::vector<GameObjectMap::iterator> iteratorList;

	for (; it != end; ++it)
	{
		pGO = (GameObject*)it->second;

		// Only update if the state is active
		int iState = pGO->GetState();

		if (iState)
		{
			if (iState == p_iState)
			{
				iteratorList.push_back(it);
				delete pGO;
			}
		}
	}

	// Clear the list now
	for (int i = 0; i < iteratorList.size(); i++)
	{
		m_mGOMap.erase(iteratorList.at(i));
	}
	
}

//------------------------------------------------------------------------------
// Method:    GetGameObject
// Parameter: const std::string & p_strGOGUID
// Returns:   GameObject*
// 
// Returns a GameObject mapped to by the given GUID. Returns NULL if no GO is 
// found.
//------------------------------------------------------------------------------
GameObject* GameObjectManager::GetGameObject(const std::string &p_strGOGUID)
{
	GameObject* pGO = NULL;
	GameObjectMap::iterator it = m_mGOMap.find(p_strGOGUID);
	if (it != m_mGOMap.end())
	{
		pGO = (GameObject*)it->second;
	}

	return pGO;
}

//------------------------------------------------------------------------------
// Method:    SetGameObjectGUID
// Parameter: GameObject * p_pGameObject
// Parameter: const std::string &p_strGOGUID
// Returns:   bool
// 
// Changes the name of the given GUID and updates the internal mapping. Returns 
// true for a successful rename, false on naming collision.
//------------------------------------------------------------------------------
bool GameObjectManager::SetGameObjectGUID(GameObject* p_pGameObject, const std::string &p_strGOGUID)
{
	GameObject* pGO = this->GetGameObject(p_strGOGUID);
	if (pGO)
	{
		// GUID already taken
		return false;
	}

	GameObjectMap::iterator it = m_mGOMap.find(p_pGameObject->GetGUID());
	if (it == m_mGOMap.end())
	{
		// No mapping - Game Object wasn't created from this GameObjectManager instance
		return false;
	}

	// Erase the old mapping, update the GUID and add it back
	m_mGOMap.erase(it);
	p_pGameObject->SetGUID(p_strGOGUID);
	m_mGOMap.insert(std::pair<std::string, GameObject*>(p_pGameObject->GetGUID(), p_pGameObject));
	return true;
}

//------------------------------------------------------------------------------
// Method:    CreateGameObject
// Parameter: const std::string & p_strGameObject
// Returns:   GameObject*
// 
// Factory creation method for GameObjects defined by an XML file. Returns NULL
// if the GameObject couldn't be properly constructed.
//------------------------------------------------------------------------------
GameObject* GameObjectManager::CreateGameObject(const std::string& p_strGameObject)
{
	// Load the document and return NULL if it fails to parse
	TiXmlDocument doc(p_strGameObject.c_str());
	if (doc.LoadFile() == false)
	{
		return NULL;
	}

	// Look for the root "GameObject" node and return NULL if it's missing
	TiXmlNode* pNode = doc.FirstChild("GameObject");
	if (pNode == NULL)
	{
		return NULL;
	}

	// Grab the scale if available
	float scale = 1.0f;
	pNode->ToElement()->QueryFloatAttribute("scale", &scale);
	

	// Create the game object
	GameObject* pGO = new GameObject(this);
	m_mGOMap.insert(std::pair<std::string, GameObject*>(pGO->GetGUID(), pGO));

	// Set the scale for the GO
	pGO->GetTransform().SetScale(glm::vec3(scale, scale, scale));

	// Iterate components in the XML and delegate to factory methods to construct components
	TiXmlNode* pComponentNode = pNode->FirstChild();
	while (pComponentNode != NULL)
	{
		const char* szComponentName = pComponentNode->Value();
		ComponentFactoryMap::iterator it = m_mComponentFactoryMap.find(szComponentName);
		if (it != m_mComponentFactoryMap.end())
		{
			ComponentFactoryMethod factory = it->second;
			ComponentBase* pComponent = factory(pComponentNode);
			if (pComponent != NULL)
			{
				pGO->AddComponent(pComponent);
			}
		}

		pComponentNode = pComponentNode->NextSibling();
	}
	
	pGO->SetState(eStateGameplay_Gameplay);
	return pGO;
}


//------------------------------------------------------------------------------
// Method:    CreateObjectGroup
// Returns:   std::vector<GameObject*>
// 
// Creates a group of game objects specific locations in the xml
//------------------------------------------------------------------------------
std::vector<GameObject*> GameObjectManager::CreateObjectGroup(const std::string& p_strGameObject)
{

	// Load the document and return empty list if it fails to parse
	TiXmlDocument doc(p_strGameObject.c_str());
	if (doc.LoadFile() == false)
	{
		return std::vector<GameObject*>();
	}

	// Look for the root "GameObject" node and return NULL if it's missing
	TiXmlNode* pNode = doc.FirstChild("ObjectGroup");
	if (pNode == NULL)
	{
		return std::vector<GameObject*>();
	}

	// List we will return
	std::vector<GameObject*> GOList;

	// First group node
	TiXmlNode* groupChild = pNode->FirstChild();	
	assert(strcmp(groupChild->Value(), "Group") == 0);

	// Parse attributes
	TiXmlElement* pGroupElement = groupChild->ToElement();

	// vector for base position
	glm::vec3 basePosition;

	// Get the position elements
	if (pGroupElement->QueryFloatAttribute("baseX", &basePosition.x) != TIXML_SUCCESS)
	{
		return std::vector<GameObject*>();
	}

	// Get the position elements
	if (pGroupElement->QueryFloatAttribute("baseY", &basePosition.y) != TIXML_SUCCESS)
	{
		return std::vector<GameObject*>();
	}

	// Get the position elements
	if (pGroupElement->QueryFloatAttribute("baseZ", &basePosition.z) != TIXML_SUCCESS)
	{
		return std::vector<GameObject*>();
	}


	// Now that base position is in, loop through the children
	TiXmlNode* objectNode = groupChild->FirstChild();

	while (objectNode != NULL)
	{

		if (strcmp(objectNode->Value(), "GameObject") == 0)
		{
			
			// Attributes that will be parsed
			std::string GOPath;
			glm::vec3 offsetPosition;

			// Element to parse from
			TiXmlElement* pGOElement = objectNode->ToElement();
			

			// Path
			const char* szName = pGOElement->Attribute("path");
			if (szName == NULL)
			{
				return std::vector<GameObject*>();
			}
			GOPath = szName;

			// Offset
			if (pGOElement->QueryFloatAttribute("offsetX", &offsetPosition.x) != TIXML_SUCCESS)
			{
				return std::vector<GameObject*>();
			}

			if (pGOElement->QueryFloatAttribute("offsetY", &offsetPosition.y) != TIXML_SUCCESS)
			{
				return std::vector<GameObject*>();
			}

			if (pGOElement->QueryFloatAttribute("offsetZ", &offsetPosition.z) != TIXML_SUCCESS)
			{
				return std::vector<GameObject*>();
			}

			
			// Create the game object
			GameObject* newObject = CreateGameObject(GOPath);
			newObject->GetTransform().SetTranslation(basePosition + offsetPosition);

			// Add it to the list we return
			GOList.push_back(newObject);

		}

		objectNode = objectNode->NextSibling();
	}
	
	return GOList;
}

//------------------------------------------------------------------------------
// Method:    RegisterComponentFactory
// Parameter: const std::string & p_strComponentId
// Parameter: ComponentFactoryMethod p_factoryMethod
// Returns:   void
// 
// Registers a component factory for a given component Id.
//------------------------------------------------------------------------------
void GameObjectManager::RegisterComponentFactory(const std::string& p_strComponentId, ComponentFactoryMethod p_factoryMethod)
{
	ComponentFactoryMap::iterator it = m_mComponentFactoryMap.find(p_strComponentId);
	if (it != m_mComponentFactoryMap.end())
	{
		return; // Already registered
	}

	// Insert it
	m_mComponentFactoryMap.insert(std::pair<std::string, ComponentFactoryMethod>(p_strComponentId, p_factoryMethod));
}

//------------------------------------------------------------------------------
// Method:    Update
// Parameter: float p_fDelta
// Returns:   void
// 
// Updates all the GameObjects*
//------------------------------------------------------------------------------
void GameObjectManager::Update(float p_fDelta)
{
	GameObject* pGO = NULL;
	GameObjectMap::iterator it = m_mGOMap.begin(), end = m_mGOMap.end();
	for (; it != end; ++it)
	{
		pGO = (GameObject*)it->second;

		// Only update if the state is active
		int iState = pGO->GetState();

		if (iState)
		{
			if (!m_mStateMachine->IsSuspended(iState))
			{
				pGO->Update(p_fDelta);
			}
		}
		else
		{
			pGO->Update(p_fDelta);
		}
	}

	// CheckCollisions();
}

//------------------------------------------------------------------------------
// Method:    SyncTransforms
// Returns:   void
// 
// Syncs the GameObject transform to the renderable component (if one 
// exists for the given GameObject).
//------------------------------------------------------------------------------
void GameObjectManager::SyncTransforms()
{
	GameObject* pGO = NULL;
	GameObjectMap::iterator it = m_mGOMap.begin(), end = m_mGOMap.end();
	for (; it != end; ++it)
	{
		pGO = (GameObject*)it->second;
	
		// If this GO has a Renderable component, sync it's transform from the parent GO
		Common::ComponentBase* pComponent = pGO->GetComponent("GOC_Renderable");
		if (pComponent)
		{
			ComponentRenderable* pRenderable = static_cast<ComponentRenderable*>(pComponent);
			pRenderable->SyncTransform();
		}
	}
}


//------------------------------------------------------------------------------
// Method:    InitDelegates
// Parameter: void
// Returns:   void
// 
// Registers events for the GOManager
//------------------------------------------------------------------------------
void GameObjectManager::InitDelegates()
{
	EventDelegate* pDelegate = new EventDelegate("GOC_ObjectDisappeared", ObjectDisappearedEvent::s_EventType, std::tr1::bind(&GameObjectManager::RemoveGOEvent, this, std::tr1::placeholders::_1));

	// Assign them
	EventManager::Instance()->AddListener(ObjectDisappearedEvent::s_EventType, pDelegate);
}

//------------------------------------------------------------------------------
// Method:    RemoveGOEvent
// Parameter: const EventDelegate::EventDataPointer& p_pEventData
// Returns:   void
// 
// Event for deleting objects
//------------------------------------------------------------------------------
void GameObjectManager::RemoveGOEvent(const EventDelegate::EventDataPointer& p_pEventData)
{
	std::shared_ptr<ObjectDisappearedEvent> pCastCoin = std::static_pointer_cast<ObjectDisappearedEvent>(p_pEventData);
	DestroyGameObject(pCastCoin->GetGO());
}


//------------------------------------------------------------------------------
// Method:    AddCollidable
// Parameter: GameObject* p_pCollidable
// Returns:   void
// 
// Adds a collidable object to the collidables map
//------------------------------------------------------------------------------
void GameObjectManager::AddCollidable(GameObject* p_pCollidable)
{
	// m_lCollidable.push_back(new Collidable(p_pCollidable->GetGUID(), p_pCollidable));
	m_mCollidable[p_pCollidable->GetGUID()] = p_pCollidable;
}

//------------------------------------------------------------------------------
// Method:    RemoveCollidable
// Parameter: const std::string& p_strGUID
// Returns:   void
// 
// Removes a collidable object from the collidables map
//------------------------------------------------------------------------------
void GameObjectManager::RemoveCollidable(const std::string& p_strGUID)
{
	GameObjectMap::iterator it = m_mCollidable.find(p_strGUID);

	if (it != m_mCollidable.end())
	{
		m_mCollidable.erase(it);
	}

}

/*
//------------------------------------------------------------------------------
// Method:	  CheckCollisions
// Parameter: 
// Returns:   void
// 
// Iterates over the map and queues collision events
//------------------------------------------------------------------------------
void GameObjectManager::CheckCollisions()
{
	
	// Only iterate if there are 2+ Collidables
	if (m_mCollidable.size() > 1)
	{
		// Weird stuff to get the list of keys so I can iterate over and not double comparisons (Had iterator issues with other methods)
		std::vector<std::string> keys = std::vector<std::string>();

		auto itBegin = m_mCollidable.begin();
		auto itEnd = m_mCollidable.end();

		for (; itBegin != itEnd; ++itBegin)
		{
			keys.push_back(itBegin->first);
		}

		// Actually iterate over the objects now
		for (int i = 0; i < keys.size() - 1; i++)
		{
			for (int j = i + 1; j < keys.size(); j++)
			{
				// Get the two objects
				GameObject* pObject1 = m_mCollidable[keys[i]];
				GameObject* pObject2 = m_mCollidable[keys[j]];

				//// Get the bounding spheres (This should be a more general collision object, but only spheres for now)
				Assignment2::ComponentBoundingSphere* pSphere1 = static_cast<Assignment2::ComponentBoundingSphere*>(pObject1->GetComponent("GOC_BoundingShape"));
				Assignment2::ComponentBoundingSphere* pSphere2 = static_cast<Assignment2::ComponentBoundingSphere*>(pObject2->GetComponent("GOC_BoundingShape"));

				//// Positions
				glm::vec3 position1 = pObject1->GetTransform().GetTranslation();
				glm::vec3 position2 = pObject2->GetTransform().GetTranslation();

				float distance = glm::distance(position1, position2);

				if (distance < pSphere1->GetRadius() + pSphere2->GetRadius())
				{
					//  New collision event
					std::shared_ptr<Common::CollisionEvent> pCollisionEvent(new Common::CollisionEvent(pObject1, pObject2));
					Common::EventManager::Instance()->QueueEvent(pCollisionEvent);
				}
			}
		}
		
	}	
	
}
*/

// Retrieves GO with given tag
std::vector<GameObject*> GameObjectManager::GetObjectsWithTag(std::string p_strTag)
{

	std::vector<GameObject*> objectsToReturn;


	// Go through the map
	GameObjectMap::iterator it = m_mGOMap.begin(), end = m_mGOMap.end();
	for (; it != end; ++it)
	{
		GameObject* pGO = (GameObject*)it->second;

		if (pGO->HasTag(p_strTag))
		{
			objectsToReturn.push_back(pGO);
		}
	}

	return objectsToReturn;

}

// Queue objects to be deleted at end of frame
void GameObjectManager::QueueGOForDeletion(GameObject* pGO)
{	
	m_lObjectsToDelete.push_back(pGO);
}


// Called at end of frame, destroys any objects in the queue
void GameObjectManager::DeleteQueuedGO()
{
	for (int i = 0; i < m_lObjectsToDelete.size(); i++)
	{
		DestroyGameObject(m_lObjectsToDelete.at(i));
	}

	m_lObjectsToDelete.clear();
}
