//------------------------------------------------------------------------
// HeliGame
//
// Created:	2012/12/06
// Author:	Carel Boers
//	
// Main game class. Derives from Common::Game.
// Altered by Matthew Champion for CSC 483
//------------------------------------------------------------------------

#ifndef HELIGAME_H
#define HELIGAME_H

#include "Game.h"
#include "GameObjectManager.h"
#include "TerrainManager.h"
#include "SceneManager.h"
#include "EventManager.h"
#include "CollisionManager.h"
#include "StateMachine.h"
#include "SceneCamera.h"
#include <list>
#include "LevelInfo.h"
#include "SpawnTracker.h"
#include "EffectTracker.h"
#include "ComponentDragCamera.h"

// Random health bar testing
#include "HealthBar.h"

class HeliGame : public Common::Game
{
	public:
		//------------------------------------------------------------------------------
		// Public methods.
		//------------------------------------------------------------------------------
		HeliGame();
		virtual ~HeliGame();

		// Static instance
		static HeliGame* GetInstance() { return s_pInstance; }
		Common::GameObjectManager* GameObjectManager() { return m_pGameObjectManager; }

	protected:	
		//------------------------------------------------------------------------------
		// Protected methods.
		//------------------------------------------------------------------------------
		virtual bool Init();
		virtual bool Update(float p_fDelta);
		virtual void Render();
		virtual void Shutdown();
		virtual void RegisterFactories();

	private:
		//------------------------------------------------------------------------------
		// Private members.
		//------------------------------------------------------------------------------

		// Static instance for easy export to Lua
		static HeliGame* s_pInstance;

		// A camera for the scene
		Common::SceneCamera* m_pSceneCamera;
		
		// Game Object Manager
		Common::GameObjectManager* m_pGameObjectManager;

		// State Machine
		Common::StateMachine* m_pStateMachine;

		// Level information
		LevelInfo* levelInfo;

		// Spawn/Effect tracker
		SpawnTracker* spawnTracker;
		EffectTracker* effectTracker;

		// Health bar
		HealthBar* healthBar;

		DirectionalLight* m_pDirLight;

		// Debug camera testing
		void UpdateCamera();
};


#endif // HELIGAME_H
