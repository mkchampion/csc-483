//------------------------------------------------------------------------
// ComponentCharacterController
//
// Class represents a projectile component, projectile gives an object a speed that it flies with once given a direction
//------------------------------------------------------------------------

#include "ComponentProjectile.h"
#include "GameObject.h"
#include "EventManager.h"
#include "ObjectDisappearedEvent.h"
#include "DynamicCollisionEvent.h"
#include "StaticCollisionEvent.h"
#include <glm/gtx/vector_angle.hpp>

// Count for projectiles
static int projectileCount = 0;

//------------------------------------------------------------------------------
// Method:    ComponentCharacterController
// Returns:   
// 
// Constructor.
//------------------------------------------------------------------------------
ComponentProjectile::ComponentProjectile()
{
	// Defaults of 0
	speed = 0.0f;
	timeActive = 0.0f;
	lifespan = 0.0f;

	// Default direction of -Z
	direction = glm::vec3(0.0f, 0.0f, -1.0f);
	
}

// Init
void ComponentProjectile::Init(const float lifespanP, const float heightP, bool hasCustomHeightP, bool homingP)
{
	speed = 0.0f;
	lifespan = lifespanP;
	projectileHeight = heightP;
	customHeight = hasCustomHeightP;
	damageDealt = 0;
	homing = homingP;

}

//------------------------------------------------------------------------------
// Method:    ~ComponentCharacterController
// Returns:   
// 
// Destructor.
//------------------------------------------------------------------------------
ComponentProjectile::~ComponentProjectile()
{
	EventManager::Instance()->QueueRemoveListener(StaticCollisionEvent::s_EventType, staticDelegate);
}

// Init for the delegates
void ComponentProjectile::InitDelegates()
{
	Common::GameObject* pGameObject = this->GetGameObject();

	std::string delegateStatic = "GOC_ProjectileStatic#" + std::to_string(projectileCount);
	staticDelegate = new EventDelegate(delegateStatic, DynamicCollisionEvent::s_EventType, std::tr1::bind(&ComponentProjectile::HandleStaticCollisions, this, std::tr1::placeholders::_1));

	// Add to the projectile count so there is different listeners
	projectileCount++;

	EventManager::Instance()->AddListener(StaticCollisionEvent::s_EventType, staticDelegate);
	
}

//------------------------------------------------------------------------------
// Method:    HandleStaticCollisions
// Parameter: p_CollisionEvent
// Returns:   void
// 
// Event for the projectile colliding with static objects in the level
//------------------------------------------------------------------------------
void ComponentProjectile::HandleStaticCollisions(EventDelegate::EventDataPointer p_CollisionEvent)
{
	Common::GameObject*  GO = this->GetGameObject();

	// Cast the event
	std::shared_ptr<StaticCollisionEvent> pCastCollision = std::static_pointer_cast<StaticCollisionEvent>(p_CollisionEvent);

	Common::GameObjectManager* pGOManager = pCastCollision->GetObject()->GetManager();

	// Bullet has collided with a static object, delete the bullet
	if (pCastCollision->GetObject() == this->GetGameObject())
	{
		pGOManager->DestroyGameObject(this->GetGameObject());
	}

}

//------------------------------------------------------------------------------
// Method:    Update
// Parameter: float p_fDelta
// Returns:   void
// 
// Polls input and sends movement instructions to relevant sibling components.
//------------------------------------------------------------------------------
void ComponentProjectile::Update(float p_fDelta)
{

	// Translate in the given direction at the given speed
	glm::vec3 currentPos = this->GetGameObject()->GetTransform().GetTranslation();
	
	// Adjust height if customly set
	if (customHeight)
	{
		currentPos.y = projectileHeight;
	}

	// Straight path / Homing
	if (!homing)
	{
		this->GetGameObject()->GetTransform().SetTranslation(currentPos + (direction * speed));
	}
	else
	{
		if (target != NULL)
		{

			// Get the difference between positions
			glm::vec3 targetPos = target->GetTransform().GetTranslation();
			glm::vec3 pos = this->GetGameObject()->GetTransform().GetTranslation();


			glm::vec3 targetDir = targetPos - pos;
			targetDir.y = 0.0f;
			targetDir = glm::normalize(targetDir);
			

			// Slowly adjust to the desired direction
			float fAngle = glm::orientedAngle(glm::vec3(0.0f, 0.0f, 1.0f), targetDir, glm::vec3(0.0f, 1.0f, 0.0f));

			// Adjust it to be 0-360 for ease of use
			if (fAngle < 0)
			{
				fAngle += 360;
			}

			// Do some calculations to figure out if I should turn left / right to reach my destination
			float currentAngle = glm::orientedAngle(glm::vec3(0.0f, 0.0f, 1.0f), direction, glm::vec3(0.0f, 1.0f, 0.0f));
			if (currentAngle < 0)
			{
				currentAngle += 360;
			}
			float angleDiff = fAngle - currentAngle;

			float turnSpeed = 280.0f * p_fDelta;
			float result = 0.0f;

			// Two directions where I don't have to loop over 0
			if (angleDiff >= -180.0f && angleDiff < 0.0f)
			{
				if (currentAngle - turnSpeed > fAngle)
					result = (currentAngle - turnSpeed);
				else
					result = (fAngle);
			}
			else if (angleDiff >= 0.0f && angleDiff < 180.0f)
			{
				if (currentAngle + turnSpeed < fAngle)
					result = (currentAngle + turnSpeed);
				else
					result = (fAngle);
			}

			// Two directions that loop over 0
			else if (angleDiff >= -360.0f && angleDiff < -180.0f)
			{
				if (currentAngle + turnSpeed < 360.0f)
				{
					result = (currentAngle + turnSpeed);
				}
				else
				{
					float newAngle = currentAngle - 360.0f + turnSpeed;

					if (newAngle > fAngle)
						result = (fAngle);
					else
						result = (newAngle);
				}
			}

			else if (angleDiff >= 180.0f && angleDiff <= 360.0f)
			{
				if (currentAngle - turnSpeed > 0.0f)
				{
					result = (currentAngle - turnSpeed);
				}
				else
				{
					float newAngle = 360.0f - currentAngle - turnSpeed;

					if (newAngle < fAngle)
						result = (fAngle);
					else
						result = (newAngle);
				}
			}
			
			// Rotate a vec3 by result
			glm::vec3 newDirection = glm::rotateY(glm::vec3(0.0f, 0.0f, 1.0f), result);
			direction = glm::normalize(newDirection);

			this->GetGameObject()->SetDirection(result);

			// Travel along direction
			this->GetGameObject()->GetTransform().SetTranslation(currentPos + (direction * speed));

		}
		else
		{
			// Try to find target
			target = FindNearestEnemy();
			
			if (target == NULL && !setInitialDir)
			{
				float currentAngle = glm::orientedAngle(glm::vec3(0.0f, 0.0f, 1.0f), direction, glm::vec3(0.0f, 1.0f, 0.0f));

				if (currentAngle < 0)
				{
					currentAngle += 360.0f;
				}

				this->GetGameObject()->SetDirection(currentAngle);
				setInitialDir = true;

			}
			this->GetGameObject()->GetTransform().SetTranslation(currentPos + (direction * speed));
		}
	}
	
	// Lifespan
	timeActive += p_fDelta;
	if (timeActive > lifespan)
	{
		//  New started moving event
		if (!flaggedForRemove)
		{
			std::shared_ptr<ObjectDisappearedEvent> pProjectileDelete(new ObjectDisappearedEvent(this->GetGameObject(), this->GetGameObject()->GetTransform().GetTranslation()));
			EventManager::Instance()->QueueEvent(pProjectileDelete);
			flaggedForRemove = true;
		}

	}
}

Common::ComponentBase* ComponentProjectile::CreateComponent(TiXmlNode* p_pNode)
{
	assert(strcmp(p_pNode->Value(), "GOC_Projectile") == 0);
	ComponentProjectile* pProjectile = new ComponentProjectile();

	float lifespan = 0.0f;
	float projectileHeight = 0.0f;
	bool hasCustomHeight = false;
	int damage = 0;
	bool homingVal = false;

	// Iterate Anim elements in the XML
	TiXmlNode* pChildNode = p_pNode->FirstChild();
	while (pChildNode != NULL)
	{
		const char* szNodeName = pChildNode->Value();

		if (strcmp(szNodeName, "Lifespan") == 0)
		{

			// Parse attributes
			TiXmlElement* pElement = pChildNode->ToElement();

			// Speed of projectile
			if (pElement->QueryFloatAttribute("value", &lifespan) != TIXML_SUCCESS)
			{
				delete pProjectile;
				return NULL;
			}
		}

		else if (strcmp(szNodeName, "ProjectileHeight") == 0)
		{
			// Parse attributes
			TiXmlElement* pElement = pChildNode->ToElement();

			// Speed of projectile
			if (pElement->QueryFloatAttribute("value", &projectileHeight) != TIXML_SUCCESS)
			{
				delete pProjectile;
				return NULL;
			}

			hasCustomHeight = true;

		}

		else if (strcmp(szNodeName, "Homing") == 0)
		{
			// Parse attributes
			TiXmlElement* pElement = pChildNode->ToElement();

			// Speed of projectile
			if (pElement->QueryBoolAttribute("value", &homingVal) != TIXML_SUCCESS)
			{
				delete pProjectile;
				return NULL;
			}
		}
		

		pChildNode = pChildNode->NextSibling();
	}

	pProjectile->Init(lifespan, projectileHeight, hasCustomHeight, homingVal);
	pProjectile->InitDelegates();
	return pProjectile;
}


// Find the nearest enemy
Common::GameObject* ComponentProjectile::FindNearestEnemy()
{
	
	Common::GameObjectManager* pGOManager = this->GetGameObject()->GetManager();

	std::vector<Common::GameObject*> enemies = pGOManager->GetObjectsWithTag("Enemy");

	// Current
	Common::GameObject* closest = NULL;
	float distance = 9999999.9f;

	// Check for better
	for (int i = 0; i < enemies.size(); i++)
	{

		Common::GameObject* check = enemies.at(i);
		glm::vec3 targetPosition = check->GetTransform().GetTranslation();
		targetPosition.y = 0.0f;

		// Bullet position
		glm::vec3 position = this->GetGameObject()->GetTransform().GetTranslation();
		position.y = 0.0f;
		
		float distanceCheck = glm::distance(targetPosition, position);

		if (distanceCheck < distance || closest == NULL)
		{
			distance = distanceCheck;
			closest = check;
		}
	}

	return closest;

}