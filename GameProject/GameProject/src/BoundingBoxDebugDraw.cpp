//------------------------------------------------------------------------
// BoundingBoxDebugDraw
//	
// Debug draw for bounding boxes
//------------------------------------------------------------------------

#include "BoundingBoxDebugDraw.h"

#if DEBUG_BOXES

BoundingBoxDebugDraw* BoundingBoxDebugDraw::s_sBoxDrawer = NULL;

//------------------------------------------------------------------------------
// Creates the singletone instance.
//------------------------------------------------------------------------------
void BoundingBoxDebugDraw::CreateInstance(wolf::LineDrawer* p_pLineDrawer)
{
	assert(s_sBoxDrawer == NULL);
	s_sBoxDrawer = new BoundingBoxDebugDraw(p_pLineDrawer);	
}

//------------------------------------------------------------------------------
// Destroys the singleton instance.
//------------------------------------------------------------------------------
void BoundingBoxDebugDraw::DestroyInstance()
{
	assert(s_sBoxDrawer != NULL);
	delete s_sBoxDrawer;
	s_sBoxDrawer = NULL;
}

//------------------------------------------------------------------------------
// Access to singleton instance.
//------------------------------------------------------------------------------
BoundingBoxDebugDraw* BoundingBoxDebugDraw::Instance()
{
	assert(s_sBoxDrawer);
	return s_sBoxDrawer;
}

// Constructor
BoundingBoxDebugDraw::BoundingBoxDebugDraw(wolf::LineDrawer* p_pLineDrawer)
{
	m_pLineDrawer = p_pLineDrawer;
}

// Destructor
BoundingBoxDebugDraw::~BoundingBoxDebugDraw()
{
	delete m_pLineDrawer;
}

void BoundingBoxDebugDraw::Render(const glm::mat4& p_mView, const glm::mat4& p_mProj)
{
	if (m_bRender)
	{
		m_pLineDrawer->Render(p_mView, p_mProj);
	}
}

void BoundingBoxDebugDraw::AddLine(const glm::vec3& p_vTo, const glm::vec3& p_vFrom)
{
	m_pLineDrawer->AddLine(p_vTo, p_vFrom, wolf::Color4(1.0f, 1.0f, 1.0f, 1.0f));
}

#endif