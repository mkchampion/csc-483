//------------------------------------------------------------------------
// LevelInfo
//
// Created:	2015/10/26
// Author:	Matthew Champion
//	
// A class that contains all the information parsed from the TMX file used to generate levels
//------------------------------------------------------------------------

#ifndef LEVEL_INFO_H
#define LEVEL_INFO_H

#include <string>
#include "tinyxml.h"
#include <map>
#include <vector>
#include <glm\glm.hpp>

// Simple struct containing a top and bottom row bound
struct FlyBounds
{
	int id;
	int minRow;
	int maxRow;
};

// Struct for elevated area textures.  Normal texture and HeightMap
struct ElevatedTextures
{
	std::string heightMapPath;
	std::string texture1;
	std::string texture2;
	std::string texture3;
	std::string texture4;
	float mountainBreak1;
	float mountainBreak2;
};

// Struct to represent an object from the TMX file.
// Contains a type, x and y positions, and an ID number.
struct TMXObjectInfo
{
	// Constructor
	TMXObjectInfo(int pID, int pX, int pY, int pRotation, float pScale, const std::string& pType)
	{ 
		id = pID; 
		x = pX; 
		y = pY; 
		rotation = pRotation; 
		scale = pScale;
		type = pType;
	}

	~TMXObjectInfo() {};

	// Values
	int id = 0;
	int x = 0;
	int y = 0;
	int rotation = 0;
	float scale = 0.0f;
	std::string type = "";
	bool canCollide = false;
	float spawnHeight = 0.0f;

};

// Struct for any of the model info
struct ModelInfo
{
	ModelInfo(float pScale, const std::string& p_modelPath, const std::string& p_texturePath, const std::string& p_vertexShader, const std::string& p_fragmentShader)
	{
		scale = pScale;
		modelPath = p_modelPath;
		texturePath = p_texturePath;
		vertexShader = p_vertexShader;
		fragmentShader = p_fragmentShader;
	}

	float scale;
	std::string modelPath;
	std::string texturePath;
	std::string vertexShader;
	std::string fragmentShader;
};

// Level info
struct LevelInfo
{

	typedef std::vector<TMXObjectInfo*> ObjectList;

	// Path (In case it needs it again)
	std::string TMXPath = "";

	// Array to hold the tile IDs
	int** tileIDs;

	// Width / Heights
	int mapWidth = 0;
	int mapHeight = 0;

	int tileWidth = 0;
	int tileHeight = 0;

	// List of the spawned objects
	ObjectList *spawnedObjects;

	// List of the renderable objects
	ObjectList *staticObjects;
	ObjectList *weaponPickups;

	// Texture/GO Maps
	std::map<int, std::string> *textureMap;
	std::map<int, ElevatedTextures*> *heightMap;
	std::map<std::string, ModelInfo*> *modelMap;
	std::map<std::string, std::string> *GOMap;

	// List of the elevated texture ID's
	std::vector<int>* elevatedIDs;
	int elevationSeparator = 0;

	// Borders
	int borderX = 0;
	int borderZ = 0;

	// List of flybounds
	std::vector<FlyBounds*>* flyingZones;

	std::vector<glm::vec3> *checkPoints;

	// ===================================
	// Functions
	// ===================================

	// Constructor / Destructor
	LevelInfo(const std::string &p_strTMXFile);
	~LevelInfo();

	// Parsing the files
	void ParseTMX();
	void ParseTextureMap(const std::string &p_strTextureMap);
	void ParseModelMap(const std::string &p_strModelMap);
	void ParseGOMap(const std::string &p_strGOMap);
	void ParseElevatedID(const std::string &p_strElevated);
	void ParseHeightMap(const std::string &p_strHeightMap);

	void IdentifyFlyBounds();

private:
	int IdentifyNextElevated(int startRow);

};


#endif