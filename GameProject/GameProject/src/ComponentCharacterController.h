//------------------------------------------------------------------------
// ComponentCharacterController
//
// Created:	2012/12/14
// Author:	Carel Boers
//	
// This class implements a character controller. It polls input and sends
// movement instructions to the relevant sibling components.
//------------------------------------------------------------------------

#ifndef COMPNENTCHARACTERCONTROLLER_H
#define COMPNENTCHARACTERCONTROLLER_H

#include "ComponentBase.h"
#include <functional>
#include "tinyxml.h"
#include <math.h>
#include <vector>
#include "GameController.h"

class ComponentCharacterController : public Common::ComponentBase
{

	public:
		//------------------------------------------------------------------------------
		// Public types.
		//------------------------------------------------------------------------------

	public:
		//------------------------------------------------------------------------------
		// Public methods.
		//------------------------------------------------------------------------------
		ComponentCharacterController();
		virtual ~ComponentCharacterController();

		// Init
		void Init(const float maxVelocityP, const float accelerationP, const float decelerationP, const float maxTiltP, const float bottomLockDistanceP);

		// Factory
		static Common::ComponentBase* CreateComponent(TiXmlNode* p_pNode);

		virtual const std::string FamilyID() { return std::string("GOC_CharacterController"); }
		virtual const std::string ComponentID(){ return std::string("GOC_CharacterController"); }
		virtual void Update(float p_fDelta);

		// Get the velocity
		float GetVelocityX() const { return velocityX; }
		float GetVelocityZ() const { return velocityZ; }
		float GetMaxVelocity() const { return maxVelocity; }

	private:
		//------------------------------------------------------------------------------
		// Private members.
		//------------------------------------------------------------------------------

		// Max Velocity / Acceleration
		float maxVelocity;
		float acceleration;
		float deceleration;

		float velocityX;
		float velocityZ;

		// Rotation
		float maxTilt;

		float triggerReset;
		float resetTracking;

		// Game Controller
		GameController*  gamepad;

		// Bounds that it will grab from the terrain manager?
		float upperX;
		float lowerX;
		float upperZ;
		float lowerZ;

		float bottomLockDistance;

		bool weaponActive = false;

		// Release for button press
		bool startReleased = false;


	};

#endif // COMPNENTCHARACTERCONTROLLER_H

