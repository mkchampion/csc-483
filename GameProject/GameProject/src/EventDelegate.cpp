//------------------------------------------------------------------------
// EventDelegate
//	
// This class acts a container for a function delegate with some additional info for comparison
//------------------------------------------------------------------------

#include "EventDelegate.h"

//------------------------------------------------------------------------------
// Method:		EventDelegate
// Parameters:  const std::string& p_strEventHandler, const EventType& p_eEventType, const FunctionDelegate& p_pFunctionDelegate
// Returns:		
// 
// Constructor
//------------------------------------------------------------------------------
EventDelegate::EventDelegate(const std::string& p_strEventHandler, const EventType& p_eEventType, const FunctionDelegate& p_pFunctionDelegate)
{
	m_strEventHandler = p_strEventHandler;
	m_eEventType = p_eEventType;
	m_pFunctionDelegate = p_pFunctionDelegate;
}


//------------------------------------------------------------------------------
// Method:		IsEqual
// Parameters:  const EventDelegate* compareDelegate
// Returns:		bool
// 
// Determines if the EventDelegates are equal
// (Tried overriding ==, but that was having some issues)
//------------------------------------------------------------------------------
bool EventDelegate::IsEqual(const EventDelegate* compareDelegate) const
{

	// Checks
	if (m_eEventType != compareDelegate->GetType())
		return false;

	if (m_strEventHandler != compareDelegate->GetHandler())
		return false;

	return true;
}
