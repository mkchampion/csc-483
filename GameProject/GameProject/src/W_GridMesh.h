//-----------------------------------------------------------------------------
// File: W_GridMesh.h
//
// Represents a group of vertices for a sort of mesh grid
//-----------------------------------------------------------------------------
#ifndef W_GRIDMESH_H
#define W_GRIDMESH_H

#include <string.h>
#include "W_Texture.h"
#include "W_TextureManager.h"
#include "W_ProgramManager.h"
#include "W_BufferManager.h"
#include "W_VertexBuffer.h"
#include "W_VertexDeclaration.h"
#include "W_MaterialManager.h"
struct ElevatedTextures;

namespace wolf
{

	// Struct for the quad
	struct MeshVertex
	{
		GLfloat x, y, z;	   // Position
		GLfloat u, v;		   // Texture coordinates
		GLfloat heightU, heightV;    // Height map coordinates
		GLfloat nx, ny, nz;	   // Normal
	};


	class GridMesh
	{

	public:

		// Constructor/Destructor
		GridMesh(const std::string& p_strMaterialName, const ElevatedTextures* textureBreakInfo, const std::string& p_strVertexProgram, const std::string& p_strFragmentProgram);
		~GridMesh();

		// Update / Render
		void Update(float p_fDelta);
		void Render(const glm::mat4& p_mView, const glm::mat4& p_mProj, const glm::mat4& p_mLight);

		// Transform setter (Same as model)
		void SetTransform(const glm::mat4& p_mWorldTransform) { m_mWorldTransform = p_mWorldTransform; }

		// Material accessor
		wolf::Material* GetMaterial() { return m_pMaterial; }

		// Adding a row
		void CreateMesh(int quadsPerTile, float tileSize, int numXTiles, int numZTiles);
		void AdjustMesh(const std::string& heightMap);

		// Method to create/declare the vertex
		bool DeclareVertexBuffer();


	private:

		// Vertex buffer and Vertex declaration to use
		wolf::VertexBuffer* m_pVertexBuffer;
		wolf::IndexBuffer* m_pIndexBuffer;
		wolf::VertexDeclaration* m_pVertexDeclaration;
		wolf::Program* m_pProgram;

		// Textures to use
		wolf::Texture* m_pTexture1;
		wolf::Texture* m_pTexture2;
		wolf::Texture* m_pTexture3;
		wolf::Texture* m_pTexture4;

		// Going to try and use materials 
		wolf::Material* m_pMaterial;

		// Transform
		glm::mat4 m_mWorldTransform;

		// Vectors for vertices / indices
		std::vector<MeshVertex> meshVertices;
		std::vector<GLuint> meshIndices;

		// Size of the mesh
		int numVertsX, numVertsZ;

		int vertexOffset = 0;
	};

}

#endif