//------------------------------------------------------------------------
// ComponentAIController
//
// Created:	2012/12/14
// Author:	Carel Boers
//	
// This class implements an AI controller. It loads a decision tree from
// XML and processes the game state to decide it's best state.
//------------------------------------------------------------------------

#include "W_Model.h"
#include "ComponentAIController.h"
#include "ComponentRenderableMesh.h"
#include "GameObject.h"
#include "PathFinder.h"
#include "ObjectDisappearedEvent.h"
#include "EventManager.h"

// AI States
#include "AIStateIdle.h"
#include "AIStateTracking.h"
#include "AIStateChasing.h"
#include "AIStateChasingTracking.h"

// For OutputDebugString
#include "windows.h"

float ComponentAIController::DECISION_RESET = 0.5f;

//------------------------------------------------------------------------------
// Method:    ComponentAIController
// Returns:   
// 
// Constructor.
//------------------------------------------------------------------------------
ComponentAIController::ComponentAIController()
	:
	m_pDecisionTree(NULL),
	m_pStateMachine(NULL)
{
	decisionTracking = 0.0f;
}

//------------------------------------------------------------------------------
// Method:    ~ComponentAIController
// Returns:   
// 
// Destructor.
//------------------------------------------------------------------------------
ComponentAIController::~ComponentAIController()
{
	if (m_pDecisionTree)
	{
		delete m_pDecisionTree;
		m_pDecisionTree = NULL;
	}

	if (m_pStateMachine)
	{
		m_pStateMachine->SetStateMachineOwner(NULL);
		delete m_pStateMachine;
		m_pStateMachine = NULL;
	}
}

//------------------------------------------------------------------------------
// Method:    Update
// Parameter: float p_fDelta
// Returns:   void
// 
// Polls input and sends movement instructions to relevant sibling components.
//------------------------------------------------------------------------------
void ComponentAIController::Update(float p_fDelta)
{
	
	// Add the update to the time tracking
	decisionTracking += p_fDelta;

	// Add to the tracking time
	if (decisionTracking > DECISION_RESET)
	{
		const AIActionNode* bestAction = m_pDecisionTree->Decide();

		char buff[128];
		sprintf(buff, "ACTION = [%s]\n", bestAction->GetAction().c_str());
		OutputDebugString(buff);

		// Do we need to switch states?
		AIState eBestState = this->MapActionToState(bestAction->GetAction());
		if (m_pStateMachine->GetCurrentState() != eBestState)
		{
			m_pStateMachine->GoToState(eBestState, bestAction->GetData());
		}

		decisionTracking = 0.0f;

	}

	m_pStateMachine->Update(p_fDelta);


	// Updating the AI's node for Dynamic Avoidance
	if (!m_pOccupiedNode)
	{
		m_pOccupiedNode = PathFinder::Instance()->grid->GetClosestNode((this->GetGameObject()->GetTransform().GetTranslation()));
		m_pOccupiedNode->occupiedCost = 10000;
		m_pOccupiedNode->occupiedFlag = true;
	}
	else
	{
		PathNode* nodeCheck = PathFinder::Instance()->grid->GetClosestNode(this->GetGameObject()->GetTransform().GetTranslation());

		// Check for a change in the occupied node
		if (m_pOccupiedNode != nodeCheck)
		{
			// Reset occupied cost and update the node.
			m_pOccupiedNode->occupiedCost = 0;
			m_pOccupiedNode->occupiedFlag = false;
			m_pOccupiedNode = nodeCheck;

			// Update the new nodes cost
			nodeCheck->occupiedCost = 10000;
			nodeCheck->occupiedFlag = true;
		}
	}

	// Check if the AI is far enough away from the player to delete (I'd say it's fine to define it as Player here, not the target)
	Common::GameObjectManager* pGOManager = this->GetGameObject()->GetManager();

	if (pGOManager->GetGameObject("Player"))
	{
		Common::GameObject* player = this->GetGameObject()->GetManager()->GetGameObject("Player");

		// Get the transform / distance
		glm::vec3 playerPos = player->GetTransform().GetTranslation();
		glm::vec3 currentPos = this->GetGameObject()->GetTransform().GetTranslation();

		float zDifference = currentPos.z - playerPos.z;

		if (zDifference > despawnDistance)
		{
			std::shared_ptr<ObjectDisappearedEvent> pEnemyDelete(new ObjectDisappearedEvent(this->GetGameObject(), this->GetGameObject()->GetTransform().GetTranslation()));
			EventManager::Instance()->QueueEvent(pEnemyDelete);
		}
	}

}

//------------------------------------------------------------------------------
// Method:    Init
// Parameter: const char * p_strDecisionTreePath
// Returns:   void
// 
// Initializes the AI controller using a decision tree defined in XML.
//------------------------------------------------------------------------------
void ComponentAIController::Init(const char* p_strDecisionTreePath, float despawnDistanceP, EnemyPathType enemyTypeP)
{
	// Create our decision tree to manage which states we go into
	m_pDecisionTree = new AIDecisionTree(this);
	m_pDecisionTree->Load(p_strDecisionTreePath);

	despawnDistance = despawnDistanceP;
	m_enemyType = enemyTypeP;

	// Initialize the StateMachine and supported states
	m_pStateMachine = new Common::StateMachine();
	m_pStateMachine->SetStateMachineOwner(this);
	m_pStateMachine->RegisterState(eAIState_Idle, new AIStateIdle());
	m_pStateMachine->RegisterState(eAIState_Tracking, new AIStateTracking());
	m_pStateMachine->RegisterState(eAIState_Chasing, new AIStateChasing());
	m_pStateMachine->RegisterState(eAIState_ChasingTracking, new AIStateChasingTracking());
}

//------------------------------------------------------------------------------
// Method:    CreateComponent
// Parameter: TiXmlNode * p_pNode
// Returns:   Common::ComponentBase*
// 
// Factory construction method. Returns an initialized ComponentAnimController
// instance if the passed in XML is valid, NULL otherwise.
//------------------------------------------------------------------------------
Common::ComponentBase* ComponentAIController::CreateComponent(TiXmlNode* p_pNode)
{
	assert(strcmp(p_pNode->Value(), "GOC_AIController") == 0);
	ComponentAIController* pAI = new ComponentAIController();

	// DecisionTree path
	std::string decisionTree = "";
	float despawnDistance = 9999.9f;
	std::string pathType = "";

	// Iterate the xml elements
	TiXmlNode* pChildNode = p_pNode->FirstChild();
	while (pChildNode != NULL)
	{
		const char* szNodeName = pChildNode->Value();

		if (strcmp(szNodeName, "DecisionTree") == 0)
		{

			// Parse attributes
			TiXmlElement* pElement = pChildNode->ToElement();

			// Name
			const char* szName = pElement->Attribute("path");
			if (szName == NULL)
			{
				delete pAI;
				return NULL;
			}

			decisionTree = szName;

		}

		else if (strcmp(szNodeName, "DespawnDistance") == 0)
		{

			// Parse attributes
			TiXmlElement* pElement = pChildNode->ToElement();

			// Speed of projectile
			if (pElement->QueryFloatAttribute("value", &despawnDistance) != TIXML_SUCCESS)
			{
				delete pAI;
				return NULL;
			}

		}

		else if (strcmp(szNodeName, "PathType") == 0)
		{

			// Parse attributes
			TiXmlElement* pElement = pChildNode->ToElement();

			// Name
			const char* szName = pElement->Attribute("value");
			if (szName == NULL)
			{
				delete pAI;
				return NULL;
			}

			pathType = szName;

		}

		pChildNode = pChildNode->NextSibling();

	}

	// Convert path type

	pAI->Init(decisionTree.c_str(), despawnDistance, MapStringToEnemyType(pathType));
	return pAI;
}