//------------------------------------------------------------------------
// ObjectDisappearedEvent
//
// An event that is triggered when objects disappear
//------------------------------------------------------------------------

#ifndef OBJECT_DISAPPEARED_H
#define OBJECT_DISAPPEARED_H

#include "BaseEventData.h"
#include "GameObject.h"


class ObjectDisappearedEvent : public BaseEventData
{

	public:

		// Static event type
		static const EventType s_EventType;

		// Constructor / Destructor
		explicit ObjectDisappearedEvent(Common::GameObject* p_pGO, const glm::vec3& p_vPos) : m_pGO(p_pGO), m_vPos(p_vPos) {}
		~ObjectDisappearedEvent() {};

		// Accessor for event type
		const EventType& GetEventType() const { return s_EventType; }

		// Acccessor for coin / position
		Common::GameObject* GetGO() const { return m_pGO; }
		glm::vec3 GetPosition() const { return m_vPos; }	

	private:

		Common::GameObject* m_pGO;
		glm::vec3 m_vPos;

	};

#endif

