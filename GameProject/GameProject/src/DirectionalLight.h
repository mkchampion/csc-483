//-----------------------------------------------------------------------------
// DirectionalLight
//
//  Weapon that acts as a cone (Effectivly, a flame thrower weapon)
//-----------------------------------------------------------------------------

#ifndef DIRECTIONALLIGHT_H
#define DIRECTIONALLIGHT_H

#include <glm\glm.hpp>
#include "SceneCamera.h"

extern bool lightCam;

struct DirectionalLight
{

	DirectionalLight();
	~DirectionalLight() {};

	// Light Info
	glm::vec3 lightDir = glm::vec3(1.0f, 1.0f, 0.0f);
	glm::vec4 lightColor = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
	glm::vec4 ambientColor = glm::vec4(0.12f, 0.12f, 0.12f, 1.0f);

	// Camera Info?
	glm::vec3 lightPos;
	glm::vec3 targetPos;
	glm::vec3 up;

	// Offsets
	float offsetX;
	float offsetY;
	float offsetZ = 0.0f;

	float targetOffsetX;
	float targetOffsetY;
	float targetOffsetZ = 0.0f;

	// Sizing
	float width;
	float height;

	glm::mat4 proj;
	glm::mat4 view;
	glm::mat4 bias;
	glm::mat4 lightMatrix;

	bool isProjDirty = true;
	bool isViewDirty = true;
	bool isLightMatrixDirty = true;

	// Target we'll use is the other camera (With some offsets)
	Common::SceneCamera* m_pCameraTarget;

	// Update
	void Update();

	void SetCameraTarget(Common::SceneCamera* p_pCameraTarget) { m_pCameraTarget = p_pCameraTarget;	}

	// Get View / Projection
	const glm::mat4& GetLightMatrix();
	const glm::mat4& GetProjectionMatrix();
	const glm::mat4& GetViewMatrix();
	const glm::mat4& GetBiasMatrix() const { return bias; }

};

#endif