//------------------------------------------------------------------------
// ComponentWeapon
//
// Describes the weapon that the GO is using
//------------------------------------------------------------------------

#include "ComponentWeapon.h"
#include "ProjectileWeapon.h"
#include "ConeWeapon.h"

static int weaponCount = 0;

// Constructor
ComponentWeapon::ComponentWeapon()
{
}

// Destructor
ComponentWeapon::~ComponentWeapon()
{
	// Delete the map for ModelInfo
	for (auto x : m_weaponList)
	{
		delete x.second;
	}
}

// Init
void ComponentWeapon::Init(const std::string& projectilePathP, float fireRateP, float speedP, int damageP, std::map<std::string, IWeapon*> weaponsP)
{
	projectilePath = projectilePathP;
	fireRate = fireRateP;
	speed = speedP;
	damage = damageP;
	m_weaponList = weaponsP;

	// Set the active weapon to the first
	m_activeWeapon = m_weaponList.at("Default");
}

// Update
void ComponentWeapon::Update(float p_fDelta)
{
}

// Set the active weapon
void ComponentWeapon::SetActiveWeapon(const std::string& weaponNameP)
{

	// Tell the current weapon to stop if it needs to?
	m_activeWeapon->StopWeapon();

	std::map<std::string, IWeapon*>::iterator it = m_weaponList.find(weaponNameP);
	if (it != m_weaponList.end())
	{
		m_activeWeapon = it->second;
		Common::GameObject* testPlease = this->GetGameObject();
		m_activeWeapon->SetTarget(this->GetGameObject());
	}
}

// Parsing
Common::ComponentBase* ComponentWeapon::CreateComponent(TiXmlNode* p_pNode)
{
	assert(strcmp(p_pNode->Value(), "GOC_Weapon") == 0);
	ComponentWeapon* pWeapon = new ComponentWeapon();

	std::map<std::string, IWeapon*> weapons;

	std::string projectilePath = "";
	float fireRate = 0.0f;
	float speedValue = 0.0f;
	int damageValue = 0;

	// Iterate Anim elements in the XML
	TiXmlNode* pChildNode = p_pNode->FirstChild();
	while (pChildNode != NULL)
	{
		const char* szNodeName = pChildNode->Value();

		
		// Parse attributes
		TiXmlElement* pElement = pChildNode->ToElement();

		// Name
		const char* szName = pElement->Attribute("name");
		if (szName == NULL)
		{

			delete pWeapon;
			return NULL;
		}
		std::string weaponName = szName;

		// Name
		const char* szPath = pElement->Attribute("path");
		if (szPath == NULL)
		{

			delete pWeapon;
			return NULL;
		}			

		std::string path = szPath;		

		if (strcmp(szNodeName, "ProjectileWeapon") == 0)
		{
			ProjectileWeapon* projectileWeapon = new ProjectileWeapon(path);
			weapons.insert(std::pair<std::string, IWeapon*>(weaponName, (IWeapon*)projectileWeapon));
		}
		else if (strcmp(szNodeName, "ConeWeapon") == 0)
		{
			ConeWeapon* coneWeapon = new ConeWeapon(path);
			weapons.insert(std::pair<std::string, IWeapon*>(weaponName, (IWeapon*)coneWeapon));
		}


		pChildNode = pChildNode->NextSibling();
	}

	pWeapon->Init(projectilePath, fireRate, speedValue, damageValue, weapons);
	return pWeapon;
}
