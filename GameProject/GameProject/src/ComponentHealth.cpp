//------------------------------------------------------------------------
// ComponentHealth
//	
// A Component that represents health values and response to projectile collisions
//------------------------------------------------------------------------

#include "ComponentHealth.h"
#include "EventManager.h"
#include "DynamicCollisionEvent.h"
#include "ConeWeaponEvent.h"
#include "ComponentProjectile.h"
#include "PlayerDamagedEvent.h"
#include "ComponentRenderableMesh.h"
#include "SoundManager.h"
#include "ExplosionInfo.h"
#include "CollisionManager.h"

static int healthCount = 0;

// Constructor
ComponentHealth::ComponentHealth()
{
}

// Destructor
ComponentHealth::~ComponentHealth()
{
	EventManager::Instance()->QueueRemoveListener(DynamicCollisionEvent::s_EventType, dynamicCollisionDelegate);
	EventManager::Instance()->QueueRemoveListener(ConeWeaponEvent::s_EventType, coneCollisionDelegate);
}

// Init
void ComponentHealth::Init(float healthValueP, bool blinksDamageP)
{
	totalHealth = healthValueP;
	currentHealth = healthValueP;
	blinkDamage = blinksDamageP;
}

// Init for the delegates
void ComponentHealth::InitDelegates()
{
	Common::GameObject* pGameObject = this->GetGameObject();

	// Create the EventDelegates
	std::string delegateDynamic = "GOC_HealthDynamic#" + std::to_string(healthCount);
	dynamicCollisionDelegate = new EventDelegate(delegateDynamic, DynamicCollisionEvent::s_EventType, std::tr1::bind(&ComponentHealth::HandleDynamicCollisions, this, std::tr1::placeholders::_1));

	std::string delegateCone = "GOC_HealthCone#" + std::to_string(healthCount);
	coneCollisionDelegate = new EventDelegate(delegateCone, ConeWeaponEvent::s_EventType, std::tr1::bind(&ComponentHealth::HandleConeWeaponCollision, this, std::tr1::placeholders::_1));

	// Increment counter for health components
	healthCount++;

	EventManager::Instance()->AddListener(DynamicCollisionEvent::s_EventType, dynamicCollisionDelegate);
	EventManager::Instance()->AddListener(ConeWeaponEvent::s_EventType, coneCollisionDelegate);

}

// Update
void ComponentHealth::Update(float p_fDelta)
{
	
	// Ease off the mask
	if (blinkDamage)
	{

		maskColor += 1.0f * p_fDelta;

		if (maskColor > 1.0f)
			maskColor = 1.0f;
		
		ComponentRenderableMesh* renderableComponent = static_cast<ComponentRenderableMesh*>(this->GetGameObject()->GetComponent("GOC_Renderable"));
		renderableComponent->SetColorMask(glm::vec4(1.0f, maskColor, maskColor, 1.0f));

	}

}

// Handling dynamic collisions (Projectiles)
void ComponentHealth::HandleDynamicCollisions(EventDelegate::EventDataPointer p_pCollisionData)
{

	// Cast the event
	std::shared_ptr<DynamicCollisionEvent> pCastCollision = std::static_pointer_cast<DynamicCollisionEvent>(p_pCollisionData);

	Common::GameObjectManager* pGOManager = pCastCollision->GetManager();

	Common::GameObject* g1 = pGOManager->GetGameObject(pCastCollision->GetObject1ID());
	Common::GameObject* g2 = pGOManager->GetGameObject(pCastCollision->GetObject2ID());

	if (g1 == NULL || g2 == NULL)
	{
		return;
	}

	// Bullet has collided with some dynamic object
	if (g1 == this->GetGameObject())
	{
		// Currently, only caring about enemies
		if (g2->HasTag("Projectile"))
		{
			HandleProjectile(g2);
		}
	}

	// Bullet has collided with some dynamic object
	else if (g2 == this->GetGameObject())
	{
		// Currently, only caring about enemies
		if (g1->HasTag("Projectile"))
		{
			HandleProjectile(g1);
		}
	}	

}

// Handling the cone weapon
void ComponentHealth::HandleConeWeaponCollision(EventDelegate::EventDataPointer p_pCollisionData)
{

	// Cast the event
	std::shared_ptr<ConeWeaponEvent> pCastCollision = std::static_pointer_cast<ConeWeaponEvent>(p_pCollisionData);

	Common::GameObjectManager* pGOManager = pCastCollision->GetManager();

	Common::GameObject* g1 = pGOManager->GetGameObject(pCastCollision->GetObject1ID());

	if (g1 == NULL)
	{
		return;
	}

	// Bullet has collided with some dynamic object
	if (g1 == this->GetGameObject())
	{
		HandleConeWeapon(pCastCollision->GetDamageValue());
	}

}

// Handling collisions from projectile
void ComponentHealth::HandleProjectile(Common::GameObject* projectileP)
{

	ComponentProjectile* projectileComponent = static_cast<ComponentProjectile*>(projectileP->GetComponent("GOC_Projectile"));

	// Enemies can't shoot each other	
	if (!(projectileComponent->enemyBullet && this->GetGameObject()->HasTag("Enemy")))
	{
		// Make sure the player can't shoot himself
		if (!(projectileComponent->parent == this->GetGameObject()))
		{

			// Manager
			Common::GameObjectManager* pGOManager = projectileComponent->GetGameObject()->GetManager();

			// Subtract from damage from health
			int damage = projectileComponent->GetDamage();
			currentHealth -= damage;

			// Check if it's the player, if it is' trigger other event
			if (strcmp(this->GetGameObject()->GetGUID().c_str(), "Player") == 0)
			{
				// Send out collision event (Maybe just handle it here?)
				std::shared_ptr<PlayerDamagedEvent> pDamage(new PlayerDamagedEvent(this->GetGameObject(), damage));
				EventManager::Instance()->QueueEvent(pDamage);
			}

			// Delete projectile
			pGOManager->DestroyGameObject(projectileComponent->GetGameObject());

			if (blinkDamage)
			{
				BlinkDamage();
			}

			// Die
			if (currentHealth <= 0)
			{
				Die();
			}

			
		}
	}

}

// Handling collisions from projectile
void ComponentHealth::HandleConeWeapon(float p_fDamage)
{
	if (this->GetGameObject()->HasTag("Enemy"))
	{
		currentHealth -= p_fDamage;

		if (blinkDamage)
		{
			BlinkDamage();
		}
		
		// Die
		if (currentHealth <= 0)
		{
			Die();
		}
	}
}

// ExplosionDamage
void ComponentHealth::HandleExplosionDamage(float p_fDamage)
{
	currentHealth -= p_fDamage;

	if (blinkDamage)
	{
		BlinkDamage();
	}

	// Die
	if (currentHealth <= 0)
	{
		Die();
	}
}

// Death
void ComponentHealth::Die()
{
	SoundManager::Instance()->PlaySoundFile("Explosion");

	if (!this->GetGameObject()->HasTag("Player"))
	{

		ExplosionInfo* newExplosion = new ExplosionInfo();
		newExplosion->duration = 1.2f;
		newExplosion->point = this->GetGameObject()->GetTransform().GetTranslation();
		newExplosion->radius = 0.2f;
		newExplosion->damagePerSecond = 3.0f;

		CollisionManager::Instance()->AddExplosion(newExplosion);


	}
	
	this->GetGameObject()->Death();
	this->GetGameObject()->GetManager()->DestroyGameObject(this->GetGameObject());
}

// Blinking damage method (Sets the material in the renderable to be something)
void ComponentHealth::BlinkDamage()
{

	// Get the renderable component for the object
	ComponentRenderableMesh* renderableComponent = static_cast<ComponentRenderableMesh*>(this->GetGameObject()->GetComponent("GOC_Renderable"));
	maskColor = 0.2f;

	renderableComponent->SetColorMask(glm::vec4(1.0f, maskColor, maskColor, 1.0f));
}

// Restore Health
void ComponentHealth::RestoreHealth(float p_fRestore)
{

	// ADd health back to the max
	currentHealth += p_fRestore;
	
	if (currentHealth > totalHealth)
	{
		currentHealth = totalHealth;
	}

}

// Parsing
Common::ComponentBase* ComponentHealth::CreateComponent(TiXmlNode* p_pNode)
{
	assert(strcmp(p_pNode->Value(), "GOC_Health") == 0);
	ComponentHealth* pHealth = new ComponentHealth();

	float health = 0;
	bool blink = false;

	// Iterate Anim elements in the XML
	TiXmlNode* pChildNode = p_pNode->FirstChild();
	while (pChildNode != NULL)
	{
		const char* szNodeName = pChildNode->Value();

		if (strcmp(szNodeName, "Health") == 0)
		{

			// Parse attributes
			TiXmlElement* pElement = pChildNode->ToElement();

			// Speed of projectile
			if (pElement->QueryFloatAttribute("value", &health) != TIXML_SUCCESS)
			{
				delete pHealth;
				return NULL;
			}
		}

		else if (strcmp(szNodeName, "BlinksDamage") == 0)
		{

			// Parse attributes
			TiXmlElement* pElement = pChildNode->ToElement();

			// Speed of projectile
			if (pElement->QueryBoolAttribute("value", &blink) != TIXML_SUCCESS)
			{
				delete pHealth;
				return NULL;
			}
		}

		pChildNode = pChildNode->NextSibling();
	}

	pHealth->Init(health, blink);
	pHealth->InitDelegates();
	return pHealth;
}



