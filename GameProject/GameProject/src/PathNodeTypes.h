//------------------------------------------------------------------------
// PathNodeTypes
//	
// Simple struct used to identify which types a node can be
// E.g. Ground / Flying / Others
//------------------------------------------------------------------------

#ifndef PATHNODETYPES_H
#define PATHNODETYPES_H

enum EnemyPathType
{
	GROUND = 0,
	FLYING,
};

struct PathNodeTypes
{
	bool b_Ground = true;
	bool b_Flying = true;
};

#endif