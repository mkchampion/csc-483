//------------------------------------------------------------------------
// AIActionData
// Structs for what an ActionNode may need, and them in a Union
//------------------------------------------------------------------------

#ifndef AIACTIONDATA_H
#define AIACTIONDATA_H

#include <string>

// Tracking / Shooting state
struct StateTracking
{
	char targetID[256];

	bool hasMeshRotation;
	int meshNum;
};

// Chasing state
struct StateChasing
{
	char targetID[256];
	float turnSpeed;
	float moveSpeed;
	float turnLeniance;
	bool tilt;
};

// Chasing and Shooting state
struct StateChasingTracking
{
	char targetID[256];

	bool hasMeshRotation;
	int meshNum;

	float turnSpeed;
	float moveSpeed;
	float turnLeniance;
	bool tilt;
	float acceleration;
	float deceleration;
	float distanceThreshold;
};

union ActionData
{
	StateTracking trackingData;
	StateChasing chasingData;
	StateChasingTracking chasingTrackingData;
};

#endif