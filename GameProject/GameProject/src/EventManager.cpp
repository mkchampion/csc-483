//------------------------------------------------------------------------
// EventManager
//	
// This class is the event manager.  Allows components to register / remove listeners
// and processes the event queue.
// Based off of EventManager from Game Coding Complete
//------------------------------------------------------------------------

#include "EventManager.h"
#include <assert.h>
#include <GL/glfw.h>

// Static instance
EventManager* EventManager::s_pEventManagerInstance = NULL;

//------------------------------------------------------------------------------
// Method:    CreateInstance
// Returns:   void
// 
// Creates the singletone instance. (Same as SceneManager::CreateInstance())
//------------------------------------------------------------------------------
void EventManager::CreateInstance()
{
	// Make sure its null
	assert(s_pEventManagerInstance == NULL);

	// Create a new EventManager
	s_pEventManagerInstance = new EventManager();

}

//------------------------------------------------------------------------------
// Method:    DestroyInstance
// Returns:   void
// 
// Destroys the singletone instance. (Same as SceneManager::CreateInstance())
//------------------------------------------------------------------------------
void EventManager::DestroyInstance()
{
	// Make sure its null
	assert(s_pEventManagerInstance != NULL);

	// delete EventManager
	delete s_pEventManagerInstance;
	s_pEventManagerInstance = NULL;

}

//------------------------------------------------------------------------------
// Method:    Instance
// Returns:   void
// 
// Returns the singletone instance. (Same as SceneManager::CreateInstance())
//------------------------------------------------------------------------------
EventManager* EventManager::Instance()
{
	assert(s_pEventManagerInstance != NULL);
	return s_pEventManagerInstance;
}

//------------------------------------------------------------------------------
// Method:    EventManager
// Returns:   
// 
// Constructor
//------------------------------------------------------------------------------
EventManager::EventManager()
{
}

//------------------------------------------------------------------------------
// Method:    ~EventManager
// Returns:   
// 
// Destructor.
//------------------------------------------------------------------------------
EventManager::~EventManager()
{
}

//------------------------------------------------------------------------------
// Method:    AddListener
// Returns:   void
// 
// Registers a listener to the event manager
//------------------------------------------------------------------------------
bool EventManager::AddListener(const EventType& p_eEventType, const EventDelegate* p_pEventDelegateT)
{

	// Get the list for this event type
	EventListenerList& lEventList = m_lEventMap[p_eEventType];

	// Setup Iterator
	EventListenerList::iterator eventIT = lEventList.begin();

	for (; eventIT != lEventList.end(); ++eventIT)
	{
		const EventDelegate* eventDelegate = (*eventIT);

		if (p_pEventDelegateT->IsEqual(eventDelegate))
		{
			printf("Shouldn't be here?\n %d");
			return false;
		}

	}


	// Just add it for now
	lEventList.push_back(p_pEventDelegateT);

	return true;
}

//------------------------------------------------------------------------------
// Method:    RemoveListener
// Returns:   void
// 
// Registers a listener to the event manager
//------------------------------------------------------------------------------
bool EventManager::RemoveListener(const EventType& p_eEventType, const EventDelegate* p_pEventDelegateT)
{

	bool removeSuccessful = false;

	// Get the list for this event type
	EventListenerList& lEventList = m_lEventMap[p_eEventType];

	// Setup Iterator
	EventListenerList::iterator eventIT = lEventList.begin();

	for (; eventIT != lEventList.end(); ++eventIT)
	{
		
		if (p_pEventDelegateT->IsEqual(*eventIT))
		{
			lEventList.remove(*eventIT);
			removeSuccessful = true;
			break;
		}
		
	}

	return removeSuccessful;
}

//------------------------------------------------------------------------------
// Method:    RemoveListener
// Returns:   void
// 
// Queues a listener to be removed at the start of the next update
//------------------------------------------------------------------------------
void EventManager::QueueRemoveListener(const EventType& p_eEventType, const EventDelegate* p_pEventDelegateT)
{
	RemoveListenerData data;
	data.type = p_eEventType;
	data.delegate = p_pEventDelegateT;

	// Add to queue
	m_lListenersToRemove.push_back(data);

}

//------------------------------------------------------------------------------
// Method:    TriggerEvent
// Returns:   void
// 
// Immediately triggers an event
//------------------------------------------------------------------------------
bool EventManager::TriggerEvent(const EventDelegate::EventDataPointer& p_pEventData)
{
	// Flag to indicate it was called
	bool functionProcessed = false;

	// Find the type of the event
	unsigned long lEventType = p_pEventData->GetEventType();

	// Find the list of delegates for that event type within the list
	auto found = m_lEventMap.find(lEventType);

	// If one is found (Not the end iterator)
	if (found != m_lEventMap.end())
	{

		// Get the list of delegate functions
		const EventListenerList& eventList = found->second;

		// Iterate over that
		EventListenerList::const_iterator delegateIT = eventList.begin();

		for (; delegateIT != eventList.end(); ++delegateIT)
		{
			// Get the function
			const EventDelegate* delegateFunction = (*delegateIT);

			// Call it
			EventDelegate::FunctionDelegate functionToCall = delegateFunction->GetDelegate();
			functionToCall(p_pEventData);
			functionProcessed = true;

		}
	}
	
	return functionProcessed;

}

//------------------------------------------------------------------------------
// Method:    QueueEvent
// Returns:   void
// 
// Queue an event for the next update cycle
//------------------------------------------------------------------------------
bool EventManager::QueueEvent(const EventDelegate::EventDataPointer& p_pEventData)
{

	// Find the type of the event
	unsigned long lEventType = p_pEventData->GetEventType();

	// Find the list of delegates for that event type within the list
	auto found = m_lEventMap.find(lEventType);

	// If one is found (Not the end iterator)
	if (found != m_lEventMap.end())
	{		
		// Queue up the event
		m_qEventQueue.push_back(p_pEventData);

		// Successfully queued
		return true;
	}
	else
	{
		return  false;
	}
}

void EventManager::ClearEvents()
{
	// Should this be the queue? (Breaks all existing listeners if it is just the event map)
	m_qEventQueue.clear();
}

//------------------------------------------------------------------------------
// Method:    Update
// Returns:   void
// 
// Registers a listener to the event manager
//------------------------------------------------------------------------------
void EventManager::Update(float p_fDelta)
{

	// Remove any listeners that were queued for removal
	for (int i = 0; i < m_lListenersToRemove.size(); i++)
	{
		RemoveListenerData data = m_lListenersToRemove.back();
		RemoveListener(data.type, data.delegate);		

		// Remove from the queue
		m_lListenersToRemove.pop_back();
	}
	
	// Get the time at the start of the update
	float fCurrentTime = glfwGetTime();

	// Using very high max value for now since I'm not sure what an appropriate one would be
	float timePassed = 0.0f;
	float maxTime = 1000.0f;

	
	// Proceed through the queue
	while (!m_qEventQueue.empty() && (timePassed < maxTime))
	{

		// Get the first event
		EventDelegate::EventDataPointer pEvent = m_qEventQueue.front();

		// Remove it
		m_qEventQueue.pop_front();
		
		// Find its typ
		const EventType& eventType = pEvent->GetEventType();

		// Go through any delegates with this event type
		auto found = m_lEventMap.find(eventType);

		// If there are any
		if (found != m_lEventMap.end())
		{

			const EventListenerList& lEventDelegateTList = found->second;

			// Go through each
			auto beginIT = lEventDelegateTList.begin();
			auto endIT = lEventDelegateTList.end();

			for (; beginIT != endIT; ++beginIT)
			{
				// Get the function
				const EventDelegate* delegateFunction = (*beginIT);

				// Call it
				EventDelegate::FunctionDelegate functionToCall = delegateFunction->GetDelegate();
				functionToCall(pEvent);
			}
		}

		// Increment time
		timePassed += (glfwGetTime() - fCurrentTime);
	}
	

}
