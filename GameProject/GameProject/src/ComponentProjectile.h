//------------------------------------------------------------------------
// ComponentCharacterController
//
// Created:	2012/12/14
// Author:	Carel Boers
//	
// This class implements a character controller. It polls input and sends
// movement instructions to the relevant sibling components.
//------------------------------------------------------------------------

#ifndef COMPNENTPROJECTILE_H
#define COMPNENTPROJECTILE_H

#include "ComponentBase.h"
#include <tinyxml.h>
#include <glm\glm.hpp>
#include "EventDelegate.h"

class ComponentProjectile : public Common::ComponentBase
{
public:
	//------------------------------------------------------------------------------
	// Public types.
	//------------------------------------------------------------------------------

public:
	//------------------------------------------------------------------------------
	// Public methods.
	//------------------------------------------------------------------------------
	ComponentProjectile();
	virtual ~ComponentProjectile();

	// Init
	void Init(const float lifespanP, const float heightP, bool customHeightP, bool homingP);

	// Factory
	static Common::ComponentBase* CreateComponent(TiXmlNode* p_pNode);

	virtual const std::string FamilyID() { return std::string("GOC_Projectile"); }
	virtual const std::string ComponentID(){ return std::string("GOC_Projectile"); }
	virtual void Update(float p_fDelta);

	void SetDirection(const glm::vec3& directionP) { direction = directionP; }
	void SetSpeed(float const speedP) { speed = speedP; }
	void SetDamage(int const damageP) { damageDealt = damageP; }

	// Delegates
	void InitDelegates();
	void HandleStaticCollisions(EventDelegate::EventDataPointer p_pCollisionData);

	int GetDamage() const { return damageDealt; }

	// Parent game object
	Common::GameObject* parent;

	// Public bool for enemy / player
	bool enemyBullet = false;

private:
	//------------------------------------------------------------------------------
	// Private members.
	//------------------------------------------------------------------------------
	// EVent Delegates (Need to be removed from EVentManager when this component is deleted)
	EventDelegate* staticDelegate;

	// Speed
	float speed;
	glm::vec3 direction;

	// Y value (Height value for the projectile)
	float projectileHeight;
	bool customHeight = false;

	// Lifespan
	float timeActive;
	float lifespan;
	bool flaggedForRemove = false;

	// Damage values
	int damageDealt;

	// Float for if it should home enemies
	bool homing = false;
	Common::GameObject* target = NULL;

	// Function to find the closest enemy available
	Common::GameObject* FindNearestEnemy();
	glm::vec3 adjustDirection(float targetDir);

	bool setInitialDir = false;
};

#endif // COMPNENTPROJECTILE_H

