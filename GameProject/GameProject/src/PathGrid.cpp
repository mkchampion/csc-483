//------------------------------------------------------------------------
// PathGrid
//	
// Grid to use for pathfinding
//------------------------------------------------------------------------

#include "PathGrid.h"
#include "PathNode.h"
#include <glm/glm.hpp>


// Constructor
PathGrid::PathGrid(float gridWidth, float gridHeight, float nodeSeperation, const glm::vec3& bottomLeft)
{
	// Find grid size;
	gridSizeX = (int)(gridWidth / nodeSeperation);
	gridSizeY = (int)(gridHeight / nodeSeperation);

	// Offset the nodes to be in the middle of a grid? (Maybe remove this)
	float offset = nodeSeperation / 2.0f;

	// Allocate for the nodes
	nodes = (PathNode***)malloc(sizeof(PathNode**) * gridSizeX);

	for (int i = 0; i < gridSizeX; i++)
	{
		nodes[i] = (PathNode**)malloc(sizeof(PathNode*) * gridSizeY);
	}

	// Set node position
	for (int i = 0; i < gridSizeX; i++)
	{
		for (int j = 0; j < gridSizeY; j++)
		{
			
			// Create the new PathNode
			PathNode* newNode = new PathNode();

			newNode->position = bottomLeft + glm::vec3(i * nodeSeperation + offset, 0.0f, -j * nodeSeperation - offset);
			newNode->occupiedCost = 0;
			
			nodes[i][j] = newNode;

		}
	}

	// Setup the neighbors
	for (int i = 0; i < gridSizeX; i++)
	{
		for (int j = 0; j < gridSizeY; j++)
		{

			// Grab neighbors
			for (int k = -1; k <= 1; k++)
			{
				for (int l = -1; l <= 1; l++)
				{
					if (k == 0 && l == 0)
						continue;

					int testX = i + k;
					int testY = j + l;

					if (testX >= 0 && testX < gridSizeX && testY >= 0 && testY < gridSizeY)
					{
						nodes[i][j]->neighbourNodes.push_back(nodes[testX][testY]);
					}
				}
			}

		}
	}
}

// Destructor
PathGrid::~PathGrid()
{
	for (int i = 0; i < gridSizeX; i++)
	{
		delete nodes[i];
	}

	delete nodes;
}

// Mark specific nodes unusable based on collision coordinates
void PathGrid::MarkUnusable(std::vector<CollisionPoints>* collisionData, float squareSize)
{
	// Removing nodes that collide with buildings
	for (int i = 0; i < gridSizeX; i++)
	{
		for (int j = 0; j < gridSizeY; j++)
		{
			glm::vec3 pointToTest = nodes[i][j]->position;

			// Check the collisionData
			for (int k = 0; k < collisionData->size(); k++)
			{
				// New Collision point for this test
				CollisionPoints testPoint;
				testPoint.bottomLeft = glm::vec4(pointToTest.x - squareSize / 2.0f, 0.0f, pointToTest.z - squareSize / 2.0f, 1.0f);
				testPoint.bottomRight = glm::vec4(pointToTest.x + squareSize / 2.0f, 0.0f, pointToTest.z - squareSize / 2.0f, 1.0f);
				testPoint.topLeft = glm::vec4(pointToTest.x - squareSize / 2.0f, 0.0f, pointToTest.z + squareSize / 2.0f, 1.0f);
				testPoint.topRight = glm::vec4(pointToTest.x + squareSize / 2.0f, 0.0f, pointToTest.z + squareSize / 2.0f, 1.0f);

				// ADjust the collisionPoints from geometry to be in world space
				CollisionPoints building;
				building.bottomLeft = collisionData->at(k).transform * collisionData->at(k).bottomLeft;
				building.bottomRight = collisionData->at(k).transform * collisionData->at(k).bottomRight;
				building.topLeft = collisionData->at(k).transform * collisionData->at(k).topLeft;
				building.topRight = collisionData->at(k).transform * collisionData->at(k).topRight;

				// Check the collision
				if (CollisionManager::Instance()->CheckCollisionPoints(&testPoint, &building))
				{
					nodes[i][j]->walkable = false;
					nodes[i][j]->m_pathNodeTypes.b_Ground = false;
				}

			}

		}
	}


}

// Mark specific nodes unusable based on collision coordinates
void PathGrid::MarkUnusableElevated(std::vector<FlyBoundsWorld*>* flyBounds)
{
	// Removing nodes that collide with buildings
	for (int i = 0; i < gridSizeX; i++)
	{
		for (int j = 0; j < gridSizeY; j++)
		{
			glm::vec3 pointToTest = nodes[i][j]->position;

			for (int k = 0; k < flyBounds->size(); k++)
			{

				float min = flyBounds->at(k)->min;
				float max = flyBounds->at(k)->max;

				if (pointToTest.z < flyBounds->at(k)->min && pointToTest.z > flyBounds->at(k)->max)
				{
					nodes[i][j]->m_pathNodeTypes.b_Ground = false;
				}

			}
			
		}
	}


}


// Get closest PathNode to a given location
PathNode* PathGrid::GetClosestNode(const glm::vec3& position)
{
	// Starting var
	PathNode* result = NULL;
	float distCheck = 99999.0f;

	for (int i = 0; i < gridSizeX; i++)
	{
		for (int j = 0; j < gridSizeY; j++)
		{
			PathNode* node = nodes[i][j];

			// Calculate distance
			float distance = glm::length(node->position - position);

			if ((result == NULL || distance < distCheck) && node->walkable)
			{
				result = node;
				distCheck = distance;
			}
		}
	}

	return result;
}

// Get closest PathNode to a given location
PathNode* PathGrid::GetClosestUsableNode(const glm::vec3& position, EnemyPathType enemyPathTypeP, PathNode* exceptionP)
{
	// Starting var
	PathNode* result = NULL;
	float distCheck = 99999.0f;

	for (int i = 0; i < gridSizeX; i++)
	{
		for (int j = 0; j < gridSizeY; j++)
		{
			PathNode* node = nodes[i][j];

			// Calculate distance
			float distance = glm::length(node->position - position);

			if ((result == NULL || distance < distCheck))
			{

				if (!CanUseType(enemyPathTypeP, node->m_pathNodeTypes))
					continue;

				// Check to see if it's the right type
				if (!node->occupiedFlag)
				{
					result = node;
					distCheck = distance;
				}

				else if (exceptionP != NULL && exceptionP == node)
				{
					result = node;
					distCheck = distance;
				}
			}
		}
	}

	return result;
}

bool PathGrid::CanUseType(EnemyPathType enemyType, PathNodeTypes nodeType)
{
	if (enemyType == EnemyPathType::GROUND && nodeType.b_Ground == false)
	{
		return false;
	}
	else if (enemyType == EnemyPathType::FLYING && nodeType.b_Flying == false)
	{
		return false;
	}

	return true;
}