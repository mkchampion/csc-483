//------------------------------------------------------------------------
// AIActionNode
//
// Created:	2012/12/14
// Author:	Carel Boers
//	
// This class represents an action (leaf) node in an AI decision tree.
//------------------------------------------------------------------------

#ifndef AIACTIONNODE_H
#define AIACTIONNODE_H

#include "tinyxml.h"
#include "AINode.h"
#include "AIActionData.h"

class AIActionNode : public AINode
{
public:
	//------------------------------------------------------------------------------
	// Public methods.
	//------------------------------------------------------------------------------
	AIActionNode(AIDecisionTree* p_pTree);
	virtual ~AIActionNode();

	virtual bool Init(TiXmlNode* p_pNodeDefinition);
	virtual const AIActionNode* Decide();
	
	// Get the action
	const std::string& GetAction() const { return m_strAction; }
	const std::string& GetParam() const { return m_strParam; }
	bool HasParam() const { return hasParam; }
	ActionData* GetData() const { return data; }

private:
	
	// Action
	std::string m_strAction;

	// Adding a paramater that can be used for the action nodes?
	std::string m_strParam;
	bool hasParam = false;

	// Union testing
	ActionData* data;
};

#endif // AIDECISIONTREE_H

