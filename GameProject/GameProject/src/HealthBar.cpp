// ================================================================
// HealthBar.h
// Class that tracks and renders a health bar based on a certain GO?
// =================================================================

#include "HealthBar.h"
#include "ComponentHealth.h"

// Constructor
HealthBar::HealthBar(Common::GameObjectManager* p_pManager, const std::string& p_strTargetID, float borderWidthP)
{

	// Set the target / manager
	m_pManager = p_pManager;
	m_pObjectID = p_strTargetID;

	// Border details
	borderWidth = borderWidthP;
	glm::vec4 borderColor = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);

	// Create the program
	m_pMaterial = wolf::MaterialManager::CreateMaterial("HealthMaterial");
	m_pMaterial->SetProgram("GameProject/GameProject/data/Shaders/simpleQuad.vsh", "GameProject/GameProject/data/Shaders/simpleQuad.fsh");

	int vertexOffset = 0;

	// Main health	
	HealthVertex v1 = { borderWidth, borderWidth, 0.50f, 0.0f, 0.0f, 1.0f };
	HealthVertex v2 = { borderWidth, borderWidth + height, 0.55f, 0.0f, 0.0f, 1.0f };	
	HealthVertex v3 = { borderWidth + width, borderWidth, 0.95f, 0.0f, 0.0f, 1.0f };
	HealthVertex v4 = { borderWidth + width, borderWidth + height, 1.0f, 0.0f, 0.0f, 1.0f };

	// Health itself
	healthVertices.push_back(v1);
	healthVertices.push_back(v2);
	healthVertices.push_back(v3);
	healthVertices.push_back(v4);

	healthRightB = 2;
	healthRightT = 3;

	// Add to the indices
	healthIndices.push_back(0);
	healthIndices.push_back(1);
	healthIndices.push_back(2);

	healthIndices.push_back(2);
	healthIndices.push_back(1);
	healthIndices.push_back(3);

	// Increment offset
	vertexOffset += 4;

	// Border?
	if (hasBorder)
	{
		// Left
		HealthVertex bl1 = { 0.0f, borderWidth, borderColor.r, borderColor.g, borderColor.b, borderColor.a };
		HealthVertex bl2 = { 0.0f, borderWidth + height, borderColor.r, borderColor.g, borderColor.b, borderColor.a };
		HealthVertex bl3 = { borderWidth, borderWidth, borderColor.r, borderColor.g, borderColor.b, borderColor.a };
		HealthVertex bl4 = { borderWidth, borderWidth + height, borderColor.r, borderColor.g, borderColor.b, borderColor.a };

		// Add left to list
		healthVertices.push_back(bl1);
		healthVertices.push_back(bl2);
		healthVertices.push_back(bl3);
		healthVertices.push_back(bl4);

		healthIndices.push_back(vertexOffset + 0);
		healthIndices.push_back(vertexOffset + 1);
		healthIndices.push_back(vertexOffset + 2);

		healthIndices.push_back(vertexOffset + 2);
		healthIndices.push_back(vertexOffset + 1);
		healthIndices.push_back(vertexOffset + 3);

		vertexOffset += 4;


		// Right
		HealthVertex br1 = { borderWidth + width, borderWidth, borderColor.r, borderColor.g, borderColor.b, borderColor.a };
		HealthVertex br2 = { borderWidth + width, borderWidth + height, borderColor.r, borderColor.g, borderColor.b, borderColor.a };
		HealthVertex br3 = { borderWidth * 2 + width, borderWidth, borderColor.r, borderColor.g, borderColor.b, borderColor.a };
		HealthVertex br4 = { borderWidth * 2 + width, borderWidth + height, borderColor.r, borderColor.g, borderColor.b, borderColor.a };

		// Add left to list
		healthVertices.push_back(br1);
		healthVertices.push_back(br2);
		healthVertices.push_back(br3);
		healthVertices.push_back(br4);

		healthIndices.push_back(vertexOffset + 0);
		healthIndices.push_back(vertexOffset + 1);
		healthIndices.push_back(vertexOffset + 2);

		healthIndices.push_back(vertexOffset + 2);
		healthIndices.push_back(vertexOffset + 1);
		healthIndices.push_back(vertexOffset + 3);

		vertexOffset += 4;


		// Top
		HealthVertex bt1 = { 0.0f, 0.0f, borderColor.r, borderColor.g, borderColor.b, borderColor.a };
		HealthVertex bt2 = { 0.0f, borderWidth, borderColor.r, borderColor.g, borderColor.b, borderColor.a };
		HealthVertex bt3 = { borderWidth * 2 + width, 0.0f, borderColor.r, borderColor.g, borderColor.b, borderColor.a };
		HealthVertex bt4 = { borderWidth * 2 + width, borderWidth, borderColor.r, borderColor.g, borderColor.b, borderColor.a };

		// Add left to list
		healthVertices.push_back(bt1);
		healthVertices.push_back(bt2);
		healthVertices.push_back(bt3);
		healthVertices.push_back(bt4);

		healthIndices.push_back(vertexOffset + 0);
		healthIndices.push_back(vertexOffset + 1);
		healthIndices.push_back(vertexOffset + 2);

		healthIndices.push_back(vertexOffset + 2);
		healthIndices.push_back(vertexOffset + 1);
		healthIndices.push_back(vertexOffset + 3);

		vertexOffset += 4;

		// Bottom
		HealthVertex bb1 = { 0.0f, borderWidth + height, borderColor.r, borderColor.g, borderColor.b, borderColor.a };
		HealthVertex bb2 = { 0.0f, borderWidth * 2 + height, borderColor.r, borderColor.g, borderColor.b, borderColor.a };
		HealthVertex bb3 = { borderWidth * 2 + width, borderWidth + height, borderColor.r, borderColor.g, borderColor.b, borderColor.a };
		HealthVertex bb4 = { borderWidth * 2 + width, borderWidth * 2 + height, borderColor.r, borderColor.g, borderColor.b, borderColor.a };

		// Add left to list
		healthVertices.push_back(bb1);
		healthVertices.push_back(bb2);
		healthVertices.push_back(bb3);
		healthVertices.push_back(bb4);

		healthIndices.push_back(vertexOffset + 0);
		healthIndices.push_back(vertexOffset + 1);
		healthIndices.push_back(vertexOffset + 2);

		healthIndices.push_back(vertexOffset + 2);
		healthIndices.push_back(vertexOffset + 1);
		healthIndices.push_back(vertexOffset + 3);

		vertexOffset += 4;

	}

	m_mWorldTransform = glm::translate(glm::vec3(0.0f, 0.0f, 0.0f));
	DeclareVertexBuffer();

}

// Destructor
HealthBar::~HealthBar()
{
}

// Declaration of the vertex buffer
bool HealthBar::DeclareVertexBuffer()
{
	// Array to pass
	HealthVertex* meshVerts = &healthVertices[0];
	GLushort* meshI = &healthIndices[0];

	m_pVertexBuffer = wolf::BufferManager::CreateVertexBuffer(meshVerts, sizeof(HealthVertex) * healthVertices.size());

	m_pIndexBuffer = wolf::BufferManager::CreateIndexBuffer(sizeof(GLushort) * healthIndices.size());
	m_pIndexBuffer->Write(meshI, sizeof(GLushort) * healthIndices.size());

	m_pVertexDeclaration = new wolf::VertexDeclaration();
	m_pVertexDeclaration->Begin();
	m_pVertexDeclaration->AppendAttribute(wolf::AT_Position, 2, wolf::CT_Float);
	m_pVertexDeclaration->AppendAttribute(wolf::AT_Color, 4, wolf::CT_Float);
	m_pVertexDeclaration->SetVertexBuffer(m_pVertexBuffer);
	m_pVertexDeclaration->SetIndexBuffer(m_pIndexBuffer);
	m_pVertexDeclaration->End();
	
	return true;

}

// Update
void HealthBar::Update(float p_fDelta)
{

	// Check manager for object
	Common::GameObject* player = m_pManager->GetGameObject(m_pObjectID);
	if (player != NULL)
	{

		// Get health component
		ComponentHealth* healthComponent = static_cast<ComponentHealth*>(player->GetComponent("GOC_Health"));

		// Get percentage health
		percentHealth = healthComponent->GetCurrentHealth() / healthComponent->GetTotalHealth();

		// Take percent of width and make that the current vertex
		float currentWidth = percentHealth * width;
		healthVertices.at(healthRightT).x = borderWidth + currentWidth;
		healthVertices.at(healthRightB).x = borderWidth + currentWidth;

		HealthVertex* meshVerts = &healthVertices[0];
		GLushort* meshI = &healthIndices[0];

		// Test out the writing
		m_pVertexBuffer->Bind();
		m_pVertexBuffer->Write(meshVerts, sizeof(HealthVertex) * healthVertices.size());

		shouldRender = true;
	}
	else
	{
		shouldRender = false;
	}

}

// Render
void HealthBar::Render(const glm::mat4& p_mView, const glm::mat4& p_mProj)
{
	if (shouldRender)
	{

		m_pVertexDeclaration->Bind();

		// Uniforms
		glm::mat4 worldViewProj = p_mProj * m_mWorldTransform;
		m_pMaterial->SetUniform("WorldViewProj", worldViewProj);

		m_pMaterial->Apply();

		// Draw!
		glDrawElements(GL_TRIANGLES, healthIndices.size(), GL_UNSIGNED_SHORT, 0);
		//glDrawArrays(GL_TRIANGLES, 0, meshVertices.size());

	}

}

void HealthBar::SetPosition(float x, float y)
{
	m_mWorldTransform = glm::translate(x, y, 0.0f);
}