//-----------------------------------------------------------------------------
// File: W_Quad.h
//
// Similar to the sprite class, but this time it is representing a quad for 3d
//-----------------------------------------------------------------------------

#include "W_Quad.h"
#include "W_Common.h"
#include "W_BufferManager.h"
#include "W_MaterialManager.h"

#include "GameProject\GameProject\src\DrawCounting.h"

using namespace wolf;

// Struct for the quad
struct QuadVertex
{
	GLfloat x, y, z;	   // Position
	GLfloat u, v;		   // Texture coordinates
	GLfloat nx, ny, nz;    // Normal
};

//------------------------------------------------------------------------------
// Method:    Quad
// Returns:   void
// 
// Constructor
//------------------------------------------------------------------------------
Quad::Quad(const std::string& p_strMaterialName, const std::string& p_strTexture, const std::string& p_strVertexProgram, const std::string& p_strFragmentProgram, const glm::vec2& p_vDimensions, const glm::vec2& p_vDefaultSize)
{
	
	// Obtain the texture scale factor
	float fTextureX = p_vDimensions.x / p_vDefaultSize.x;
	float fTextureY = p_vDimensions.y / p_vDefaultSize.y;

	// Define the vertices for the quad (Similar to W_Sprite)  (Order is position/texture/normal.  Normal starts as pointing in negative z for this quad)
	const QuadVertex quadVertices[] =
	{
		{ 0.0f           , 0.0f, 0.0f           , 0.0f     , 0.0f     , 0.0f, 1.0f, 0.0f},
		{ 0.0f           , 0.0f, -p_vDimensions.y, 0.0f    , fTextureY, 0.0f, 1.0f, 0.0f },
		{ p_vDimensions.x, 0.0f, 0.0f           , fTextureX, 0.0f     , 0.0f, 1.0f, 0.0f},

		{ 0.0f           , 0.0f, -p_vDimensions.y, 0.0f     , fTextureY, 0.0f, 1.0f, 0.0f },
		{ p_vDimensions.x, 0.0f, 0.0f            , fTextureX, 0.0f     , 0.0f, 1.0f, 0.0f },
		{ p_vDimensions.x, 0.0f, -p_vDimensions.y, fTextureX, fTextureY, 0.0f, 1.0f, 0.0f },
	};

	m_mWorldTransform = glm::translate(0.0f, 0.0f, 0.0f);
	
	// Create the program
	m_pMaterial = wolf::MaterialManager::CreateMaterial(p_strMaterialName);
	m_pTexture = wolf::TextureManager::CreateTexture(p_strTexture);

	// Wrap mode
	m_pTexture->SetWrapMode(wolf::Texture::WrapMode::WM_Repeat, wolf::Texture::WrapMode::WM_Repeat);

	// Create material
	m_pMaterial->SetProgram(p_strVertexProgram, p_strFragmentProgram);
	m_pMaterial->SetTexture("texture1", m_pTexture);
	
	// Create the vertex buffer
	m_pVertexBuffer = wolf::BufferManager::CreateVertexBuffer(quadVertices, sizeof(QuadVertex) * 6);

	// Vertex declaration
	m_pVertexDeclaration = new wolf::VertexDeclaration();
	m_pVertexDeclaration->Begin();
	m_pVertexDeclaration->AppendAttribute(wolf::AT_Position, 3, wolf::CT_Float);
	m_pVertexDeclaration->AppendAttribute(wolf::AT_TexCoord1, 2, wolf::CT_Float);
	m_pVertexDeclaration->AppendAttribute(wolf::AT_Normal, 3, wolf::CT_Float);
	m_pVertexDeclaration->SetVertexBuffer(m_pVertexBuffer);
	m_pVertexDeclaration->End();
}

//------------------------------------------------------------------------------
// Method:    ~Quad
// Returns:   void
// 
// Destructor
//------------------------------------------------------------------------------
Quad::~Quad()
{
}

//------------------------------------------------------------------------------
// Method:    Update
// Returns:   void
// 
// Updates the Quad
//------------------------------------------------------------------------------
void Quad::Update(float p_fDelta)
{

}

//------------------------------------------------------------------------------
// Method:    Render
// Returns:   void
// 
// Renders the quad
//------------------------------------------------------------------------------
void Quad::Render(const glm::mat4& p_mView, const glm::mat4& p_mProj)
{
	m_pVertexDeclaration->Bind();

	// Uniforms
	glm::mat4 worldViewProj = p_mProj * p_mView * m_mWorldTransform;
	m_pMaterial->SetUniform("WorldViewProj", worldViewProj);

	// Get the inverse for the world
	glm::mat4 mWorldIT = glm::inverse(m_mWorldTransform);
	mWorldIT = glm::transpose(mWorldIT);

	// Setting it
	m_pMaterial->SetUniform("WorldIT", mWorldIT);

	m_pMaterial->Apply();

	// Draw the elements
	glDrawArrays(GL_TRIANGLES, 0, 6);

	// Draw counting
	DrawCalls += 1;

}