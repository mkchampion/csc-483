//------------------------------------------------------------------------
// HeliGame
//
// Created:	2012/12/06
// Author:	Carel Boers
//	
// Main game class. Derives from Common::Game.
//------------------------------------------------------------------------

#include "HeliGame.h"
#include "GameObject.h"
#include <time.h>
#include "BoundingBoxDebugDraw.h"
#include "PathGrid.h"
#include "PathFinder.h"
#include "RenderTargetManager.h"
#include "SoundManager.h"
#include "DirectionalLight.h"

// Testing for BoundingBox
#include "SceneCamera.h"

// States
#include "States.h"
#include "StateGameplay.h"
#include "StatePaused.h"

// Includes for components
#include "ComponentRenderableMesh.h"
#include "ComponentCharacterController.h"
#include "ComponentProjectile.h"
#include "ComponentBoundingBox.h"
#include "ComponentHealth.h"
#include "ComponentAIController.h"
#include "ComponentWeapon.h"
#include "ComponentParticleEffect.h"
#include "ComponentWeaponPickup.h"
#include "ComponentIdleMotion.h"
#include "ComponentFlightAdjustment.h"
#include "ComponentHealthPickup.h"
#include "ComponentDrops.h"

#include "DrawCounting.h"

// Counting
int DrawCalls = 0;

// Debug camera
static float lastTime = 0;
static float lastTimeFPS = 0;
static float secondCount = 0;
static float frameCount = 0;
static float cameraSpeed = 100.0f;

HeliGame* HeliGame::s_pInstance = NULL;

//------------------------------------------------------------------------------
// Method:    HeliGame
// Parameter: void
// Returns:   
// 
// Constructor
//------------------------------------------------------------------------------
HeliGame::HeliGame()
	: 
	m_pSceneCamera(NULL),
	m_pGameObjectManager(NULL),
	m_pStateMachine(NULL)
{
	s_pInstance = this;
}

//------------------------------------------------------------------------------
// Method:    ~HeliGame
// Parameter: void
// Returns:   
// 
// Destructor.
//------------------------------------------------------------------------------
HeliGame::~HeliGame()
{
	assert(!m_pGameObjectManager);
	assert(!m_pSceneCamera);
	
	delete levelInfo;
}

//------------------------------------------------------------------------------
// Method:    Init
// Returns:   bool
// 
// Overridden from Common::Game::Init(). Called when it's time to initialize our
// game. Returns true on successful initialization, false otherwise. If false is
// returned, the game will shut down.
//------------------------------------------------------------------------------
bool HeliGame::Init()
{
	
	// Seeding the random
	srand(time(NULL));
	rand();
	rand();

	
	// Setting Lights
	m_pDirLight = new DirectionalLight();
	glm::vec3 lightDir = glm::vec3(1.0f, 1.0f, -1.0f);
	glm::vec4 lightColor = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
	glm::vec4 ambientColor = glm::vec4(0.16f, 0.16f, 0.16f, 1.0f);
	m_pDirLight->lightDir = lightDir;
	m_pDirLight->lightColor = lightColor;
	m_pDirLight->ambientColor = ambientColor;
	m_pDirLight->offsetX = 300.0f;
	m_pDirLight->offsetY = 400.0f;
	m_pDirLight->offsetZ = -300.0f;
	m_pDirLight->targetOffsetX = 50.0f;
	m_pDirLight->targetOffsetY = 0.0f;
	m_pDirLight->targetOffsetZ = -100.0f;
	/*m_pDirLight->offsetX = 200.0f;
	m_pDirLight->offsetY = 200.0f;
	m_pDirLight->offsetZ = -200.0f;
	m_pDirLight->targetOffsetX = 100.0f;
	m_pDirLight->targetOffsetY = 0.0f;
	m_pDirLight->targetOffsetZ = -100.0f;*/
	m_pDirLight->width = 400.0f;
	m_pDirLight->height = 500.0f;


	// Level Information being parsed in
	levelInfo = new LevelInfo("GameProject/GameProject/data/Level/TestLevel7.tmx");


	// Creation of TerrainManager with info from the level
	TerrainManager::CreateInstance();
	TerrainManager::Instance()->SetDirectionalLight(m_pDirLight);

	TerrainManager::Instance()->GenQuads(levelInfo->tileIDs, levelInfo->tileWidth, levelInfo->tileHeight, levelInfo->mapWidth, levelInfo->mapHeight, levelInfo->textureMap);
	TerrainManager::Instance()->GenModels(levelInfo->staticObjects, levelInfo->modelMap);
	TerrainManager::Instance()->GenMeshes(levelInfo->tileIDs, levelInfo->flyingZones, levelInfo->tileWidth, levelInfo->tileHeight, levelInfo->mapWidth, levelInfo->textureMap, levelInfo->heightMap ,levelInfo->borderX);

	TerrainManager::Instance()->upperX = (levelInfo->mapWidth - levelInfo->borderX) * levelInfo->tileWidth;
	TerrainManager::Instance()->upperZ = -(levelInfo->mapHeight - levelInfo->borderZ) * levelInfo->tileHeight;
	TerrainManager::Instance()->lowerX= levelInfo->borderX * levelInfo->tileWidth;
	TerrainManager::Instance()->lowerZ = -(levelInfo->borderZ) * levelInfo->tileHeight;
	TerrainManager::Instance()->SetFlyingBounds(levelInfo->flyingZones, levelInfo->tileHeight);


	// Initialize our Scene Manager
	Common::SceneManager::CreateInstance();
	Common::SceneManager::Instance()->SetDirectionalLight(m_pDirLight);


	// Initialize Collision Manager
	CollisionManager::CreateInstance();


	// Event Manager
	EventManager::CreateInstance();
	EventManager::Instance();

	// Creation of line drawer for debugging
#if DEBUG_BOXES
	wolf::LineDrawer* lineDraw = new wolf::LineDrawer();
	lineDraw->Init("GameProject/GameProject/data/Shaders/lines.vsh", "GameProject/GameProject/data/Shaders/lines.fsh");	
	BoundingBoxDebugDraw::CreateInstance(lineDraw);
#endif

	// Initialize our GameObjectManager
	m_pGameObjectManager = new Common::GameObjectManager();
	RegisterFactories();


	// Initialize the StateMachine and supported states and go to the main state
	m_pStateMachine = new Common::StateMachine();
	m_pStateMachine->RegisterState(eStateGameplay_Gameplay, new StateGameplay());
	m_pStateMachine->RegisterState(eStateGameplay_Paused, new StatePaused());
	m_pStateMachine->PushState(eStateGameplay_Gameplay, NULL);
	m_pGameObjectManager->SetStateMachine(m_pStateMachine);


	// Set up the collision stuff after the state to make sure the camera is setup
	CollisionManager::Instance()->LoadCollisionMap("GameProject/GameProject/data/Level/CollisionMap.xml");
	CollisionManager::Instance()->SetStaticCollisionData(levelInfo->staticObjects);


	// Adjust the grid
	PathGrid* levelGrid = new PathGrid((float)levelInfo->mapWidth * levelInfo->tileWidth, (float)levelInfo->mapHeight * levelInfo->tileHeight, levelInfo->tileWidth / 1.0f, glm::vec3(0.0f, 0.0f, 0.0f));
	levelGrid->MarkUnusable(CollisionManager::Instance()->GetCollisionPoints(), 10.0f);
	levelGrid->MarkUnusableElevated(TerrainManager::Instance()->GetFlyingBounds());

	PathFinder::CreateInstance();
	PathFinder::Instance()->grid = levelGrid;


	// Create the spawn tracker	
	Common::GameObject* playerObject = m_pGameObjectManager->GetObjectsWithTag("Player").at(0);
	spawnTracker = new SpawnTracker("Player", levelInfo->spawnedObjects, levelInfo->GOMap, levelInfo->checkPoints, levelInfo->weaponPickups);
	spawnTracker->SetSpawnDistance(120.0f);	

	// Last minute addition for end of level
	glm::vec3 fireworkPos = glm::vec3((levelInfo->mapWidth * levelInfo->tileWidth) / 2.0f, 40.0f, -(levelInfo->mapHeight * levelInfo->tileHeight) + (10.0f * levelInfo->tileHeight));
	spawnTracker->SetFireworkPosition(fireworkPos);

	// Culling target for the Terrain/Collision managers
	TerrainManager::Instance()->SetGOManager(m_pGameObjectManager);
	TerrainManager::Instance()->SetCullTarget("Player");

	CollisionManager::Instance()->SetGOManager(m_pGameObjectManager);
	CollisionManager::Instance()->SetPlayerID("Player");


	// Effect tracker
	effectTracker = new EffectTracker();
	effectTracker->InitDelegates();
	effectTracker->SetManager(m_pGameObjectManager);
	effectTracker->SetPlayer("Player");


	// Setup RenderTargetManager
	RenderTargetManager::CreateInstance();


	// Health testing
	healthBar = new HealthBar(m_pGameObjectManager, "Player", 5.0f);
	healthBar->SetPosition(GetWindowWidth() * 0.1f, GetWindowHeight() * 0.8f);


	// Setup the sounds!
	SoundManager::CreateInstance();
	SoundManager::Instance()->ReadChannels("GameProject/GameProject/data/AudioSetup/Channels.xml");
	SoundManager::Instance()->ReadSounds("GameProject/GameProject/data/AudioSetup/Sounds.xml");
	SoundManager::Instance()->PlaySoundFile("BGMusic");

	// Set the DirectionalLights camera target for shadow purposes
	m_pDirLight->SetCameraTarget(Common::SceneManager::Instance()->GetCamera());


	// Everything initialized OK.
	return true;
}

//------------------------------------------------------------------------------
// Method:    Update
// Parameter: float p_fDelta
// Returns:   bool
// 
// Overridden from Common::Game::Update(). Called once per frame during the main
// loop of the game. The game state is updated here. Returns true on success, 
// false otherwise. If false is returned, the game will shutdown.
//------------------------------------------------------------------------------
bool HeliGame::Update(float p_fDelta)
{

	m_pStateMachine->Update(p_fDelta);
	m_pGameObjectManager->Update(p_fDelta);
	
	// Update event
	if (m_pStateMachine->GetCurrentState() == eStateGameplay_Gameplay)
	{
		EventManager::Instance()->Update(p_fDelta);

		Common::SceneManager::Instance()->Update(p_fDelta);
		TerrainManager::Instance()->Update(p_fDelta);

		m_pDirLight->Update();

		// Grab camera
		Common::SceneCamera* cam = TerrainManager::Instance()->GetCamera();
		CollisionManager::Instance()->Update(p_fDelta, cam->GetProjectionMatrix(), cam->GetViewMatrix(), cam->GetAspectRatio());

#if DEBUG_BOXES
		// Toggle physics debug rendering
		static bool bLastKeyDown = false;
		bool bCurrentKeyDown = glfwGetKey('Z');
		if (bCurrentKeyDown && !bLastKeyDown)
		{
			BoundingBoxDebugDraw::Instance()->ToggleRender();
		}

		bLastKeyDown = bCurrentKeyDown;
#endif

		spawnTracker->Update(p_fDelta);
		effectTracker->Update(p_fDelta, cam);

		// Delete queued GO
		m_pGameObjectManager->DeleteQueuedGO();

		healthBar->Update(p_fDelta);

		SoundManager::Instance()->Update();
	}


	// Testing
	if (glfwGetKey(GLFW_KEY_SPACE) == GLFW_PRESS)
	{
		lightCam = true;
	}
	else
	{
		lightCam = false;
	}

	return true;
}

//------------------------------------------------------------------------------
// Method:    Render
// Returns:   void
// 
// Overridden from Common::Game::Render(). Called once per frame during the main
// loop of the game. Any objects in the game that need to be drawn are rendered
// here.
//------------------------------------------------------------------------------
void HeliGame::Render()
{

	// Sync transforms to render components
	m_pGameObjectManager->SyncTransforms();

	// Shadows
	glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);

	RenderTargetManager::Instance()->BindShadowMapRender();
	if (!RenderTargetManager::Instance()->isShadowComplete())
	{
		return;
	}

	// Render into the shadow buffer setup
	glEnable(GL_POLYGON_OFFSET_FILL);
	glPolygonOffset(2.0, 3.0);
	TerrainManager::Instance()->Render(true);
	Common::SceneManager::Instance()->Render(true);
	glDisable(GL_POLYGON_OFFSET_FILL);

	// Reset color mask
	glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);

	RenderTargetManager::Instance()->BindMainScene();
	if (!RenderTargetManager::Instance()->IsMainComplete())
	{
		return;
	}


#if DEBUG_BOXES
	Common::SceneCamera* camera = Common::SceneManager::Instance()->GetCamera();
	BoundingBoxDebugDraw::Instance()->Render(camera->GetProjectionMatrix(), camera->GetViewMatrix());
#endif

	TerrainManager::Instance()->Render();	
	Common::SceneManager::Instance()->Render();

	//printf("Draw Calls: %d\n", DrawCalls);
	DrawCalls = 0;

	// Tell the renderManager to render (Should handle  stuff?)
	RenderTargetManager::Instance()->Render();

	healthBar->Render(glm::lookAt(glm::vec3(0.0f, 0.0f, 200.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f)), glm::ortho(0.0f, (float)HeliGame::GetInstance()->GetWindowWidth(), (float)HeliGame::GetInstance()->GetWindowHeight(), 0.0f, 0.0f, 1000.0f));

}

//------------------------------------------------------------------------------
// Method:    Shutdown
// Returns:   void
// 
// Called when the game is shutting down. All dynamic memory needs to be cleaned
// up.
//------------------------------------------------------------------------------
void HeliGame::Shutdown()
{
	// Clear our Game Objects
	m_pGameObjectManager->DestroyAllGameObjects();
	delete m_pGameObjectManager;
	m_pGameObjectManager = NULL;

	// Delete our camera
	if (m_pSceneCamera)
	{
		delete m_pSceneCamera;
		m_pSceneCamera = NULL;
	}

	// Destroy the Scene Manager
	Common::SceneManager::DestroyInstance();
}


//------------------------------------------------------------------------------
// Method:    RegisterFactories
// Returns:   void
//------------------------------------------------------------------------------
void HeliGame::RegisterFactories()
{
	// Register factories
	m_pGameObjectManager->RegisterComponentFactory("GOC_RenderableMesh", ComponentRenderableMesh::CreateComponent);
	m_pGameObjectManager->RegisterComponentFactory("GOC_CharacterController", ComponentCharacterController::CreateComponent);
	m_pGameObjectManager->RegisterComponentFactory("GOC_Projectile", ComponentProjectile::CreateComponent);
	m_pGameObjectManager->RegisterComponentFactory("GOC_DragCamera", ComponentDragCamera::CreateComponent);
	m_pGameObjectManager->RegisterComponentFactory("GOC_BoundingBox", ComponentBoundingBox::CreateComponent);
	m_pGameObjectManager->RegisterComponentFactory("GOC_Health", ComponentHealth::CreateComponent);
	m_pGameObjectManager->RegisterComponentFactory("GOC_AIController", ComponentAIController::CreateComponent);
	m_pGameObjectManager->RegisterComponentFactory("GOC_Weapon", ComponentWeapon::CreateComponent);
	m_pGameObjectManager->RegisterComponentFactory("GOC_Particle", ComponentParticleEffect::CreateComponent);
	m_pGameObjectManager->RegisterComponentFactory("GOC_WeaponPickup", ComponentWeaponPickup::CreateComponent);
	m_pGameObjectManager->RegisterComponentFactory("GOC_IdleMotion", ComponentIdleMotion::CreateComponent);
	m_pGameObjectManager->RegisterComponentFactory("GOC_FlightAdjustment", ComponentFlightAdjustment::CreateComponent);
	m_pGameObjectManager->RegisterComponentFactory("GOC_HealthPickup", ComponentHealthPickup::CreateComponent);
	m_pGameObjectManager->RegisterComponentFactory("GOC_Drops", ComponentDrops::CreateComponent);
}



// Update the camera based on key presses/ mouse movement
void HeliGame::UpdateCamera()
{

	// Getting the time change and setting lastTime
	double currentTime = glfwGetTime();
	float deltaTime = (float)(currentTime - lastTime);
	lastTime = currentTime;


	//// Input, each will call the collisions method with the future position	
	//if (glfwGetKey(GLFW_KEY_UP) == GLFW_PRESS)
	//{
	//	position = position + deltaTime*(cameraSpeed * glm::vec3(0.0f, 0.0f, -1.0f));
	//	lookAt = lookAt + deltaTime*(cameraSpeed * glm::vec3(0.0f, 0.0f, -1.0f));
	//}

	//if (glfwGetKey(GLFW_KEY_LEFT) == GLFW_PRESS)
	//{
	//	position = position + deltaTime*(cameraSpeed * glm::vec3(-1.0f, 0.0f, 0.0f));
	//	lookAt = lookAt + deltaTime*(cameraSpeed * glm::vec3(-1.0f, 0.0f, 0.0f));
	//}

	//if (glfwGetKey(GLFW_KEY_DOWN) == GLFW_PRESS)
	//{
	//	position = position + deltaTime*(cameraSpeed * glm::vec3(0.0f, 0.0f, 1.0f));
	//	lookAt = lookAt + deltaTime*(cameraSpeed * glm::vec3(0.0f, 0.0f, 1.0f));
	//}

	//if (glfwGetKey(GLFW_KEY_RIGHT) == GLFW_PRESS)
	//{
	//	position = position + deltaTime* (cameraSpeed * glm::vec3(1.0f, 0.0f, 0.0f));
	//	lookAt = lookAt + deltaTime*(cameraSpeed * glm::vec3(1.0f, 0.0f, 0.0f));
	//}
}