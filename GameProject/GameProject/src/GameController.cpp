#include "GameController.h"



// Constructor
GameController::GameController(int controllerNumP, float deadZoneXP, float deadZoneYP)
{
	controllerNum = controllerNumP;
	deadZoneX = deadZoneXP;
	deadZoneY = deadZoneYP;
}

// Destructor
GameController::~GameController()
{
}

// Check if connected
bool GameController::IsConnected()
{
	ZeroMemory(&controllerState, sizeof(XINPUT_STATE));
	DWORD result = XInputGetState(controllerNum, &controllerState);

	if (result == ERROR_SUCCESS)
	{
		return true;
	}
	else
	{
		return false;
	}
}

// Update the game controller
void GameController::UpdateGameController()
{
	ZeroMemory(&controllerState, sizeof(XINPUT_STATE));
	XInputGetState(controllerNum, &controllerState);
	
	leftX = fmaxf(-1.0f, (float)controllerState.Gamepad.sThumbLX / GameController::JOYSTICK_MAX);
	leftY = fmaxf(-1.0f, (float)controllerState.Gamepad.sThumbLY / GameController::JOYSTICK_MAX);

	rightX = fmaxf(-1.0f, (float)controllerState.Gamepad.sThumbRX / GameController::JOYSTICK_MAX);
	rightY = fmaxf(-1.0f, (float)controllerState.Gamepad.sThumbRY / GameController::JOYSTICK_MAX);

	// Deadzones
	if (abs(leftX) < deadZoneX)
	{
		leftX = 0.0f;
	}
	if (abs(leftY) < deadZoneY)
	{
		leftY = 0.0f;
	}
	if (abs(rightX) < deadZoneX)
	{
		rightX = 0.0f;
	}
	if (abs(rightY) < deadZoneY)
	{
		rightY= 0.0f;
	}

}

// Retrieving the current state
XINPUT_STATE GameController::GetControllerState()
{
	ZeroMemory(&controllerState, sizeof(XINPUT_STATE));
	XInputGetState(controllerNum, &controllerState);

	return controllerState;
}

bool GameController::IsButtonPressed(unsigned short p_Button)
{
	return (controllerState.Gamepad.wButtons & p_Button);
}