//------------------------------------------------------------------------
// ComponentHealthPickup
//	
// Component meant to be used for health
//------------------------------------------------------------------------

#include "ComponentHealthPickup.h"
#include "DynamicCollisionEvent.h"
#include "EventManager.h"
#include "ComponentHealth.h"
#include "SoundManager.h"

static int pickupCount = 0;

ComponentHealthPickup::ComponentHealthPickup()
{
}

ComponentHealthPickup::~ComponentHealthPickup()
{
	EventManager::Instance()->QueueRemoveListener(DynamicCollisionEvent::s_EventType, dynamicCollisionDelegate);
}

void ComponentHealthPickup::Init(float healthRestoredP, std::string soundNameP)
{
	healthRestored = healthRestoredP;
	soundName = soundNameP;
}

void ComponentHealthPickup::InitDelegates()
{
	Common::GameObject* pGameObject = this->GetGameObject();

	// Create the EventDelegates
	std::string delegateDynamic = "GOC_HealthPickup#" + std::to_string(pickupCount);
	dynamicCollisionDelegate = new EventDelegate(delegateDynamic, DynamicCollisionEvent::s_EventType, std::tr1::bind(&ComponentHealthPickup::HandleCollisions, this, std::tr1::placeholders::_1));

	// Increment counter for health components
	pickupCount++;

	EventManager::Instance()->AddListener(DynamicCollisionEvent::s_EventType, dynamicCollisionDelegate);
}

// Parsing
Common::ComponentBase* ComponentHealthPickup::CreateComponent(TiXmlNode* p_pNode)
{
	assert(strcmp(p_pNode->Value(), "GOC_HealthPickup") == 0);
	ComponentHealthPickup* pPickup = new ComponentHealthPickup();

	float healthRestored = 0.0f;
	std::string soundNameT = "";

	// Iterate Anim elements in the XML
	TiXmlNode* pChildNode = p_pNode->FirstChild();
	while (pChildNode != NULL)
	{
		const char* szNodeName = pChildNode->Value();

		if (strcmp(szNodeName, "Health") == 0)
		{

			// Parse attributes
			TiXmlElement* pElement = pChildNode->ToElement();

			// Speed of projectile
			if (pElement->QueryFloatAttribute("value", &healthRestored) != TIXML_SUCCESS)
			{
				delete pPickup;
				return NULL;
			}
		}

		else if (strcmp(szNodeName, "Sound") == 0)
		{

			// Parse attributes
			TiXmlElement* pElement = pChildNode->ToElement();

			// Name
			const char* szName = pElement->Attribute("value");
			if (szName == NULL)
			{
				delete pPickup;
				return NULL;
			}

			soundNameT = szName;
		}

		pChildNode = pChildNode->NextSibling();
	}

	pPickup->Init(healthRestored, soundNameT);
	pPickup->InitDelegates();
	return pPickup;
}

void ComponentHealthPickup::Update(float p_fDelta)
{
	// Check if the player is within range of this object?
	Common::GameObject* player = this->GetGameObject()->GetManager()->GetGameObject("Player");
	
	if (player)
	{

		// Get current position / player position
		glm::vec3 currentPosition = this->GetGameObject()->GetTransform().GetTranslation();
		glm::vec3 playerPosition = player->GetTransform().GetTranslation();

		// If player has passed it
		if (currentPosition.z > playerPosition.z)
		{

			if (currentPosition.z - playerPosition.z > 250)
			{
				Common::GameObjectManager* manager = player->GetManager();
				manager->QueueGOForDeletion(this->GetGameObject());
			}

		}
	}

}


void ComponentHealthPickup::HandleCollisions(EventDelegate::EventDataPointer p_pCollisionData)
{

	// Cast the event
	std::shared_ptr<DynamicCollisionEvent> pCastCollision = std::static_pointer_cast<DynamicCollisionEvent>(p_pCollisionData);

	Common::GameObjectManager* pGOManager = pCastCollision->GetManager();

	Common::GameObject* g1 = pGOManager->GetGameObject(pCastCollision->GetObject1ID());
	Common::GameObject* g2 = pGOManager->GetGameObject(pCastCollision->GetObject2ID());

	if (g1 == NULL || g2 == NULL)
		return;

	// Bullet has collided with some dynamic object
	if (g1 == this->GetGameObject())
	{
		// Currently, only caring about enemies
		if (g2->HasTag("Player"))
		{
			// Get health component
			ComponentHealth* playerHealth = static_cast<ComponentHealth*>(g2->GetComponent("GOC_Health"));
			playerHealth->RestoreHealth(healthRestored);
			SoundManager::Instance()->PlaySoundFile(soundName);
			pGOManager->DestroyGameObject(g1);
		}
	}

	// Bullet has collided with some dynamic object
	else if (g2 == this->GetGameObject())
	{
		// Currently, only caring about enemies
		if (g1->HasTag("Player"))
		{
			// Get health component
			ComponentHealth* playerHealth = static_cast<ComponentHealth*>(g1->GetComponent("GOC_Health"));
			playerHealth->RestoreHealth(healthRestored);
			SoundManager::Instance()->PlaySoundFile(soundName);
			pGOManager->DestroyGameObject(g2);
		}
	}

}
