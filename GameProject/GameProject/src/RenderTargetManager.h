//------------------------------------------------------------------------
// RenderTargetManager
//	
// Represents some sort of Managing class for RenderTargets
//------------------------------------------------------------------------

#ifndef RENDERTARGETMANAGER_H
#define RENDERTARGETMANAGER_H

#include "W_RenderTarget.h"

struct EffectAndTarget
{
	wolf::Material* mat;
	wolf::RenderTarget* renderTarget;
};

class RenderTargetManager
{

public:

	// Singleton methods
	static void CreateInstance();
	static void DestroyInstance();
	static RenderTargetManager* Instance();

	// Render final / more?
	void Render();

	//  Binding the main scene
	void BindMainScene();
	bool IsMainComplete() const;
	wolf::RenderTarget* GetMainScene() const { return m_pMainRender; }

	// SHadow map
	void BindShadowMapRender();
	bool isShadowComplete() const;
	wolf::RenderTarget* GetShadowRender() const { return m_pShadowMapRender; }

	// Add an effect
	void SetEffects(std::vector<EffectAndTarget>* effects);

private:

	RenderTargetManager();
	~RenderTargetManager();

	// Instance
	static RenderTargetManager* s_pRenderManager;

	// Intermediary effects that are added
	std::vector<EffectAndTarget>* m_pEffects;

	// Two necessary renderbuffers
	wolf::RenderTarget* m_pScreen;
	wolf::RenderTarget* m_pMainRender;
	wolf::RenderTarget* m_pShadowMapRender;

	// Simple 2d program
	wolf::Material* m_pToScreenMat;
};

#endif