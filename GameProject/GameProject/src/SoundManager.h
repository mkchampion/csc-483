//------------------------------------------------------------------------
// SoundManager
//	
// A sound manager, sets up FMOD / Reads in sounds / etc
//------------------------------------------------------------------------

#ifndef SOUNDMANAGER_H
#define SOUNDMANAGER_H

#include "Sound.h"
#include "fmod.h"
#include <map>
#include <tinyxml.h>

class SoundManager
{

	typedef std::map<std::string, FMOD::Channel*> ChannelList;
	typedef std::map<std::string, float> ChannelVolume;
	typedef std::map<std::string, Sound*> SoundList;

public:

	// Instance
	static void CreateInstance();
	static void DestroyInstance();
	static SoundManager* Instance();
	
	// Read in channels / sounds
	void ReadChannels(std::string p_strChannelPath);
	void ReadSounds(std::string p_strSoundsPath);

	void PlaySoundFile(std::string p_strSoundName);
	void StopSoundFile(std::string p_strSoundName);

	void Update();

private:

	// Constructor / Destructor
	SoundManager();
	virtual ~SoundManager();

	static SoundManager* s_pSoundManagerInstance;

	// FMOD Details
	FMOD::System* m_pSystem;

	ChannelList m_lChannels;
	ChannelVolume m_lVolumes;
	SoundList m_lSoundList;

	FMOD::Channel* testChannel = 0;


	
};


#endif