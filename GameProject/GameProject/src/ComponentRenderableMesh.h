//------------------------------------------------------------------------
// ComponentRenderableMesh
//
// Created:	2012/12/14
// Author:	Carel Boers
//	
// This class implements a renderable component that is a mesh.
//------------------------------------------------------------------------

#ifndef COMPONENTRENDERABLEMESH_H
#define COMPONENTRENDERABLEMESH_H

#include "W_Model.h"
#include "ComponentRenderable.h"
#include "tinyxml.h"

class ComponentRenderableMesh : public Common::ComponentRenderable
{
public:
	//------------------------------------------------------------------------------
	// Public methods.
	//------------------------------------------------------------------------------
	ComponentRenderableMesh();
	virtual ~ComponentRenderableMesh();

	// Factory
	static Common::ComponentBase* ComponentRenderableMesh::CreateComponent(TiXmlNode* p_pNode);

	virtual const std::string ComponentID(){ return std::string("GOC_RenderableMesh"); }
	virtual void Update(float p_fDelta) {}

	//------------------------------------------------------------------------------
	// Public methods for "GOC_Renderable" family of components
	//------------------------------------------------------------------------------
	virtual void Init(const std::string& p_strPath, const std::string &p_strTexturePath, const std::string &p_strVertexProgramPath, const std::string &p_strFragmentProgramPath);
	virtual void SyncTransform();
	wolf::Model* GetModel() { return m_pModel; }

	// Set the rotation correction if necessary
	void SetRotationCorrection(const float p_fRotation) { m_fRotationCorrection = p_fRotation; }

	void SetMeshRotation(int meshNumP, bool linkedToBase, float speedP, float rotation, const glm::vec3& axisP);
	void SetMeshRotation(wolf::MeshRotationInfo meshRotationP);
	void StopMeshRotation(int meshNumP);

	// Method to add a custom transform
	void AddCustomTransform(int meshIDP, wolf::CustomTransform* transformP){ m_mCustomTransforms->insert(std::pair<int, wolf::CustomTransform*>(meshIDP, transformP)); }

	// Set uniform for this mesh
	void SetColorMask(const glm::vec4& colorMaskP);

private:
	//------------------------------------------------------------------------------
	// Private members.
	//------------------------------------------------------------------------------

	wolf::Model* m_pModel;
	float m_fRotationCorrection = 0.0f;

	// Another list of the custom transforms, given to model to use (Renderable handles allocation / deallocation)
	std::map<int, wolf::CustomTransform*>* m_mCustomTransforms;
};


#endif // COMPONENTRENDERABLEMESH_H

