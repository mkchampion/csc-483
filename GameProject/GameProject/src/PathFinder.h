//------------------------------------------------------------------------
// PathFinder
//	
// Class actually used to find a path after being given a grid of nodes
//------------------------------------------------------------------------

#ifndef PATHFINDER_H
#define PATHFINDER_H

#include "PathGrid.h"
#include <list>

class PathFinder
{

	// Typedef of list
	typedef std::list<glm::vec3> NodeList;

public:

	static void CreateInstance();
	static void DestroyInstance();
	static PathFinder* Instance();

	// Grid to use
	PathGrid* grid;

	// Find a path
	NodeList FindPath(const glm::vec3& startPos, const glm::vec3& targetPos, EnemyPathType enemyPathTypeP, PathNode* exceptionP);

private:

	// Constructor/Destructor are private because we're a Singleton
	PathFinder();
	virtual ~PathFinder();

	// Static singleton instance
	static PathFinder* s_pPathFinderInstance;

};

#endif