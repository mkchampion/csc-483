//------------------------------------------------------------------------
// ComponentHealth
//	
// A Component that represents health values and response to projectile collisions
//------------------------------------------------------------------------


#ifndef COMPONENTHEALTH_H
#define COMPONENTHEALTH_H

#include "ComponentBase.h"
#include "EventDelegate.h"
#include <tinyxml.h>
#include "GameObjectManager.h"


class ComponentHealth : public Common::ComponentBase
{
public:

	// Constructor / Destructor
	ComponentHealth();
	virtual ~ComponentHealth();

	// Init
	void Init(float healthValueP, bool blinkDamageP);

	// Factory
	static Common::ComponentBase* CreateComponent(TiXmlNode* p_pNode);

	// Update/Family
	virtual void Update(float p_fDelta);
	virtual const std::string FamilyID() { return std::string("GOC_Health"); }
	virtual const std::string ComponentID(){ return std::string("GOC_Health"); }

	float GetCurrentHealth() const { return currentHealth; }
	float GetTotalHealth() const { return totalHealth; }
	void RestoreHealth(float p_fRestore);

	// Event Handling
	void InitDelegates();
	void HandleDynamicCollisions(EventDelegate::EventDataPointer p_pCollisionData);
	void HandleConeWeaponCollision(EventDelegate::EventDataPointer p_pConeData);
	void HandleExplosionDamage(float p_fDamage);

	void DestroyGO();

private:

	// Handling the damage
	void HandleProjectile(Common::GameObject* projectileP);
	void HandleConeWeapon(float p_fDamage);

	// blinking damage (Done through material);
	void BlinkDamage();

	// Death
	void Die();

	// Delegate for events, need to remove on deletion
	EventDelegate* dynamicCollisionDelegate;
	EventDelegate* coneCollisionDelegate;

	// Health Values
	float currentHealth;
	float totalHealth;

	bool blinkDamage;
	float maskColor = 1.0f;

};


#endif 
