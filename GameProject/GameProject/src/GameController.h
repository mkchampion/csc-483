#ifndef GAME_CONTROLLER
#define GAME_CONTROLLER

#include <Windows.h>
#include <Xinput.h>
#include <math.h>

struct FloatVec2
{
	float x, y;
};

class GameController
{

	// Controller Slot number (Shouldn't typically be changed after setup)
	int controllerNum;

	// State for the controller (If it needs accessed, need to go through setter because it should refresh it)
	XINPUT_STATE controllerState;

	// Deadzones
	float deadZoneX;
	float deadZoneY;

public:

	// Constant for the max joystick value 
	static const int JOYSTICK_MAX = 32767;

	// Joystick values (Factoring in dead zones)
	float leftX;
	float leftY;
	float rightX;
	float rightY;	

	// Constructor / Destructor
	GameController(int controllerNumP, float deadZoneXP, float deadZoneYP);
	~GameController();

	// Check for connection / Getting state
	bool IsConnected();

	// Update method
	void UpdateGameController();
	bool IsButtonPressed(unsigned short);

	// If they want the state directly
	XINPUT_STATE GetControllerState();
	
};

#endif