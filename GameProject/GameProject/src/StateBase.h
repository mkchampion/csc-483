//------------------------------------------------------------------------
// StateBase
//
// Created:	2013/02/10
// Author:	Carel Boers
//	
// Base class for any states driven by StateMachine.
//------------------------------------------------------------------------

#ifndef STATEBASE_H
#define STATEBASE_H

#include "StateMachine.h"

namespace Common
{
	class StateBase
	{
		// To allow setting membership.
		friend class StateMachine;

	public:
		//------------------------------------------------------------------------------
		// Public methods.
		//------------------------------------------------------------------------------
		StateBase() : m_pStateMachine(NULL) { stateData = new ActionData; }
		virtual ~StateBase() { delete stateData; }
			
		// Required interface for all subclasses.
		virtual void Enter(ActionData* p_data) = 0;
		virtual void Update(float p_fDelta) = 0;
		virtual void Exit() = 0;

		// Suspend / Resume to change the suspended flag
		virtual void Suspend() { m_bSuspended = true; }
		virtual void Resume() { m_bSuspended = false; }
		virtual bool IsSuspended() const { return m_bSuspended; }

	private:
		//------------------------------------------------------------------------------
		// Private methods.
		//------------------------------------------------------------------------------

		void SetStateMachineMembership(StateMachine* p_pStateMachine) { m_pStateMachine = p_pStateMachine; }

	protected:
		//------------------------------------------------------------------------------
		// Protected members
		//------------------------------------------------------------------------------
		
		// State machine that this state is registered with
		StateMachine* m_pStateMachine;

		// Suspend flag
		bool m_bSuspended = false;

		ActionData* stateData;
	};
} // namespace Common

#endif

