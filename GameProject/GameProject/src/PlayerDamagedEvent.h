//------------------------------------------------------------------------
// PlayerDamagedEvent
//
// An event that is triggered when the player specifically takes damage
//------------------------------------------------------------------------

#ifndef PLAYERDAMAGEDEVENT_H
#define PLAYERDAMAGEDEVENT_H

#include "BaseEventData.h"
#include "GameObject.h"

class PlayerDamagedEvent : public BaseEventData
{

public:

	// Static event type
	static const EventType s_EventType;

	// Constructor / Destructor
	explicit PlayerDamagedEvent(Common::GameObject* p_pObject, float p_fDamage) : m_pPlayer(p_pObject), damageValue(p_fDamage) {}
	~PlayerDamagedEvent() {};

	// Accessor for event type
	const EventType& GetEventType() const { return s_EventType; }

	// Acccessor for object
	Common::GameObject* GetPlayer() const { return m_pPlayer; }
	float GetDamage() const { return damageValue; }

private:

	Common::GameObject* m_pPlayer;
	float damageValue;

};

#endif

