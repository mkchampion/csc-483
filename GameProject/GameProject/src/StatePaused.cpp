//------------------------------------------------------------------------
// StatePaused
//	
// An altered class from the StateMainMenu
//------------------------------------------------------------------------

#include "HeliGame.h"
#include "GameObject.h"
#include "StatePaused.h"
#include "SceneManager.h"
#include "States.h"
#include "EventManager.h"

// Constructor
StatePaused::StatePaused()
{
}

// Destructor
StatePaused::~StatePaused()
{
}

// Enter
void StatePaused::Enter(ActionData* p_data)
{
	m_pGameController = new GameController(0, 0.2f, 0.12f);
}

// Update
void StatePaused::Update(float p_fDelta)
{
	m_pGameController->UpdateGameController();

	if (!startReleased)
	{
		if (!m_pGameController->IsButtonPressed(XINPUT_GAMEPAD_START))
			startReleased = true;
	}

	if (m_pGameController->IsButtonPressed(XINPUT_GAMEPAD_START) && startReleased)
	{
		this->m_pStateMachine->PopState();	
		startReleased = false;
	}
}

//------------------------------------------------------------------------------
// Method:    Exit
// Returns:   void
// 
// Called when this state becomes inactive.
//------------------------------------------------------------------------------
void StatePaused::Exit()
{
	HeliGame::GetInstance()->GameObjectManager()->DestroyAllStateObjects(eStateGameplay_Paused);
	//EventManager::Instance()->ClearEvents();
}