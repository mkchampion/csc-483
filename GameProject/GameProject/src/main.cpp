//------------------------------------------------------------------------
// main
//
// Created:	2012/12/06
// Author:	Carel Boers
//	
// Main entry point of the game. Create a game, and run it.
//------------------------------------------------------------------------

#include <stdio.h>
#include "LevelInfo.h"
#include "HeliGame.h"

// FMOD Testing
#include "fmod.hpp"
#include "fmod_common.h"
#include <Windows.h>

int main(int argc, char* argv[])
{
	HeliGame game = HeliGame();
	return game.Run();
}


