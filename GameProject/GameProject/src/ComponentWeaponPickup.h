//------------------------------------------------------------------------
// ComponentWeaponPickup.h
//	
// Component meant to be used for weapon collection purposes
//------------------------------------------------------------------------

#ifndef COMPONENTWEAPONPICKUP_H
#define COMPONENTWEAPONPICKUP_H

#include <string>
#include "EventDelegate.h"
#include "GameObjectManager.h"

class ComponentWeaponPickup : public Common::ComponentBase
{

public:

	// Structors
	ComponentWeaponPickup();
	~ComponentWeaponPickup();

	void Init(const std::string& weaponNameP, std::string soundNameP);

	// Factory
	static Common::ComponentBase* CreateComponent(TiXmlNode* p_pNode);

	// Update/Family
	virtual void Update(float p_fDelta);
	virtual const std::string FamilyID() { return std::string("GOC_WeaponPickup"); }
	virtual const std::string ComponentID(){ return std::string("GOC_WeaponPickup"); }

	// Event Handling
	void InitDelegates();
	void HandleCollisions(EventDelegate::EventDataPointer p_pCollisionData);
	

private:

	// Only using for weapons right now
	std::string weaponName;

	// Delegate for events, need to remove on deletion
	EventDelegate* dynamicCollisionDelegate;

	std::string soundName = "";

};

#endif