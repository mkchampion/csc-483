// Simple struct to contain info about explosions

#ifndef EXPLOSIONINFO_H
#define EXPLOSIONINFO_H

#include <glm\glm.hpp>

struct ExplosionInfo
{
	glm::vec3 point;
	float radius = 0.0f;
	float duration = 0.0f;
	float timeActive = 0.0f;
	float damagePerSecond = 3.0f;
};

#endif