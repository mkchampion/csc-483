//------------------------------------------------------------------------
// SceneManager
//
// Created:	2012/12/23
// Author:	Carel Boers
//	
// A simple scene manager to manage our models and camera.
//------------------------------------------------------------------------

#include "SceneManager.h"
#include "RenderTargetManager.h"

#include <algorithm>

using namespace Common;

// Static singleton instance
SceneManager* SceneManager::s_pSceneManagerInstance = NULL;


//------------------------------------------------------------------------------
// Method:    CreateInstance
// Returns:   void
// 
// Creates the singletone instance.
//------------------------------------------------------------------------------
void SceneManager::CreateInstance()
{
	assert(s_pSceneManagerInstance == NULL);
	s_pSceneManagerInstance = new SceneManager();
}

//------------------------------------------------------------------------------
// Method:    DestroyInstance
// Returns:   void
// 
// Destroys the singleton instance.
//------------------------------------------------------------------------------
void SceneManager::DestroyInstance()
{
	assert(s_pSceneManagerInstance != NULL);
	delete s_pSceneManagerInstance;
	s_pSceneManagerInstance = NULL;
}

//------------------------------------------------------------------------------
// Method:    Instance
// Returns:   SceneManager::SceneManager*
// 
// Access to singleton instance.
//------------------------------------------------------------------------------
SceneManager* SceneManager::Instance()
{
	assert(s_pSceneManagerInstance);
	return s_pSceneManagerInstance;
}

//------------------------------------------------------------------------------
// Method:    SceneManager
// Returns:   
// 
// Constructor.
//------------------------------------------------------------------------------
SceneManager::SceneManager()
	:
	m_pCamera(NULL)
{
}

//------------------------------------------------------------------------------
// Method:    ~SceneManager
// Returns:   
// 
// Destructor.
//------------------------------------------------------------------------------
SceneManager::~SceneManager()
{
}

//------------------------------------------------------------------------------
// Method:    AddModel
// Parameter: wolf::Model * p_pModel
// Returns:   void
// 
// Adds a model the scene manager.
//------------------------------------------------------------------------------
void SceneManager::AddModel(wolf::Model* p_pModel)
{
	m_lModelList.push_back(p_pModel);
}

//------------------------------------------------------------------------------
// Method:    RemoveModel
// Parameter: wolf::Model * p_pModel
// Returns:   void
// 
// Removes a model from the scene manager.
//------------------------------------------------------------------------------
void SceneManager::RemoveModel(wolf::Model* p_pModel)
{
	ModelList::iterator it = std::find(m_lModelList.begin(), m_lModelList.end(), p_pModel);
	if (it != m_lModelList.end())
	{
		m_lModelList.erase(it);
	}	
}

//------------------------------------------------------------------------------
// Method:    Clear
// Returns:   void
// 
// Clears the list of models in the scene manager.
//------------------------------------------------------------------------------
void SceneManager::ClearModels()
{
	m_lModelList.clear();
}

//------------------------------------------------------------------------------
// Method:    AddModel
// Parameter: wolf::Model * p_pModel
// Returns:   void
// 
// Adds a model the scene manager.
//------------------------------------------------------------------------------
void SceneManager::AddEffect(Effect* p_pEffect)
{
	m_lEffectList.push_back(p_pEffect);
}

//------------------------------------------------------------------------------
// Method:    ClearEffects
// Returns:   void
// 
// Clears the list of effects in the scene manager.
//------------------------------------------------------------------------------
void SceneManager::ClearEffects()
{
	m_lEffectList.clear();
}

//------------------------------------------------------------------------------
// Method:    AttachCamera
// Parameter: SceneCamera * p_pCamera
// Returns:   void
// 
// Attaches the given camera to the scene
//------------------------------------------------------------------------------
void SceneManager::AttachCamera(SceneCamera* p_pCamera)
{
	m_pCamera = p_pCamera;
}

//------------------------------------------------------------------------------
// Method:    GetCamera
// Returns:   SceneCamera*
// 
// Returns the active camera.
//------------------------------------------------------------------------------
SceneCamera* SceneManager::GetCamera()
{
	return m_pCamera;
}


//------------------------------------------------------------------------------
// Method:    Update
// Returns:   void
// 
// Iterates the list of models and sprites and updates them.
// For now it does the small camera translations as well
//------------------------------------------------------------------------------
void SceneManager::Update(float p_fDelta)
{

	// Grab front and end of ModelList
	ModelList::iterator itModel = m_lModelList.begin();
	ModelList::iterator endModel = m_lModelList.end();

	for (; itModel != endModel; itModel++)
	{
		// Grab and update current model
		wolf::Model* pCurrentModel = static_cast<wolf::Model*>(*itModel);
		pCurrentModel->Update(p_fDelta);
	}

	// Vector for particle effects that will be deleted
	std::vector<Effect*> effectsToDelete;

	// Same for effects
	EffectList::iterator itEffect = m_lEffectList.begin();
	EffectList::iterator endEffect = m_lEffectList.end();

	for (; itEffect != endEffect; itEffect++)
	{
		// Grab and update current model
		Effect* pCurrentEffect = static_cast<Effect*>(*itEffect);
		pCurrentEffect->Update(p_fDelta);

		// Check if we can  delete it
		if (pCurrentEffect->IsReadyToDelete())
			effectsToDelete.push_back(pCurrentEffect);
	}

	// Clean up dead effects
	for (int i = 0; i < effectsToDelete.size(); i++)
	{
		EffectList::iterator it = std::find(m_lEffectList.begin(), m_lEffectList.end(), effectsToDelete.at(i));
		if (it != m_lEffectList.end())
		{
			m_lEffectList.erase(it);
		}

		printf("Deleting Effect\n");

		// Delete it as well
		delete effectsToDelete.at(i);
	}
}


//------------------------------------------------------------------------------
// Method:    Render
// Returns:   void
// 
// Iterates the list of models, applies the camera params to the shader and 
// renders the model.
//------------------------------------------------------------------------------
void SceneManager::Render(bool useLight)
{
	// Can't render without a camera
	if (m_pCamera == NULL)
	{
		return;
	}

	// Get the view/proj matrices from the camera
	glm::mat4 mProj, mView;
	wolf::RenderTarget* shadow = RenderTargetManager::Instance()->GetShadowRender();

	if (lightCam || useLight)
	{
		mProj = directionalLight->GetProjectionMatrix();
		mView = directionalLight->GetViewMatrix();
	}
	else
	{
		mProj = m_pCamera->GetProjectionMatrix();
		mView = m_pCamera->GetViewMatrix();
	}

	if (!useLight)
	{
		// Same for effects
		EffectList::iterator itEffect = m_lEffectList.begin();
		EffectList::iterator endEffect = m_lEffectList.end();

		for (; itEffect != endEffect; itEffect++)
		{
			// Grab and update current model
			Effect* pCurrentEffect = static_cast<Effect*>(*itEffect);
			pCurrentEffect->Render(mProj, mView);
		}
	}

	// Iterate over the list of models and render them
	ModelList::iterator it = m_lModelList.begin(), end = m_lModelList.end();
	for (; it != end; ++it)
	{
		wolf::Model* pModel = static_cast<wolf::Model*>(*it);

		// Set SHadow map
		std::map<std::string, wolf::Material*>* modelMats = pModel->GetMaterialMap();

		for (auto x : *modelMats)
		{
			x.second->SetTexture("shadowMap", shadow->GetDepthBufferT());
		}

		pModel->Render(mView, mProj, directionalLight->GetLightMatrix());
	}


	
}