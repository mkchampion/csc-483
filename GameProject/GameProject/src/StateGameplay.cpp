//------------------------------------------------------------------------
// StateGameplay
//
// Created:	2013/02/10
// Author:	Carel Boers
//	
// Gameplay state.
//
// Modified for Assignment 3 by Matthew Champion
//------------------------------------------------------------------------


#include "StateGameplay.h"
#include "States.h"
#include "HeliGame.h"
#include "SceneManager.h"
#include "TerrainManager.h"
#include "ComponentRenderableMesh.h"

//------------------------------------------------------------------------------
// Method:    StateGameplay
// Returns:   
// 
// Constructor.
//------------------------------------------------------------------------------
StateGameplay::StateGameplay()
{
}

//------------------------------------------------------------------------------
// Method:    ~StateGameplay
// Returns:   
// 
// Destructor.
//------------------------------------------------------------------------------
StateGameplay::~StateGameplay()
{
}

//------------------------------------------------------------------------------
// Method:    Enter
// Returns:   void
// 
// Called when this state becomes active.
//------------------------------------------------------------------------------
void StateGameplay::Enter(ActionData* p_stateData)
{
	
	// Making some GameObjects!
	Common::GameObjectManager* GOManager = HeliGame::GetInstance()->GameObjectManager();
	GOManager->InitDelegates();

	// Make the GO for the chopper
	Common::GameObject* helicopter = GOManager->CreateGameObject("GameProject/GameProject/data/GameObjects/Helicopter.xml");
	helicopter->GetTransform().SetTranslationXYZ(336.0f, 40.0f, -150.0f);
	//helicopter->GetTransform().SetScaleXYZ(4.0f, 4.0f, 4.0f);
	helicopter->SetDirection(180.0f);
	helicopter->AddTag("Player");
	GOManager->SetGameObjectGUID(helicopter, "Player");

	ComponentRenderableMesh* meshComponent = static_cast<ComponentRenderableMesh*>(helicopter->GetComponent("GOC_Renderable"));
	/*meshComponent->SetMeshRotation(2, true, 8.0f, 0.0f, glm::vec3(0.0f, 1.0f, 0.0f));
	meshComponent->SetMeshRotation(3, true, 8.0f, 0.0f, glm::vec3(1.0f, 0.0f, 0.0f));*/

	// Camera to use
	Common::GameObject* camera = GOManager->CreateGameObject("GameProject/GameProject/data/GameObjects/DragCamera.xml");
	GOManager->SetGameObjectGUID(camera, "Camera");

	// Attach the camera
	ComponentDragCamera* camComponent = static_cast<ComponentDragCamera*>(camera->GetComponent("GOC_Camera"));
	camComponent->SetTarget(helicopter);
	TerrainManager::Instance()->AttachCamera(camComponent->GetSceneCamera());
	Common::SceneManager::Instance()->AttachCamera(camComponent->GetSceneCamera());


	// Create each enemy / delete just so textures are loaded in (Super hack fix?)
	//Common::GameObject* enemy1 = GOManager->CreateGameObject("GameProject/GameProject/data/GameObjects/Tank.xml");
	//Common::GameObject* enemy2 = GOManager->CreateGameObject("GameProject/GameProject/data/GameObjects/Humvee.xml");
	//enemy1->GetTransform().SetTranslationXYZ(-1000.0f, 0.0f, 0.0f);
	//enemy2->GetTransform().SetTranslationXYZ(-1000.0f, 0.0f, 0.0f);
	//
	//GOManager->QueueGOForDeletion(enemy1);
	//GOManager->QueueGOForDeletion(enemy2);
	
}

//------------------------------------------------------------------------------
// Method:    Update
// Parameter: float p_fDelta
// Returns:   void
// 
// Called each from when this state is active.
//------------------------------------------------------------------------------
void StateGameplay::Update(float p_fDelta)
{
}

//------------------------------------------------------------------------------
// Method:    Exit
// Returns:   void
// 
// Called when this state becomes inactive.
//------------------------------------------------------------------------------
void StateGameplay::Exit()
{	
}