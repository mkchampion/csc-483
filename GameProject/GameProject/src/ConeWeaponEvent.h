//------------------------------------------------------------------------
// ConeWeaponEvent
//
// An event that is triggered when an enemy collides with a cone weapon
//------------------------------------------------------------------------

#ifndef CONEWEAPONCOLLISION_H
#define CONEWEAPONCOLLISION_H

#include "BaseEventData.h"
#include "GameObject.h"

class ConeWeaponEvent : public BaseEventData
{

public:

	// Static event type
	static const EventType s_EventType;

	// Constructor / Destructor
	explicit ConeWeaponEvent(Common::GameObject* p_pObject1, float p_fDamage) 
	{
		m_pManager = p_pObject1->GetManager();
		object1ID = p_pObject1->GetGUID();
		damageValue = p_fDamage;
	}

	~ConeWeaponEvent() {};

	// Accessor for event type
	const EventType& GetEventType() const { return s_EventType; }

	// Accesor by ID
	std::string GetObject1ID() const { return object1ID; }
	float GetDamageValue() const { return damageValue; }

	// Manager
	Common::GameObjectManager* GetManager() const { return m_pManager; }


private:

	// Object ID / Damage (Only passing damage for now, may change to the whole weapon / object if necessary)
	std::string object1ID;
	float damageValue;

	// Manager
	Common::GameObjectManager* m_pManager;

};

#endif

