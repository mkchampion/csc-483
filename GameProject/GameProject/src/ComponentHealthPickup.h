//------------------------------------------------------------------------
// ComponentHealthPickup.h
//	
// Component meant to be used for health pickups
// Should probably be a child class of a ComponentPickup?
//------------------------------------------------------------------------

#ifndef COMPONENTHEALTHPICKUP_H
#define COMPONENTHEALTHPICKUP_H

#include <string>
#include "EventDelegate.h"
#include "GameObjectManager.h"

class ComponentHealthPickup : public Common::ComponentBase
{

public:

	// Structors
	ComponentHealthPickup();
	~ComponentHealthPickup();

	void Init(float healthRestoredP, std::string soundNameP);

	// Factory
	static Common::ComponentBase* CreateComponent(TiXmlNode* p_pNode);

	// Update/Family
	virtual void Update(float p_fDelta);
	virtual const std::string FamilyID() { return std::string("GOC_HealthPickup"); }
	virtual const std::string ComponentID(){ return std::string("GOC_HealthPickup"); }

	// Event Handling
	void InitDelegates();
	void HandleCollisions(EventDelegate::EventDataPointer p_pCollisionData);


private:

	// Only using for weapons right now
	float healthRestored;

	// Delegate for events, need to remove on deletion
	EventDelegate* dynamicCollisionDelegate;

	std::string soundName = "";

};

#endif