//-----------------------------------------------------------------------------
// File:			ProjectileWeapon.h
// Original Author:	Matthew Champion
//-----------------------------------------------------------------------------

#ifndef PROJECTILEWEAPON_H
#define PROJECTILEWEAPON_H

#include "IWeapon.h"
#include <string>

class ProjectileWeapon : IWeapon
{
	
public:

	// Read weapon from file
	ProjectileWeapon(const std::string& weaponPathP);
	~ProjectileWeapon();

	void Update(float p_fDelta, Common::GameObject* ownerP, const glm::vec2& dirP);
	void StopWeapon() {}
	void SetTarget(Common::GameObject* p_pTarget) {};

private:

	// Projectile to fire (Uses another GO file)
	std::string projectilePath;

	// Values
	float damage;
	float fireRate;
	float speed;

	std::string soundName = "";

	// Boolean for if it splits into multiple shots?
	bool splitShot;
	int splitCount;
	float splitAngle;

	// Reset times
	bool canFire = true;
	float resetTime = 0.0f;
	double timeOfLastShot = 0.0;

};

#endif