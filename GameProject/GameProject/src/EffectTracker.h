//------------------------------------------------------------------------
// EffectTracker
//	
// A class that determines what effects should be used depending on gameplay state
// and the parameters that they are fed, etc.  Hardcoding effects mostly
//------------------------------------------------------------------------

#ifndef EFFECTTRACKER_H
#define EFFECTTRACKER_H

#include "RenderTargetManager.h";
#include "GameObjectManager.h"
#include "SceneCamera.h"

class EffectTracker
{


public:

	EffectTracker();
	~EffectTracker();

	void SetPlayer(const std::string& targetIDP) { playerID = targetIDP; }
	void SetManager(Common::GameObjectManager* p_pManager) { m_pManager = p_pManager; }

	void Update(float p_fDelta, const Common::SceneCamera* p_pCam);

	// Delegates
	void InitDelegates();
	void HandlePlayerDamage(EventDelegate::EventDataPointer p_pPlayerDamage);

private:

	// Effects
	EffectAndTarget damageEffect;
	EffectAndTarget horizontalBlur;
	EffectAndTarget verticalBlur;
	EffectAndTarget blurredGrad;
	EffectAndTarget deathEffect;

	void UpdateDamage(float p_fDelta);	

	// Manager
	Common::GameObjectManager* m_pManager;

	// ID for the player
	std::string playerID;


	// List of effects
	std::vector<EffectAndTarget>* m_pEffects;

	float testGrad = 0.0f;
	float maxGrad = 0.15f;

	// Death Timer
	float deathTimer = 0.0f;
	float fadeTime = 3.5f;

	float velocityDeath = 0.0f;
	glm::vec4 lastCameraPos;

};

#endif