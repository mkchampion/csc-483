//------------------------------------------------------------------------
// W_RenderTarget
//	
// Attempted wrapper around FBO's. 
// Based on the RenderTarget written by Gordon Wood for CSC 436, but from scratch to try and relearn the core
//------------------------------------------------------------------------

#include "W_RenderTarget.h"
#include "W_BufferManager.h"

using namespace wolf;

// Vertex for the quad that we'll render onto
struct QuadVertex
{
	GLfloat x, y;
	GLfloat u, v;
};

// Next power of 2 function taken from CSC436
static unsigned int nextPow2(unsigned int p_ui)
{
	p_ui--;
	p_ui |= p_ui >> 1;
	p_ui |= p_ui >> 2;
	p_ui |= p_ui >> 4;
	p_ui |= p_ui >> 8;
	p_ui |= p_ui >> 16;
	p_ui++;
	return p_ui;
}

// Constructor
RenderTarget::RenderTarget(unsigned int widthP, unsigned int heightP, float debugScale, bool isScreen)
{

	m_bIsScreen = isScreen;

	// Assign the width/height
	m_uWidth = (float)widthP*debugScale;
	m_uHeight = (float)heightP*debugScale;

	// Texture widths
	m_uTextureWidth = (float)nextPow2(widthP);
	m_uTextureHeight = (float)nextPow2(heightP);

	// Actually create the FrameBuffer (If not just the screen)
	if (!isScreen)
	{
		glGenFramebuffers(1, &m_uiFBO);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}
	else
	{
		m_uiFBO = 0;
		m_bHasColorTexture = false;
		m_bHasDepthTexture = false;
	}

	// Create the vertices
	QuadVertex quadVertices[] =
	{
		{ -1.0f, -1.0f, 0.0f, 0.0f },
		{ 1.0f, -1.0f, 1.0f, 0.0f },
		{ -1.0f, 1.0f, 0.0f, 1.0f },

		{ -1.0f, 1.0f, 0.0f, 1.0f },
		{ 1.0f, -1.0f, 1.0f, 0.0f },
		{ 1.0f, 1.0f, 1.0f, 1.0f }
	};

	// Adjust for texture width
	for (int i = 0; i < 6; i++)
	{
		quadVertices[i].u *= m_uWidth / m_uTextureWidth;
		quadVertices[i].v *= m_uHeight / m_uTextureHeight;
	}

	// Setup buffer/declaration
	m_pVertexBuffer = wolf::BufferManager::CreateVertexBuffer(quadVertices, sizeof(QuadVertex) * 6);
	m_pVertexDeclaration = new wolf::VertexDeclaration();
	m_pVertexDeclaration->Begin();
	m_pVertexDeclaration->AppendAttribute(wolf::AT_Position, 2, wolf::CT_Float);
	m_pVertexDeclaration->AppendAttribute(wolf::AT_TexCoord1, 2, wolf::CT_Float);
	m_pVertexDeclaration->SetVertexBuffer(m_pVertexBuffer);
	m_pVertexDeclaration->End();

}

// Destructor
RenderTarget::~RenderTarget()
{


	// Delete the FBO
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glDeleteFramebuffers(1, &m_uiFBO);
}

// Setting the color buffer to have a texture
void RenderTarget::SetupColorBuffer(bool isTexture)
{
	// Bind FBO
	glBindFramebuffer(GL_FRAMEBUFFER, m_uiFBO);

	if (isTexture)
	{
		// Generate/bind texture
		m_pColorBufferT = TextureManager::CreateTexture(NULL, m_uTextureWidth, m_uTextureHeight, wolf::Texture::Format::FMT_4444);
		m_uiColorBuffer = m_pColorBufferT->GetID();

		glBindTexture(GL_TEXTURE_2D, m_uiColorBuffer);

		// Set parameters
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

		// Attach
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_pColorBufferT->GetID(), 0);

		m_bHasColorTexture = true;
	}
	else
	{
		// Generate/bind renderbuffer
		glGenRenderbuffers(1, &m_uiColorBuffer);
		glBindRenderbuffer(GL_RENDERBUFFER, m_uiColorBuffer);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_RGBA, (unsigned int)m_uTextureWidth, (unsigned int)m_uTextureHeight);

		// Attach
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, m_uiColorBuffer);
	}

	// Unbind FBO
	glBindFramebuffer(GL_FRAMEBUFFER, 0);


}

// Set Depth Buffer as texture
void RenderTarget::SetupDepthBuffer(bool isTexture)
{
	// Bind FBO
	glBindFramebuffer(GL_FRAMEBUFFER, m_uiFBO);

	if (isTexture)
	{
		// Generate/bind texture
		m_pDepthBufferT = TextureManager::CreateTexture(NULL, m_uTextureWidth, m_uTextureHeight, wolf::Texture::Format::FMT_DEPTH24);
		m_uiDepthBuffer = m_pDepthBufferT->GetID();

		glBindTexture(GL_TEXTURE_2D, m_uiDepthBuffer);

		// Set parameters
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
		
		// Attach
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, m_pDepthBufferT->GetID(), 0);

		m_bHasDepthTexture = true;
	}
	else
	{
		// Generate/bind renderbuffer
		glGenRenderbuffers(1, &m_uiDepthBuffer);
		glBindRenderbuffer(GL_RENDERBUFFER, m_uiDepthBuffer);		
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, (unsigned int)m_uTextureWidth, (unsigned int)m_uTextureHeight);

		// Attach
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, m_uiDepthBuffer);
	}

	// Unbind FBO
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

// Bind this FBO
void RenderTarget::Bind() const
{
	// Bind
	glBindFramebuffer(GL_FRAMEBUFFER, m_uiFBO);

	// Clear
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Viewport
	glViewport(0, 0, m_uWidth, m_uHeight);

}

// Render
void RenderTarget::Render(Material* p_pMaterial, RenderTarget* p_pRenderTarget) const
{

	// Bind
	Bind();

	if (m_bIsScreen || m_bHasColorTexture)
	{
		p_pRenderTarget->GetColorBufferT()->Bind();
		p_pMaterial->SetTexture("ColorTex", p_pRenderTarget->GetColorBufferT());
	}

	m_pVertexDeclaration->Bind();	
	p_pMaterial->Apply();
	glDrawArrays(GL_TRIANGLES, 0, 6);

}

// Check for Completeness
bool RenderTarget::IsRenderTargetComplete()
{

	GLenum fboStatus;

	fboStatus = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	switch (fboStatus)
	{
	case GL_FRAMEBUFFER_COMPLETE:
		return true;

	default:
		return false;
		
	}

}