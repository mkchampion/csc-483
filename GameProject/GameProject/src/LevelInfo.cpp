//------------------------------------------------------------------------
// LevelInfo
//
// Created:	2015/10/26
// Author:	Matthew Champion
//	
// A class that contains all the information parsed from the TMX file used to generate levels
//------------------------------------------------------------------------


#include "LevelInfo.h"

// Temp? Make only debug?
#include "ErrorMessage.h"

// Constructor
LevelInfo::LevelInfo(const std::string &p_strTMXPath)
{
	TMXPath = p_strTMXPath;

	// Spawned objects
	spawnedObjects = new ObjectList();
	weaponPickups = new ObjectList();

	// Map allocation
	staticObjects = new ObjectList();
	textureMap = new std::map<int, std::string>();
	modelMap = new std::map<std::string, ModelInfo*>();
	GOMap = new std::map<std::string, std::string>();
	flyingZones = new std::vector<FlyBounds*>();
	heightMap = new std::map<int, ElevatedTextures*>();
	elevatedIDs = new std::vector<int>();
	checkPoints = new std::vector<glm::vec3>();

	// Parse everything in
	ParseTMX();

	// Identify the bounds of Flying Zones
	IdentifyFlyBounds();


}

// Destructor
LevelInfo::~LevelInfo()
{

	// Delete static objects
	for (int i = 0; i < staticObjects->size(); i++)
	{
		delete staticObjects->at(i);
	}
	staticObjects->clear();
	delete staticObjects;

	// Delete the map for ModelInfo
	for (auto x : *modelMap)
	{
		delete x.second;
	}
	delete modelMap;

	// Texture map
	delete textureMap;
	
	// Height map
	for (int i = 0; i < heightMap->size(); i++)
	{
		delete heightMap->at(i);
	}
	heightMap->clear();
	delete heightMap;
	
	// GIDs
	for (int i = 0; i < mapWidth; i++)
	{
		delete tileIDs[i];
	}
	delete tileIDs;
	
	// Flying Zones
	for (int i = 0; i < flyingZones->size(); i++)
	{
		delete flyingZones->at(i);
	}
	flyingZones->clear();
	delete flyingZones;

	// Elevated IDs
	elevatedIDs->clear();
	delete elevatedIDs;

	checkPoints->clear();
	delete checkPoints;

}

// Reading in the base data from the TMX
void LevelInfo::ParseTMX()
{

	// Opening up the given TMX file
	TiXmlDocument doc(TMXPath.c_str());

	// If it can't open it, print an error
	if (doc.LoadFile() == false)
	{
		DisplayError("TMX Error", "Failed to locate / load TMX file");
	}
	else
	{
		// Initial map element to get height/widths
		TiXmlNode* mapNode = doc.FirstChild("map");
		TiXmlElement* mapElement = mapNode->ToElement();

		// Get any additional properties (TextureMap / ModelMap / Default Scale)
		TiXmlNode* propertiesNode = mapNode->FirstChild("properties");
		TiXmlElement* propertyElement = propertiesNode->FirstChildElement("property");

		// Default length (Used for scale, for objects in Tiled)
		int defaultLength;

		// Going through the properties (Mostly other maps I know I need to parse)
		while (propertyElement)
		{

			// Grab the name of the elemnt
			const char* valueName = propertyElement->Attribute("name");

			if (strcmp(valueName, "BorderX") == 0)
			{
				propertyElement->QueryIntAttribute("value", &borderX);
			}

			else if (strcmp(valueName, "BorderZ") == 0)
			{
				propertyElement->QueryIntAttribute("value", &borderZ);
			}

			else if (strcmp(valueName, "TextureMap") == 0)
			{
				std::string textureMap = propertyElement->Attribute("value");

				// Read in all the info from the TextureMap
				ParseTextureMap(textureMap);
			}

			else if (strcmp(valueName, "ModelMap") == 0)
			{
				std::string modelMap = propertyElement->Attribute("value");

				// Read in all the info from the TextureMap
				ParseModelMap(modelMap);
			}

			else if (strcmp(valueName, "GOMap") == 0)
			{
				std::string GOMap = propertyElement->Attribute("value");

				// Read in all the info from the TextureMap
				ParseGOMap(GOMap);
			}

			else if (strcmp(valueName, "HeightMap") == 0)
			{
				std::string heightMap = propertyElement->Attribute("value");

				// Read in all the info from the TextureMap
				ParseHeightMap(heightMap);
			}

			else if (strcmp(valueName, "ElevatedIDs") == 0)
			{
				std::string elevatedIDfile = propertyElement->Attribute("value");

				// Read in IDs
				ParseElevatedID(elevatedIDfile);
			}

			else if (strcmp(valueName, "ObjectLength") == 0)
			{
				propertyElement->QueryIntAttribute("value",&defaultLength);
			}

			else if (strcmp(valueName, "ElevationSeparator") == 0)
			{
				propertyElement->QueryIntAttribute("value", &elevationSeparator);
			}
		

			propertyElement = propertyElement->NextSiblingElement("property");

		}

		// width / height of the map	
		mapWidth = 0;
		mapHeight = 0;		

		if (mapElement->QueryIntAttribute("width",&mapWidth) != TIXML_SUCCESS)
		{
			DisplayError("TMX Error", "Failed to parse map width");
			return;
		}
		
		if (mapElement->QueryIntAttribute("height", &mapHeight) != TIXML_SUCCESS)
		{
			DisplayError("TMX Error", "Failed to parse map height");
			return;
		}

		// width / height of the tiles
		tileWidth = 0;
		tileHeight = 0;

		if (mapElement->QueryIntAttribute("tilewidth", &tileWidth) != TIXML_SUCCESS)
		{
			DisplayError("TMX Error", "Failed to parse tile width");
			return;
		}

		if (mapElement->QueryIntAttribute("tileheight", &tileHeight) != TIXML_SUCCESS)
		{
			DisplayError("TMX Error", "Failed to parse tile height");
			return;
		}


		// Get the first layer node (Will only be 1 for my levels, if I need to expand it to more later, I can)
		TiXmlNode* layerNode = mapNode->FirstChild("layer");
		TiXmlNode* tileNode = layerNode->FirstChild("data")->FirstChild("tile");
		

		// 2D Array to contain the GID's
		tileIDs = (int**)malloc(sizeof(int) * mapHeight);

		for (int i = 0; i < mapHeight; i++)
		{
			tileIDs[i] = (int*)malloc(sizeof(int) * mapWidth);
		}	

 		// tileNodes
		for (int i = mapHeight - 1; i >= 0; i--)
		{
			if (tileNode == NULL)
				break;

			for (int j = 0; j < mapWidth; j++)
			{
				TiXmlElement *tileElement = tileNode->ToElement();

				if (tileElement->QueryIntAttribute("gid", &tileIDs[i][j]) != TIXML_SUCCESS)
				{
					DisplayError("TMX Error", "Error with a tile GID");
					return;
				}

				tileNode = tileNode->NextSibling();
			}
		}

		for (int i = 0; i < mapHeight; i++)
		{
			for (int j = 0; j < mapWidth; j++)
			{
				int test = tileIDs[i][j];
				printf("%d\n", test);
			}
		}
		
		// Go through the object groups
		TiXmlNode* objectGroupNode = mapNode->FirstChild("objectgroup");
		
		while (objectGroupNode)
		{

			const char* groupName = objectGroupNode->ToElement()->Attribute("name");

			// Grab the child element for the type
			TiXmlNode* groupProperties = objectGroupNode->FirstChildElement("properties");
			TiXmlElement* groupProperty = groupProperties->FirstChildElement("property");



			// Properties for the object group
			std::string groupType = "";
			bool canCollide = false;


			// Go through the properties of the group
			while (groupProperty)
			{
				const char* propertyName = groupProperty->Attribute("name");
				if (propertyName == NULL)
				{
					printf("Error reading property name \n");
				}

				// IF the property is a type
				if (strcmp(propertyName, "Type") == 0)
				{
					const char* tempType = groupProperty->Attribute("value");
					if (propertyName == NULL)
					{
						printf("Error reading group type \n");
					}
					groupType = tempType;
				}

				// IF the property is a type
				if (strcmp(propertyName, "CanCollide") == 0)
				{
					if (groupProperty->QueryBoolAttribute("value", &canCollide) != TIXML_SUCCESS)
					{
						printf("Error reading collision flag");
					}
				}

				groupProperty = groupProperty->NextSiblingElement("property");
			}
			

			
			// Vector to hold the children data
			ObjectList objects = ObjectList();

			// Go through the groups children
			TiXmlElement* object = objectGroupNode->FirstChildElement("object");
			while (object)
			{
				int id = 0;
				int x = 0;
				int y = 0;
				int rotation = 0;
				int width = defaultLength;
				std::string objectType = "";

				// ID
				if (object->QueryIntAttribute("id", &id) != TIXML_SUCCESS)
				{
					DisplayError("TMX Error", "Error with an object ID");
					return;
				}

				// X
				if (object->QueryIntAttribute("x", &x) != TIXML_SUCCESS)
				{
					DisplayError("TMX Error", "Error with object X coordinate");
					return;
				}

				// Y
				if (object->QueryIntAttribute("y", &y) != TIXML_SUCCESS)
				{
					DisplayError("TMX Error", "Error with object Y coordinate");
					return;
				}

				// Rotation
				if (object->QueryIntAttribute("rotation", &rotation) == TIXML_NO_ATTRIBUTE)
				{
					printf("No rotation for object, leaving at 0\n");
				}

				// Width (Used for scale)
				if (object->QueryIntAttribute("width", &width) == TIXML_NO_ATTRIBUTE)
				{
					printf("No scale adjustment for object, leaving at default %d\n", defaultLength);
				}

				// May have

				const char* tempType = object->Attribute("type");
				if (tempType == NULL)
				{
					printf("Error reading model path from model map\n");
				}
				objectType = tempType;

				float spawnHeight = 0.0f;

				// Check for properties
				TiXmlNode* objectProperties = object->FirstChildElement("properties");
				if (objectProperties != NULL)
				{
					TiXmlElement* objectProperty = objectProperties->FirstChildElement("property");

					// Go through the properties of the group
					while (objectProperty)
					{
						const char* propertyName = objectProperty->Attribute("name");
						if (propertyName == NULL)
						{
							printf("Error reading property name \n");
						}

						// IF the property is a type
						if (strcmp(propertyName, "Height") == 0)
						{
							objectProperty->QueryFloatAttribute("value", &spawnHeight);
						}

						objectProperty = objectProperty->NextSiblingElement("property");
					}
				}
				


				// Adjust the y value to go in the -z direction and from the bottom of the map
				y = -1 * (mapHeight * tileHeight - y);

				// Find the scale
				float scaleValue = (float)width / defaultLength;

				// Create an info object and add it to the current vector
				TMXObjectInfo* newObject = new TMXObjectInfo(id, x, y, rotation, scaleValue, objectType);
				newObject->canCollide = canCollide;
				newObject->spawnHeight = spawnHeight;
				objects.push_back(newObject);
	
				// Move on
				object = object->NextSiblingElement("object");

			}


			// If if was spawn definitions
			if (strcmp(groupType.c_str(), "Spawn") == 0)
			{
				*spawnedObjects = objects;
			}

			// If if was spawn definitions
			else if (strcmp(groupType.c_str(), "WeaponPickup") == 0)
			{
				*weaponPickups = objects;
			}

			// Static renderable definitions
			else if (strcmp(groupType.c_str(), "Render") == 0)
			{
				for (int i = 0; i < objects.size(); i++)
				{
					staticObjects->push_back(objects.at(i));
				}
			}

			// Checkpoints
			else if (strcmp(groupType.c_str(), "Checkpoint") == 0)
			{
				for (int i = 0; i < objects.size(); i++)
				{
					glm::vec3 pos = glm::vec3(objects.at(i)->x, 40.0f, objects.at(i)->y);
					checkPoints->push_back(pos);
				}

			}

			// Next group
			objectGroupNode = objectGroupNode->NextSibling("objectgroup");
		}
	}
}

// Reading in the data from the texture map
void LevelInfo::ParseTextureMap(const std::string &p_strTextureMap)
{
	// Opening up the given TMX file
	printf("%s\n", p_strTextureMap.c_str());
	TiXmlDocument doc(p_strTextureMap.c_str());

	// If it can't open it, print an error
	if (doc.LoadFile() == false)
	{
		DisplayError("TMX Error", "Failed to locate / load texture map");
	}
	else
	{

		TiXmlNode* mapNode = doc.FirstChild("TextureMap");

		// Go through each child element of the node
		TiXmlElement* textureLink = mapNode->FirstChildElement("Link");
		while (textureLink)
		{
						
			int tileID = 0;
			std::string texturePath = "";

			// ID
			if (textureLink->QueryIntAttribute("tileID", &tileID) != TIXML_SUCCESS)
			{
				DisplayError("TMX Error", "Error with tileID in texture map");
			}

			// Path
			const char* path = textureLink->Attribute("texturePath");
			if (path == NULL)
			{
				DisplayError("TMX Error", "Error with texture path in texture map");
			}
			texturePath = path;


			textureMap->insert(std::pair<int, std::string>(tileID, texturePath));
			
			textureLink = textureLink->NextSiblingElement("Link");
		}
	}
}

// Reading in the data from the texture map
void LevelInfo::ParseModelMap(const std::string &p_strModelMap)
{
	// Opening up the given TMX file
	printf("%s\n", p_strModelMap.c_str());
	TiXmlDocument doc(p_strModelMap.c_str());

	// If it can't open it, print an error
	if (doc.LoadFile() == false)
	{
		DisplayError("TMX Error", "Failed to locate / load model map");
	}
	else
	{

		TiXmlNode* mapNode = doc.FirstChild("ModelMap");

		// Go through each child element of the node
		TiXmlElement* modelLink = mapNode->FirstChildElement("Link");
		while (modelLink)
		{

			std::string objectType = "";

			float scale = 1.0f;
			std::string modelPath = "";
			std::string texturePath = "";
			std::string vertexPath = "";
			std::string fragmentPath = "";

			// Type
			const char* type = modelLink->Attribute("type");
			if (type == NULL)
			{
				printf("Error reading type from model map\n");
			}
			objectType = type;

			// Scale
			if (modelLink->QueryFloatAttribute("Scale", &scale) != TIXML_SUCCESS)
			{
				printf("Error reading type from scale from model map\n");
			}

			// Model path
			const char* tempModel = modelLink->Attribute("ModelPath");
			if (tempModel == NULL)
			{
				printf("Error reading model path from model map\n");
			}
			modelPath = tempModel;

			// Texture path
			const char* tempTexture = modelLink->Attribute("TexturePath");
			if (tempTexture == NULL)
			{
				printf("Error reading texture path from model map\n");
			}
			texturePath = tempTexture;

			// Vertex path
			const char* tempVertex = modelLink->Attribute("VertexPath");
			if (tempVertex == NULL)
			{
				printf("Error reading vertex path from model map\n");
			}
			vertexPath = tempVertex;
			
			// Fragment path
			const char* tempFragment = modelLink->Attribute("FragmentPath");
			if (tempFragment == NULL)
			{
				printf("Error reading fragment path from model map\n");
			}
			fragmentPath = tempFragment;

			// Create the model info struct
			ModelInfo* info = new ModelInfo(scale, modelPath, texturePath, vertexPath, fragmentPath);

			modelMap->insert(std::pair<std::string, ModelInfo*>(objectType, info));

			modelLink = modelLink->NextSiblingElement("Link");
		}
	}
}

// Reading in the data from the texture map
void LevelInfo::ParseGOMap(const std::string &p_strGOMap)
{
	// Opening up the given TMX file
	printf("%s\n", p_strGOMap.c_str());
	TiXmlDocument doc(p_strGOMap.c_str());

	// If it can't open it, print an error
	if (doc.LoadFile() == false)
	{
		DisplayError("TMX Error", "Failed to locate / load GO map");
	}
	else
	{

		TiXmlNode* mapNode = doc.FirstChild("GOMap");

		// Go through each child element of the node
		TiXmlElement* GOLink = mapNode->FirstChildElement("Link");
		while (GOLink)
		{

			std::string objectType = "";
			std::string GOPath = "";

			// Model path
			const char* tempType = GOLink->Attribute("type");
			if (tempType == NULL)
			{
				printf("Error reading model path from model map\n");
			}
			objectType = tempType;

			// Texture path
			const char* tempPath = GOLink->Attribute("GOPath");
			if (tempPath == NULL)
			{
				printf("Error reading texture path from model map\n");
			}
			GOPath = tempPath;
			
			GOMap->insert(std::pair<std::string, std::string>(objectType, GOPath));

			GOLink = GOLink->NextSiblingElement("Link");
		}
	}
}

void LevelInfo::ParseElevatedID(const std::string &p_strElevated)
{
	// Opening up the given TMX file
	printf("%s\n", p_strElevated.c_str());
	TiXmlDocument doc(p_strElevated.c_str());

	// If it can't open it, print an error
	if (doc.LoadFile() == false)
	{
		DisplayError("TMX Error", "Failed to locate / load Elevated Texture ID's");
	}
	else
	{

		TiXmlNode* mapNode = doc.FirstChild("ElevatedTiles");

		// Go through each child element of the node
		TiXmlElement* elevatedTile = mapNode->FirstChildElement("ID");
		while (elevatedTile)
		{

			int tileID = 0;

			// ID
			if (elevatedTile->QueryIntAttribute("value", &tileID) != TIXML_SUCCESS)
			{
				DisplayError("TMX Error", "Error with ID in elevated ID map");
			}

			elevatedIDs->push_back(tileID);

			elevatedTile = elevatedTile->NextSiblingElement("ID");
		}
	}
}

// Reading in the data from the texture map
void LevelInfo::ParseHeightMap(const std::string &p_strHeightMap)
{
	// Opening up the given TMX file
	printf("%s\n", p_strHeightMap.c_str());
	TiXmlDocument doc(p_strHeightMap.c_str());

	// If it can't open it, print an error
	if (doc.LoadFile() == false)
	{
		DisplayError("TMX Error", "Failed to locate / load Height map");
	}
	else
	{

		TiXmlNode* mapNode = doc.FirstChild("HeightMap");

		// Go through each child element of the node
		TiXmlElement* heightMapLink = mapNode->FirstChildElement("Link");
		while (heightMapLink)
		{

			int heightMapID = 0;
			ElevatedTextures* newElevation = new ElevatedTextures();

			// ID
			if (heightMapLink->QueryIntAttribute("ID", &heightMapID) != TIXML_SUCCESS)
			{
				DisplayError("TMX Error", "Error with ID in height map");
			}

			// Path
			const char* path = heightMapLink->Attribute("heightMap");
			if (path == NULL)
			{
				DisplayError("TMX Error", "Error with texture path in height map");
			}
			newElevation->heightMapPath = path;

			// Path
			const char* texture1Path = heightMapLink->Attribute("texture1");
			if (path == NULL)
			{
				DisplayError("TMX Error", "Error with peak path in height map");
			}
			newElevation->texture1 = texture1Path;

			// Path
			const char* texture2Path = heightMapLink->Attribute("texture2");
			if (path == NULL)
			{
				DisplayError("TMX Error", "Error with peak path in height map");
			}
			newElevation->texture2 = texture2Path;

			// Path
			const char* texture3Path = heightMapLink->Attribute("texture3");
			if (path == NULL)
			{
				DisplayError("TMX Error", "Error with peak path in height map");
			}
			newElevation->texture3 = texture3Path;

			// Path
			const char* texture4Path = heightMapLink->Attribute("texture4");
			if (path == NULL)
			{
				DisplayError("TMX Error", "Error with peak path in height map");
			}
			newElevation->texture4 = texture4Path;

			// Monutain break
			if (heightMapLink->QueryFloatAttribute("break1", &newElevation->mountainBreak1) != TIXML_SUCCESS)
			{
				DisplayError("TMX Error", "Error with ID in height map");
			}

			// Monutain break
			if (heightMapLink->QueryFloatAttribute("break2", &newElevation->mountainBreak2) != TIXML_SUCCESS)
			{
				DisplayError("TMX Error", "Error with ID in height map");
			}

			heightMap->insert(std::pair<int, ElevatedTextures*>(heightMapID, newElevation));

			heightMapLink = heightMapLink->NextSiblingElement("Link");
		}
	}
}


// Returns the y value in the map of the area where there is next an elevated terrain boundry
int LevelInfo::IdentifyNextElevated(int startRow)
{
	int value = -1;

	for (int i = startRow; i < mapHeight; i++)
	{
		for (int j = 0; j < mapWidth; j++)
		{

			// Found the first part of an elevated area
			if (tileIDs[i][j] == elevationSeparator)
			{
				printf("Found row #%d\n", i);
				return i;
			}
		}
	}

	return value;
}


void LevelInfo::IdentifyFlyBounds()
{
	
	bool morePairs = true;
	int boundsID = 0;
	int currentRow = 0;

	while (morePairs)
	{

		// Grab the row of the first
		int firstRow = IdentifyNextElevated(currentRow);

		// There won't be any more pairs
		if (firstRow == -1)
		{
			break;
			morePairs = false;
		}

		int secondRow = IdentifyNextElevated(firstRow + 1);
		
		// No second row found
		if (secondRow == -1)
		{
			break;
			morePairs = false;
		}

		currentRow = secondRow + 1;		

		// If it made it this far, add a flying bound
		FlyBounds* newBounds = new FlyBounds();
		newBounds->minRow = firstRow;
		newBounds->maxRow = secondRow;
		newBounds->id = boundsID;

		flyingZones->push_back(newBounds);

		// Up ID
		boundsID++;

	}
	
}

