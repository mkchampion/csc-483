//------------------------------------------------------------------------
// ComponentRenderableMesh
//
// Created:	2012/12/14
// Author:	Carel Boers
//	
// This class implements a component that is renderable.
//------------------------------------------------------------------------

#include "W_Model.h"
#include "ComponentRenderableMesh.h"
#include "GameObject.h"
#include "SceneManager.h"

//------------------------------------------------------------------------------
// Method:    ComponentRenderableMesh
// Returns:   
// 
// Constructor.
//------------------------------------------------------------------------------
ComponentRenderableMesh::ComponentRenderableMesh() : m_pModel(NULL)
{
	m_mCustomTransforms = new std::map<int, wolf::CustomTransform*>();
}

//------------------------------------------------------------------------------
// Method:    ~ComponentRenderableMesh
// Returns:   
// 
// Destructor.
//------------------------------------------------------------------------------
ComponentRenderableMesh::~ComponentRenderableMesh()
{
	// Remove the model from the scene
	Common::SceneManager::Instance()->RemoveModel(m_pModel);

	// Delete the model
	if (m_pModel)
	{
		delete m_pModel;
		m_pModel = NULL;

		// Delete custom transforms
		for (auto x : *m_mCustomTransforms)
		{
			delete x.second;
		}
		delete m_mCustomTransforms;
	}
}

//------------------------------------------------------------------------------
// Method:    Init
// Parameter: const std::string & p_strPath
// Parameter: const std::string & p_strTexturePath
// Parameter: const std::string & p_strVertexProgramPath
// Parameter: const std::string & p_strFragmentProgramPath
// Returns:   void
// 
// Initializes this component by loading the renderable object.
//------------------------------------------------------------------------------
void ComponentRenderableMesh::Init(const std::string& p_strPath, const std::string &p_strTexturePath, const std::string &p_strVertexProgramPath, const std::string &p_strFragmentProgramPath)
{
	m_pModel = new wolf::Model(p_strPath, p_strTexturePath, p_strVertexProgramPath, p_strFragmentProgramPath);
	Common::SceneManager::Instance()->AddModel(m_pModel);

	// Give the model the custom mesh transforms
	m_pModel->SetCustomTransform(m_mCustomTransforms);

	// Get material map
	std::map<std::string, wolf::Material*>* modelMats = m_pModel->GetMaterialMap();

	// Set lighting uniforms on all materials,
	for (auto x : *modelMats)
	{
		x.second->SetUniform("LightDir", glm::normalize(Common::SceneManager::Instance()->GetDirectionalLight()->lightDir));
		x.second->SetUniform("LightColor", Common::SceneManager::Instance()->GetDirectionalLight()->lightColor);
		x.second->SetUniform("Ambient", Common::SceneManager::Instance()->GetDirectionalLight()->ambientColor);
		x.second->SetUniform("ColorMask", glm::vec4(1.0f, 1.0f, 1.0f, 1.0f));
	}
}

//------------------------------------------------------------------------------
// Method:    SyncTransform
// Returns:   void
// 
// TODO: document me.
//------------------------------------------------------------------------------
void ComponentRenderableMesh::SyncTransform()
{
	// Weird transform sync when the rotation seems to be off
	glm::mat4 transform = this->GetGameObject()->GetTransform().GetTransformation();
	glm::mat4 rotation = glm::rotate(m_fRotationCorrection, glm::vec3(0.0f, 1.0f, 0.0f));
	glm::mat4 newTransform = transform * rotation;

	m_pModel->SetTransform(newTransform);

	Common::Transform trans = this->GetGameObject()->GetTransform();
	m_pModel->SetTransformComponents(trans.GetTranslation(), trans.GetRotation(), trans.GetScale());
	
}

void ComponentRenderableMesh::SetMeshRotation(int meshNumP, bool linkedToBase, float speedP, float rotation, const glm::vec3& axisP)
{
	// Grab the mesh from the model
	wolf::MeshRotationInfo* meshInfo = m_pModel->GetMeshRotation(meshNumP);

	if (meshInfo != NULL)
	{
		meshInfo->id = meshNumP;
		meshInfo->isRotating = true;
		meshInfo->isLinkedToBase = linkedToBase;
		meshInfo->currentRotation = rotation;
		meshInfo->rotationAxis = axisP;
		meshInfo->rotationSpeed = speedP;
	}
}

void ComponentRenderableMesh::SetMeshRotation(wolf::MeshRotationInfo meshRotationP)
{
	// Grab the mesh from the model
	wolf::MeshRotationInfo* meshInfo = m_pModel->GetMeshRotation(meshRotationP.id);

	if (meshInfo != NULL)
	{
		meshInfo->id = meshRotationP.id;
		meshInfo->isRotating = true;
		meshInfo->isLinkedToBase = meshRotationP.isLinkedToBase;
		meshInfo->currentRotation = meshRotationP.currentRotation;
		meshInfo->rotationAxis = meshRotationP.rotationAxis;
		meshInfo->rotationSpeed = meshRotationP.rotationSpeed;
	}
}

void ComponentRenderableMesh::StopMeshRotation(int meshNumP)
{
	// Grab the mesh from the model
	wolf::MeshRotationInfo* meshInfo = m_pModel->GetMeshRotation(meshNumP);
	if (meshInfo != NULL)
	{
		meshInfo->isRotating = false;
	}
}

void ComponentRenderableMesh::SetColorMask(const glm::vec4& colorMaskP)
{
	std::map<std::string, wolf::Material*>* modelMats = m_pModel->GetMaterialMap();

	// Set lighting uniforms on all materials,
	for (auto x : *modelMats)
	{
		x.second->SetUniform("ColorMask", colorMaskP);
	}
}

//------------------------------------------------------------------------------
// Method:    CreateComponent
// Parameter: TiXmlNode * p_pNode
// Returns:   Common::ComponentBase*
// 
// Factory construction method. Returns an initialized ComponentRenderableMesh
// instance if the passed in XML is valid, NULL otherwise.
//------------------------------------------------------------------------------
Common::ComponentBase* ComponentRenderableMesh::CreateComponent(TiXmlNode* p_pNode)
{
	assert(strcmp(p_pNode->Value(), "GOC_RenderableMesh") == 0);
	ComponentRenderableMesh* pRenderableMesh = new ComponentRenderableMesh();

	// Attributes that need set
	std::string strModelPath;
	std::string strTexturePath;
	std::string strVShader;
	std::string strFShader;
	float rotationCorrection = 0.0f;
	float scale = 1.0f;

	// Mesh rotations being read in
	std::vector<wolf::MeshRotationInfo> meshRotations;

	// Iterate Anim elements in the XML
	TiXmlNode* pChildNode = p_pNode->FirstChild();
	while (pChildNode != NULL)
	{
		const char* szNodeName = pChildNode->Value();

		if (strcmp(szNodeName, "Model") == 0)
		{
			// Parse attributes
			TiXmlElement* pElement = pChildNode->ToElement();

			// Name
			const char* szName = pElement->Attribute("path");
			if (szName == NULL)
			{
				delete pRenderableMesh;
				return NULL;
			}

			strModelPath = szName;
		}

		else if (strcmp(szNodeName, "Textures") == 0)
		{

			// Parse attributes
			TiXmlElement* pElement = pChildNode->ToElement();

			// Name
			const char* szName = pElement->Attribute("path");
			if (szName == NULL)
			{
				delete pRenderableMesh;
				return NULL;
			}

			strTexturePath = szName;
		}

		else if (strcmp(szNodeName, "VertexProgram") == 0)
		{

			// Parse attributes
			TiXmlElement* pElement = pChildNode->ToElement();

			// Name
			const char* szName = pElement->Attribute("path");
			if (szName == NULL)
			{
				delete pRenderableMesh;
				return NULL;
			}

			strVShader = szName;
		}

		else if (strcmp(szNodeName, "FragmentProgram") == 0)
		{

			// Parse attributes
			TiXmlElement* pElement = pChildNode->ToElement();

			// Name
			const char* szName = pElement->Attribute("path");
			if (szName == NULL)
			{
				delete pRenderableMesh;
				return NULL;
			}

			strFShader = szName;
		}

		else if (strcmp(szNodeName, "RotationCorrection") == 0)
		{

			// Parse attributes
			TiXmlElement* pElement = pChildNode->ToElement();

			// Rotation Correction
			if (pElement->QueryFloatAttribute("value", &rotationCorrection) != TIXML_SUCCESS)
			{
				delete pRenderableMesh;
				return NULL;
			}
		}

		else if (strcmp(szNodeName, "Scale") == 0)
		{

			// Parse attributes
			TiXmlElement* pElement = pChildNode->ToElement();

			// Rotation Correction
			if (pElement->QueryFloatAttribute("value", &scale) != TIXML_SUCCESS)
			{
				delete pRenderableMesh;
				return NULL;
			}
		}

		else if (strcmp(szNodeName, "MeshTransforms") == 0)
		{

			// Parse through the transforms
			TiXmlNode* meshNode = pChildNode->FirstChild("Mesh");

			// Grab the ID
			int meshID;

			if (meshNode->ToElement()->QueryIntAttribute("ID", &meshID) != TIXML_SUCCESS)
			{
				printf("No ID found for mesh");
				delete pRenderableMesh;
				return NULL;
			}

			// Grab translation
			glm::mat4 translation;
			TiXmlElement* translationElement = meshNode->FirstChildElement("Translation");

			if (translationElement != NULL)
			{
				
				// Parse the x/y/z
				float x, y, z;

				if (translationElement->QueryFloatAttribute("x", &x) != TIXML_SUCCESS)
				{
					x = 0.0f;
				}

				if (translationElement->QueryFloatAttribute("y", &y) != TIXML_SUCCESS)
				{
					y = 0.0f;
				}

				if (translationElement->QueryFloatAttribute("z", &z) != TIXML_SUCCESS)
				{
					z = 0.0f;
				}

				translation = glm::translate(glm::vec3(x, y, z));
				
			}

			// Rotation
			glm::mat4 rotation;
			TiXmlElement* rotationElement = meshNode->FirstChildElement("Rotation");

			if (rotationElement != NULL)
			{

				// Parse the amount and axis x/y/z
				float amount, x, y, z;

				if (rotationElement->QueryFloatAttribute("rotationAmount", &amount) != TIXML_SUCCESS)
				{
					x = 0.0f;
				}

				if (rotationElement->QueryFloatAttribute("x", &x) != TIXML_SUCCESS)
				{
					x = 0.0f;
				}

				if (rotationElement->QueryFloatAttribute("y", &y) != TIXML_SUCCESS)
				{
					y = 0.0f;
				}

				if (rotationElement->QueryFloatAttribute("z", &z) != TIXML_SUCCESS)
				{
					z = 0.0f;
				}
				
				rotation = glm::rotate(amount, x, y, z);

			}

			// Scale
			glm::mat4 scale;
			TiXmlElement* scaleElement = meshNode->FirstChildElement("Scale");

			if (scaleElement != NULL)
			{

				// Parse the amount
				float x, y, z;

				if (scaleElement->QueryFloatAttribute("x", &x) != TIXML_SUCCESS)
				{
					x = 1.0f;
				}

				if (scaleElement->QueryFloatAttribute("y", &y) != TIXML_SUCCESS)
				{
					y = 1.0f;
				}

				if (scaleElement->QueryFloatAttribute("z", &z) != TIXML_SUCCESS)
				{
					z = 1.0f;
				}

				scale = glm::scale(glm::vec3(x, y, z));

			}

			// Make the transform
			wolf::CustomTransform* customTransform = new wolf::CustomTransform();
			customTransform->translation = translation;
			customTransform->rotation = rotation;
			customTransform->scale = scale;

			pRenderableMesh->AddCustomTransform(meshID, customTransform);

		}

		else if (strcmp(szNodeName, "MeshRotation") == 0)
		{
			// Parse attributes
			TiXmlElement* pElement = pChildNode->ToElement();

			// Rotation
			wolf::MeshRotationInfo meshRotation;

			if (pElement->QueryIntAttribute("meshNum", &meshRotation.id) != TIXML_SUCCESS)
			{
				delete pRenderableMesh;
				return NULL;
			}

			if (pElement->QueryBoolAttribute("linkedToBase", &meshRotation.isLinkedToBase) != TIXML_SUCCESS)
			{
				delete pRenderableMesh;
				return NULL;
			}

			if (pElement->QueryFloatAttribute("speed", &meshRotation.rotationSpeed) != TIXML_SUCCESS)
			{
				delete pRenderableMesh;
				return NULL;
			}

			if (pElement->QueryFloatAttribute("rotationAmount", &meshRotation.currentRotation) != TIXML_SUCCESS)
			{
				delete pRenderableMesh;
				return NULL;
			}

			if (pElement->QueryFloatAttribute("axisX", &meshRotation.rotationAxis.x) != TIXML_SUCCESS)
			{
				delete pRenderableMesh;
				return NULL;
			}

			if (pElement->QueryFloatAttribute("axisY", &meshRotation.rotationAxis.y) != TIXML_SUCCESS)
			{
				delete pRenderableMesh;
				return NULL;
			}

			if (pElement->QueryFloatAttribute("axisZ", &meshRotation.rotationAxis.z) != TIXML_SUCCESS)
			{
				delete pRenderableMesh;
				return NULL;
			}

			// Add to list
			meshRotations.push_back(meshRotation);

		}

		pChildNode = pChildNode->NextSibling();
	}
	
	// Setup and return
	pRenderableMesh->Init(strModelPath, strTexturePath, strVShader, strFShader);
	pRenderableMesh->SetRotationCorrection(rotationCorrection);
	
	// Set the rotations now
	for (int i = 0; i < meshRotations.size(); i++)
	{
		pRenderableMesh->SetMeshRotation(meshRotations.at(i));
	}

	return pRenderableMesh;
}