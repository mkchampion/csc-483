#include "W_GridMesh.h"
#include "W_Common.h"
#include "GameProject\GameProject\src\DrawCounting.h"
#include "LevelInfo.h"

using namespace wolf;

//------------------------------------------------------------------------------
// Method:    BuildableMesh
// Returns:   void
// 
// Constructor
//------------------------------------------------------------------------------
GridMesh::GridMesh(const std::string& p_strMaterialName, const ElevatedTextures* textureBreakInfo, const std::string& p_strVertexProgram, const std::string& p_strFragmentProgram)
{
	vertexOffset = 0;

	m_mWorldTransform = glm::translate(0.0f, 0.0f, 0.0f);

	// Create the program
	m_pMaterial = wolf::MaterialManager::CreateMaterial(p_strMaterialName);

	// First texture
	m_pTexture1 = wolf::TextureManager::CreateTexture(textureBreakInfo->texture1);
	m_pTexture1->SetWrapMode(wolf::Texture::WrapMode::WM_Repeat, wolf::Texture::WrapMode::WM_Repeat);

	// second texture
	m_pTexture2 = wolf::TextureManager::CreateTexture(textureBreakInfo->texture2);
	m_pTexture2->SetWrapMode(wolf::Texture::WrapMode::WM_Repeat, wolf::Texture::WrapMode::WM_Repeat);

	// Third / Fourth textures
	m_pTexture3 = wolf::TextureManager::CreateTexture(textureBreakInfo->texture3);
	m_pTexture3->SetWrapMode(wolf::Texture::WrapMode::WM_Repeat, wolf::Texture::WrapMode::WM_Repeat);

	m_pTexture4 = wolf::TextureManager::CreateTexture(textureBreakInfo->texture4);
	m_pTexture4->SetWrapMode(wolf::Texture::WrapMode::WM_Repeat, wolf::Texture::WrapMode::WM_Repeat);

	// Create material
	m_pMaterial->SetProgram(p_strVertexProgram, p_strFragmentProgram);
	m_pMaterial->SetTexture("texture1", m_pTexture1);
	m_pMaterial->SetTexture("texture2", m_pTexture2);
	m_pMaterial->SetTexture("texture3", m_pTexture3);
	m_pMaterial->SetTexture("texture4", m_pTexture4);

	// Give it the break data
	m_pMaterial->SetUniform("Break1", textureBreakInfo->mountainBreak1);
	m_pMaterial->SetUniform("Break2", textureBreakInfo->mountainBreak2);
}

// Destructor
GridMesh::~GridMesh()
{
}

// Declaration of the vertex buffer
bool GridMesh::DeclareVertexBuffer()
{
	// Array to pass
	MeshVertex* meshVerts = &meshVertices[0];
	GLuint* meshI = &meshIndices[0];

	m_pVertexBuffer = wolf::BufferManager::CreateVertexBuffer(meshVerts, sizeof(MeshVertex) * meshVertices.size());

	m_pIndexBuffer = wolf::BufferManager::CreateIndexBuffer(sizeof(GLuint) * meshIndices.size());
	m_pIndexBuffer->Write(meshI, sizeof(GLuint) * meshIndices.size());

	m_pVertexDeclaration = new wolf::VertexDeclaration();
	m_pVertexDeclaration->Begin();
	m_pVertexDeclaration->AppendAttribute(wolf::AT_Position, 3, wolf::CT_Float);
	m_pVertexDeclaration->AppendAttribute(wolf::AT_TexCoord1, 2, wolf::CT_Float);
	m_pVertexDeclaration->AppendAttribute(wolf::AT_TexCoord2, 2, wolf::CT_Float);
	m_pVertexDeclaration->AppendAttribute(wolf::AT_Normal, 3, wolf::CT_Float);
	m_pVertexDeclaration->SetVertexBuffer(m_pVertexBuffer);
	m_pVertexDeclaration->SetIndexBuffer(m_pIndexBuffer);
	m_pVertexDeclaration->End();

	return true;

}

void GridMesh::CreateMesh(int quadsPerTile, float tileSize, int numXTiles, int numZTiles)
{
	// Number of quads	
	int numXQuads = (numXTiles) * quadsPerTile;
	int numZQuads = (numZTiles) * quadsPerTile;

	numVertsX = numXQuads + 1;
	numVertsZ = numZQuads + 1;

	// Same width/depth
	float quadWidth = tileSize / (float)quadsPerTile;
	float quadDepth = quadWidth;

	// Height map coordinate info
	float heightMapXIncrement = 1 / (float)(numXQuads);

	float heightMapZIncrement = 1 / (float)(numZQuads);

	// Creating all the vertices
	for (int z = 0; z < numZQuads + 1; z++)
	{

		for (int x = 0; x < numXQuads + 1; x++)
		{
			GLfloat u = (x * quadWidth) / tileSize;
			GLfloat v = (z * quadDepth) / tileSize;

			// Height map coordinates
			GLfloat uHeight = (x * heightMapXIncrement);
			GLfloat vHeight = (z * heightMapZIncrement);

			if (uHeight > 1.0f || vHeight > 1.0f || uHeight < 0.0f || vHeight < 0.0f)
			{
				int test = 5;
			}

			float vx = x * quadWidth;
			float vz = z * -quadDepth;

			MeshVertex vertex = {x * quadWidth, 0.0f, z*-quadDepth, u, v, uHeight, vHeight, 0.0f, 1.0f, 0.0f };
			meshVertices.push_back(vertex);
		}
	}


	// Creating all the indices
	for (int z = 0; z < numZQuads; z++)
	{

		for (int x = 0; x < numXQuads; x++)
		{
			// Triangle indices
			unsigned int i1 = (z * (numXQuads + 1)) + x;
			unsigned int i2 = ((z + 1) * (numXQuads + 1)) + x;
			unsigned int i3 = (z * (numXQuads + 1)) + x + 1;
			unsigned int i4 = ((z + 1) * (numXQuads + 1)) + x + 1;

			// Get the vertices for debug purposes
			MeshVertex v1 = meshVertices[i1];
			MeshVertex v2 = meshVertices[i2];
			MeshVertex v3 = meshVertices[i3];
			MeshVertex v4 = meshVertices[i4];

			meshIndices.push_back(i1);
			meshIndices.push_back(i2);
			meshIndices.push_back(i3);

			meshIndices.push_back(i2);
			meshIndices.push_back(i4);
			meshIndices.push_back(i3);

		}
	}
}

//------------------------------------------------------------------------------
// Method:    Render
// Returns:   void
// 
// Renders the quad
//------------------------------------------------------------------------------
void GridMesh::Render(const glm::mat4& p_mView, const glm::mat4& p_mProj, const glm::mat4& p_mLight)
{
	m_pVertexDeclaration->Bind();

	// Uniforms
	glm::mat4 worldViewProj = p_mProj * p_mView * m_mWorldTransform;
	m_pMaterial->SetUniform("WorldViewProj", worldViewProj);

	glm::mat4 lightMatrix = p_mLight * m_mWorldTransform;
	m_pMaterial->SetUniform("LightMatrix", lightMatrix);

	m_pMaterial->Apply();

	// Draw!
	glDrawElements(GL_TRIANGLES, meshIndices.size(), GL_UNSIGNED_INT, 0);

	// Draw counting
	DrawCalls += 1;

}

void GridMesh::AdjustMesh(const std::string& p_pFile)
{
	GLFWimage img;
	glfwReadImage(p_pFile.c_str(), &img, 0);

	// Adjust height
	for (int i = 0; i < meshVertices.size(); i++)
	{
		MeshVertex v = meshVertices.at(i);

		// Get corresponding pixel in image
		int uPixel = v.heightU * (img.Width - 1);
		int vPixel = v.heightV * (img.Height - 1);

		int position = ((vPixel * img.Width) + (uPixel)) * img.BytesPerPixel;

		unsigned char R = img.Data[position];
		unsigned char G = img.Data[position + 1];
		unsigned char B = img.Data[position + 2];

		float rFloat = (int)R / 255.0f;
		float gFloat = (int)G / 255.0f;
		float bFloat = (int)B / 255.0f;

		// Adjust the Y value of the vertex
		float distortion = 0.30f * rFloat + 0.59f * gFloat + 0.11f * bFloat;
		meshVertices.at(i).y = distortion * 60.0f;

	}

	m_pMaterial->SetUniform("Distortion", 60.0f);


	// Adjust normals
	for (int i = 0; i < numVertsZ; i++)
	{	
		for (int j = 0; j < numVertsX; j++)
		{
			
			// Grab the vertex in what would be an array like formation
			MeshVertex* v = &meshVertices.at(i * numVertsX + j);

			// Vector for normals to check against
			std::vector<glm::vec3> normalsToAverage;

			// Have room to go up/left (2 Triangles here)
			if (i < numVertsZ - 1 && j > 0)
			{
				MeshVertex left = meshVertices.at(i * numVertsX + (j - 1));
				MeshVertex up = meshVertices.at((i + 1) * numVertsX + j);
				MeshVertex upLeft = meshVertices.at((i + 1) * numVertsX + (j - 1));

				// Normal for Left / Up-Left
				glm::vec3 V = glm::vec3(left.x - v->x, left.y - v->y, left.z - v->z);
				glm::vec3 U = glm::vec3(upLeft.x - v->x, upLeft.y - v->y, upLeft.z - v->z);

				glm::vec3 norm1;
				norm1.x = (U.y * V.z) - (U.z * V.y);
				norm1.y = (U.z * V.x) - (U.x * V.z);
				norm1.z = (U.x * V.y) - (U.y * V.x);
				norm1 = glm::normalize(norm1);

				// Normal for Up / Up-Left
				U = glm::vec3(up.x - v->x, up.y - v->y, up.z - v->z);
				V = glm::vec3(upLeft.x - v->x, upLeft.y - v->y, upLeft.z - v->z);

				glm::vec3 norm2;
				norm2.x = (U.y * V.z) - (U.z * V.y);
				norm2.y = (U.z * V.x) - (U.x * V.z);
				norm2.z = (U.x * V.y) - (U.y * V.x);
				norm2 = glm::normalize(norm2);

				// Add those normals to the vector
				normalsToAverage.push_back(norm1);
				normalsToAverage.push_back(norm2);

			}

			// Down/right (2 Triangles here)
			if (i > 0 && j < numVertsX - 1)
			{
				MeshVertex right = meshVertices.at(i * numVertsX + (j + 1));
				MeshVertex down = meshVertices.at((i - 1) * numVertsX + j);
				MeshVertex downRight = meshVertices.at((i - 1) * numVertsX + (j + 1));

				// Normal for Right / Down-Right
				glm::vec3 V = glm::vec3(right.x - v->x, right.y - v->y, right.z - v->z);
				glm::vec3 U = glm::vec3(downRight.x - v->x, downRight.y - v->y, downRight.z - v->z);

				glm::vec3 norm1;
				norm1.x = (U.y * V.z) - (U.z * V.y);
				norm1.y = (U.z * V.x) - (U.x * V.z);
				norm1.z = (U.x * V.y) - (U.y * V.x);
				norm1 = glm::normalize(norm1);

				// Normal for Down / Down-Right
				U = glm::vec3(down.x - v->x, down.y - v->y, down.z - v->z);
				V = glm::vec3(downRight.x - v->x, downRight.y - v->y, downRight.z - v->z);

				glm::vec3 norm2;
				norm2.x = (U.y * V.z) - (U.z * V.y);
				norm2.y = (U.z * V.x) - (U.x * V.z);
				norm2.z = (U.x * V.y) - (U.y * V.x);
				norm2 = glm::normalize(norm2);

				// Add those normals to the vector
				normalsToAverage.push_back(norm1);
				normalsToAverage.push_back(norm2);
			}

			// Down/Left (1 Triangle)
			if (i > 0 && j > 0)
			{
				MeshVertex left = meshVertices.at(i * numVertsX + (j - 1));
				MeshVertex down = meshVertices.at((i - 1) * numVertsX + j);

				// Normal for Left / Down
				glm::vec3 U = glm::vec3(left.x - v->x, left.y - v->y, left.z - v->z);
				glm::vec3 V = glm::vec3(down.x - v->x, down.y - v->y, down.z - v->z);

				glm::vec3 norm1;
				norm1.x = (U.y * V.z) - (U.z * V.y);
				norm1.y = (U.z * V.x) - (U.x * V.z);
				norm1.z = (U.x * V.y) - (U.y * V.x);
				norm1 = glm::normalize(norm1);

				normalsToAverage.push_back(norm1);
			}

			// Up/Right (1 Triangle)
			if (i < numVertsZ - 1 && j < numVertsX - 1)
			{
				MeshVertex up = meshVertices.at((i + 1) * numVertsX + j);
				MeshVertex right = meshVertices.at(i * numVertsX + (j + 1));

				// Normal for Right / Up
				glm::vec3 V = glm::vec3(up.x - v->x, up.y - v->y, up.z - v->z);
				glm::vec3 U = glm::vec3(right.x - v->x, right.y - v->y, right.z - v->z);

				glm::vec3 norm1;
				norm1.x = (U.y * V.z) - (U.z * V.y);
				norm1.y = (U.z * V.x) - (U.x * V.z);
				norm1.z = (U.x * V.y) - (U.y * V.x);
				norm1 = glm::normalize(norm1);

				normalsToAverage.push_back(norm1);
			}

			// Average the normals
			glm::vec3 averageNormal = glm::vec3(0.0f, 0.0f, 0.0f);

			for (int i = 0; i < normalsToAverage.size(); i++)
			{
				averageNormal.x += normalsToAverage.at(i).x;
				averageNormal.y += normalsToAverage.at(i).y;
				averageNormal.z += normalsToAverage.at(i).z;
			}

			averageNormal.x / (float)normalsToAverage.size();
			averageNormal.y / (float)normalsToAverage.size();
			averageNormal.z / (float)normalsToAverage.size();

			averageNormal = glm::normalize(averageNormal);

			v->nx = averageNormal.x;
			v->ny = averageNormal.y;
			v->nz = averageNormal.z;
			
		}
	}

}