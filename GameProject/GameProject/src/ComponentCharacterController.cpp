//------------------------------------------------------------------------
// ComponentCharacterController
//
// Created:	2012/12/14
// Author:	Carel Boers
//	
// This class implements a character controller. It polls input and sends
// movement instructions to the relevant sibling components.
//------------------------------------------------------------------------

#include "ComponentCharacterController.h"
#include "ComponentRenderableMesh.h"
#include "GameObject.h"
#include "TerrainManager.h"
#include "ComponentProjectile.h"
#include "ComponentWeapon.h"
#include <memory>

static int countTesting = 0;

static const float speed = 1.0f;

//------------------------------------------------------------------------------
// Method:    ComponentCharacterController
// Returns:   
// 
// Constructor.
//------------------------------------------------------------------------------
ComponentCharacterController::ComponentCharacterController()
{
	velocityX = 0.0f;
	velocityZ = 0.0f;

	triggerReset = 0.4f;
	resetTracking = 0.0f;

	gamepad = new GameController(0, 0.2f, 0.12f);

	// Grabbing limits from TerrainManager (Requires terrain manager to be setup first, which should always be the case)
	upperX = TerrainManager::Instance()->upperX;
	lowerX = TerrainManager::Instance()->lowerX;
	upperZ = TerrainManager::Instance()->upperZ;
	lowerZ = TerrainManager::Instance()->lowerZ;

}

// Init
void ComponentCharacterController::Init(const float maxVelocityP, const float accelerationP, const float decelerationP, const float maxTiltP, const float bottomLockDistanceP)
{
	maxVelocity = maxVelocityP;
	acceleration = accelerationP;
	deceleration = decelerationP;
	maxTilt = maxTiltP;
	bottomLockDistance = bottomLockDistanceP;
}

//------------------------------------------------------------------------------
// Method:    ~ComponentCharacterController
// Returns:   
// 
// Destructor.
//------------------------------------------------------------------------------
ComponentCharacterController::~ComponentCharacterController()
{
	delete gamepad;
}

//------------------------------------------------------------------------------
// Method:    Update
// Parameter: float p_fDelta
// Returns:   void
// 
// Polls input and sends movement instructions to relevant sibling components.
//------------------------------------------------------------------------------
void ComponentCharacterController::Update(float p_fDelta)
{

	// Refresh game pad
	gamepad->UpdateGameController();

	if (!startReleased)
	{
		if (!gamepad->IsButtonPressed(XINPUT_GAMEPAD_START))
			startReleased = true;
	}

	if (gamepad->IsButtonPressed(XINPUT_GAMEPAD_START) && startReleased)
	{
		this->GetGameObject()->GetManager()->GetStateMachine()->SuspendState(eStateGameplay_Gameplay);
		this->GetGameObject()->GetManager()->GetStateMachine()->PushState(eStateGameplay_Paused, NULL);
		startReleased = false;		
	}

	// Determine max velocity
	float currentMaxX = abs(gamepad->leftX * maxVelocity);
	float currentMaxZ = abs(gamepad->leftY * maxVelocity);

	// Try moving forward based on input
	if (gamepad->leftY > 0.0f)
	{
		// Add to the current velocity
		if (velocityZ > -currentMaxZ)
		{
			if (velocityZ > 0.0f)
				velocityZ -= deceleration;
			else
				velocityZ -= acceleration;
		}

		if (velocityZ < -currentMaxZ)
		{
			velocityZ += deceleration;

			if (velocityZ > currentMaxZ)
				velocityZ = -currentMaxZ;
		}
	}

	else if (gamepad->leftY < 0.0f)
	{
		// Add to the current velocity
		if (velocityZ < currentMaxZ)
		{
			if (velocityZ < 0.0f)
				velocityZ += deceleration;
			else
				velocityZ += acceleration;
		}

		if (velocityZ > currentMaxZ)
		{
			velocityZ -= deceleration;

			if (velocityZ < currentMaxZ)
				velocityZ = currentMaxZ;
		}
	}

	else if (gamepad->leftY == 0.0f)
	{
		// Close in on 0 velocity
		if (velocityZ < 0.0f)
		{
			velocityZ += deceleration;

			if (velocityZ > 0.0f)
			{
				velocityZ = 0.0f;
			}
		}

		else if (velocityZ > 0.0f)
		{
			velocityZ += -deceleration;

			if (velocityZ < 0.0f)
			{
				velocityZ = 0.0f;
			}
		}
	}

	// Left/Right Movement
	if (gamepad->leftX < 0.0f)
	{

		// Add to the current velocity
		if (velocityX > -currentMaxX)
		{
			if (velocityX > 0.0f)
				velocityX -= deceleration;
			else
				velocityX -= acceleration;
		}

		if (velocityX < -currentMaxX)
		{
			velocityX += deceleration;

			if (velocityX > -currentMaxX)
				velocityX = -currentMaxX;
		}

	}

	else if (gamepad->leftX > 0.0f)
	{
		// Add to the current velocity
		if (velocityX < currentMaxX)
		{
			if (velocityX < 0.0f)
				velocityX += deceleration;
			else
				velocityX += acceleration;
		}

		if (velocityX > currentMaxX)
		{
			velocityX -= deceleration;

			if (velocityX < currentMaxX)
			{
				velocityX = currentMaxX;
			}
		}
	}

	else if (gamepad->leftX == 0.0f)
	{
		// Close in on 0 velocity
		if (velocityX < 0.0f)
		{
			velocityX += deceleration;

			if (velocityX > 0.0f)
			{
				velocityX = 0.0f;
			}
		}

		else if (velocityX > 0.0f)
		{
			velocityX += -deceleration;

			if (velocityX < 0.0f)
			{
				velocityX = 0.0f;
			}
		}
	}


	ComponentWeapon* weapon = static_cast<ComponentWeapon*>(this->GetGameObject()->GetComponent("GOC_Weapon"));
	IWeapon* activeWeapon = weapon->GetActiveWeapon();

	// Reading Right stick position and generating a normal 
	if (gamepad->rightX != 0.0f || gamepad->rightY != 0.0f)
	{

		// Get the normal for the direction
		glm::vec2 normalRight = glm::normalize(glm::vec2(gamepad->rightX, gamepad->rightY));


		if (gamepad->IsButtonPressed(XINPUT_GAMEPAD_RIGHT_SHOULDER))
		{
			activeWeapon->Update(p_fDelta, this->GetGameObject(), normalRight);
			weaponActive = true;
		}

		else if (weaponActive)
		{
			activeWeapon->StopWeapon();
			weaponActive = false;
		}

		/*activeWeapon->Update(p_fDelta, this->GetGameObject(), normalRight);
		weaponActive = true;*/


	}

	//else if (weaponActive)
	//{
	//	activeWeapon->StopWeapon();
	//	weaponActive = false;
	//}
		

	// Adjusting its translation based on speeds
	glm::vec3 velocity = glm::vec3(velocityX, 0.0f, velocityZ);

	// Check if it goes past theoretical borders
	glm::vec3 currentPos = this->GetGameObject()->GetTransform().GetTranslation();

	if (!(currentPos.x + velocity.x < upperX && currentPos.x + velocity.x > lowerX))
	{
		velocity.x = 0.0f;
	}

	if (!(currentPos.z + velocity.z > upperZ && currentPos.z + velocity.z < lowerZ))
	{
		velocity.z = 0.0f;
	}

	this->GetGameObject()->GetTransform().Translate(velocity);

	// Tilting the helicopter
	float currentRotationX = -velocityX * maxTilt;
	float currentRotationZ = velocityZ * maxTilt;

	this->GetGameObject()->GetTransform().SetRotation(glm::vec3(currentRotationZ, 0.0f, currentRotationX));

	// Updating the boundry
	float bottomOffset = this->GetGameObject()->GetTransform().GetTranslation().z + bottomLockDistance;
	if (bottomOffset < lowerZ)
	{
		lowerZ = this->GetGameObject()->GetTransform().GetTranslation().z + bottomLockDistance;
	}
		
}

Common::ComponentBase* ComponentCharacterController::CreateComponent(TiXmlNode* p_pNode)
{
	assert(strcmp(p_pNode->Value(), "GOC_CharacterController") == 0);
	ComponentCharacterController* pCharacterController = new ComponentCharacterController();

	float maxVelocity = 0.0f;
	float acceleration = 0.0f;
	float deceleration = 0.0f;
	float maxTilt = 0.0f;
	float bottomLock = 0.0f;

	// Iterate Anim elements in the XML
	TiXmlNode* pChildNode = p_pNode->FirstChild();
	while (pChildNode != NULL)
	{
		const char* szNodeName = pChildNode->Value();
			
		if (strcmp(szNodeName, "MaxVelocity") == 0)
		{

			// Parse attributes
			TiXmlElement* pElement = pChildNode->ToElement();

			// Rotation Correction
			if (pElement->QueryFloatAttribute("value", &maxVelocity) != TIXML_SUCCESS)
			{
				delete pCharacterController;
				return NULL;
			}
		}

		else if (strcmp(szNodeName, "Acceleration") == 0)
		{

			// Parse attributes
			TiXmlElement* pElement = pChildNode->ToElement();

			// Rotation Correction
			if (pElement->QueryFloatAttribute("value", &acceleration) != TIXML_SUCCESS)
			{
				delete pCharacterController;
				return NULL;
			}
		}

		else if (strcmp(szNodeName, "Deceleration") == 0)
		{

			// Parse attributes
			TiXmlElement* pElement = pChildNode->ToElement();

			// Rotation Correction
			if (pElement->QueryFloatAttribute("value", &deceleration) != TIXML_SUCCESS)
			{
				delete pCharacterController;
				return NULL;
			}
		}

		else if (strcmp(szNodeName, "MaxTilt") == 0)
		{

			// Parse attributes
			TiXmlElement* pElement = pChildNode->ToElement();

			// Rotation Correction
			if (pElement->QueryFloatAttribute("value", &maxTilt) != TIXML_SUCCESS)
			{
				delete pCharacterController;
				return NULL;
			}
		}

		else if (strcmp(szNodeName, "BottomLockDistance") == 0)
		{

			// Parse attributes
			TiXmlElement* pElement = pChildNode->ToElement();

			// Rotation Correction
			if (pElement->QueryFloatAttribute("value", &bottomLock) != TIXML_SUCCESS)
			{
				delete pCharacterController;
				return NULL;
			}
		}

		pChildNode = pChildNode->NextSibling();
	}

	pCharacterController->Init(maxVelocity, acceleration, deceleration, maxTilt, bottomLock);
	return pCharacterController;
}
