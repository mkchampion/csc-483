//------------------------------------------------------------------------
// DynamicCollisionEvent
//
// An event that is triggered when two dynamic objects collide
//------------------------------------------------------------------------

#ifndef DYNAMICCOLLISION_H
#define DYNAMICCOLLISION_H

#include "BaseEventData.h"
#include "GameObject.h"

class DynamicCollisionEvent : public BaseEventData
{

public:

	// Static event type
	static const EventType s_EventType;

	// Constructor / Destructor
	explicit DynamicCollisionEvent(Common::GameObject* p_pObject1, Common::GameObject* p_pObject2) : m_pObject1(p_pObject1), m_pObject2(p_pObject2)
	{
		m_pManager = p_pObject1->GetManager();
		object1ID = p_pObject1->GetGUID();
		object2ID = p_pObject2->GetGUID();
	}

	~DynamicCollisionEvent() {};

	// Accessor for event type
	const EventType& GetEventType() const { return s_EventType; }

	// Acccessor for object
	Common::GameObject* GetObject1() const { return m_pObject1; }
	Common::GameObject* GetObject2() const { return m_pObject2; }

	// Accesor by ID
	std::string GetObject1ID() const { return object1ID; }
	std::string GetObject2ID() const { return object2ID; }

	// Manager
	Common::GameObjectManager* GetManager() const { return m_pManager; }


private:

	Common::GameObject* m_pObject1;
	Common::GameObject* m_pObject2;

	// Also include ID's so we can check if it's still active through the manager?
	std::string object1ID;
	std::string object2ID;

	// Manager
	Common::GameObjectManager* m_pManager;

};

#endif

