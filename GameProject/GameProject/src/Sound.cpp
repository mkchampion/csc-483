//------------------------------------------------------------------------
// Sound.h

// Collection of some of the requirements for a sound
//------------------------------------------------------------------------

#include "Sound.h"

// Constructor
Sound::Sound(bool isStreamP, bool isLoopP, std::string p_strName, std::string p_strPath, std::string p_strChannel)
{
	m_strName = p_strName;
	m_strPath = p_strPath;
	m_strChannel = p_strChannel;
	isStream = isStreamP;
	isLoop = isLoopP;
}

// Destructor
Sound::~Sound()
{
	m_pSound->release();
}