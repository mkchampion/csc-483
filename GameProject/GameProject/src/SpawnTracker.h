//------------------------------------------------------------------------
// SpawnTracker
//
// Created:	2015/10/26
// Author:	Matthew Champion
//	
// A simple object to contain information about which objects should spawn
//------------------------------------------------------------------------

#ifndef SPAWNTRACKER_H
#define SPAWNTRACKER_H

#include "LevelInfo.h"
#include "GameObject.h"
#include <algorithm>

// Spawn Tracker
class SpawnTracker
{

public:

	// Constructor / Destructor
	SpawnTracker(std::string targetP, std::vector<TMXObjectInfo*>* spawnListP, std::map<std::string, std::string>* GOMapP, std::vector<glm::vec3>* checkPointsP, std::vector<TMXObjectInfo*>* weaponsP);
	~SpawnTracker();

	// Update
	void Update(float p_fDelta);

	void SetGOManager(Common::GameObjectManager* p_pGOManager) { m_pGOManager = p_pGOManager; }
	void SetTarget(std::string p_strTargetID) { m_strTargetID = p_strTargetID; }
	void SetSpawnDistance(const float spawnDistanceP) { spawnDistance = spawnDistanceP; }
	void SetFireworkPosition(const glm::vec3& fireworkPosP) { fireworkPos = fireworkPosP; }

private:

	// Target that determines if an entity spawns or not
	Common::GameObjectManager* m_pGOManager;
	std::string m_strTargetID;

	// List of objects that determines if it spawns / powerups as well
	std::vector<TMXObjectInfo*>* spawnList;
	int spawnPos;

	std::vector<glm::vec3>* checkPoints;

	std::vector<TMXObjectInfo*>* weaponPickups;
	int weaponPos;
	int weaponReset;

	// Map for GO
	std::map<std::string, std::string>* GOMap;

	// Distance in the Z direction needed to spawn
	float spawnDistance;

	// Checks for respawning
	bool timerStarted;
	float resetTime;
	float spawnTimer;

	// Fire level finishing
	bool levelFinished = false;
	glm::vec3 fireworkPos;

	// Spawn position
	glm::vec3 currentSpawn;
};

#endif