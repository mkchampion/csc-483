//------------------------------------------------------------------------
// PathGrid
//	
// Grid to use for pathfinding
//------------------------------------------------------------------------

#ifndef PATHGRID_H
#define PATHGRID_H

#include <vector>
#include "PathNode.h"
#include "CollisionManager.h"
#include "TerrainManager.h"
#include "PathNodeTypes.h"

class PathGrid
{

public:

	// Generate Grid
	PathGrid(float gridWidth, float gridHeight, float nodeSeperation, const glm::vec3& bottomLeft);
	~PathGrid();
	
	void MarkUnusable(std::vector<CollisionPoints>* collisionData, float squareSize);
	void MarkUnusableElevated(std::vector<FlyBoundsWorld*>* flyBounds);

	// Path node array
	PathNode*** nodes;

	PathNode* GetClosestNode(const glm::vec3& position);
	PathNode* GetClosestUsableNode(const glm::vec3& position, EnemyPathType enemyPathTypeP, PathNode* exceptionP);

	int GetSizeX() const { return gridSizeX; }
	int GetSizeY() const { return gridSizeY; }

	bool PathGrid::CanUseType(EnemyPathType enemyType, PathNodeTypes nodeType);

private:

	int gridSizeX, gridSizeY;

};


#endif