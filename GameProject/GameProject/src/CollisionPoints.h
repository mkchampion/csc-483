// Simple struct to contain collision points

#ifndef COLLISIONPOINTS_H
#define COLLISIONPOINTS_H

#include <glm\glm.hpp>

struct CollisionPoints
{
	glm::mat4 transform;
	glm::vec4 topLeft;
	glm::vec4 topRight;
	glm::vec4 bottomLeft;
	glm::vec4 bottomRight;
};

#endif