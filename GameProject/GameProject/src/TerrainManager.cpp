//------------------------------------------------------------------------
// TerrainManager.h
//
// Created:	2015/10/29
// Author: Matthew Champion
//	
// This class serves as a manager for all the static terrain and models that
// should  be appearing in the scene.  The ground / buildings / trees.
//------------------------------------------------------------------------

#include "TerrainManager.h"
#include "CollisionManager.h"
#include "RenderTargetManager.h"

TerrainManager* TerrainManager::s_pTerrainManagerInstance = NULL;

// Create Instance
void TerrainManager::CreateInstance()
{
	assert(s_pTerrainManagerInstance == NULL);
	s_pTerrainManagerInstance = new TerrainManager();
}

// Destroy Instance
void TerrainManager::DestroyInstance()
{
	assert(s_pTerrainManagerInstance != NULL);
	delete s_pTerrainManagerInstance;
	s_pTerrainManagerInstance = NULL;
}

// Get Instance
TerrainManager* TerrainManager::Instance()
{
	assert(s_pTerrainManagerInstance);
	return s_pTerrainManagerInstance;
}

// Constructor
TerrainManager::TerrainManager()
{
	// Setup Flybounds
	m_lFlyBounds = new std::vector<FlyBoundsWorld*>();
}

// Destructor
TerrainManager::~TerrainManager()
{
	for (int i = 0; i < m_lFlyBounds->size(); i++)
	{
		delete m_lFlyBounds->at(i);
	}
	m_lFlyBounds->clear();
	delete m_lFlyBounds;
	
}

void TerrainManager::AttachCamera(Common::SceneCamera* p_pCamera)
{
	m_pCamera = p_pCamera;
}

// Update
void TerrainManager::Update(float p_fDelta)
{

	Common::GameObject* target = m_pGOManager->GetGameObject(m_strCullTargetID);

	if (target != NULL)
	{
		// Cull certain static models based on position
		m_activeModels.clear();
		glm::vec3 pos = target->GetTransform().GetTranslation();

		for (int i = 0; i < m_totalModels.size(); i++)
		{
			// Check against mesh position
			TMXObjectInfo* objectInfo = m_totalModels.at(i);

			if (abs(pos.z - objectInfo->y) < 200)
			{
				m_activeModels.push_back(objectInfo);
			}

		}
	}

	CollisionManager::Instance()->SetStaticCollisionData(&m_activeModels);

}

// Render
void TerrainManager::Render(bool useLight)
{
	if (m_pCamera == NULL)
	{
		return;
	}

	// Get the view/proj matrices from the camera
	glm::mat4 mProj, mView;
	wolf::RenderTarget* shadow = RenderTargetManager::Instance()->GetShadowRender();

	if (lightCam || useLight)
	{
		mProj = directionalLight->GetProjectionMatrix();
		mView = directionalLight->GetViewMatrix();
	}
	else
	{
		mProj = m_pCamera->GetProjectionMatrix();
		mView = m_pCamera->GetViewMatrix();
	}

	// Render the ground
	for (int i = 0; i < m_activeGroundMeshes.size(); i++)
	{
		
		m_activeGroundMeshes.at(i)->GetMaterial()->SetTexture("shadowMap", shadow->GetDepthBufferT());
		m_activeGroundMeshes.at(i)->Render(mView, mProj, directionalLight->GetLightMatrix());
	}

	// Render through the list of active objects
	for (int i = 0; i < m_activeModels.size(); i++)
	{
		TMXObjectInfo* oInfo = m_activeModels.at(i);

		// Get the model it should render from the type
		wolf::Model* modelToRender = m_modelMap.at(oInfo->type);

		// Create the transform it should be
		glm::mat4 worldTransform = glm::translate((float)oInfo->x, 0.0f, (float)(oInfo->y)) * glm::rotate((float)oInfo->rotation, 0.0f, 1.0f, 0.0f) * glm::scale(oInfo->scale, oInfo->scale, oInfo->scale);

		// Set SHadow map
		std::map<std::string, wolf::Material*>* modelMats = modelToRender->GetMaterialMap();

		for (auto x : *modelMats)
		{
			x.second->SetTexture("shadowMap", shadow->GetDepthBufferT());
		}

		// Render		
		modelToRender->Render(mView, mProj, worldTransform, directionalLight->GetLightMatrix());
	}

	// Go through active meshes
	for (int i = 0; i < m_activeMeshes.size(); i++)
	{
		wolf::GridMesh* mesh = m_activeMeshes.at(i);
		mesh->GetMaterial()->SetTexture("shadowMap", shadow->GetDepthBufferT());
		mesh->Render(mView, mProj, directionalLight->GetLightMatrix());
	}

}


void TerrainManager::GenQuads(int** p_Quads, int tileWidth, int tileHeight, int mapWidth, int mapHeight, std::map<int, std::string>* textureMap)
{
	// Map of id to 
	std::map<int, std::vector<glm::vec3>> quadMap;

	//// Iterate through the quads
	//for (int i = 0; i < mapHeight; i++)
	//{
	//	for (int j = 0; j < mapWidth; j++)
	//	{
	//		int ID = p_Quads[i][j];

	//		// String version of ID
	//		std::string strID = std::to_string(ID);	

	//		// Testing
	//		if (!(ID == 20 || ID == 25))
	//		{
	//			// Create the quad if it exists
	//			if (textureMap->count(ID) != 0)
	//			{
	//				
	//				// Get the quad position
	//				glm::vec3 quadPosition = glm::vec3(j*(float)tileWidth, 0.0f, i*(-1.0 * tileHeight));

	//			}
	//		}
	//	}
	//}	

	// Iterate through the quads
	for (int i = 0; i < mapHeight; i++)
	{
		for (int j = 0; j < mapWidth; j++)
		{
			int ID = p_Quads[i][j];

			if (!(ID == 20 || ID == 25))
			{

				// If found in map, add the position, if not, create new vector then add
				std::map<int, std::vector<glm::vec3>>::iterator it = quadMap.find(ID);

				// Found
				if (it != quadMap.end())
				{
					glm::vec3 quadPos = glm::vec3(j*(float)tileWidth, 0.0f, i*(-1.0 * (float)tileHeight));
					it->second.push_back(quadPos);
				}

				// Not found
				else
				{
					glm::vec3 quadPos = glm::vec3(j*(float)tileWidth, 0.0f, i*(-1.0 * (float)tileHeight));

					std::vector<glm::vec3> quadPositions;
					quadPositions.push_back(quadPos);

					quadMap.insert(std::pair<int, std::vector<glm::vec3>>(ID, quadPositions));
				}
			}
		}
	}

	int groundNum = 0;

	// Create groundMeshes for each part of the map
	for (auto x : quadMap)
	{

		// Get the texture to use
		int textureID = x.first;
		std::string texturePath = "";

		// If found in map, use it
		std::map<int, std::string>::iterator it = textureMap->find(textureID);

		// Found
		if (it != textureMap->end())
		{
			texturePath = it->second;
		}		

		// Create groundMesh
		std::string meshID = std::to_string(groundNum);
		wolf::GroundMesh* groundMesh = new wolf::GroundMesh("Ground" + meshID, texturePath, "GameProject/GameProject/data/Shaders/texturedQuadLight.vsh", "GameProject/GameProject/data/Shaders/texturedQuadLight.fsh");	

		groundMesh->GetMaterial()->SetUniform("LightDir", glm::normalize(directionalLight->lightDir));
		groundMesh->GetMaterial()->SetUniform("LightColor", directionalLight->lightColor);
		
		groundMesh->GetMaterial()->SetUniform("Ambient", directionalLight->ambientColor);
		
		// Go through the vector and add quads
		std::vector<glm::vec3> quads = x.second;

		for (int i = 0; i < quads.size(); i++)
		{
			groundMesh->AddGroundQuad(quads.at(i), tileWidth, tileHeight, 1);
		}

		groundMesh->DeclareVertexBuffer();
		m_activeGroundMeshes.push_back(groundMesh);

		groundNum++;
	}
}

void TerrainManager::GenModels(std::vector<TMXObjectInfo*> *objectList, std::map<std::string, ModelInfo*> *models)
{
	int breakTest = 1;
	// Go through model map and fill it with actual models
	for (auto x : *models)
	{

		ModelInfo* info = x.second;
		wolf::Model* newModel = new wolf::Model(info->modelPath, info->texturePath, info->vertexShader, info->fragmentShader);

		// Get material map
		std::map<std::string, wolf::Material*>* modelMats = newModel->GetMaterialMap();
		
		for (auto x : *modelMats)
		{
			x.second->SetUniform("LightDir", glm::normalize(directionalLight->lightDir));
			x.second->SetUniform("LightColor", directionalLight->lightColor);
			x.second->SetUniform("Ambient", directionalLight->ambientColor);
		}

		// Set the scale based on Model file
		newModel->SetTransform(glm::scale(info->scale, info->scale, info->scale));

		// Add it to the model map
		m_modelMap.insert(std::pair<std::string, wolf::Model*>(x.first, newModel));
	}

	// Go through the objects and find corresponding model
	for (int i = 0; i < objectList->size(); i++)
	{
		// Add to the total list of objects to be rendered
		m_totalModels.push_back(objectList->at(i));
		
	}

}


void TerrainManager::GenMeshes(int** tileIDsP, std::vector<FlyBounds*>* flyBoundsP, int tileWidth, int tileHeight, int mapWidth, std::map<int, std::string>* textureMap, std::map<int, ElevatedTextures*>* heightMap, int borderWidth)
{
	for (int i = 0; i < flyBoundsP->size(); i++)
	{
		FlyBounds* bounds = flyBoundsP->at(i);
		
		// Get the texture to use
		std::map<int, ElevatedTextures*>::iterator it = heightMap->find(bounds->id);
		if (it != heightMap->end())
		{

			// Id for the mesh
			std::string meshID = std::to_string(bounds->minRow);

			// Make a mesh to use
			wolf::GridMesh* elevatedMesh = new wolf::GridMesh("ElevatedMesh" + meshID, it->second, "GameProject/GameProject/data/Shaders/elevatedMesh.vsh", "GameProject/GameProject/data/Shaders/elevatedMesh.fsh");

			elevatedMesh->GetMaterial()->SetUniform("LightDir", glm::normalize(directionalLight->lightDir));
			elevatedMesh->GetMaterial()->SetUniform("LightColor", directionalLight->lightColor);
			
			elevatedMesh->GetMaterial()->SetUniform("Ambient", directionalLight->ambientColor);

			// Set the transform to translate in the Z direction however much the minimum row is
			elevatedMesh->SetTransform(glm::translate(glm::vec3(0.0f, 0.0f, bounds->minRow * -tileHeight)));

			// zOffset calculation
			int zOffsetCount = 0;

			// Z Size
			int meshHeight = bounds->maxRow - bounds->minRow + 1;
			int meshWidth = mapWidth;

			// Create the grid
			elevatedMesh->CreateMesh(15, tileWidth, meshWidth, meshHeight);

			elevatedMesh->AdjustMesh(it->second->heightMapPath);
			elevatedMesh->DeclareVertexBuffer();
			m_activeMeshes.push_back(elevatedMesh);

		}

		else
		{
			printf("No Corresponding Texture in Height Map");
		}
	}
}

void TerrainManager::SetFlyingBounds(std::vector<FlyBounds*> *flyBoundsP, int tileHeight)
{

	for (int i = 0; i < flyBoundsP->size(); i++)
	{
		FlyBounds* bounds = flyBoundsP->at(i);

		FlyBoundsWorld* worldBounds = new FlyBoundsWorld();
		worldBounds->min = -bounds->minRow * tileHeight;
		worldBounds->max = -(bounds->maxRow + 1) * tileHeight;

		m_lFlyBounds->push_back(worldBounds);

	}
}