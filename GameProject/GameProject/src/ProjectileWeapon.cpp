//-----------------------------------------------------------------------------
// File:			ProjectileWeapon.cpp
// Original Author:	Matthew Champion
//-----------------------------------------------------------------------------

#include "ProjectileWeapon.h"
#include "ComponentProjectile.h"
#include <tinyxml.h>
#include <GL/glfw.h>
#include <glm/gtx/vector_angle.hpp>
#include "SoundManager.h"

ProjectileWeapon::ProjectileWeapon(const std::string& weaponPathP)
{

	// Default Values
	splitShot = false;
	splitCount = 0;
	splitAngle = 0.0f;

	// Opening up the given TMX file
	TiXmlDocument doc(weaponPathP.c_str());

	// If it can't open it, print an error
	if (doc.LoadFile() == false)
	{
		printf("TMX Error", "Failed to locate / load TMX file");
		return;
	}
	else
	{
		// Initial weapon
		TiXmlNode* weaponNode = doc.FirstChild("Weapon");

		// Get other properties
		TiXmlNode* pChildNode = weaponNode->FirstChild();
		while (pChildNode != NULL)
		{
			const char* szNodeName = pChildNode->Value();

			if (strcmp(szNodeName, "Projectile") == 0)
			{

				// Parse attributes
				TiXmlElement* pElement = pChildNode->ToElement();

				// Name
				const char* szName = pElement->Attribute("path");
				if (szName == NULL)
				{
					return;
				}

				projectilePath = szName;
			}

			else if (strcmp(szNodeName, "FireRate") == 0)
			{

				// Parse attributes
				TiXmlElement* pElement = pChildNode->ToElement();

				// Rotation Correction
				if (pElement->QueryFloatAttribute("value", &fireRate) != TIXML_SUCCESS)
				{
					return;
				}
			}

			else if (strcmp(szNodeName, "Speed") == 0)
			{

				// Parse attributes
				TiXmlElement* pElement = pChildNode->ToElement();

				// Rotation Correction
				if (pElement->QueryFloatAttribute("value", &speed) != TIXML_SUCCESS)
				{
					return;
				}
			}

			else if (strcmp(szNodeName, "Damage") == 0)
			{

				// Parse attributes
				TiXmlElement* pElement = pChildNode->ToElement();

				// Rotation Correction
				if (pElement->QueryFloatAttribute("value", &damage) != TIXML_SUCCESS)
				{
					return;
				}
			}

			else if (strcmp(szNodeName, "SplitShot") == 0)
			{

				// Parse attributes
				TiXmlElement* pElement = pChildNode->ToElement();

				splitShot = true;

				if (pElement->QueryIntAttribute("SplitCount", &splitCount) != TIXML_SUCCESS)
				{
					return;
				}

				// Rotation Correction
				if (pElement->QueryFloatAttribute("SplitAngle", &splitAngle) != TIXML_SUCCESS)
				{
					return;
				}
			}

			else if (strcmp(szNodeName, "Sound") == 0)
			{

				// Parse attributes
				TiXmlElement* pElement = pChildNode->ToElement();

				// Name
				const char* szName = pElement->Attribute("value");
				if (szName == NULL)
				{
					return;
				}

				soundName = szName;
			}

			pChildNode = pChildNode->NextSibling();
		}
	}
}

ProjectileWeapon::~ProjectileWeapon()
{
}

void ProjectileWeapon::Update(float p_fDelta, Common::GameObject* ownerP, const glm::vec2& dirP)
{
	
	if (!canFire)
	{
		resetTime += p_fDelta;

		if (resetTime > fireRate || (glfwGetTime() - timeOfLastShot) > fireRate)
		{
			canFire = true;
			resetTime = 0.0f;
		}
	}
	else
	{

		// Play the sound?
		SoundManager::Instance()->PlaySoundFile(soundName);

		// Make the GO?
		Common::GameObject* projectile = ownerP->GetManager()->CreateGameObject(projectilePath);
		glm::vec3 translation = ownerP->GetTransform().GetTranslation();
		projectile->GetTransform().SetTranslation(translation);
		ComponentProjectile* pProjectile = static_cast<ComponentProjectile*>(projectile->GetComponent("GOC_Projectile"));
		glm::vec3 direction = glm::normalize(glm::rotateY(glm::vec3(dirP.x, 0.0f, -dirP.y), 0.0f));

		float currentAngle = glm::orientedAngle(glm::vec3(0.0f, 0.0f, 1.0f), direction, glm::vec3(0.0f, 1.0f, 0.0f));
		projectile->SetDirection(currentAngle);
		pProjectile->SetDirection(glm::vec3(dirP.x, 0.0f, -dirP.y));
		pProjectile->SetDamage(damage);
		pProjectile->SetSpeed(speed);

		projectile->AddTag("Projectile");
		pProjectile->parent = ownerP;


		if (ownerP->HasTag("Enemy"))
			pProjectile->enemyBullet = true;
						
		

		if (splitShot)
		{
			
			// Get half the amount
			int halfCount = splitCount / 2;

			// Make the negative angled ones
			for (int i = 1; i <= halfCount; i++)
			{
				float shotAngle = -i*splitAngle;
				
				// Rotate a vector by that amount
				glm::vec3 shotDirection = glm::normalize(glm::rotateY(glm::vec3(dirP.x, 0.0f, -dirP.y), shotAngle));
				float currentAngle = glm::orientedAngle(glm::vec3(0.0f, 0.0f, 1.0f), shotDirection, glm::vec3(0.0f, 1.0f, 0.0f));

				// Make the projectile
				Common::GameObject* projectile = ownerP->GetManager()->CreateGameObject(projectilePath);
				glm::vec3 translation = ownerP->GetTransform().GetTranslation();
				projectile->GetTransform().SetTranslation(translation);
				ComponentProjectile* pProjectile = static_cast<ComponentProjectile*>(projectile->GetComponent("GOC_Projectile"));

				projectile->SetDirection(currentAngle);
				pProjectile->SetDirection(shotDirection);
				pProjectile->SetDamage(damage);
				pProjectile->SetSpeed(speed);

				projectile->AddTag("Projectile");
				pProjectile->parent = ownerP;

				if (ownerP->HasTag("Enemy"))
					pProjectile->enemyBullet = true;
			}

			// Make the negative angled ones
			for (int i = 1; i <= halfCount; i++)
			{
				float shotAngle = i*splitAngle;

				// Rotate a vector by that amount
				glm::vec3 shotDirection = glm::normalize(glm::rotateY(glm::vec3(dirP.x, 0.0f, -dirP.y), shotAngle));
				float currentAngle = glm::orientedAngle(glm::vec3(0.0f, 0.0f, 1.0f), shotDirection, glm::vec3(0.0f, 1.0f, 0.0f));

				// Make the projectile
				Common::GameObject* projectile = ownerP->GetManager()->CreateGameObject(projectilePath);
				glm::vec3 translation = ownerP->GetTransform().GetTranslation();
				projectile->GetTransform().SetTranslation(translation);
				ComponentProjectile* pProjectile = static_cast<ComponentProjectile*>(projectile->GetComponent("GOC_Projectile"));

				projectile->SetDirection(currentAngle);
				pProjectile->SetDirection(shotDirection);
				pProjectile->SetDamage(damage);
				pProjectile->SetSpeed(speed);

				projectile->AddTag("Projectile");
				pProjectile->parent = ownerP;

				if (ownerP->HasTag("Enemy"))
					pProjectile->enemyBullet = true;
			}


		}

		// Reset firing
		canFire = false;
		timeOfLastShot = glfwGetTime();
	}
}
