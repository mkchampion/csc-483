//------------------------------------------------------------------------
// AIStateChasingTracking
//
// State for an enemy chasing a player
//------------------------------------------------------------------------

#include "AIStateChasingTracking.h"
#include "ComponentAIController.h"
#include "ComponentRenderableMesh.h"
#include <glm/gtx/vector_angle.hpp>
#include "PathFinder.h"
#include "ComponentWeapon.h"
#include "ComponentProjectile.h"

int testCount = 0;


// Constructor
AIStateChasingTracking::AIStateChasingTracking()
{
	fireRate = 1.5f;
	fireTimer = 0.0f;
	distanceThreshold = 50.0f;
	currentSpeed = 0.0f;
	tiltX = 0.0f;
}

// Destructor
AIStateChasingTracking::~AIStateChasingTracking()
{
}

bool AIStateChasingTracking::EvaluateTargetDistance()
{

	ComponentAIController* pController = static_cast<ComponentAIController*>(m_pStateMachine->GetStateMachineOwner());

	// Look for specified game object and see if we're close enough to it
	Common::GameObject* pTargetGameObject = pController->GetGameObject()->GetManager()->GetGameObject(targetID);
	if (pTargetGameObject)
	{
		const glm::vec3& vOurPosition = pController->GetGameObject()->GetTransform().GetTranslation();
		const glm::vec3& vTargetPosition = pTargetGameObject->GetTransform().GetTranslation();

		glm::vec3 vDiff = vOurPosition - vTargetPosition;
		if (glm::length(vDiff) < distanceThreshold)
		{
			return true;
		}
	}

	// Not close enough or specified game object doesn't exist
	return false;
}

// Enter
void AIStateChasingTracking::Enter(ActionData* p_stateData)
{
	ComponentAIController* controller = static_cast<ComponentAIController*>(m_pStateMachine->GetStateMachineOwner());
	Common::GameObject* gameObject = controller->GetGameObject();

	// Target
	target = gameObject->GetManager()->GetGameObject(std::string(p_stateData->chasingTrackingData.targetID));
	targetID = target->GetGUID();

	// Turnspeed
	turnSpeed = p_stateData->chasingTrackingData.turnSpeed;
	moveSpeed = p_stateData->chasingTrackingData.moveSpeed;
	turnLeniance = p_stateData->chasingTrackingData.turnLeniance;
	acceleration = p_stateData->chasingTrackingData.acceleration;
	deceleration = p_stateData->chasingTrackingData.deceleration;
	distanceThreshold = p_stateData->chasingTrackingData.distanceThreshold;

	// Mesh / Turret rotations
	hasMeshRotation = p_stateData->chasingTrackingData.hasMeshRotation;
	if (hasMeshRotation)
	{
		meshNum = p_stateData->chasingTrackingData.meshNum;
	}
}

// Update
void AIStateChasingTracking::Update(float delta)
{

	ComponentAIController* pController = static_cast<ComponentAIController*>(m_pStateMachine->GetStateMachineOwner());

	if (pController->GetGameObject()->GetManager()->GetGameObject(targetID) != NULL)
	{

		Common::Transform& transform = pController->GetGameObject()->GetTransform();
		Common::Transform& targetTransform = target->GetTransform();

		// Set y on both to 0
		glm::vec3 transformAdjusted = transform.GetTranslation();
		glm::vec3 targetAdjusted = targetTransform.GetTranslation();

		transformAdjusted.y = 0.0f;
		targetAdjusted.y = 0.0f;
		
		glm::vec3 movePos = transformAdjusted;

		glm::vec3 vDiff;

		// Within distance
		if (!EvaluateTargetDistance())
		{


			// Check for a path
			if (currentPath.empty())
			{
				currentPath = PathFinder::Instance()->FindPath(transformAdjusted, targetAdjusted, pController->GetEnemyPathType(), pController->GetOccupiedNode());
			}

			// Different from previous
			else
			{
				PathNode* endNode = PathFinder::Instance()->grid->GetClosestUsableNode(targetAdjusted, pController->GetEnemyPathType(), pController->GetOccupiedNode());
				glm::vec3 currentTarget = currentPath.back();

				if (endNode->position != currentTarget)
				{
					currentPath = PathFinder::Instance()->FindPath(transformAdjusted, targetAdjusted, pController->GetEnemyPathType(), pController->GetOccupiedNode());
				}
			}


			// Find target to move to
			if (currentPath.size() > 0)
			{
				// First 
				movePos = static_cast<glm::vec3>(*currentPath.begin());

				// If position is here, grab next node
				if (currentPath.size() > 1 && glm::length(movePos - transformAdjusted) < 1.0f)
				{
					currentPath.pop_front();
					movePos = static_cast<glm::vec3>(*currentPath.begin());
				}
			}

			// We have target position, move towards it
			vDiff = movePos - transformAdjusted;
			vDiff.y = 0.0f;
			vDiff = glm::normalize(vDiff);

			// Set last dir so it can drift when going to shooting
			lastDir = vDiff;

			float testX = abs(vDiff.x);
			float testZ = abs(vDiff.z);

			if (!(testX < 0.05f && testZ < 0.05f))
			{

				// Rotate facing direction
				float fAngle = glm::orientedAngle(glm::vec3(0.0f, 0.0f, 1.0f), vDiff, glm::vec3(0.0f, 1.0f, 0.0f));

				Common::GameObject* pGO = pController->GetGameObject();

				// Adjust it to be 0-360 for ease of use
				if (fAngle < 0)
				{
					fAngle += 360;
				}

				// Do some calculations to figure out if I should turn left / right to reach my destination
				float currentAngle = pGO->GetDirection();
				float angleDiff = fAngle - pGO->GetDirection();

				bool turnLeft = false;
				bool turnRight = false;

				if (abs(angleDiff) > 0.05f)
				{

					// Two directions where I don't have to loop over 0
					if (angleDiff >= -180.0f && angleDiff < 0.0f)
					{
						turnRight = true;
						testCount++;
						if (currentAngle - turnSpeed > fAngle)
							pGO->SetDirection(currentAngle - turnSpeed);
						else
							pGO->SetDirection(fAngle);
					}
					else if (angleDiff >= 0.0f && angleDiff < 180.0f)
					{
						turnLeft = true;
						if (currentAngle + turnSpeed < fAngle)
							pGO->SetDirection(currentAngle + turnSpeed);
						else
							pGO->SetDirection(fAngle);
					}

					// Two directions that loop over 0
					// Left?
					else if (angleDiff >= -360.0f && angleDiff < -180.0f)
					{
						turnRight = true;
						if (currentAngle + turnSpeed < 360.0f)
						{
							pGO->SetDirection(currentAngle + turnSpeed);
						}
						else
						{
							float newAngle = currentAngle - 360.0f + turnSpeed;

							if (newAngle > fAngle)
								pGO->SetDirection(fAngle);
							else
								pGO->SetDirection(newAngle);
						}
					}

					else if (angleDiff >= 180.0f && angleDiff <= 360.0f)
					{
						turnLeft = true;
						if (currentAngle - turnSpeed > 0.0f)
						{
							pGO->SetDirection(currentAngle - turnSpeed);
						}
						else
						{
							float newAngle = 360.0f - currentAngle - turnSpeed;

							if (newAngle < fAngle)
								pGO->SetDirection(fAngle);
							else
								pGO->SetDirection(newAngle);
						}
					}
				}

				float maxTilt = 30.0f;
				float tiltSpeed = 20.0f;
				if (turnRight)
				{
					tiltX += tiltSpeed * delta;
					if (tiltX > maxTilt)
					{
						tiltX = maxTilt;
					}
				}
				else if (turnLeft)
				{
					tiltX -= tiltSpeed * delta;
					if (tiltX < -maxTilt)
					{
						tiltX = -maxTilt;
					}
				}

				// Go to 0
				else
				{

					if (tiltX > 0.0f)
					{
						tiltX -= tiltSpeed * delta;
						if (tiltX < 0.0f)
						{
							tiltX = 0.0f;
						}
					}

					else if (tiltX < 0.0f)
					{
						tiltX += tiltSpeed * delta;
						if (tiltX > 0.0f)
						{
							tiltX = 0.0f;
						}
					}

				}

				transform.SetRotationXYZ(currentSpeed*0.5f, pGO->GetDirection(), -tiltX);

				// Adjust it to be 0-360 for ease of use
				float tempDirection = pGO->GetDirection();
				tempDirection = fmod(tempDirection, 360.0f);

				currentSpeed += acceleration * delta;
				if (currentSpeed > moveSpeed)
					currentSpeed = moveSpeed;

				if (tempDirection < fAngle + turnLeniance && pGO->GetDirection() > fAngle - turnLeniance)
				{
					transform.Translate(vDiff * currentSpeed * delta);
				}

			}



			// Adjust the target position to be on the same y to prevent jumps during tracking
			// Shooting while moving taken out, reusing this state with some additional changes to make it suitable for helicopters?
			// ================================================================================
			//glm::vec3 vDiffTarget = targetAdjusted - transformAdjusted;
			//vDiffTarget = glm::normalize(vDiffTarget);

			//Common::GameObject* pGO = pController->GetGameObject();

			//if (hasMeshRotation)
			//{
			//	// Rotate facing direction	
			//	float fAngle = glm::orientedAngle(glm::vec3(0.0f, 0.0f, 1.0f), vDiffTarget, glm::vec3(0.0f, 1.0f, 0.0f));

			//	// Adjusting the mesh to face the target
			//	ComponentRenderableMesh* mesh = static_cast<ComponentRenderableMesh*>(pGO->GetComponent("GOC_Renderable"));
			//	mesh->SetMeshRotation(meshNum, false, 0.0f, fAngle, glm::vec3(0.0f, 1.0f, 0.0f));
			//}

			//// Grab the weapon component if it exists
			//ComponentWeapon* weapon = static_cast<ComponentWeapon*>(pGO->GetComponent("GOC_Weapon"));
			//IWeapon* activeWeapon = weapon->GetActiveWeapon();

			//activeWeapon->Update(delta, pGO, glm::vec2(vDiffTarget.x, -vDiffTarget.z));
			// ================================================================================
		}
		else
		{

			currentSpeed -= deceleration * delta;
			if (currentSpeed < 0.0f)
				currentSpeed = 0.0f;

			// Shooting while moving taken out, reusing this state with some additional changes to make it suitable for helicopters?
			glm::vec3 vDiffTarget = targetAdjusted - transformAdjusted;
			vDiffTarget = glm::normalize(vDiffTarget);

			Common::GameObject* pGO = pController->GetGameObject();

			if (hasMeshRotation)
			{
				// Rotate facing direction	
				float fAngle = glm::orientedAngle(glm::vec3(0.0f, 0.0f, 1.0f), vDiffTarget, glm::vec3(0.0f, 1.0f, 0.0f));

				// Adjusting the mesh to face the target
				ComponentRenderableMesh* mesh = static_cast<ComponentRenderableMesh*>(pGO->GetComponent("GOC_Renderable"));
				mesh->SetMeshRotation(meshNum, false, 0.0f, fAngle, glm::vec3(0.0f, 1.0f, 0.0f));
			}

			// Grab the weapon component if it exists
			ComponentWeapon* weapon = static_cast<ComponentWeapon*>(pGO->GetComponent("GOC_Weapon"));
			IWeapon* activeWeapon = weapon->GetActiveWeapon();

			activeWeapon->Update(delta, pGO, glm::vec2(vDiffTarget.x, -vDiffTarget.z));

			// Check to see if it is facing direction of where it was last heading
			// ========================================================
			float fAngle = glm::orientedAngle(glm::vec3(0.0f, 0.0f, 1.0f), lastDir, glm::vec3(0.0f, 1.0f, 0.0f));

			// Adjust it to be 0-360 for ease of use
			if (fAngle < 0)
			{
				fAngle += 360;
			}

			// Do some calculations to figure out if I should turn left / right to reach my destination
			float currentAngle = pGO->GetDirection();
			float angleDiff = fAngle - pGO->GetDirection();

			if (abs(angleDiff) < 0.05f)
			{
				// Keep translating in the direction it's moving?
				transform.Translate(lastDir * currentSpeed * delta);
			}			
			// ========================================================

			transform.SetRotationXYZ(currentSpeed * 0.5f, pGO->GetDirection(), 0.0f);
		}

	}
	else
	{
		// If we don't have a target, we shouldn't be in a chase state; go to idle
		m_pStateMachine->GoToState(ComponentAIController::eAIState_Idle, stateData);
	}
}

// Exit
void AIStateChasingTracking::Exit()
{

}
