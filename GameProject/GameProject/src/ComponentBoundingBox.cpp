//------------------------------------------------------------------------
// ComponentBoundingBox
//	
// A Component that represents a bounding box being placed on a game object
//------------------------------------------------------------------------

#include "ComponentBoundingBox.h"
#include <glm/gtc/matrix_transform.hpp>
#include "GameObject.h"
#include "BoundingBoxDebugDraw.h"
#include "CollisionManager.h"
#include "SceneManager.h"


// Constructor
ComponentBoundingBox::ComponentBoundingBox()
{
	CollisionManager::Instance()->AddCollisionComponent(this);
}

// Destructor
ComponentBoundingBox::~ComponentBoundingBox()
{
	CollisionManager::Instance()->DeleteCollisionComponent(this);
}

// Init
void ComponentBoundingBox::Init(const glm::vec2& offsetP, float widthP, float depthP)
{	
	// Create the small local transforms for the 
	baseBL = glm::vec3(offsetP.x, 0.0f, offsetP.y);
	baseBR = glm::vec3(offsetP.x + widthP, 0.0f, offsetP.y);
	baseTL = glm::vec3(offsetP.x, 0.0f, offsetP.y - depthP);
	baseTR =  glm::vec3(offsetP.x + widthP, 0.0f, offsetP.y - depthP);

	baseWidth = widthP;
	baseDepth = depthP;
}

void ComponentBoundingBox::Update(float p_fDelta)
{
	
	// Update the positions based on the game objects transform
	Common::GameObject* GO = this->GetGameObject();
	glm::vec3 translation = GO->GetTransform().GetTranslation();
	glm::quat rotation = GO->GetTransform().GetRotation();
	glm::vec3 scaleMat = GO->GetTransform().GetScale();

	// Camera transforms
	glm::mat4 proj = Common::SceneManager::Instance()->GetCamera()->GetProjectionMatrix();
	glm::mat4 view = Common::SceneManager::Instance()->GetCamera()->GetViewMatrix();
	glm::mat4 projView = proj * view;

	// Adjust the points for this 
	adjustedTL = proj * view * GO->GetTransform().GetTransformation() * glm::vec4(baseTL, 1.0f);
	adjustedTR = proj * view * GO->GetTransform().GetTransformation() * glm::vec4(baseTR, 1.0f);
	adjustedBL = proj * view * GO->GetTransform().GetTransformation() * glm::vec4(baseBL, 1.0f);
	adjustedBR = proj * view * GO->GetTransform().GetTransformation() * glm::vec4(baseBR, 1.0f);

	// Divide by W?
	float aspectRatio = Common::SceneManager::Instance()->GetCamera()->GetAspectRatio();
	adjustedTL.x = (adjustedTL.x / adjustedTL.w) * aspectRatio;
	adjustedTR.x = (adjustedTR.x / adjustedTR.w) * aspectRatio;
	adjustedBL.x = (adjustedBL.x / adjustedBL.w) * aspectRatio;
	adjustedBR.x = (adjustedBR.x / adjustedBR.w) * aspectRatio;

	adjustedTL.y = adjustedTL.y / adjustedTL.w;
	adjustedTR.y = adjustedTR.y / adjustedTR.w;
	adjustedBL.y = adjustedBL.y / adjustedBL.w;
	adjustedBR.y = adjustedBR.y / adjustedBR.w;
	

#if DEBUG_BOXES
	// Add it to the debug drawer
	glm::vec4 debugTL = GO->GetTransform().GetTransformation() * glm::vec4(baseTL, 1.0f);
	glm::vec4 debugTR = GO->GetTransform().GetTransformation() * glm::vec4(baseTR, 1.0f);
	glm::vec4 debugBL = GO->GetTransform().GetTransformation() * glm::vec4(baseBL, 1.0f);
	glm::vec4 debugBR = GO->GetTransform().GetTransformation() * glm::vec4(baseBR, 1.0f);

	debugTL.y = 0.0f;
	debugBL.y = 0.0f;
	debugTR.y = 0.0f;
	debugBR.y = 0.0f;

	BoundingBoxDebugDraw::Instance()->AddLine(glm::vec3(debugBL), glm::vec3(debugBR));
	BoundingBoxDebugDraw::Instance()->AddLine(glm::vec3(debugBR), glm::vec3(debugTR));
	BoundingBoxDebugDraw::Instance()->AddLine(glm::vec3(debugTR), glm::vec3(debugTL));
	BoundingBoxDebugDraw::Instance()->AddLine(glm::vec3(debugTL), glm::vec3(debugBL));
#endif
	
}

Common::ComponentBase* ComponentBoundingBox::CreateComponent(TiXmlNode* p_pNode)
{
	assert(strcmp(p_pNode->Value(), "GOC_BoundingBox") == 0);
	ComponentBoundingBox* pBoundingComponent = new ComponentBoundingBox();

	glm::vec2 offset = glm::vec2(0.0f, 0.0f);
	float width = 0.0f;
	float depth = 0.0f;

	// Iterate Anim elements in the XML
	TiXmlNode* pChildNode = p_pNode->FirstChild();
	while (pChildNode != NULL)
	{
		const char* szNodeName = pChildNode->Value();

		if (strcmp(szNodeName, "Offset") == 0)
		{

			// Parse attributes
			TiXmlElement* pElement = pChildNode->ToElement();

			// x offset
			if (pElement->QueryFloatAttribute("x", &offset.x) != TIXML_SUCCESS)
			{
				delete pBoundingComponent;
				return NULL;
			}

			// z offset (in +z?)
			if (pElement->QueryFloatAttribute("z", &offset.y) != TIXML_SUCCESS)
			{
				delete pBoundingComponent;
				return NULL;
			}
		}

		else if (strcmp(szNodeName, "Width") == 0)
		{

			// Parse attributes
			TiXmlElement* pElement = pChildNode->ToElement();

			// Rotation Correction
			if (pElement->QueryFloatAttribute("value", &width) != TIXML_SUCCESS)
			{
				delete pBoundingComponent;
				return NULL;
			}
		}

		else if (strcmp(szNodeName, "Depth") == 0)
		{

			// Parse attributes
			TiXmlElement* pElement = pChildNode->ToElement();

			// Rotation Correction
			if (pElement->QueryFloatAttribute("value", &depth) != TIXML_SUCCESS)
			{
				delete pBoundingComponent;
				return NULL;
			}
		}

		pChildNode = pChildNode->NextSibling();
	}

	pBoundingComponent->Init(offset, width, depth);
	return pBoundingComponent;
}

// Getter for width
float ComponentBoundingBox::GetAdjustedWidth()
{
	return baseWidth * this->GetGameObject()->GetTransform().GetScale().x;
}

// Getter for depth
float ComponentBoundingBox::GetAdjustedDepth()
{
	return baseDepth * this->GetGameObject()->GetTransform().GetScale().z;
}


