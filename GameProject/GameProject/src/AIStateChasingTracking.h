//------------------------------------------------------------------------
// AIStateChasingTracking
//
// State for an enemy chasing a player while shooting
//------------------------------------------------------------------------

#ifndef STATECHASINGTRACKING_H
#define STATECHASINGTRACKING_H

#include "StateBase.h"
#include "GameObject.h"
#include <list>
#include <glm\glm.hpp>

class AIStateChasingTracking : public Common::StateBase
{
public:
	//------------------------------------------------------------------------------
	// Public methods.
	//------------------------------------------------------------------------------
	AIStateChasingTracking();
	virtual ~AIStateChasingTracking();

	// Overridden from StateBase
	virtual void Enter(ActionData* p_stateData);
	virtual void Update(float p_fDelta);
	virtual void Exit();

private:

	// Current path/last position
	std::list<glm::vec3> currentPath;
	glm::vec3 lastPos;

	// Target to chase
	Common::GameObject* target;
	std::string targetID;

	float turnSpeed;
	float currentSpeed;
	float moveSpeed;
	float acceleration;
	float deceleration;
	float turnLeniance;

	// Last direction
	glm::vec3 lastDir;

	float distanceThreshold;

	// Mesh rotation info
	bool hasMeshRotation = false;
	int meshNum = -1;

	// Weapon fire rate
	float fireRate;
	float fireTimer;

	// Checking for enemy (Duplicating from other place, should figure out how not to?)
	bool EvaluateTargetDistance();

	float tiltX;

};

#endif
