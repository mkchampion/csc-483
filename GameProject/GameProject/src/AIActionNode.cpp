//------------------------------------------------------------------------
// AIActionNode
//
// Created:	2012/12/14
// Author:	Carel Boers
//	
// This class represents an action (leaf) node in an AI decision tree.
//------------------------------------------------------------------------

#include "AIActionNode.h"

//------------------------------------------------------------------------------
// Method:    AIActionNode
// Parameter: TiXmlNode * p_pNode
// Returns:   
// 
// Constructor.
//------------------------------------------------------------------------------
AIActionNode::AIActionNode(AIDecisionTree* p_pTree)
	:
	AINode(p_pTree)
{
	data = new ActionData();
}

//------------------------------------------------------------------------------
// Method:    ~AIDecisionNode
// Returns:   
// 
// Destructor.
//------------------------------------------------------------------------------
AIActionNode::~AIActionNode()
{
}

//------------------------------------------------------------------------------
// Method:    Init
// Parameter: TiXmlNode * p_pNodeDefinition
// Returns:   void
// 
// Initialize the action node.
//------------------------------------------------------------------------------
bool AIActionNode::Init(TiXmlNode* p_pNodeDefinition)
{
	TiXmlElement* pElement = p_pNodeDefinition->ToElement();
	m_strAction = pElement->Attribute("action");

	// Grab the paramater if it exists
	if (std::strcmp(m_strAction.c_str(), "ACTION_TRACKING") == 0)
	{
		const char* param = pElement->Attribute("target");
		std::string test = std::string(param);

		if (param != NULL)
		{
			strcpy(data->trackingData.targetID, param);
		}		

		// Mesh number / Rotation		
		if (pElement->QueryIntAttribute("mesh", &data->trackingData.meshNum) == TIXML_SUCCESS)
		{
			data->trackingData.hasMeshRotation = true;
		}	
		else
		{
			data->trackingData.hasMeshRotation = false;
		}
	}

	else if (std::strcmp(m_strAction.c_str(), "ACTION_CHASING") == 0)
	{
		const char* param = pElement->Attribute("target");
		std::string test = std::string(param);

		if (param != NULL)
		{
			strcpy(data->chasingData.targetID, param);
		}		

		if (pElement->QueryFloatAttribute("turnSpeed", &data->chasingData.turnSpeed) != TIXML_SUCCESS)
		{
			data->chasingData.turnSpeed = -1;
		}

		if (pElement->QueryFloatAttribute("moveSpeed", &data->chasingData.moveSpeed) != TIXML_SUCCESS)
		{
			data->chasingData.moveSpeed = 1.0f;
		}

		if (pElement->QueryFloatAttribute("turnLeniance", &data->chasingData.turnLeniance) != TIXML_SUCCESS)
		{
			data->chasingData.turnLeniance = 0.0f;
		}

		if (pElement->QueryBoolAttribute("tilt", &data->chasingData.tilt) != TIXML_SUCCESS)
		{
			data->chasingData.tilt = false;
		}

	}

	else if (std::strcmp(m_strAction.c_str(), "ACTION_CHASINGTRACKING") == 0)
	{
		const char* param = pElement->Attribute("target");
		std::string test = std::string(param);

		if (param != NULL)
		{
			strcpy(data->chasingTrackingData.targetID, param);
		}

		if (pElement->QueryFloatAttribute("turnSpeed", &data->chasingTrackingData.turnSpeed) != TIXML_SUCCESS)
		{
			data->chasingTrackingData.turnSpeed = -1;
		}

		if (pElement->QueryFloatAttribute("moveSpeed", &data->chasingTrackingData.moveSpeed) != TIXML_SUCCESS)
		{
			data->chasingTrackingData.moveSpeed = 1.0f;
		}

		if (pElement->QueryFloatAttribute("turnLeniance", &data->chasingTrackingData.turnLeniance) != TIXML_SUCCESS)
		{
			data->chasingTrackingData.turnLeniance = 0.0f;
		}

		if (pElement->QueryFloatAttribute("acceleration", &data->chasingTrackingData.acceleration) != TIXML_SUCCESS)
		{
			data->chasingTrackingData.acceleration = 0.0f;
		}

		if (pElement->QueryFloatAttribute("deceleration", &data->chasingTrackingData.deceleration) != TIXML_SUCCESS)
		{
			data->chasingTrackingData.deceleration = 0.0f;
		}

		if (pElement->QueryFloatAttribute("distance", &data->chasingTrackingData.distanceThreshold) != TIXML_SUCCESS)
		{
			data->chasingTrackingData.distanceThreshold = 0.0f;
		}

		if (pElement->QueryBoolAttribute("tilt", &data->chasingTrackingData.tilt) != TIXML_SUCCESS)
		{
			data->chasingTrackingData.tilt= false;
		}

		// Mesh number / Rotation		
		if (pElement->QueryIntAttribute("mesh", &data->chasingTrackingData.meshNum) == TIXML_SUCCESS)
		{
			data->chasingTrackingData.hasMeshRotation = true;
		}
		else
		{
			data->chasingTrackingData.hasMeshRotation = false;
		}

	}
	
	return true;
}

//------------------------------------------------------------------------------
// Method:    Decide
// Returns:   const std::string &
// 
// Decision logic.
//------------------------------------------------------------------------------
const AIActionNode* AIActionNode::Decide()
{
	return this;
}