//------------------------------------------------------------------------
// ComponentWeaponPickup.h
//	
// Component meant to be used for weapon collection purposes
//------------------------------------------------------------------------

#include "ComponentWeaponPickup.h"
#include "DynamicCollisionEvent.h"
#include "EventManager.h"
#include "ComponentWeapon.h"
#include "SoundManager.h"

static int pickupCount = 0;

ComponentWeaponPickup::ComponentWeaponPickup()
{
}

ComponentWeaponPickup::~ComponentWeaponPickup()
{
	EventManager::Instance()->QueueRemoveListener(DynamicCollisionEvent::s_EventType, dynamicCollisionDelegate);
}

void ComponentWeaponPickup::Init(const std::string& weaponNameP, std::string soundNameP)
{
	weaponName = weaponNameP;
	soundName = soundNameP;
}

void ComponentWeaponPickup::InitDelegates()
{
	Common::GameObject* pGameObject = this->GetGameObject();

	// Create the EventDelegates
	std::string delegateDynamic = "GOC_WeaponPickup#" + std::to_string(pickupCount);
	dynamicCollisionDelegate = new EventDelegate(delegateDynamic, DynamicCollisionEvent::s_EventType, std::tr1::bind(&ComponentWeaponPickup::HandleCollisions, this, std::tr1::placeholders::_1));

	// Increment counter for health components
	pickupCount++;

	EventManager::Instance()->AddListener(DynamicCollisionEvent::s_EventType, dynamicCollisionDelegate);
}

// Update
void ComponentWeaponPickup::Update(float p_fDelta)
{

	// Check if the player is within range of this object?
	Common::GameObject* player = this->GetGameObject()->GetManager()->GetGameObject("Player");
	

	if (player)
	{

		// Get current position / player position
		glm::vec3 currentPosition = this->GetGameObject()->GetTransform().GetTranslation();
		glm::vec3 playerPosition = player->GetTransform().GetTranslation();

		// If player has passed it
		if (currentPosition.z > playerPosition.z)
		{

			if (currentPosition.z - playerPosition.z > 250)
			{
				Common::GameObjectManager* manager = player->GetManager();
				manager->QueueGOForDeletion(this->GetGameObject());
			}

		}
	}


}

// Parsing
Common::ComponentBase* ComponentWeaponPickup::CreateComponent(TiXmlNode* p_pNode)
{
	assert(strcmp(p_pNode->Value(), "GOC_WeaponPickup") == 0);
	ComponentWeaponPickup* pWeapon = new ComponentWeaponPickup();

	std::string weaponName = "Default";
	std::string soundNameT = "";

	// Iterate Anim elements in the XML
	TiXmlNode* pChildNode = p_pNode->FirstChild();
	while (pChildNode != NULL)
	{
		const char* szNodeName = pChildNode->Value();

		if (strcmp(szNodeName, "Weapon") == 0)
		{

			// Parse attributes
			TiXmlElement* pElement = pChildNode->ToElement();

			// Path
			const char* szName = pElement->Attribute("name");
			if (szName == NULL)
			{
				delete pWeapon;
				return NULL;
			}

			weaponName = szName;
		}

		else if (strcmp(szNodeName, "Sound") == 0)
		{

			// Parse attributes
			TiXmlElement* pElement = pChildNode->ToElement();

			// Name
			const char* szName = pElement->Attribute("value");
			if (szName == NULL)
			{
				delete pWeapon;
				return NULL;
			}

			soundNameT = szName;
		}

		pChildNode = pChildNode->NextSibling();
	}

	pWeapon->Init(weaponName, soundNameT);
	pWeapon->InitDelegates();
	return pWeapon;
}


void ComponentWeaponPickup::HandleCollisions(EventDelegate::EventDataPointer p_pCollisionData)
{


	// Cast the event
	std::shared_ptr<DynamicCollisionEvent> pCastCollision = std::static_pointer_cast<DynamicCollisionEvent>(p_pCollisionData);

	Common::GameObjectManager* pGOManager = pCastCollision->GetManager();

	Common::GameObject* g1 = pGOManager->GetGameObject(pCastCollision->GetObject1ID());
	Common::GameObject* g2 = pGOManager->GetGameObject(pCastCollision->GetObject2ID());

	if (g1 == NULL || g2 == NULL)
		return;

	// Bullet has collided with some dynamic object
	if (g1 == this->GetGameObject())
	{
		// Currently, only caring about enemies
		if (g2->HasTag("Player"))
		{
			// Set the active weapon on the player
			ComponentWeapon* playerWeapons = static_cast<ComponentWeapon*>(g2->GetComponent("GOC_Weapon"));
			playerWeapons->SetActiveWeapon(weaponName);
			SoundManager::Instance()->PlaySoundFile(soundName);
			pGOManager->DestroyGameObject(g1);
		}
	}

	// Bullet has collided with some dynamic object
	else if (g2 == this->GetGameObject())
	{
		// Currently, only caring about enemies
		if (g1->HasTag("Player"))
		{

			// Set the active weapon on the player
			ComponentWeapon* playerWeapons = static_cast<ComponentWeapon*>(g1->GetComponent("GOC_Weapon"));
			playerWeapons->SetActiveWeapon(weaponName);
			SoundManager::Instance()->PlaySoundFile(soundName);
			pGOManager->DestroyGameObject(g2);
		}
	}

}