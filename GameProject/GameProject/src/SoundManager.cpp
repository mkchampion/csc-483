//------------------------------------------------------------------------
// SoundManager
//	
// A sound manager, sets up FMOD / Reads in sounds / etc
//------------------------------------------------------------------------

#include "SoundManager.h"
#include <assert.h>

SoundManager* SoundManager::s_pSoundManagerInstance = NULL;

// Create Instance
void SoundManager::CreateInstance()
{
	assert(s_pSoundManagerInstance == NULL);
	s_pSoundManagerInstance = new SoundManager();
}

// Destroy Instance
void SoundManager::DestroyInstance()
{
	assert(s_pSoundManagerInstance != NULL);
	delete s_pSoundManagerInstance;
	s_pSoundManagerInstance = NULL;
}

// Get Instance
SoundManager* SoundManager::Instance()
{
	assert(s_pSoundManagerInstance != NULL);
	return s_pSoundManagerInstance;
}

// Constructor
SoundManager::SoundManager()
{
	FMOD_RESULT result;

	result = FMOD::System_Create(&m_pSystem);

	// Errors
	if (result != FMOD_OK)
	{
		printf("Error setting up sound system\n");
		return;
	}
	
	// Init
	result = m_pSystem->init(32, FMOD_INIT_NORMAL, 0);
	if (result != FMOD_OK)
	{
		printf("Error with sound system initialization\n");
		return;
	}

}

// Destructor
SoundManager::~SoundManager()
{
	
	// Release system
	m_pSystem->release();
}

// Update
void SoundManager::Update()
{
	m_pSystem->update();
}

// Reading in the channels
void SoundManager::ReadChannels(std::string p_strChannels)
{
	TiXmlDocument doc(p_strChannels.c_str());

	if (doc.LoadFile() == false)
	{
		printf("Error reading channels file\n");
		return;
	}
	else
	{
		// Initial node
		TiXmlNode* channelNode = doc.FirstChild("Channels");

		// Get the children
		TiXmlElement* channel = channelNode->FirstChildElement("Channel");

		// Go through the channels
		while (channel)
		{

			// Get the channel name
			const char* channelName = channel->Attribute("name");			
			FMOD::Channel* newChannel = 0;

			float volume = 1.0f;
			if (channel->QueryFloatAttribute("volume", &volume) != TIXML_SUCCESS)
			{
				volume = 1.0f;
			};


			// Add to map
			m_lChannels.insert(std::pair<std::string, FMOD::Channel*>(std::string(channelName), newChannel));
			m_lVolumes.insert(std::pair<std::string, float>(std::string(channelName), volume));
			
			channel = channel->NextSiblingElement("Channel");
		}
	}
}

// Reading in the sounds
void SoundManager::ReadSounds(std::string p_strSounds)
{
	TiXmlDocument doc(p_strSounds.c_str());

	if (doc.LoadFile() == false)
	{
		printf("Error reading channels file\n");
		return;
	}
	else
	{
		// Initial node
		TiXmlNode* soundNode = doc.FirstChild("Sounds");

		// Get the children
		TiXmlElement* sound = soundNode->FirstChildElement("Sound");

		// Go through the channels
		while (sound)
		{

			// Get the channel name
			std::string soundName = std::string(sound->Attribute("name"));
			std::string soundPath = std::string(sound->Attribute("path"));
			std::string soundChannel = std::string(sound->Attribute("channel"));

			bool isStream = false;
			sound->QueryBoolAttribute("stream", &isStream);

			bool isLoop = false;
			sound->QueryBoolAttribute("loop", &isStream);

			// Create the sound
			Sound* newSound = new Sound(isStream, isLoop, soundName, soundPath, soundChannel);

			// Actually create the sound
			FMOD_RESULT result;
			if (isStream)
			{
				result = m_pSystem->createStream(soundPath.c_str(), FMOD_LOOP_NORMAL, 0, newSound->GetSoundA());
			}
			else
			{
				if (isLoop)
				{
					result = m_pSystem->createSound(soundPath.c_str(), FMOD_LOOP_NORMAL, 0, newSound->GetSoundA());
				}
				else
				{
					result = m_pSystem->createSound(soundPath.c_str(), FMOD_DEFAULT, 0, newSound->GetSoundA());
				}
			}
			if (result != FMOD_OK)
			{
				printf("Error creating sound\n");
			}

			// Add to map
			m_lSoundList.insert(std::pair<std::string, Sound*>(soundName, newSound));

			sound = sound->NextSiblingElement("Sound");
		}
	}
}

// Play Sound
void SoundManager::PlaySoundFile(std::string p_strSound)
{

	// Find the corresponding sound in the map
	SoundList::iterator it = m_lSoundList.find(p_strSound);

	Sound* sound;

	if (it != m_lSoundList.end())
	{
		sound = it->second;
	}
	else
	{
		printf("No such sound\n");
		return;
	}


	// Search for the corresponding channel
	std::string channelName = sound->GetChannel();
	ChannelList::iterator channelIT = m_lChannels.find(channelName);

	if (channelIT != m_lChannels.end())
	{
	}
	else
	{
		printf("Sound doesn't have an existing channel\n");
		return;
	}

	// No errors, play sound
	FMOD_RESULT result = m_pSystem->playSound(sound->GetSound(), 0, false, &channelIT->second);
	if (result != FMOD_OK)
	{
		printf("Something else went wrong with the sound\n");
		return;
	}

	// Set volume
	channelIT->second->setVolume(m_lVolumes.find(channelName)->second);
}

// Stop Sound
void SoundManager::StopSoundFile(std::string p_strSound)
{

	// Find the corresponding sound in the map
	SoundList::iterator it = m_lSoundList.find(p_strSound);

	Sound* sound;

	if (it != m_lSoundList.end())
	{
		sound = it->second;
	}
	else
	{
		printf("No such sound\n");
		return;
	}

	// Search for the corresponding channel
	std::string channelName = sound->GetChannel();
	ChannelList::iterator channelIT = m_lChannels.find(channelName);
	
	if (channelIT != m_lChannels.end())
	{
	}
	else
	{
		printf("Sound doesn't have an existing channel\n");
		return;
	}

	// No errors, play sound
	FMOD_RESULT result = channelIT->second->stop();
	if (result != FMOD_OK)
	{
		printf("Channel couldn't stop properly");
		return;
	}
}