//------------------------------------------------------------------------
// GameObjectManager
//
// Created:	2012/12/26
// Author:	Carel Boers
//	
// Manages the collection of Game Objects used by a game.
//------------------------------------------------------------------------

#ifndef GAMEOBJECTMANAGER_H
#define GAMEOBJECTMANAGER_H

#include "GameObject.h"
#include <map>
#include <vector>

#include "tinyxml.h"	// For parsing GameObject XML

#include "EventDelegate.h"

#include "StateMachine.h"



namespace Common
{

	class GameObjectManager
	{
	public:
		// Typedef for convenience
		typedef std::map<std::string, GameObject*> GameObjectMap;

		typedef ComponentBase*(*ComponentFactoryMethod)(TiXmlNode* p_pNode);
		typedef std::map<std::string, ComponentFactoryMethod> ComponentFactoryMap;

		//---------------------------------------------------------------------
		// Public interface
		//---------------------------------------------------------------------
		GameObjectManager();
		~GameObjectManager();

		GameObject* CreateGameObject();
		void DestroyGameObject(GameObject* p_pGameObject);
		void DestroyAllGameObjects();
		void DestroyAllStateObjects(int p_iState);
		GameObject* GetGameObject(const std::string &p_strGOGUID);
		bool SetGameObjectGUID(GameObject* p_pGameObject, const std::string &p_strGOGUID);

		// Queueing GO for end of frame
		void QueueGOForDeletion(GameObject* p_pGameObject);
		void DeleteQueuedGO();

		// Data driven helpers
		GameObject* CreateGameObject(const std::string& p_strGameObject);
		void RegisterComponentFactory(const std::string& p_strComponentId, ComponentFactoryMethod);

		// Method to create a group of objects
		std::vector<GameObject*> CreateObjectGroup(const std::string& p_strObjectGroup);

		void Update(float p_fDelta);
		void SyncTransforms();
		void SyncPositions();

		// Iterator access to allow outside traversal of GameObjects
		GameObjectMap::iterator Begin()	{ return m_mGOMap.begin(); }
		GameObjectMap::iterator End()	{ return m_mGOMap.end(); }

		// Add to / retrieve collision vector
		void AddCollidable(GameObject* p_pCollidableObject);
		void RemoveCollidable(const std::string& p_strGUID);
		GameObjectMap::iterator GetCollidableBegin() { return m_mCollidable.begin(); }
		GameObjectMap::iterator GetCollidableEnd() { return m_mCollidable.begin(); }

		// Checking for collisions
		// void CheckCollisions();

		// Checking collisions in here for now?
		void HandleCollisions();

		// InitDelegates
		void InitDelegates();

		void SetStateMachine(StateMachine* p_pStateMachine) { m_mStateMachine = p_pStateMachine; }
		StateMachine* GetStateMachine() const { return m_mStateMachine; }

		// Responding to remove events
		void RemoveGOEvent(const EventDelegate::EventDataPointer& p_pEventData);

		// Get a list of objects with a certain tag
		std::vector<GameObject*> GetObjectsWithTag(std::string p_strTag);

	private:
		//---------------------------------------------------------------------
		// Private members
		//---------------------------------------------------------------------

		// Map of Game Objects
		GameObjectMap m_mGOMap;

		// Map of Component factories
		ComponentFactoryMap m_mComponentFactoryMap;

		// Collidables
		GameObjectMap m_mCollidable;

		// Vector of GO to delete
		std::vector<Common::GameObject*> m_lObjectsToDelete;

		// State machine
		StateMachine* m_mStateMachine;
	};
} // namespace Common

#endif // GAMEOBJECTMANAGER_H