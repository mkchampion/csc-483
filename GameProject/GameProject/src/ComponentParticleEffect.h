//------------------------------------------------------------------------
// ComponentParticleEffect
//	
// This class implements a particle effect component that can be attached / moved with a GO
//------------------------------------------------------------------------

#ifndef COMPNENTPARTICLEEFFECT_H
#define COMPNENTPARTICLEEFFECT_H

#include "ComponentBase.h"
#include "ParticleSystem\Effect.h"
#include <tinyxml.h>
#include <glm\glm.hpp>


// Struct for Effect / Rotation
struct EffectData
{
	Effect* effect;
	std::string effectPath;
	bool RotatesWithGO;
	glm::vec3 offset;
};

class ComponentParticleEffect : public Common::ComponentBase
{
public:
	//------------------------------------------------------------------------------
	// Public types.
	//------------------------------------------------------------------------------

public:
	//------------------------------------------------------------------------------
	// Public methods.
	//------------------------------------------------------------------------------
	ComponentParticleEffect();
	virtual ~ComponentParticleEffect();

	// Init
	void Init(std::vector<EffectData*> idleEffects, std::vector<EffectData*> deathEffects);

	// Factory
	static Common::ComponentBase* CreateComponent(TiXmlNode* p_pNode);

	virtual const std::string FamilyID() { return std::string("GOC_Particle"); }
	virtual const std::string ComponentID(){ return std::string("GOC_Particle"); }
	virtual void Update(float p_fDelta);
	virtual void ComponentDeath();

private:
	//------------------------------------------------------------------------------
	// Private members.
	//------------------------------------------------------------------------------
	
	// Effect
	std::vector<EffectData*> m_lIdleEffectList;
	std::vector<EffectData*> m_lDeathEffectList;
};

#endif // COMPNENTPROJECTILE_H

