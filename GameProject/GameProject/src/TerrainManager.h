//------------------------------------------------------------------------
// TerrainManager.h
//
// Created:	2015/10/29
// Author: Matthew Champion
//	
// This class serves as a manager for all the static terrain and models that
// should  be appearing in the scene.  The ground / buildings / trees.
//------------------------------------------------------------------------

#ifndef TERRAINMANAGER_H
#define TERRAINMANAGER_H

#include "SceneCamera.h"
#include "W_Quad.h"
#include "W_Model.h"
#include "W_GridMesh.h"
#include "W_GroundMesh.h"
#include "LevelInfo.h"
#include "GameObject.h"
#include <vector>
#include "DirectionalLight.h"

struct FlyBoundsWorld
{
	float min;
	float max;
};

class TerrainManager
{

	// Typedef for quads
	typedef std::vector<wolf::GroundMesh*> GroundMeshList;
	typedef std::vector<wolf::Model*> ModelList;
	typedef std::vector<wolf::GridMesh*> ElevatedMeshList;
	typedef std::map<std::string, wolf::Model*> ModelMap;

	public:

		// Singleton methods
		static void CreateInstance();
		static void DestroyInstance();
		static TerrainManager* Instance();	

		// Methods for adding quads
		void AttachCamera(Common::SceneCamera* p_pCamera);
		Common::SceneCamera* GetCamera() { return m_pCamera; }

		// Generating the quads
		void GenQuads(int** p_Quads, int tileWidth, int tileHeight, int width, int height, std::map<int, std::string>* textureMap);
		void GenModels(std::vector<TMXObjectInfo*> *objects, std::map<std::string, ModelInfo*> *models);
		void GenMeshes(int** tileIDsP, std::vector<FlyBounds*> *flyBoundsP, int tileWidth, int tileHeight, int mapWidth, std::map<int, std::string>* textureMap, std::map<int, ElevatedTextures*>* heightMap, int borderWidth);

		void SetFlyingBounds(std::vector<FlyBounds*> *flyBoundsP, int tileHeight);
		std::vector<FlyBoundsWorld*>* GetFlyingBounds(){ return m_lFlyBounds; }

		void SetGOManager(Common::GameObjectManager* p_pGOManager) { m_pGOManager = p_pGOManager; }
		void SetCullTarget(std::string p_strCullTarget) { m_strCullTargetID = p_strCullTarget; }

		void Render(bool useLight = false);
		void Update(float p_fDelta);

		// Map borders 
		float lowerX;
		float upperX;
		float lowerZ;
		float upperZ;				

		void SetDirectionalLight(DirectionalLight* lightP) { directionalLight = lightP; }

	private:

		TerrainManager();
		~TerrainManager();

		// Static singleton instance
		static TerrainManager* s_pTerrainManagerInstance;

		
		GroundMeshList m_activeGroundMeshes;

		// Map for models
		ModelMap m_modelMap;
		std::vector<TMXObjectInfo*> m_totalModels;
		std::vector<TMXObjectInfo*> m_activeModels;

		// Meshes
		ElevatedMeshList m_totalMeshes;
		ElevatedMeshList m_activeMeshes;

		// Light
		DirectionalLight* directionalLight;

		// Camera
		Common::SceneCamera* m_pCamera;

		// Cull target
		std::string m_strCullTargetID;
		Common::GameObjectManager* m_pGOManager;

		// Vector for flybounds
		std::vector<FlyBoundsWorld*>* m_lFlyBounds;
			
};

#endif