#include <Windows.h>
#include <string>

#ifndef ERROR_MESSAGE
#define ERROR_MESSAGE

// Single function to display an error
void DisplayError(const std::string& errorTitle, const std::string& errorMessage)
{
	// Mostly useless test error
	int msgBox = MessageBox(NULL, (LPCSTR)errorMessage.c_str(), (LPCSTR)errorTitle.c_str(), MB_ICONERROR | MB_OK);
}

#endif