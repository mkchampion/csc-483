//------------------------------------------------------------------------
// Sound.h

// Collection of some of the requirements for a sound
//------------------------------------------------------------------------

#ifndef SOUND_H
#define SOUND_H

#include "fmod.hpp"
#include <string>

class Sound
{

public:

	Sound(bool isStreamP, bool isLoopP, std::string p_strName, std::string p_strPath, std::string p_strChannel);
	~Sound();

	// Sound
	FMOD::Sound** GetSoundA() { return &m_pSound; }
	FMOD::Sound* GetSound() { return m_pSound; }

	std::string GetChannel() const { return m_strChannel; }

private:

	// Sound name / FMOD pointer
	std::string m_strName;
	FMOD::Sound* m_pSound;

	// Path
	std::string m_strPath;

	// Channel Name to use
	std::string m_strChannel;

	bool isStream;
	bool isLoop;

};

#endif