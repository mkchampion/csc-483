//-----------------------------------------------------------------------------
// File: W_Quad.h
//
// Similar to the sprite class, but this time it is representing a quad for 3d
//-----------------------------------------------------------------------------
#ifndef W_QUAD_H
#define W_QUAD_H

#include <string.h>
#include "W_Texture.h"
#include "W_TextureManager.h"
#include "W_ProgramManager.h"
#include "W_BufferManager.h"
#include "W_VertexBuffer.h"
#include "W_VertexDeclaration.h"
#include "W_MaterialManager.h"

namespace wolf
{

	class Quad
	{

	public:

		// Constructor/Destructor
		Quad(const std::string& p_strMaterialName, const std::string& p_strTexture, const std::string& p_strVertexProgram, const std::string& p_strFragmentProgram, const glm::vec2& p_vDimensions, const glm::vec2& p_vDefaultSize);
		~Quad();

		// Update / Render
		void Update(float p_fDelta);
		void Render(const glm::mat4& p_mView, const glm::mat4& p_mProj);

		// Transform setter (Same as model)
		void SetTransform(const glm::mat4& p_mWorldTransform) { m_mWorldTransform = p_mWorldTransform; }

		// Material accessor
		wolf::Material* GetMaterial() { return m_pMaterial; }

	private:

		// Vertex buffer and Vertex declaration to use
		wolf::VertexBuffer* m_pVertexBuffer;
		wolf::VertexDeclaration* m_pVertexDeclaration;
		wolf::Program* m_pProgram;
		wolf::Texture* m_pTexture;

		// Going to try and use materials 
		wolf::Material* m_pMaterial;

		// Transform
		glm::mat4 m_mWorldTransform;
			

	};

}

#endif