//------------------------------------------------------------------------
// AIStateTracking
//
// State for the AI to be rotating / facing the player or something of that sort
//------------------------------------------------------------------------

#ifndef STATETRACKING_H
#define STATETRACKING_H

#include "StateBase.h"
#include "GameObject.h"

class AIStateTracking : public Common::StateBase
{
public:
	//------------------------------------------------------------------------------
	// Public methods.
	//------------------------------------------------------------------------------
	AIStateTracking();
	virtual ~AIStateTracking();

	// Overridden from StateBase
	virtual void Enter(ActionData* p_stateData);
	virtual void Update(float p_fDelta);
	virtual void Exit();

private:

	// Target to track?
	Common::GameObject* m_pTarget;

	// Mesh rotation info
	bool hasMeshRotation = false;
	int meshNum = -1;

	// Weapon fire rate
	float fireRate;
	float fireTimer;

};

#endif
