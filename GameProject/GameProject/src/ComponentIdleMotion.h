//------------------------------------------------------------------------
// ComponentIdleMotion.h
//	
// Component meant to be used to have the pickups idle / spin
//------------------------------------------------------------------------

#ifndef COMPONENTIDLEMOTION_H
#define COMPONENTIDLEMOTION_H

#include "GameObjectManager.h"
#include <glm\glm.hpp>

class ComponentIdleMotion : public Common::ComponentBase
{

public:

	// Structors
	ComponentIdleMotion();
	~ComponentIdleMotion();

	void Init(float minBobP, float maxBobP, float bobSpeedP, float rotationSpeedP, float tiltAngleP, const glm::vec3& tiltAxisP, float offsetP);

	// Factory
	static Common::ComponentBase* CreateComponent(TiXmlNode* p_pNode);

	// Update/Family
	virtual void Update(float p_fDelta);
	virtual const std::string FamilyID() { return std::string("GOC_IdleMotion"); }
	virtual const std::string ComponentID(){ return std::string("GOC_IdleMotion"); }

	void SetStartHeight(const glm::vec3& startPositionP);

private:

	// Min/max bob
	float min, max;
	float bobSpeed;

	float offset;

	// Bool for direction
	bool bobbingUp = true;
	bool isDrop = false;

	// Rotation speed
	float rotationSpeed;

	// Tilt? (Should maybe tilt the model to make it look a bit better)
	float tilt;
	glm::vec3 tiltAxis;

	// Flag for if it's set initial tilt
	bool initialTilt = false;

};

#endif