//------------------------------------------------------------------------
// BoundingBoxDebugDraw
//	
// Debug draw for bounding boxes
//------------------------------------------------------------------------

#ifndef BOUNDINGBOXDEBUGDRAW_H
#define BOUNDINGBOXDEBUGDRAW_H

// Debug flag
#define DEBUG_BOXES 0
#if DEBUG_BOXES

#include "W_LineDrawer.h"
#include "ComponentBoundingBox.h"

class BoundingBoxDebugDraw
{

public:

	static void CreateInstance(wolf::LineDrawer* p_pLineDrawer);
	static void DestroyInstance();
	static BoundingBoxDebugDraw* Instance();

	// Update to Add the lines from any bounding boxes
	void Render(const glm::mat4& p_mView, const glm::mat4& p_mProj);

	void AddLine(const glm::vec3& p_vTo, const glm::vec3& p_vFrom);

	void ToggleRender() { m_bRender = !m_bRender; }

private:

	// Constructor given a LineDrawer
	BoundingBoxDebugDraw(wolf::LineDrawer* p_pLineDrawer);
	~BoundingBoxDebugDraw();
	
	static BoundingBoxDebugDraw* s_sBoxDrawer;
	
	// Vector of bounding box components
	std::vector<ComponentBoundingBox*> m_lBoundingBoxes;

	// LineDrawer to use
	wolf::LineDrawer* m_pLineDrawer;

	bool m_bRender = false;

};

#endif

#endif