//------------------------------------------------------------------------
// SceneManager
//
// Created:	2012/12/23
// Author:	Carel Boers
//	
// A scene manager that handles the dynamic models
//------------------------------------------------------------------------

#ifndef COMMON_SCENEMANAGER_H
#define COMMON_SCENEMANAGER_H

#include "W_Model.h"
#include "SceneCamera.h"
#include "ParticleSystem\Effect.h"
#include "DirectionalLight.h"

namespace Common
{
	class SceneManager
	{
		// Typedef for convenience
		typedef std::vector<wolf::Model*> ModelList;
		typedef std::vector<Effect*> EffectList;

	public:
		//------------------------------------------------------------------------------
		// Public methods.
		//------------------------------------------------------------------------------
		static void CreateInstance();
		static void DestroyInstance();
		static SceneManager* Instance();

		// Models
		void AddModel(wolf::Model* p_pModel);
		void RemoveModel(wolf::Model* p_pModel);
		void ClearModels();

		// Effects
		void AddEffect(Effect* p_pEffect);
		// void RemoveEffect(Effect* p_pEffect);
		void ClearEffects();

		void AttachCamera(SceneCamera* p_pCamera);
		SceneCamera* GetCamera();

		void Render(bool useLight = false);
		void Update(float p_fDelta);

		void SetDirectionalLight(DirectionalLight* lightP) { directionalLight = lightP; }
		DirectionalLight* GetDirectionalLight() const { return directionalLight; }


	private:
		//------------------------------------------------------------------------------
		// Private methods.
		//------------------------------------------------------------------------------
		
		// Constructor/Destructor are private because we're a Singleton
		SceneManager();
		virtual ~SceneManager();

	private:
		//------------------------------------------------------------------------------
		// Private members.
		//------------------------------------------------------------------------------

		// Static singleton instance
		static SceneManager* s_pSceneManagerInstance;

		// A list of models to render
		ModelList m_lModelList;

		EffectList m_lEffectList;

		// Lighting information
		DirectionalLight* directionalLight;

		// A camera to view the scene
		SceneCamera* m_pCamera;

	};

} // namespace common

#endif // COMMON_SCENEMANAGER_H