//------------------------------------------------------------------------
// ComponentDrops
//	
// Component to specify what enemies will drop (Could be a weapon / health pickup)
//------------------------------------------------------------------------

#include "ComponentDrops.h"
#include "ComponentIdleMotion.h"

// Constructor
ComponentDrops::ComponentDrops()
{
}

// Destructor
ComponentDrops::~ComponentDrops()
{

}

void ComponentDrops::ComponentDeath()
{
	// Get a random number, check chances and spawn that item
	float chance = (float)rand() / (float)RAND_MAX;

	bool dropItem = false;
	DropRatesItem itemToDrop;

	for (int i = 0; i < m_lDropTable.size(); i++)
	{
		DropRatesItem current = m_lDropTable.at(i);

		if (chance < current.dropChance)
		{
			itemToDrop = current;
			dropItem = true;
			break;
		}
	}

	if (dropItem)
	{
		// Get position of current object
		glm::vec3 pos = this->GetGameObject()->GetTransform().GetTranslation();

		Common::GameObject* newDrop = this->GetGameObject()->GetManager()->CreateGameObject(itemToDrop.gameObjectPath);
		newDrop->AddTag("ItemDrop");

		ComponentIdleMotion* idleMotion = static_cast<ComponentIdleMotion*>(newDrop->GetComponent("GOC_IdleMotion"));
		idleMotion->SetStartHeight(pos);

		newDrop->GetTransform().SetTranslation(pos);

	}
}

// Init
void ComponentDrops::Init(std::vector<DropRatesItem> p_lDropTable)
{
	m_lDropTable = p_lDropTable;
}


// Reading from file
Common::ComponentBase* ComponentDrops::CreateComponent(TiXmlNode* p_pNode)
{
	assert(strcmp(p_pNode->Value(), "GOC_Drops") == 0);
	ComponentDrops* pDrop = new ComponentDrops();

	std::vector<DropRatesItem> lDropTable;

	// Iterate Anim elements in the XML
	TiXmlNode* pChildNode = p_pNode->FirstChild();
	while (pChildNode != NULL)
	{
		const char* szNodeName = pChildNode->Value();

		// Assume order is increasing
		if (strcmp(szNodeName, "Drop") == 0)
		{

			DropRatesItem drop;

			// Parse attributes
			TiXmlElement* pElement = pChildNode->ToElement();

			// Rotation Correction
			if (pElement->QueryFloatAttribute("chance", &drop.dropChance) != TIXML_SUCCESS)
			{
				delete pDrop;
				return NULL;
			}

			// Path
			const char* szName = pElement->Attribute("path");
			if (szName == NULL)
			{
				delete pDrop;
				return NULL;
			}

			drop.gameObjectPath = szName;

			// Add to list
			lDropTable.push_back(drop);

		}
		
		pChildNode = pChildNode->NextSibling();
	}

	pDrop->Init(lDropTable);
	return pDrop;
}