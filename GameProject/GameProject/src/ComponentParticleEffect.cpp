//------------------------------------------------------------------------
// ComponentParticleEffect
//	
// This class implements a particle effect component that can be attached / moved with a GO
//------------------------------------------------------------------------

#include "ComponentParticleEffect.h"
#include "SceneManager.h"
#include "GameObject.h"

// Count for projectiles
static int projectileCount = 0;

//------------------------------------------------------------------------------
// Method:    ComponentCharacterController
// Returns:   
// 
// Constructor.
//------------------------------------------------------------------------------
ComponentParticleEffect::ComponentParticleEffect()
{
}

// Init
void ComponentParticleEffect::Init(std::vector<EffectData*> idleEffects, std::vector<EffectData*> deathEffects)
{
	m_lIdleEffectList = idleEffects;
	m_lDeathEffectList = deathEffects;

	// Load effects
	for (int i = 0; i < m_lIdleEffectList.size(); i++)
	{
		EffectData* currentData = m_lIdleEffectList.at(i);

		currentData->effect = new Effect(currentData->effectPath);
		Common::SceneManager::Instance()->AddEffect(currentData->effect);
	}
}

//------------------------------------------------------------------------------
// Method:    ~ComponentCharacterController
// Returns:   
// 
// Destructor.
//------------------------------------------------------------------------------
ComponentParticleEffect::~ComponentParticleEffect()
{
	// Idle Effects
	for (int i = 0; i < m_lIdleEffectList.size(); i++)
	{
		// Flag effects to be deleted when they expire
		EffectData* currentData = m_lIdleEffectList.at(i);
		currentData->effect->FlagForDelete();

		// Delete EffectData
		delete m_lIdleEffectList.at(i);
	}
}

// Death
void ComponentParticleEffect::ComponentDeath()
{

	for (int i = 0; i < m_lDeathEffectList.size(); i++)
	{
		// Add the effects to the SceneManager once this GO is deleted
		EffectData* currentData = m_lDeathEffectList.at(i);
		currentData->effect = new Effect(currentData->effectPath);

		// Set current position
		glm::vec3 GOPosition = this->GetGameObject()->GetTransform().GetTranslation();
		glm::vec3 newPos = GOPosition + currentData->offset;

		currentData->effect->SetPosition(newPos);

		// rotation
		if (currentData->RotatesWithGO)
		{
			currentData->effect->SetRotation(this->GetGameObject()->GetDirection());
		}


		Common::SceneManager::Instance()->AddEffect(currentData->effect);

		// Delete data
		delete m_lDeathEffectList.at(i);
	}
}


//------------------------------------------------------------------------------
// Method:    Update
// Parameter: float p_fDelta
// Returns:   void
// 
// Polls input and sends movement instructions to relevant sibling components.
//------------------------------------------------------------------------------
void ComponentParticleEffect::Update(float p_fDelta)
{

	// Update Idle effects
	for (int i = 0; i < m_lIdleEffectList.size(); i++)
	{
		EffectData* currentData = m_lIdleEffectList.at(i);
		Effect* currentEffect = currentData->effect;

		glm::vec3 GOPosition = this->GetGameObject()->GetTransform().GetTranslation();

		// Rotation
		if (currentData->RotatesWithGO)
		{
			currentEffect->SetRotation(this->GetGameObject()->GetDirection());
			
			// Transform offset by rotation
			glm::vec4 rotatedOffset = glm::vec4(currentData->offset, 1.0f);			
			rotatedOffset = glm::rotate(this->GetGameObject()->GetDirection(), 0.0f, 1.0f, 0.0f) * rotatedOffset;
			
			glm::vec3 newOffset = glm::vec3(rotatedOffset);
			glm::vec3 newPos = GOPosition + newOffset;
			currentEffect->SetPosition(newPos);

		}
		else
		{
			glm::vec3 newPos = GOPosition + currentData->offset;
			currentEffect->SetPosition(newPos);
		}
	}

}

Common::ComponentBase* ComponentParticleEffect::CreateComponent(TiXmlNode* p_pNode)
{
	assert(strcmp(p_pNode->Value(), "GOC_Particle") == 0);
	ComponentParticleEffect* pParticle = new ComponentParticleEffect();

	// Vectors to fill with effects
	std::vector<EffectData*> idleEffects;
	std::vector<EffectData*> deathEffects;

	// Iterate Anim elements in the XML
	TiXmlNode* pChildNode = p_pNode->FirstChild();
	while (pChildNode != NULL)
	{
		const char* szNodeName = pChildNode->Value();

		if (strcmp(szNodeName, "IdleEffect") == 0)
		{

			// Effect to fill + defaults
			EffectData* newEffect = new EffectData();
			newEffect->RotatesWithGO = false;
			newEffect->offset = glm::vec3(0.0f, 0.0f, 0.0f);

			// Parse attributes
			TiXmlElement* pElement = pChildNode->ToElement();

			// Path
			const char* szName = pElement->Attribute("path");
			if (szName == NULL)
			{
				delete pParticle;
				return NULL;
			}

			newEffect->effectPath = szName;

			// Rotates with GO
			if (pElement->QueryBoolAttribute("RotatesWithGO", &newEffect->RotatesWithGO) != TIXML_SUCCESS)
			{
				delete pParticle;
				return NULL;
			}

			// Offset
			if (pElement->QueryFloatAttribute("OffsetX", &newEffect->offset.x) != TIXML_SUCCESS)
			{
				delete pParticle;
				return NULL;
			}

			if (pElement->QueryFloatAttribute("OffsetY", &newEffect->offset.y) != TIXML_SUCCESS)
			{
				delete pParticle;
				return NULL;
			}

			if (pElement->QueryFloatAttribute("OffsetZ", &newEffect->offset.z) != TIXML_SUCCESS)
			{
				delete pParticle;
				return NULL;
			}

			// Add to vector
			idleEffects.push_back(newEffect);

		}

		else if (strcmp(szNodeName, "DeathEffect") == 0)
		{

			// Effect to fill + defaults
			EffectData* newEffect = new EffectData();
			newEffect->RotatesWithGO = false;
			newEffect->offset = glm::vec3(0.0f, 0.0f, 0.0f);

			// Parse attributes
			TiXmlElement* pElement = pChildNode->ToElement();

			// Path
			const char* szName = pElement->Attribute("path");
			if (szName == NULL)
			{
				delete pParticle;
				return NULL;
			}

			newEffect->effectPath = szName;

			// Rotates with GO
			if (pElement->QueryBoolAttribute("RotatesWithGO", &newEffect->RotatesWithGO) != TIXML_SUCCESS)
			{
				delete pParticle;
				return NULL;
			}

			// Offset
			if (pElement->QueryFloatAttribute("OffsetX", &newEffect->offset.x) != TIXML_SUCCESS)
			{
				delete pParticle;
				return NULL;
			}

			if (pElement->QueryFloatAttribute("OffsetY", &newEffect->offset.y) != TIXML_SUCCESS)
			{
				delete pParticle;
				return NULL;
			}

			if (pElement->QueryFloatAttribute("OffsetZ", &newEffect->offset.z) != TIXML_SUCCESS)
			{
				delete pParticle;
				return NULL;
			}

			// Add to vector
			deathEffects.push_back(newEffect);

		}
		
		pChildNode = pChildNode->NextSibling();
	}

	pParticle->Init(idleEffects, deathEffects);
	return pParticle;
}
