//------------------------------------------------------------------------
// ComponentBoundingBox
//	
// A Component that represents a bounding box being placed on a game object
//------------------------------------------------------------------------


#ifndef COMPONENTBOUNDINGBOX_H
#define COMPONENTBOUNDINGBOX_H

#include "ComponentBase.h"
#include <tinyxml.h>
#include <glm\glm.hpp>


class ComponentBoundingBox : public Common::ComponentBase
{
public:

	// Constructor / Destructor
	ComponentBoundingBox();
	virtual ~ComponentBoundingBox();

	// Init
	void Init(const glm::vec2& offsetP, float widthP, float depthP);

	// Factory
	static Common::ComponentBase* CreateComponent(TiXmlNode* p_pNode);

	// Update/Family
	virtual void Update(float p_fDelta);
	virtual const std::string FamilyID() { return std::string("GOC_BoundingBox"); }
	virtual const std::string ComponentID(){ return std::string("GOC_BoundingBox"); }

	// Translations based on 0 rotation (Top will be further -z)
	glm::vec3 baseTL;
	glm::vec3 baseTR;
	glm::vec3 baseBL;
	glm::vec3 baseBR;

	// Adjusted for GO position
	glm::vec4 adjustedTL;
	glm::vec4 adjustedTR;
	glm::vec4 adjustedBL;
	glm::vec4 adjustedBR;

	float GetAdjustedWidth();
	float GetAdjustedDepth();

private:
	float baseWidth;
	float baseDepth;

};


#endif 
