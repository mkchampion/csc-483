//------------------------------------------------------------------------
// CameraComponent
//	
// A CameraComponent that can be attached to a GameObject,
// Will have children classes that implement specific behavior
//------------------------------------------------------------------------


#ifndef COMPONENTCAMERA_H
#define COMPONENTCAMERA_H

#include "ComponentBase.h"
#include "SceneCamera.h"


class ComponentCamera : public Common::ComponentBase
{
public:

	// Constructor / Destructor
	ComponentCamera();
	virtual ~ComponentCamera();
		
	// Update/Family
	virtual void Update() {};
	virtual const std::string FamilyID() { return std::string("GOC_Camera"); }

	// Accessor for SceneCamera
	Common::SceneCamera* GetSceneCamera() { return m_pSceneCamera; }
		
		
protected:
	//------------------------------------------------------------------------------
	// Private members.
	//------------------------------------------------------------------------------

	// Has a SceneCamera (Since that just wraps all lower openGL stuff)
	Common::SceneCamera* m_pSceneCamera;

};


#endif 
