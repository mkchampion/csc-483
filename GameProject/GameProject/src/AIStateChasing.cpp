//------------------------------------------------------------------------
// AIStateChanios
//
// State for an enemy chasing a player
//------------------------------------------------------------------------

#include "AIStateChasing.h"
#include "ComponentAIController.h"
#include "ComponentRenderableMesh.h"
#include <glm/gtx/vector_angle.hpp>
#include "PathFinder.h"


// Constructor
AIStateChasing::AIStateChasing()
{
}

// Destructor
AIStateChasing::~AIStateChasing()
{
}

// Enter
void AIStateChasing::Enter(ActionData* p_stateData)
{
	ComponentAIController* controller = static_cast<ComponentAIController*>(m_pStateMachine->GetStateMachineOwner());
	Common::GameObject* gameObject = controller->GetGameObject();

	target = gameObject->GetManager()->GetGameObject(std::string(p_stateData->chasingData.targetID));
	targetID = target->GetGUID();

	turnSpeed = p_stateData->chasingData.turnSpeed;
	moveSpeed = p_stateData->chasingData.moveSpeed;
	turnLeniance = p_stateData->chasingData.turnLeniance;
	tilt = p_stateData->chasingData.tilt;
}

// Update
void AIStateChasing::Update(float delta)
{

	ComponentAIController* pController = static_cast<ComponentAIController*>(m_pStateMachine->GetStateMachineOwner());

	if (pController->GetGameObject()->GetManager()->GetGameObject(targetID) != NULL)
	{
		
		Common::Transform& transform = pController->GetGameObject()->GetTransform();
		Common::Transform& targetTransform = target->GetTransform();

		// Set y on both to 0
		glm::vec3 transformAdjusted = transform.GetTranslation();
		glm::vec3 targetAdjusted = targetTransform.GetTranslation();

		transformAdjusted.y = 0.0f;
		targetAdjusted.y = 0.0f;

		// Check for a path
		if (currentPath.empty())
		{
			currentPath = PathFinder::Instance()->FindPath(transformAdjusted, targetAdjusted, pController->GetEnemyPathType(), NULL);
		}

		// Different from previous
		else
		{
			PathNode* endNode = PathFinder::Instance()->grid->GetClosestUsableNode(targetAdjusted, pController->GetEnemyPathType(), pController->GetOccupiedNode());
			glm::vec3 currentTarget = currentPath.back();

			if (endNode->position != currentTarget)
			{
				currentPath = PathFinder::Instance()->FindPath(transformAdjusted, targetAdjusted, pController->GetEnemyPathType(), pController->GetOccupiedNode());
			}
		}

		glm::vec3 movePos = transformAdjusted;

		// Find target to move to
		if (currentPath.size() > 0)
		{
			// First 
			movePos = static_cast<glm::vec3>(*currentPath.begin());

			// If position is here, grab next node
			if (currentPath.size() > 1 && glm::length(movePos - transformAdjusted) < 5.0f)
			{
				currentPath.pop_front();
				movePos = static_cast<glm::vec3>(*currentPath.begin());
			}
		}

		// We have target position, move towards it
		glm::vec3 vDiff = movePos - transformAdjusted;

		if (vDiff.x < 1.0f && vDiff.x > -1.0f && vDiff.z > -1.0f && vDiff.z < 1.0f)
			return;


		vDiff.y = 0.0f;
		vDiff = glm::normalize(vDiff);
		
		if (!(vDiff.x == 0 && vDiff.z == 0))
		{

			// Rotate facing direction
			float fAngle = glm::orientedAngle(glm::vec3(0.0f, 0.0f, 1.0f), vDiff, glm::vec3(0.0f, 1.0f, 0.0f));

			Common::GameObject* pGO = pController->GetGameObject();

			// Adjust it to be 0-360 for ease of use
			if (fAngle < 0)
			{
				fAngle += 360;
			}

			// Do some calculations to figure out if I should turn left / right to reach my destination
			float currentAngle = pGO->GetDirection();
			float angleDiff = fAngle - pGO->GetDirection();
			
			// Two directions where I don't have to loop over 0
			if (angleDiff >= -180.0f && angleDiff < 0.0f)
			{
				if (currentAngle - turnSpeed > fAngle)
				{
					pGO->SetDirection(currentAngle - turnSpeed);
				}
				else
					pGO->SetDirection(fAngle);
			}
			else if (angleDiff >= 0.0f && angleDiff < 180.0f)
			{
				if (currentAngle + turnSpeed < fAngle)
				{
					pGO->SetDirection(currentAngle + turnSpeed);
					
				}
				else
					pGO->SetDirection(fAngle);
			}

			// Two directions that loop over 0
			else if (angleDiff >= -360.0f && angleDiff < -180.0f)
			{
				if (currentAngle + turnSpeed < 360.0f)
				{
					pGO->SetDirection(currentAngle + turnSpeed);
				}
				else
				{
					float newAngle = currentAngle - 360.0f + turnSpeed;

					if (newAngle > fAngle)
						pGO->SetDirection(fAngle);
					else
						pGO->SetDirection(newAngle);
				}
			}

			else if (angleDiff >= 180.0f && angleDiff <= 360.0f)
			{
				if (currentAngle - turnSpeed > 0.0f)
				{
					pGO->SetDirection(currentAngle - turnSpeed);
				}
				else
				{
					float newAngle = 360.0f - currentAngle - turnSpeed;

					if (newAngle < fAngle)
						pGO->SetDirection(fAngle);
					else
						pGO->SetDirection(newAngle);
				}
			}

			if (tilt)
			{
				transform.SetRotationXYZ(30.0f, pGO->GetDirection(), 0.0f);
			}
			else
			{
				transform.SetRotationXYZ(0.0f, pGO->GetDirection(), 0.0f);
			}
			
			// Adjust it to be 0-360 for ease of use
			float tempDirection = pGO->GetDirection();
			tempDirection = fmod(tempDirection, 360.0f);		

			if (tempDirection < fAngle + turnLeniance && pGO->GetDirection() > fAngle - turnLeniance)
			{
				transform.Translate(vDiff * moveSpeed * delta);
			}

		}

	}
	else
	{
		// If we don't have a target, we shouldn't be in a chase state; go to idle
		m_pStateMachine->GoToState(ComponentAIController::eAIState_Idle, stateData);
	}
}

// Exit
void AIStateChasing::Exit()
{

}
