//------------------------------------------------------------------------
// PathNode
//	
// Node used for pathfinding with the grid
//------------------------------------------------------------------------

#ifndef PATHNODE_H
#define PATHNODE_H

#include <vector>
#include <glm/glm.hpp>
#include "PathNodeTypes.h"

struct PathNode
{
	// Position and neighbour nodes
	glm::vec3 position;
	std::vector<PathNode*> neighbourNodes;

	// Cached values for finding path, same as 465
	int G;
	int H;

	// Parent node for tracing back
	PathNode* parent;

	// Cost or even usable
	int occupiedCost = 0;
	bool occupiedFlag = false;
	bool walkable = true;

	bool bInOpenList;
	bool bInClosedList;

	PathNodeTypes m_pathNodeTypes;

	PathNode() : G(0), H(0), parent(NULL), bInOpenList(false), bInClosedList(false) 
	{
		m_pathNodeTypes.b_Flying = true;
		m_pathNodeTypes.b_Ground = true;
	}
};


#endif