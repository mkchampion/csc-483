//------------------------------------------------------------------------
// ComponentAIController
//
// Created:	2012/12/14
// Author:	Carel Boers
//	
// This class implements an AI controller. It loads a decision tree from
// XML and processes the game state to decide it's best state.
//------------------------------------------------------------------------

#ifndef COMPNENTAICONTROLLER_H
#define COMPNENTAICONTROLLER_H

#include "ComponentBase.h"
#include "AIDecisionTree.h"
#include "StateMachine.h"
#include "PathNode.h"

class ComponentAIController : public Common::ComponentBase
{
public:

	// Static reset time for AI
	static float DECISION_RESET;

	//------------------------------------------------------------------------------
	// Public types.
	//------------------------------------------------------------------------------

	// Supported AI behaviours
	enum AIState
	{
		eAIState_None = -1,
		eAIState_Idle = 0,
		eAIState_Tracking,
		eAIState_Chasing,
		eAIState_ChasingTracking,
	};

public:
	//------------------------------------------------------------------------------
	// Public methods.
	//------------------------------------------------------------------------------
	ComponentAIController();
	virtual ~ComponentAIController();
		
	// Factory
	static Common::ComponentBase* CreateComponent(TiXmlNode* p_pNode);

	virtual const std::string FamilyID() { return std::string("GOC_CharacterController"); }
	virtual const std::string ComponentID(){ return std::string("GOC_CharacterController"); }
	virtual void Update(float p_fDelta);

	virtual void Init(const char* p_strDecisionTreePath, float despawnDistanceP, EnemyPathType enemyTypeP);

	Common::StateMachine* GetStateMachine() { return m_pStateMachine; }

	// Mapping method
	AIState MapActionToState(const std::string& p_strAction)
	{
		if (p_strAction.compare("ACTION_IDLE") == 0)			{ return eAIState_Idle; }
		else if (p_strAction.compare("ACTION_TRACKING") == 0)	{ return eAIState_Tracking; }
		else if (p_strAction.compare("ACTION_CHASING") == 0)	{ return eAIState_Chasing; }
		else if (p_strAction.compare("ACTION_CHASINGTRACKING") == 0) { return eAIState_ChasingTracking; }
		else													{ return eAIState_None; }
	}

	// Private conversion  function from string to type
	static EnemyPathType MapStringToEnemyType(const std::string& p_strType)
	{
		if (p_strType.compare("GROUND") == 0) { return EnemyPathType::GROUND; }
		else if (p_strType.compare("FLYING") == 0) { return EnemyPathType::FLYING; }

		// Ground as default
		else { return EnemyPathType::GROUND; }
	}

	PathNode* GetOccupiedNode() const { return m_pOccupiedNode; }
	EnemyPathType GetEnemyPathType() const { return m_enemyType; }


private:
	//------------------------------------------------------------------------------
	// Private members.
	//------------------------------------------------------------------------------

	// Our decision tree
	AIDecisionTree* m_pDecisionTree;

	// Our state machine
	Common::StateMachine* m_pStateMachine;

	// Occupied node
	PathNode* m_pOccupiedNode = NULL;

	// Distance to despawn from
	float despawnDistance;

	float decisionTracking;

	// Has an enemy type
	EnemyPathType m_enemyType;



};


#endif // COMPNENTAICONTROLLER_H

