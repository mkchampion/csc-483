//------------------------------------------------------------------------
// ComponentWeapon
//
// Describes the weapon that the GO is using
//------------------------------------------------------------------------

#ifndef COMPONENTWEAPON_H
#define COMPONENTWEAPON_H

#include "ComponentBase.h"
#include <tinyxml.h>
#include <vector>
#include "IWeapon.h"

class ComponentWeapon : public Common::ComponentBase
{

	std::map<std::string, IWeapon*> m_weaponList;
	IWeapon* m_activeWeapon;
	int activeIndex;

public:
	//------------------------------------------------------------------------------
	// Public methods.
	//------------------------------------------------------------------------------
	ComponentWeapon();
	virtual ~ComponentWeapon();

	// Init
	void Init(const std::string& projectilePathP, float fireRateP, float speedP, int damageP, std::map<std::string, IWeapon*> weaponsP);

	// Factory
	static Common::ComponentBase* CreateComponent(TiXmlNode* p_pNode);

	virtual const std::string FamilyID() { return std::string("GOC_Weapon"); }
	virtual const std::string ComponentID(){ return std::string("GOC_Weapon"); }
	virtual void Update(float p_fDelta);

	// Projectile to fire (Uses another GO file)
	std::string projectilePath;

	// Fire rate
	float fireRate;
	float speed;
	float damage;

	void ReadWeaponFile(const std::string& weaponPath);

	IWeapon* GetActiveWeapon() const { return m_activeWeapon; }
	void SetActiveWeapon(const std::string& weaponNameP);

};

#endif // COMPNENTPROJECTILE_H

