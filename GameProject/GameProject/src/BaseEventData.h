//------------------------------------------------------------------------
// BaseEventData.h
//
// The basic event data class that will be used with EventManager
//
// Based off of the BaseEventData from Game Coding Complete but with some components removed
//------------------------------------------------------------------------

#ifndef BASEEVENTDATA_H
#define BASEEVENTDATA_H

#include "IEventData.h"

class BaseEventData : public IEventData
{


public:

	// Explicit constructor
	explicit BaseEventData(const float p_fTimeStamp = 0.0f) : m_fTimeStamp(p_fTimeStamp){};

	// Destructor
	virtual ~BaseEventData() {};

	// Accessor for timestamp
	float GetTimeStamp() const { return m_fTimeStamp; }

	// Virtual EventType again
	virtual const EventType& GetEventType() const = 0;


private:

	// Timestamp
	const float m_fTimeStamp;

};

#endif