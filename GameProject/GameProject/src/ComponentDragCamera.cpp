#include "ComponentDragCamera.h"
#include "GameObject.h"

//------------------------------------------------------------------------------
// Method:    ComponentDragCamera
// Returns:   void
// 
// Constructor
//------------------------------------------------------------------------------
ComponentDragCamera::ComponentDragCamera()
{
	verticalDrag = 0.0f;
	horizontalDrag = 0.0f;
	offsetY = 0.0f;
	offsetZ = 0.0f;
}

//------------------------------------------------------------------------------
// Method:    Init
// Returns:   void
// 
// Sets Parameters
//------------------------------------------------------------------------------
void ComponentDragCamera::Init(const float verticalP, const float horizontalP, const float yOffsetP, const float zOffsetP)
{
	verticalDrag = verticalP;
	horizontalDrag = horizontalP;

	offsetY = yOffsetP;
	offsetZ = zOffsetP;

}

//------------------------------------------------------------------------------
// Method:    Update
// Returns:   void
// 
// Tracking cameras update
//------------------------------------------------------------------------------
void ComponentDragCamera::Update(float p_fDelta)
{
	// Get the targets position and direction
	if (this->GetGameObject()->GetManager()->GetGameObject(m_strTargetID) != NULL) // Checking for Manager being null, not certain why the object itself isn't == NULL
	{
		// Target position
		glm::vec3 targetPos = m_pTarget->GetTransform().GetTranslation();
		glm::vec3 cameraPos = m_pSceneCamera->GetPos();
		glm::vec3 cameraTarget = m_pSceneCamera->GetTarget();

		// Check if the position of the helicopter has moved a certain distance in either direction
		if (abs(targetPos.x - cameraPos.x) > horizontalDrag)
		{
			if (targetPos.x > cameraPos.x)
			{
				m_pSceneCamera->SetPos(glm::vec3(targetPos.x - horizontalDrag, cameraPos.y, cameraPos.z));
				m_pSceneCamera->SetTarget(glm::vec3(targetPos.x - horizontalDrag, cameraTarget.y, cameraTarget.z));
			}
			else
			{
				m_pSceneCamera->SetPos(glm::vec3(targetPos.x + horizontalDrag, cameraPos.y, cameraPos.z));
				m_pSceneCamera->SetTarget(glm::vec3(targetPos.x + horizontalDrag, cameraTarget.y, cameraTarget.z));
			}
		}

		// Readjust camera position (Fixes issue where they are both occurring)
		cameraPos = m_pSceneCamera->GetPos();

		if (abs(targetPos.z - (cameraPos.z - offsetZ)) > verticalDrag)
		{
			if (targetPos.z > (cameraPos.z - offsetZ))
			{
				m_pSceneCamera->SetPos(glm::vec3(cameraPos.x, cameraPos.y, targetPos.z - verticalDrag + offsetZ));
				m_pSceneCamera->SetTarget(glm::vec3(cameraPos.x, cameraTarget.y, cameraPos.z - offsetZ));
			}
			else
			{
				m_pSceneCamera->SetPos(glm::vec3(cameraPos.x, cameraPos.y, targetPos.z + verticalDrag + offsetZ));
				m_pSceneCamera->SetTarget(glm::vec3(cameraPos.x, cameraTarget.y, cameraPos.z - offsetZ));
			}
		}

		// Adjust the camera to the correct y value (Needs to check all the time)
		cameraPos = m_pSceneCamera->GetPos();
		m_pSceneCamera->SetPos(glm::vec3(cameraPos.x, targetPos.y + offsetY, cameraPos.z));
	}
}

// Setting Target directly
void ComponentDragCamera::SetTarget(Common::GameObject* p_pTarget)
{
	// Initial setting of the target
	m_pTarget = p_pTarget;
	m_strTargetID = m_pTarget->GetGUID();

	// Place the camera at the target accordingly
	glm::vec3 position = m_pTarget->GetTransform().GetTranslation();
	m_pSceneCamera->SetPos(position + glm::vec3(0.0f, offsetY, offsetZ));
	m_pSceneCamera->SetTarget(position);
}

// Setting target via ID
void ComponentDragCamera::SetTarget(const std::string& p_strTargetID, Common::GameObjectManager* p_pGOManager)
{
	// Initial setting of the target
	m_strTargetID = p_strTargetID;
	m_pTarget = p_pGOManager->GetGameObject(m_strTargetID);

}

//------------------------------------------------------------------------------
// Method:    CreateComponent
// Parameter: TiXmlNode * p_pNode
// Returns:   Common::ComponentBase*
// 
// Factory construction method. Returns an initialized ComponentAnimController
// instance if the passed in XML is valid, NULL otherwise.
//------------------------------------------------------------------------------
Common::ComponentBase* ComponentDragCamera::CreateComponent(TiXmlNode* p_pNode)
{
	assert(strcmp(p_pNode->Value(), "GOC_DragCamera") == 0);
	ComponentDragCamera* pCamera = new ComponentDragCamera();

	// Drag values
	float vertical = 0.0f;
	float horizontal = 0.0f;
	float yOffset = 0.0f;
	float zOffset = 0.0f;


	// Iterate the xml elements
	TiXmlNode* pChildNode = p_pNode->FirstChild();
	while (pChildNode != NULL)
	{
		const char* szNodeName = pChildNode->Value();
		
		if (strcmp(szNodeName, "DragValues") == 0)
		{

			// Parse attributes
			TiXmlElement* pElement = pChildNode->ToElement();

			// Rotation Correction
			if (pElement->QueryFloatAttribute("vertical", &vertical) != TIXML_SUCCESS)
			{
				delete pCamera;
				return NULL;
			}

			// Rotation Correction
			if (pElement->QueryFloatAttribute("horizontal", &horizontal) != TIXML_SUCCESS)
			{
				delete pCamera;
				return NULL;
			}

		}

		if (strcmp(szNodeName, "Offsets") == 0)
		{

			// Parse attributes
			TiXmlElement* pElement = pChildNode->ToElement();

			// Rotation Correction
			if (pElement->QueryFloatAttribute("y", &yOffset) != TIXML_SUCCESS)
			{
				delete pCamera;
				return NULL;
			}

			// Rotation Correction
			if (pElement->QueryFloatAttribute("z", &zOffset) != TIXML_SUCCESS)
			{
				delete pCamera;
				return NULL;
			}

		}

		pChildNode = pChildNode->NextSibling();

	}

	pCamera->Init(vertical, horizontal, yOffset, zOffset);
	return pCamera;
}
