//------------------------------------------------------------------------
// ComponentIdleMotion.h
//	
// Component meant to be used to have the pickups idle / spin
//------------------------------------------------------------------------

#include "ComponentIdleMotion.h"
#include "ComponentRenderableMesh.h"

// Constructor
ComponentIdleMotion::ComponentIdleMotion()
{
}

// Destructor
ComponentIdleMotion::~ComponentIdleMotion()
{
}

//Init
void ComponentIdleMotion::Init(float minBobP, float maxBobP, float bobSpeedP, float rotationSpeedP, float tiltAngleP, const glm::vec3& tiltAxisP, float offsetP)
{
	min = minBobP;
	max = maxBobP;
	bobSpeed = bobSpeedP;
	rotationSpeed = rotationSpeedP;
	tilt = tiltAngleP;
	tiltAxis = tiltAxisP;
	offset = offsetP;

}

void ComponentIdleMotion::SetStartHeight(const glm::vec3& startPositionP)
{
	this->GetGameObject()->GetTransform().SetTranslation(startPositionP);

	// Set the high / low positions
	min = startPositionP.y - min + offset;
	max = startPositionP.y + max + offset;

	isDrop = true;

}

// Update
void ComponentIdleMotion::Update(float p_fDelta)
{

	if (!initialTilt)
	{
		// Set the tilt rotation of the model
		this->GetGameObject()->GetTransform().SetRotationXYZ(tilt, 0.0f, 0.0f);
		initialTilt = true;
	}

	// Rotate the GO
	this->GetGameObject()->GetTransform().RotateXYZ(0.0f, rotationSpeed * p_fDelta, 0.0f);


	// Move up
	if (bobbingUp)
	{


		this->GetGameObject()->GetTransform().TranslateXYZ(0.0f, bobSpeed * p_fDelta, 0.0f);

		if (this->GetGameObject()->GetTransform().GetTranslation().y > max)
		{
			glm::vec3 translation = this->GetGameObject()->GetTransform().GetTranslation();
			this->GetGameObject()->GetTransform().SetTranslationXYZ(translation.x, max, translation.z);
			bobbingUp = false;
		}
	}
	else
	{
		this->GetGameObject()->GetTransform().TranslateXYZ(0.0f, -bobSpeed * p_fDelta, 0.0f);

		if (this->GetGameObject()->GetTransform().GetTranslation().y < min)
		{
			glm::vec3 translation = this->GetGameObject()->GetTransform().GetTranslation();
			this->GetGameObject()->GetTransform().SetTranslationXYZ(translation.x, min, translation.z);
			bobbingUp = true;
		}
	}
}

// Parsing
Common::ComponentBase* ComponentIdleMotion::CreateComponent(TiXmlNode* p_pNode)
{
	assert(strcmp(p_pNode->Value(), "GOC_IdleMotion") == 0);
	ComponentIdleMotion* pIdle = new ComponentIdleMotion();

	
	float minTemp = 0.0f;
	float maxTemp = 0.0f;
	float bobSpeedTemp = 0.0f;
	float tiltTemp = 0.0f;
	glm::vec3 tiltAxisTemp;
	float rotationSpeedTemp = 0.0f;
	float offsetTemp = 0.0f;

	// Iterate Anim elements in the XML
	TiXmlNode* pChildNode = p_pNode->FirstChild();
	while (pChildNode != NULL)
	{
		const char* szNodeName = pChildNode->Value();

		if (strcmp(szNodeName, "Bobbing") == 0)
		{

			// Parse attributes
			TiXmlElement* pElement = pChildNode->ToElement();

			if (pElement->QueryFloatAttribute("min", &minTemp) != TIXML_SUCCESS)
			{
				delete pIdle;
				return NULL;
			}

			if (pElement->QueryFloatAttribute("max", &maxTemp) != TIXML_SUCCESS)
			{
				delete pIdle;
				return NULL;
			}

			if (pElement->QueryFloatAttribute("speed", &bobSpeedTemp) != TIXML_SUCCESS)
			{
				delete pIdle;
				return NULL;
			}

			if (pElement->QueryFloatAttribute("offset", &offsetTemp) != TIXML_SUCCESS)
			{
				delete pIdle;
				return NULL;
			}
		}

		if (strcmp(szNodeName, "RotationSpeed") == 0)
		{

			// Parse attributes
			TiXmlElement* pElement = pChildNode->ToElement();

			// Rotation Correction
			if (pElement->QueryFloatAttribute("speed", &rotationSpeedTemp) != TIXML_SUCCESS)
			{
				delete pIdle;
				return NULL;
			}
		}

		if (strcmp(szNodeName, "Tilt") == 0)
		{

			// Parse attributes
			TiXmlElement* pElement = pChildNode->ToElement();

			// Rotation Correction
			if (pElement->QueryFloatAttribute("angle", &tiltTemp) != TIXML_SUCCESS)
			{
				delete pIdle;
				return NULL;
			}

			// Rotation Correction
			if (pElement->QueryFloatAttribute("x", &tiltAxisTemp.x) != TIXML_SUCCESS)
			{
				delete pIdle;
				return NULL;
			}

			// Rotation Correction
			if (pElement->QueryFloatAttribute("y", &tiltAxisTemp.y) != TIXML_SUCCESS)
			{
				delete pIdle;
				return NULL;
			}

			// Rotation Correction
			if (pElement->QueryFloatAttribute("z", &tiltAxisTemp.z) != TIXML_SUCCESS)
			{
				delete pIdle;
				return NULL;
			}
		}

		pChildNode = pChildNode->NextSibling();
	}

	pIdle->Init(minTemp, maxTemp, bobSpeedTemp, rotationSpeedTemp, tiltTemp, tiltAxisTemp, offsetTemp);
	return pIdle;
}
