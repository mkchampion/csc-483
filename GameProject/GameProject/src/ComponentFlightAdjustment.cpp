//------------------------------------------------------------------------
// ComponentFlightAdjustment.h
//	
// Component used to adjust game objects if they go over flight boundries
//------------------------------------------------------------------------

#include "ComponentFlightAdjustment.h"
#include "TerrainManager.h"

// Constructor
ComponentFlightAdjustment::ComponentFlightAdjustment()
{
	m_lFlyBoundsWorld = TerrainManager::Instance()->GetFlyingBounds();
}

// Destructor
ComponentFlightAdjustment::~ComponentFlightAdjustment()
{
}

// Init
void ComponentFlightAdjustment::Init(float normHeightP, float elevatedHeightP, float speedP)
{
	normHeight = normHeightP;
	elevatedHeight = elevatedHeightP;
	elevationSpeed = speedP;
}

// Update
void ComponentFlightAdjustment::Update(float p_fDelta)
{
	
	glm::vec3 currentPos = this->GetGameObject()->GetTransform().GetTranslation();
	glm::vec3 velocity = glm::vec3(0.0f, 0.0f, 0.0f);
	bool elevated = false;

	// Check bounds
	for (int i = 0; i < m_lFlyBoundsWorld->size(); i++)
	{
		FlyBoundsWorld* bounds = m_lFlyBoundsWorld->at(i);

		if (currentPos.z < bounds->min && currentPos.z > bounds->max)
		{
			elevated = true;
			break;
		}
	}

	
	// Adjust position
	if (elevated)
	{
		if (currentPos.y < elevatedHeight)
			velocity.y = elevationSpeed*p_fDelta;
		else
		{
			velocity.y = 0.0f;
			this->GetGameObject()->GetTransform().SetTranslationXYZ(currentPos.x, elevatedHeight, currentPos.z);
		}
	}
	else
	{
		if (currentPos.y > normHeight)
			velocity.y = -elevationSpeed*p_fDelta;
		else
		{
			velocity.y = 0.0f;
			this->GetGameObject()->GetTransform().SetTranslationXYZ(currentPos.x, normHeight, currentPos.z);
		}
	}

	// Translate by velocity
	this->GetGameObject()->GetTransform().Translate(velocity);

}

// Reading from file
Common::ComponentBase* ComponentFlightAdjustment::CreateComponent(TiXmlNode* p_pNode)
{
	assert(strcmp(p_pNode->Value(), "GOC_FlightAdjustment") == 0);
	ComponentFlightAdjustment* pFlight = new ComponentFlightAdjustment();

	float normHeightT = 0.0f;
	float elevatedHeightT = 0.0f;
	float elevationSpeedT = 0.0f;

	// Iterate Anim elements in the XML
	TiXmlNode* pChildNode = p_pNode->FirstChild();
	while (pChildNode != NULL)
	{
		const char* szNodeName = pChildNode->Value();


		if (strcmp(szNodeName, "NormHeight") == 0)
		{

			// Parse attributes
			TiXmlElement* pElement = pChildNode->ToElement();

			// Rotation Correction
			if (pElement->QueryFloatAttribute("value", &normHeightT) != TIXML_SUCCESS)
			{
				delete pFlight;
				return NULL;
			}
		}

		else if (strcmp(szNodeName, "ElevatedHeight") == 0)
		{

			// Parse attributes
			TiXmlElement* pElement = pChildNode->ToElement();

			// Rotation Correction
			if (pElement->QueryFloatAttribute("value", &elevatedHeightT) != TIXML_SUCCESS)
			{
				delete pFlight;
				return NULL;
			}
		}

		else if (strcmp(szNodeName, "ElevationSpeed") == 0)
		{

			// Parse attributes
			TiXmlElement* pElement = pChildNode->ToElement();

			// Rotation Correction
			if (pElement->QueryFloatAttribute("value", &elevationSpeedT) != TIXML_SUCCESS)
			{
				delete pFlight;
				return NULL;
			}
		}

		pChildNode = pChildNode->NextSibling();
	}

	pFlight->Init(normHeightT, elevatedHeightT, elevationSpeedT);
	return pFlight;
}