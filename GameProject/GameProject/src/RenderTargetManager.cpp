//------------------------------------------------------------------------
// RenderTargetManager
//	
// Represents some sort of Managing class for RenderTargets
//------------------------------------------------------------------------

#include "RenderTargetManager.h"
#include "HeliGame.h"

// Instance
RenderTargetManager* RenderTargetManager::s_pRenderManager = NULL;

// Create Instance
void RenderTargetManager::CreateInstance()
{
	assert(s_pRenderManager == NULL);
	s_pRenderManager = new RenderTargetManager();
}

// Destroy Instance
void RenderTargetManager::DestroyInstance()
{
	assert(s_pRenderManager != NULL);
	delete s_pRenderManager;
	s_pRenderManager = NULL;
}

// Get Instance
RenderTargetManager* RenderTargetManager::Instance()
{
	assert(s_pRenderManager != NULL);
	return s_pRenderManager;
}

//  Constructor
RenderTargetManager::RenderTargetManager()
{
	
	// First buffer that everything is default rendered to
	m_pToScreenMat = wolf::MaterialManager::CreateMaterial("ToScreenMat");
	m_pToScreenMat->SetProgram("GameProject/GameProject/data/Shaders/2d.vsh", "GameProject/GameProject/data/Shaders/2d.fsh");
	m_pMainRender = new wolf::RenderTarget(HeliGame::GetInstance()->GetWindowWidth(), HeliGame::GetInstance()->GetWindowHeight(), 1.0f, false);
	m_pMainRender->SetupColorBuffer(true);
	m_pMainRender->SetupDepthBuffer(false);

	// First buffer that everything is default rendered to
	m_pShadowMapRender = new wolf::RenderTarget(2048, 2048, 1.0f, false);
	//m_pShadowMapRender->SetupColorBuffer(true);
	m_pShadowMapRender->SetupDepthBuffer(true);
	

	// Make screen
	m_pScreen = new wolf::RenderTarget(HeliGame::GetInstance()->GetWindowWidth(), HeliGame::GetInstance()->GetWindowHeight(), 1.0f, true);

	m_pEffects = new std::vector<EffectAndTarget>();

}

// Destructor
RenderTargetManager::~RenderTargetManager()
{

}

// Bind main scene
void RenderTargetManager::BindMainScene()
{
	m_pMainRender->Bind();
}

// Check Main completeness
bool RenderTargetManager::IsMainComplete() const
{
	return m_pMainRender->IsRenderTargetComplete();
}

// Bind main scene
void RenderTargetManager::BindShadowMapRender()
{
	m_pShadowMapRender->Bind();
}

// Bind main scene
bool RenderTargetManager::isShadowComplete() const
{
	return m_pShadowMapRender->IsRenderTargetComplete();
}

// Add Effect
void RenderTargetManager::SetEffects(std::vector<EffectAndTarget>* effects)
{
	m_pEffects = effects;
}

// Render
void RenderTargetManager::Render()
{

	// Track the previous
	wolf::RenderTarget* previous = m_pMainRender;

	// Go through each effect
	for (int i = 0; i < m_pEffects->size(); i++)
	{
		wolf::RenderTarget* current = m_pEffects->at(i).renderTarget;
		if (current->IsRenderTargetComplete())
		{
			current->Render(m_pEffects->at(i).mat, previous);
			previous = current;
		}
		else
		{
			int test = 5;
		}
	}
	
	// Render to screen
	if (m_pScreen->IsRenderTargetComplete())
	{
		m_pScreen->Render(m_pToScreenMat, previous);
	}
	else
	{
		int test = 56;
	}

	

}
