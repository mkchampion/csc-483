//-----------------------------------------------------------------------------
// W_GroundMesh
//
// Represents a group of vertices for the ground
//-----------------------------------------------------------------------------

#ifndef W_GROUNDMESH_H
#define W_GROUNDMESH_H

#include <string.h>
#include "W_Texture.h"
#include "W_TextureManager.h"
#include "W_ProgramManager.h"
#include "W_BufferManager.h"
#include "W_VertexBuffer.h"
#include "W_VertexDeclaration.h"
#include "W_MaterialManager.h"
#include "GameProject\GameProject\src\DirectionalLight.h"

namespace wolf
{

	// Struct for the quad
	struct GroundVertex
	{
		GLfloat x, y, z;	   // Position
		GLfloat u, v;		   // Texture coordinates
		GLfloat nx, ny, nz;    // Normal
	};


	class GroundMesh
	{

	public:

		// Constructor/Destructor
		GroundMesh(const std::string& p_strMaterialName, const std::string& p_strTexture, const std::string& p_strVertexProgram, const std::string& p_strFragmentProgram);
		~GroundMesh();

		// Update / Render
		void Update(float p_fDelta);
		void Render(const glm::mat4& p_mView, const glm::mat4& p_mProj, const glm::mat4& p_mLight);

		// Transform setter (Same as model)
		void SetTransform(const glm::mat4& p_mWorldTransform) { m_mWorldTransform = p_mWorldTransform; }

		// Material accessor
		wolf::Material* GetMaterial() { return m_pMaterial; }

		// Adding a quad?
		void AddGroundQuad(const glm::vec3& positionP, float quadWidth, float quadDepth, int textureNum);

		// Method to create/declare the vertex
		bool DeclareVertexBuffer();

	private:

		// Vertex buffer and Vertex declaration to use
		wolf::VertexBuffer* m_pVertexBuffer;
		wolf::IndexBuffer* m_pIndexBuffer;
		wolf::VertexDeclaration* m_pVertexDeclaration;
		wolf::Program* m_pProgram;
		wolf::Texture* m_pTexture;

		// Going to try and use materials 
		wolf::Material* m_pMaterial;

		// Transform
		glm::mat4 m_mWorldTransform;

		// Vectors for vertices / indices
		std::vector<GroundVertex> meshVertices;
		std::vector<GLushort> meshIndices;

		int vertexOffset = 0;
	};

}

#endif