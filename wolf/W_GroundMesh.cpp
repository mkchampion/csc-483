//-----------------------------------------------------------------------------
// W_GroundMesh
//
// Represents a group of vertices for the ground
//-----------------------------------------------------------------------------

#include "W_GroundMesh.h"
#include "GameProject\GameProject\src\DrawCounting.h"
#include <glm/gtc/matrix_transform.hpp>

using namespace wolf;

//------------------------------------------------------------------------------
// Method:    BuildableMesh
// Returns:   void
// 
// Constructor
//------------------------------------------------------------------------------
GroundMesh::GroundMesh(const std::string& p_strMaterialName, const std::string& p_strTexture, const std::string& p_strVertexProgram, const std::string& p_strFragmentProgram)
{
	vertexOffset = 0;

	m_mWorldTransform = glm::translate(0.0f, 0.0f, 0.0f);

	// Create the program
	m_pMaterial = wolf::MaterialManager::CreateMaterial(p_strMaterialName);
	m_pTexture = wolf::TextureManager::CreateTexture(p_strTexture);

	// Wrap mode
	m_pTexture->SetWrapMode(wolf::Texture::WrapMode::WM_Repeat, wolf::Texture::WrapMode::WM_Repeat);

	// Create material
	m_pMaterial->SetProgram(p_strVertexProgram, p_strFragmentProgram);
	m_pMaterial->SetTexture("texture1", m_pTexture);
}

// Destructor
GroundMesh::~GroundMesh()
{
}

// Declaration of the vertex buffer
bool GroundMesh::DeclareVertexBuffer()
{
	// Array to pass
	GroundVertex* meshVerts = &meshVertices[0];
	GLushort* meshI = &meshIndices[0];

	m_pVertexBuffer = wolf::BufferManager::CreateVertexBuffer(meshVerts, sizeof(GroundVertex) * meshVertices.size());

	m_pIndexBuffer = wolf::BufferManager::CreateIndexBuffer(sizeof(GLushort) * meshIndices.size());
	m_pIndexBuffer->Write(meshI, sizeof(GLushort) * meshIndices.size());

	m_pVertexDeclaration = new wolf::VertexDeclaration();
	m_pVertexDeclaration->Begin();
	m_pVertexDeclaration->AppendAttribute(wolf::AT_Position, 3, wolf::CT_Float);
	m_pVertexDeclaration->AppendAttribute(wolf::AT_TexCoord1, 2, wolf::CT_Float);
	m_pVertexDeclaration->AppendAttribute(wolf::AT_Normal, 3, wolf::CT_Float);
	m_pVertexDeclaration->SetVertexBuffer(m_pVertexBuffer);
	m_pVertexDeclaration->SetIndexBuffer(m_pIndexBuffer);
	m_pVertexDeclaration->End();

	return true;

}

void GroundMesh::AddGroundQuad(const glm::vec3& positionP, float quadWidth, float quadDepth, int textureNum)
{
	// Create vertices for that quad
	GroundVertex v1 = { positionP.x, positionP.y, positionP.z, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f };
	GroundVertex v2 = { positionP.x + quadWidth, positionP.y, positionP.z, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f };
	GroundVertex v3 = { positionP.x, positionP.y, positionP.z - quadDepth, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f };
	GroundVertex v4 = { positionP.x + quadWidth, positionP.y, positionP.z - quadDepth, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f };

	// Add vertices to the vector
	meshVertices.push_back(v1);
	meshVertices.push_back(v2);
	meshVertices.push_back(v3);
	meshVertices.push_back(v4);

	// Add to the indices
	meshIndices.push_back(vertexOffset);
	meshIndices.push_back(vertexOffset + 2);
	meshIndices.push_back(vertexOffset + 1);

	meshIndices.push_back(vertexOffset + 1);
	meshIndices.push_back(vertexOffset + 3);
	meshIndices.push_back(vertexOffset + 2);

	vertexOffset += 4;
}

//------------------------------------------------------------------------------
// Method:    Render
// Returns:   void
// 
// Renders the quad
//------------------------------------------------------------------------------
void GroundMesh::Render(const glm::mat4& p_mView, const glm::mat4& p_mProj, const glm::mat4& p_mLight)
{
	m_pVertexDeclaration->Bind();

	// Uniforms
	glm::mat4 worldViewProj = p_mProj * p_mView * m_mWorldTransform;
	m_pMaterial->SetUniform("WorldViewProj", worldViewProj);

	// Light
	glm::mat4 biasLightWVP = p_mLight * m_mWorldTransform;
	m_pMaterial->SetUniform("LightMatrix", biasLightWVP);

	m_pMaterial->Apply();

	// Draw!
	glDrawElements(GL_TRIANGLES, meshIndices.size(), GL_UNSIGNED_SHORT, 0);

	// Draw counting
	DrawCalls += 1;

}