//-----------------------------------------------------------------------------
// File:			W_Model.h
// Original Author:	Gordon Wood
//
// Class representing a model that can be loaded from a POD file
//
// ASSUMPTIONS:
//	1) POD files will always be exported as Index Triangle Lists
//	2) No more than 8 UV channels in the POD file
//	3) Model is exported with interleaved data
//-----------------------------------------------------------------------------
#ifndef W_MODEL_H
#define W_MODEL_H

#include "W_Types.h"
#include "W_VertexBuffer.h"
#include "W_IndexBuffer.h"
#include "W_BufferManager.h"
#include "W_Material.h"
#include "W_VertexDeclaration.h"
#include "W_TextureManager.h"
#include "W_ProgramManager.h"
#include "PVRTModelPOD.h"
#include <string>
#include <map>
#include <vector>

namespace wolf
{

	BlendMode convertBlendModes(EPODBlendFunc PODBlendFunc);
	BlendEquation convertBlendEquation(EPODBlendOp PODBlendEquation);

struct MeshRotationInfo
{
	int id = 0;
	bool isRotating = false;
	bool isLinkedToBase = false;
	float rotationSpeed;
	float currentRotation;
	glm::vec3 rotationAxis;
};

struct CustomTransform
{
	glm::mat4 translation;
	glm::mat4 rotation;
	glm::mat4 scale;
};

class Model
{
	public:
		//-------------------------------------------------------------------------
		// PUBLIC INTERFACE
		//-------------------------------------------------------------------------
		Model(const std::string& p_strFile, const std::string& p_strTexturePrefix = "", const std::string& p_strVertexProgram = "", const std::string& p_strFragmentProgram = "");
		~Model();

		void Update(float p_fDelta);
		void Render(const glm::mat4& p_mView, const glm::mat4& p_mProj, const glm::mat4& p_mLight);

		void SetAnimFrame(float p_fFrame) { m_fFrame = p_fFrame; m_pod.SetFrame(m_fFrame); }
		wolf::Material* GetMaterial() { return m_pMaterial; }
		wolf::Material* GetMaterial(const std::string& p_strMatName);
		void SetTransform(const glm::mat4& p_mWorldTransform) { m_mWorldTransform = p_mWorldTransform; }
		void SetTransformComponents(const glm::vec3& p_mTranslation, const glm::quat& p_mRotation, const glm::vec3& p_mScale);

		// Get the map of materials
		std::map<std::string, wolf::Material*>* GetMaterialMap() { return &m_mMaterials; }


		// A seperate render method for reusing the same model?
		void Render(const glm::mat4& p_mView, const glm::mat4& p_mProj, const glm::mat4& p_mWorldTransform, const glm::mat4& p_mLight);

		// Grab a mesh rotation
		MeshRotationInfo* GetMeshRotation(const int meshNumP);

		// Add a custom mesh transform
		void SetCustomTransform(std::map<int, CustomTransform*>* customTransformsP) { m_mCustomMeshTransforms = customTransformsP; };
		//-------------------------------------------------------------------------

	private:
		//-------------------------------------------------------------------------
		// PRIVATE TYPES
		//-------------------------------------------------------------------------
		struct Mesh
		{
			wolf::VertexBuffer* m_pVB;
			wolf::IndexBuffer* m_pIB;
			wolf::VertexDeclaration* m_pDecl;
		};
		//-------------------------------------------------------------------------

		//-------------------------------------------------------------------------
		// PRIVATE METHODS
		//-------------------------------------------------------------------------
		//-------------------------------------------------------------------------

		//-------------------------------------------------------------------------
		// PRIVATE MEMBERS
		//-------------------------------------------------------------------------
		float				m_fFrame;
		CPVRTModelPOD		m_pod;
		std::vector<Mesh>	m_lMeshes;
		Material*			m_pMaterial;
		glm::mat4			m_mWorldTransform;
		std::map<std::string, Material*> m_mMaterials;

		// Transform components (Set, may not be used)
		glm::vec3			m_mTranslation;
		glm::quat			m_mRotation;
		glm::vec3			m_mScale;


		// For individual mesh rotation
		std::map<int, MeshRotationInfo*> m_mMeshRotationMap;
		std::map<int, CustomTransform*>* m_mCustomMeshTransforms;
		//-------------------------------------------------------------------------
};


}

#endif



