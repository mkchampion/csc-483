//-----------------------------------------------------------------------------
// File: W_BuildableMesh.h
//
// Represents a group of vertices for a sort of mesh grid
// Can be added onto row by row and altered by the use of a texture
//-----------------------------------------------------------------------------
#ifndef W_BUILDABLEMESH_H
#define W_BUILDABLEMESH_H

#include <string.h>
#include "W_Texture.h"
#include "W_TextureManager.h"
#include "W_ProgramManager.h"
#include "W_BufferManager.h"
#include "W_VertexBuffer.h"
#include "W_VertexDeclaration.h"
#include "W_MaterialManager.h"

namespace wolf
{

	// Struct for the quad
	struct MeshVertex
	{
		GLfloat x, y, z;	   // Position
		GLfloat u, v;		   // Texture coordinates
		GLfloat heightU, heightV;    // Height map coordinates
		GLfloat nx, ny, nz;	   // Normal
	};


	class BuildableMesh
	{

	public:

		// Constructor/Destructor
		BuildableMesh(const std::string& p_strMaterialName, const std::string& p_strTexture, const std::string& p_strVertexProgram, const std::string& p_strFragmentProgram);
		BuildableMesh(){};
		~BuildableMesh();

		// Update / Render
		void Update(float p_fDelta);
		void Render(const glm::mat4& p_mView, const glm::mat4& p_mProj);

		// Transform setter (Same as model)
		void SetTransform(const glm::mat4& p_mWorldTransform) { m_mWorldTransform = p_mWorldTransform; }

		// Material accessor
		wolf::Material* GetMaterial() { return m_pMaterial; }

		// Adding a row
		void AddMeshRow(int numXTiles, int quadsPerTile, float tileSize, float xOffset, float zOffset, float mapWidth, float meshHeight);
		void AdjustMesh(const std::string& heightMap);

		// Method to create/declare the vertex
		bool DeclareVertexBuffer();


	private:

		// Vertex buffer and Vertex declaration to use
		wolf::VertexBuffer* m_pVertexBuffer;
		wolf::IndexBuffer* m_pIndexBuffer;
		wolf::VertexDeclaration* m_pVertexDeclaration;
		wolf::Program* m_pProgram;
		wolf::Texture* m_pTexture;

		// Going to try and use materials 
		wolf::Material* m_pMaterial;

		// Transform
		glm::mat4 m_mWorldTransform;

		// Vectors for vertices / indices
		std::vector<MeshVertex> meshVertices;
		std::vector<GLushort> meshIndices;

		int vertexOffset = 0;
	};

}

#endif