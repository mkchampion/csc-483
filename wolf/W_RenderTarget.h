//------------------------------------------------------------------------
// W_RenderTarget
//	
// Attempted wrapper around FBO's. 
// Based on the RenderTarget written by Gordon Wood for CSC 436, but from scratch to try and relearn the core
//------------------------------------------------------------------------

#ifndef W_RENDERTARGET_H
#define W_RENDERTARGET_H

#include <GL\glew.h>
#include "W_VertexDeclaration.h"
#include "W_Program.h"
#include "W_MaterialManager.h"

namespace wolf
{

	class RenderTarget
	{

	public:

		// Constructor / Destructor
		RenderTarget(unsigned int p_uiWidth, unsigned int p_uiHeight, float debugScale, bool isScreen);
		~RenderTarget();

		// Accessing Buffers
		Texture* GetColorBufferT() const { return m_pColorBufferT; }
		Texture* GetDepthBufferT() const { return m_pDepthBufferT; }

		GLuint GetColorBuffer() const { return m_uiColorBuffer; }
		GLuint GetDepthBuffer() const { return m_uiDepthBuffer; }

		// Methods to set buffers up?
		void SetupColorBuffer(bool isTexture);
		void SetupDepthBuffer(bool isTexture);

		// Bind this RenderTarget
		void Bind() const;
		void Render(Material* p_pMaterial, RenderTarget* p_pRenderTarget) const;

		// Check if complete
		bool IsRenderTargetComplete();

	private:

		// Ints for buffers / FBO
		GLuint m_uiFBO;

		// Texture
		Texture* m_pColorBufferT;
		Texture* m_pDepthBufferT;

		// For RenderBuffers
		GLuint m_uiColorBuffer;
		GLuint m_uiDepthBuffer;

		bool m_bHasColorTexture = false;
		bool m_bHasDepthTexture = false;

		// Vertex Decleration / Buffer
		wolf::VertexDeclaration* m_pVertexDeclaration;
		wolf::VertexBuffer* m_pVertexBuffer;

		// Width/Height
		float m_uWidth;
		float m_uHeight;

		// Texture width/height
		float m_uTextureWidth;
		float m_uTextureHeight;

		// Bool for screen
		bool m_bIsScreen;

	};
}

#endif