#include "W_BuildableMesh.h"
#include "GameProject\GameProject\src\DrawCounting.h"

using namespace wolf;

//------------------------------------------------------------------------------
// Method:    BuildableMesh
// Returns:   void
// 
// Constructor
//------------------------------------------------------------------------------
BuildableMesh::BuildableMesh(const std::string& p_strMaterialName, const std::string& p_strTexture, const std::string& p_strVertexProgram, const std::string& p_strFragmentProgram)
{
	vertexOffset = 0;

	m_mWorldTransform = glm::translate(0.0f, 0.0f, 0.0f);

	// Create the program
	m_pMaterial = wolf::MaterialManager::CreateMaterial(p_strMaterialName);
	m_pTexture = wolf::TextureManager::CreateTexture(p_strTexture);

	// Wrap mode
	m_pTexture->SetWrapMode(wolf::Texture::WrapMode::WM_Repeat, wolf::Texture::WrapMode::WM_Repeat);

	// Create material
	m_pMaterial->SetProgram(p_strVertexProgram, p_strFragmentProgram);
	m_pMaterial->SetTexture("texture1", m_pTexture);
}

// Destructor
BuildableMesh::~BuildableMesh()
{
}

// Declaration of the vertex buffer
bool BuildableMesh::DeclareVertexBuffer()
{
	// Array to pass
	MeshVertex* meshVerts = &meshVertices[0];
	GLushort* meshI = &meshIndices[0];

	m_pVertexBuffer = wolf::BufferManager::CreateVertexBuffer(meshVerts, sizeof(MeshVertex) * meshVertices.size());

	//m_pIndexBuffer = wolf::BufferManager::CreateIndexBuffer(sizeof(GLushort) * meshIndices.size());
	//m_pIndexBuffer->Write(meshI, sizeof(GLushort) * meshIndices.size());

	m_pVertexDeclaration = new wolf::VertexDeclaration();
	m_pVertexDeclaration->Begin();
	m_pVertexDeclaration->AppendAttribute(wolf::AT_Position, 3, wolf::CT_Float);
	m_pVertexDeclaration->AppendAttribute(wolf::AT_TexCoord1, 2, wolf::CT_Float);
	m_pVertexDeclaration->AppendAttribute(wolf::AT_TexCoord2, 2, wolf::CT_Float);
	m_pVertexDeclaration->AppendAttribute(wolf::AT_Normal, 3, wolf::CT_Float);
	m_pVertexDeclaration->SetVertexBuffer(m_pVertexBuffer);
	m_pVertexDeclaration->End();

	return true;

}

void BuildableMesh::AddMeshRow(int numXTiles, int quadsPerTile, float tileSize, float xOffset, float zOffset, float mapWidth, float meshHeight)
{

	int vertexCount = 0;

	// Number of quads
	int numXQuads = numXTiles * quadsPerTile;
	int numZQuads = quadsPerTile;

	// Same width/depth
	float quadWidth = tileSize / (float)quadsPerTile;
	float quadDepth = quadWidth;

	// Height map coordinate info
	float heightMapXIncrement = 1 / (mapWidth * quadsPerTile);
	float heightMapXOffset = xOffset / (mapWidth * tileSize);

	float heightMapZIncrement = 1 / (meshHeight * quadsPerTile);
	float heightMapZOffset = -zOffset / (meshHeight * tileSize);

	// Temp vector for vertices
	std::vector<MeshVertex> tempVertices;

	// Creating all the vertices
	for (int z = 0; z < numZQuads + 1; z++)
	{
		for (int x = 0; x < numXQuads + 1; x++)
		{			
			GLfloat u = (x * quadWidth) / tileSize;
			GLfloat v = (z * quadDepth) / tileSize;

			// Height map coordinates
			GLfloat uHeight = (x * heightMapXIncrement);
			GLfloat vHeight = heightMapZOffset + (z * heightMapZIncrement);

			MeshVertex vertex = { xOffset + (x * quadWidth), 0.0f, zOffset + (z*-quadDepth), u, v, uHeight, vHeight, 0.0f, 1.0f, 0.0f };
			tempVertices.push_back(vertex);
			vertexCount++;
		}
	}


	// Creating all the indices
	for (int z = 0; z < numZQuads; z++)
	{
		for (int x = 0; x < numXQuads; x++)
		{
			// Triangle indices
			unsigned short i1 = (z * (numXQuads + 1)) + x;
			unsigned short i2 = ((z + 1) * (numXQuads + 1)) + x;
			unsigned short i3 = (z * (numXQuads + 1)) + x + 1;
			unsigned short i4 = ((z + 1) * (numXQuads + 1)) + x + 1;

			// Getting vertices
			MeshVertex v1 = tempVertices[i1];
			MeshVertex v2 = tempVertices[i2];
			MeshVertex v3 = tempVertices[i3];
			MeshVertex v4 = tempVertices[i4];

			meshIndices.push_back(i1);
			meshIndices.push_back(i2);
			meshIndices.push_back(i3);

			meshIndices.push_back(i2);
			meshIndices.push_back(i4);
			meshIndices.push_back(i3);

			// Add vertices to the vertex buffer
			meshVertices.push_back(v1);
			meshVertices.push_back(v2);
			meshVertices.push_back(v3);

			meshVertices.push_back(v2);
			meshVertices.push_back(v4);
			meshVertices.push_back(v3);
			

		}
	}

	// Adjust the vertex offset by 
	vertexOffset += vertexCount;
}

//------------------------------------------------------------------------------
// Method:    Render
// Returns:   void
// 
// Renders the quad
//------------------------------------------------------------------------------
void BuildableMesh::Render(const glm::mat4& p_mView, const glm::mat4& p_mProj)
{
	m_pVertexDeclaration->Bind();

	// Uniforms
	glm::mat4 worldViewProj = p_mProj * p_mView * m_mWorldTransform;
	m_pMaterial->SetUniform("WorldViewProj", worldViewProj);
	
	m_pMaterial->Apply();

	// Draw!
	//glDrawElements(GL_TRIANGLES, meshIndices.size(), GL_UNSIGNED_SHORT, 0);
	glDrawArrays(GL_TRIANGLES, 0, meshVertices.size());

	// Draw counting
	DrawCalls += 1;

}

void BuildableMesh::AdjustMesh(const std::string& p_pFile)
{
	GLFWimage img;
	glfwReadImage(p_pFile.c_str(), &img, 0);

	for (int i = 0; i < meshVertices.size(); i++)
	{
		MeshVertex v = meshVertices.at(i);

		// Get corresponding pixel in image
		int uPixel = v.heightU * (img.Width - 1);
		int vPixel = v.heightV * (img.Height - 1);

		int position = ((vPixel * img.Width) + (uPixel)) * img.BytesPerPixel;

		unsigned char R = img.Data[position];
		unsigned char G = img.Data[position + 1];
		unsigned char B = img.Data[position + 2];

		float rFloat = (int)R / 255.0f;
		float gFloat = (int)G / 255.0f;
		float bFloat = (int)B / 255.0f;

		// Adjust the Y value of the vertex
		float distortion = 0.30f * rFloat + 0.59f * gFloat + 0.11f * bFloat;
		meshVertices.at(i).y = distortion * 60.0f;

	}


	// Go through them again, this time adjusting the normal
	for (int i = 0; i < meshVertices.size(); i += 3)
	{
		MeshVertex* v1 = &meshVertices[i];
		MeshVertex* v2 = &meshVertices[i+1];
		MeshVertex* v3 = &meshVertices[i+2];

		if (v1->y > 0.0f || v2->y > 0.0f || v3->y > 0.0f)
		{
			int test = 6;
		}

		// Calculate the normal of them?
		glm::vec3 U = glm::vec3(v3->x - v1->x, v3->y - v1->y, v3->z - v1->z);
		glm::vec3 V = glm::vec3(v2->x - v1->x, v2->y - v1->y, v2->z - v1->z);

		glm::vec3 normal;		
		normal.x = (U.y * V.z) - (U.z * V.y);
		normal.y = (U.z * V.x) - (U.x * V.z);
		normal.z = (U.x * V.y) - (U.y * V.x);
		normal = glm::normalize(normal);

		// Set all the vertex normal to match
		v1->nx = normal.x;
		v1->ny = normal.y;
		v1->nz = normal.z;

		v2->nx = normal.x;
		v2->ny = normal.y;
		v2->nz = normal.z;

		v3->nx = normal.x;
		v3->ny = normal.y;
		v3->nz = normal.z;

	}

}